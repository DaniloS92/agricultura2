from django.db import models
from django.contrib.auth.models import User, Group, Permission

class seg_log_acciones(models.Model):
    la_fecha_hora = models.DateTimeField("Fecha y Hora",auto_now=True)
    la_accion = models.CharField("Accion",max_length=100)
    usu_id = models.ForeignKey(User, verbose_name="Usuario")

    class Meta:
        db_table = 'logs'
        verbose_name = 'Logica de accion'
        verbose_name_plural = 'Logica de acciones'
        default_permissions = ()
        #permissions = (
        #    ("list_seg_log_acciones", "Puede listar logs"),
        #    ("add_seg_log_acciones", "Puede agregar logs"),
        #    ("change_seg_log_acciones", "Puede editar logs"),
        #    ("delete_seg_log_acciones", "Puede eliminar logs"),
        #)

    # def __unicode__(self):
    #     return unicode(self.la_accion+" - "+self.usu_id.get_full_name())

class seg_menu(models.Model):
    men_id = models.AutoField(primary_key=True)
    men_nombre = models.CharField(max_length=100)
    men_estado = models.BooleanField(default=True)
    men_icono = models.CharField(max_length=100, blank=True, null=True)
    men_ruta = models.CharField(max_length=100, default='')
    men_tipo = models.CharField(max_length=20)
    men_id_padre = models.ForeignKey('self', blank=True, null=True)
    men_estado_url = models.BooleanField(default=False)
    men_plantilla = models.CharField(max_length=100, blank=True, null=True)
    men_id_active = models.CharField(max_length=100, blank=True, null=True, default=".")
    permisos = models.OneToOneField(Permission, blank=True, null=True)

    class Meta:
        db_table = 'menu'
        verbose_name = 'Menu'
        verbose_name_plural = 'Menus'

        default_permissions = ()
        #permissions = (
        #    ("list_menu", "Puede listar menus"),
        #)

    def __unicode__(self):
        return unicode(self.men_nombre)


class seg_apps(models.Model):
    app_id =            models.AutoField(primary_key=True)
    app_nombre=         models.CharField(max_length=100)
    app_descripcion=    models.CharField(max_length=250)
    app_logo =          models.ImageField(null=True, blank=True, upload_to='aplicaciones')
    app_iotmach =       models.BooleanField(default=False)
    app_enlace =        models.CharField(max_length=100)
    app_color =         models.CharField(max_length=50)
    app_abreviatura =   models.CharField(max_length=75)
    app_encriptar   =   models.BooleanField(default=True)
    app_clave =         models.CharField(max_length=75)
    app_valor_clave =   models.CharField(max_length=75)


    class Meta:
        db_table = 'aplicaciones'
        verbose_name = 'Aplicacion'
        verbose_name_plural = 'Aplicaciones'

        default_permissions = (['add','list'])
        # permissions = (
        #    ("list_menu", "Puede listar menus"),
        # )

    def __unicode__(self):
        return unicode(self.app_nombre)


class usuario_app(models.Model):
    app_id=models.ForeignKey(seg_apps)
    user_id=models.ForeignKey(User)

    class Meta:
        default_permissions = ()
        # permissions = (
        #    ("list_menu", "Puede listar menus"),
        # )