from django.contrib import admin
from app_seguridad.models import *
from app_empresa.models import *
from django.contrib.auth.models import Permission, ContentType

class seg_menuAdmin(admin.ModelAdmin):
    list_display = ('men_id', 'men_nombre', 'men_estado','men_icono','men_ruta', 'men_plantilla')
    list_filter= ['men_nombre']
    search_fields = ['men_nombre']
    list_per_page = 20


class seg_log_accionesAdmin(admin.ModelAdmin):
    list_display = ('la_fecha_hora', 'la_accion', 'usu_id')
    list_filter= ['la_fecha_hora']
    search_fields = ['la_fecha_hora']
    list_per_page = 20

class contenido(admin.ModelAdmin):
    list_display = ('app_label', 'model')

admin.site.register(seg_menu, seg_menuAdmin)
admin.site.register(seg_log_acciones, seg_log_accionesAdmin)
admin.site.register(Permission)
admin.site.register(ContentType, contenido)

class aplicaciones(admin.ModelAdmin):
    list_display = ('app_nombre', 'app_abreviatura', 'app_enlace', 'app_iotmach')
    list_per_page = 25

admin.site.register(seg_apps, aplicaciones)

