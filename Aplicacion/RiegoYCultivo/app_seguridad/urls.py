from django.conf.urls import patterns,url
from .views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

     #Usuarios
    url(r'^usuarios/crear/$', formUsuarios),
    url(r'^usuarios/guardar/$', guardarUsuarios),
    url(r'^usuarios/listar/$', listarUsuarios),
    url(r'^usuarios/inhabilitar/$', inhabUsuario),
    url(r'^usuarios/cambio/$', cambioPass),
    url(r'^usuarios/recuperar/$', recuperar_cuenta),
    url(r'^usuarios/agregar/rol/$', agregar_rol),
    url(r'^usuarios/accesos/(\d+)/$', modAcceso),
    url(r'^usuarios/cambiar/(\d+)/$', cambiarAcceso),

                       # #Roles
    url(r'^roles/crear/$', formRoles),
    url(r'^roles/guardar/$', guardarRoles),
    url(r'^roles/listar/$', listarRoles),
    url(r'^roles/nombres/$', cambiarNombre),
    url(r'^roles/permisos/$', cambiarPermisos),
    # url(r'^roles/usuarios/$', cambiarUsuarios),
    # #  url(r'^roles/inhabilitar/$', inhabRoles),

    ## Aplicaciones
    url(r'^aplicaciones/crear/$', frmAplicaciones),
    url(r'^aplicaciones/guardar/$', guardarApps),
    url(r'^aplicaciones/listar/$', listarApps),
    url(r'^aplicaciones/editar/(\d+)/$', editApps),
    url(r'^aplicaciones/modificar/(\d+)/$', modApps),
    url(r'^aplicaciones/eliminar/$', eliminarApps),

                       # #Logs
    # url(r'^logs/ver/$', formLogs),
)