from __future__ import unicode_literals

from django.apps import AppConfig


class SeguridadConfig(AppConfig):
    name = 'app_seguridad'
    verbose_name = 'Seguridad'