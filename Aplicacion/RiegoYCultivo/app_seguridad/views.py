# -*- coding: utf-8 -*-
import json

from django.http import JsonResponse
from socketIO_client import SocketIO,BaseNamespace
from django.shortcuts import redirect, HttpResponse, HttpResponseRedirect, render_to_response, RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import *
from django.contrib import messages
from django.db.models import Q #para hacer filtros or en un queryset
from django.core.mail import EmailMultiAlternatives

from .models import *
from agricultura.metodos import *
from app_empresa.models import *

class autenticacion(BaseNamespace):
    def on_aaa_response(self, *args):
        print 'on_aaa_response', args

#------------------------------------------- Usuarios

@login_required(login_url='/')
def formUsuarios(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('auth.add_user'):
            dict['lstRoles'] = Group.objects.all()
            if getEmpresa(request).emp_nombre_comercial == 'IOTMACH':
                dict['apps'] = seg_apps.objects.all()
            else:
                dict['apps'] = seg_apps.objects.exclude(app_iotmach=True)

            return render_to_response("seguridad/usuarios/frmUsuarios.html", dict, context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,context_instance = RequestContext(request))
    else:
        return redirect('/')

def socketLDAP(socket, datos):
    socketIO = SocketIO('127.0.0.1', 6001)
    vAutenticacion = socketIO.define(autenticacion, '/autenticacion')
    vAutenticacion.emit(socket, datos)
    socketIO.wait(seconds=2)

@login_required(login_url='/')
@csrf_exempt
def guardarUsuarios(request):
    if request.user.has_perm('auth.add_user'):
        if not User.objects.filter(username=request.POST['txtUsuario']).exists():

            persona=proy_persona.objects.create(per_nombres = request.POST['txtNombre'],
                                    per_apellidos = request.POST['txtApellido'],
                                    per_direccion = request.POST['txtDireccion'],
                                    per_telef_movil = request.POST['txtTlfono'],
                                    per_email = request.POST['txtCorreo'],
                                    per_telef_fijo = "0",
                                    emp_id = getEmpresa(request),
                                    )

            lst_app = 'server'
            persona.apps.add(seg_apps.objects.get(app_abreviatura='server'))
            aplicaciones = seg_apps.objects.all()
            for i in request.POST:
                for j in aplicaciones:
                    if i == j.app_abreviatura:
                        lst_app += ',' + j.app_abreviatura
                        persona.apps.add(j)

            vUsuario = User(username=request.POST['txtUsuario'],
                            first_name=request.POST['txtNombre'],
                            last_name=request.POST['txtApellido'],
                            email=request.POST['txtCorreo'],
                            id_persona=persona)
            vUsuario.set_password(request.POST['txtUsuario']+'_'+request.POST['txtTlfono'])
            vUsuario.save()
            vUsuario.groups.add(Group.objects.get(id=request.POST['listRoles']))

            datos = {'cn': vUsuario.first_name,
                     'sn':vUsuario.last_name,
                     'direccion': persona.per_direccion,
                     'telefono':persona.per_telef_movil,
                     'mail':vUsuario.email,
                     'uuid':vUsuario.username,
                     'lst_app':lst_app,
                     'ou': getEmpresa(request).emp_ruc,
                     'uidNumber': vUsuario.pk
                     }

            socketLDAP('guardar_usuario', datos)

            acciones("Guarda Usuario "+vUsuario.get_full_name(), request.user)
            messages.add_message(request, messages.SUCCESS, 'Registro de Usuario Guardado Correctamente')
        else:
            messages.error(request,"Ya esta registrado ese nombre de Usuario")
        return redirect('/seguridad/usuarios/crear')
    else:
        return render_to_response("seguridad/errores/error_403.html",context_instance = RequestContext(request))

@login_required(login_url="/")
@csrf_exempt
def agregar_rol(request):
    dict={}
    if request.method == "POST":
        _rol_nombre = request.POST["txtRol"]
        _rol_permisos = request.POST["listPermisos"]
        permisos = json.loads(_rol_permisos)
        # print permisos
        if ((len(_rol_nombre)<=200 and len(_rol_nombre)!=0) and len(_rol_permisos)!=0):
            try:
                repetido = Group.objects.all()
                for i in repetido:
                    if _rol_nombre.strip() == i.name.strip(): #eliminar_tildes(_rol_nombre).upper() == eliminar_tildes(i.rol_nombre).upper():
                        dict = {
                            'mensaje': "Ya existe un rol con el mismo nombre",
                            'result': 'X'
                        }
                        return HttpResponse(json.dumps(dict), content_type="application/json")

                vRol=Group.objects.create(name=request.POST['txtRol'], id_empresa=getEmpresa(request))

                for i in permisos:
                    vRol.permissions.add(Permission.objects.get(id=i))
                dict = {
                    'mensaje': "Registro de Rol Guardado Correctamente.",
                    'result': 'OK',
                    'vRol_id': vRol.id,
                    'vRol_nombre' : vRol.name,
                }
            except Exception as e:
                dict = {
                    'mensaje': "Dyuyay sufrio un error inesperado, vuelva a intentar.",
                    'result': 'X'
                }
        else:
            dict = {
                'mensaje': "Algunos de los campos han superado el limite de caracteres permitido.",
                'result': 'X'
            }
    data=json.dumps(dict)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def listarUsuarios(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa']=getEmpresa(request)
        if request.user.has_perm('auth.list_user'):
            dict['listUsuarios'] = User.objects.filter(id_persona__emp_id=getEmpresa(request)).exclude(is_superuser=True, is_staff=True)
            return render_to_response("seguridad/usuarios/lstUsuarios.html", dict,context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def inhabUsuario(request):
    if request.user.is_authenticated():
        if request.user.has_perm('auth.change_user'):
            vUsuario=User.objects.get(id=request.GET['idUser'])
            if request.GET['activo'] == 'true':
                vUsuario.is_active=True
            else:
                vUsuario.is_active=False
            vUsuario.save()
            acciones("CAMBIO ESTADO DE "+vUsuario.get_full_name(), request.user)
            return JsonResponse({'mensaje': 'Actualizacion de estado de '+ vUsuario.get_full_name() + ' correctamente','tipo':'success'})
        else:
            return render_to_response("seguridad/errores/error_403.html", context_instance = RequestContext(request))
    else:
        return redirect('/')



@login_required(login_url='/')
def cambioPass(request):
    if request.user.is_authenticated():
        vUser=User.objects.get(id=request.user.id)
        if vUser.check_password(request.POST['txtPasswordActual']):
            vUser.set_password(request.POST['txtPasswordN'])

            datos={'usuario': request.user.username,
                   'pass': request.POST['txtPasswordN'],
                   'empresa': getEmpresa(request).emp_ruc,
                   }

            socketLDAP('actualizar_pass', datos)

            vUser.save()
            acciones('Cambio de clave', request.user)
            messages.add_message(request, messages.SUCCESS, 'Su contraseña ha sido actualizada correctamente')
        else:
            messages.add_message(request, messages.ERROR, 'La contraseña actual ingresada no es la correcta')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('/')

@login_required(login_url='/')
@csrf_exempt
def modAcceso(request, codigo):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('auth.list_user'):
            vUser=User.objects.get(id=codigo)
            dict['rol'] = vUser.groups.all()
            dict['aplicaciones'] = vUser.id_persona.apps.all()
            dict['datos']= vUser
            dict['lstRoles'] = Group.objects.all()
            if getEmpresa(request).emp_nombre_comercial == 'IOTMACH':
                dict['apps'] = seg_apps.objects.all()
            else:
                dict['apps'] = seg_apps.objects.exclude(app_iotmach=True)
            return render_to_response("seguridad/usuarios/accesos.html", dict,
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,
                                      context_instance=RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def cambiarAcceso(request, codigo):
    if request.user.is_authenticated():
        vUser = User.objects.get(id=codigo)
        vUser.groups.clear()
        vUser.id_persona.apps.clear()
        vUser.groups.add(Group.objects.get(id=request.POST['listRoles']))

        lst_app = 'server'
        vUser.id_persona.apps.add(seg_apps.objects.get(app_abreviatura='server'))
        aplicaciones = seg_apps.objects.all()
        for i in request.POST:
            for j in aplicaciones:
                if i == j.app_abreviatura:
                    lst_app += ',' + j.app_abreviatura
                    vUser.id_persona.apps.add(j)
        datos={'usuario': vUser.username,
               'empresa': getEmpresa(request).emp_ruc,
               'apps': lst_app
               }
        socketIO = SocketIO('127.0.0.1', 6001)
        vAutenticacion = socketIO.define(autenticacion, '/autenticacion')
        vAutenticacion.emit('actualizar_acceso', datos)
        socketIO.wait(seconds=2)

        messages.add_message(request, messages.SUCCESS, 'Los accesos se actualizaron correctamente')

        return HttpResponseRedirect('/seguridad/usuarios/listar/')
    else:
        return redirect('/')

def recuperar_cuenta(request):
    txt_username=request.GET['txt_username']
    txt_email=request.GET['txt_email']
    response = redirect('http://127.0.0.1:6001/')
    if User.objects.filter(email=txt_email, username=txt_username, is_active=True).exists():
        asunto="Recuperar Cuenta en IOTMACH"

        text_content= ""

        html_content = "<div style='color:#ffffff;background-color: #253561;background-image: url(http://drive.google.com/uc?export=view&id=0B1kIyBXgMcA2RzNDQ2ZiTjlYYWc);width: 100%'>" \
                        "<h3 style='padding-top: 1%;padding-bottom: 1%' align='center'>Recuperar Cuenta de IOTMACH" \
                        "</h3><p style='padding: 2%'>Nuestros servidores han recibido la solicitud para recuperar su cuenta, si ud ha realizado esta solicitud de clic " \
                        "<a href='http://"+request.get_host()+"/recuperar_cuenta/"+str(txt_username)+"/"+str(txt_email)+"/' style='color: red' > en este enlace </a>" \
                        "caso contrario ignore el mismo.</p><p> Estamos orgullos de servirte, no olvides visitar nuestro sitio WEB en " \
                        "<a href='http://172.30.193.36/' style='color: #ffffff'>IOTMACH</a>" \
                        "</p></div>"
        mail=EmailMultiAlternatives(asunto, text_content, to=[txt_email])
        mail.attach_alternative(html_content, "text/html")

        try:
            mail.send()
            # messages.add_message(request, messages.SUCCESS, "Verifique su correo electrónico")
            response.set_cookie('messages','Verifique su correo electronico-success')
        except Exception as e:
            # messages.add_message(request, messages.ERROR, "Verifique su conexión a internet")
            response.set_cookie('messages', "Verifique su conexion a internet-error")
    else:
        # messages.add_message(request, messages.ERROR, "El nombre de usuario o correo electrónico son incorrectos")
        response.set_cookie('messages', 'El nombre de usuario o correo electronico son incorrectos-error')

    # return redirect('/')
    # return redirect('http://127.0.0.1:6001/')
    return response
#------------------------------------------- Roles
@login_required(login_url='/')
def formRoles(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('auth.add_group'):
            dict['item']=Permission.objects.all().exclude(Q(content_type__model='permission')|
                                                          Q(content_type__model='contenttype')|
                                                          Q(content_type__model='logentry')|
                                                          Q(content_type__model='session')
                                                          )
            return render_to_response("seguridad/roles/frmRoles.html", dict, context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
@csrf_exempt
def guardarRoles(request):
    if request.user.is_authenticated():
        if request.user.has_perm('auth.add_group'):
            vRol=Group.objects.create(name=request.POST['txtRol'])
            permisos=request.POST.getlist('listPermisos')
            # print(permisos)
            for i in permisos:
                # print(i)
                vRol.permissions.add(Permission.objects.get(id=i))

            acciones("Guardo Rol "+vRol.name, request.user)
            messages.add_message(request, messages.SUCCESS, 'Registro de Rol guardado correctamente')
            return redirect('/seguridad/roles/crear/')
        return render_to_response("seguridad/errores/error_403.html", context_instance = RequestContext(request))
    return redirect('/')

@login_required(login_url='/')
def listarRoles(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa']=getEmpresa(request)
        if request.user.has_perm('auth.list_group'):
            dict['lstRoles'] = Group.objects.all()
            dict['lstMenu'] = Permission.objects.all().exclude(Q(content_type__model='permission')|
                                                          Q(content_type__model='contenttype')|
                                                          Q(content_type__model='logentry')|
                                                          Q(content_type__model='session')
                                                          )

            dict['lstUsuarios'] = User.objects.all().exclude(is_staff=True, is_superuser=True)
            for i in User.objects.all():
                for j in i.groups.all():
                    print(j.name)
            # dict['lstPermisos'] = seg_permiso.objects.all().order_by('id')
            # dict['lstUsuRol'] = seg_usuario_rol.objects.all().order_by('id')
            return render_to_response("seguridad/roles/lstRoles.html", dict,context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
@csrf_exempt
def cambiarNombre(request):
    if request.user.is_authenticated():
        if request.user.has_perm('auth.change_user'):
            vRol=Group.objects.get(id=request.GET['id'])
            vRol.name=request.GET['nombre']
            vRol.save()
            acciones("CAMBIO NOMBRE DE "+vRol.name, request.user)
            return JsonResponse({'mensaje':'Nombre de '+ vRol.name+' actualizado correctamente','tipo':'success'})
        else:
            return JsonResponse({'mensaje':'ocurrio un problema','tipo':'error'})
    else:
        return JsonResponse({'mensaje':'no has iniciado sesion','tipo':'error'})

@login_required(login_url='/')
@csrf_exempt
def cambiarPermisos(request):
    if request.user.is_authenticated():
        if request.user.has_perm('auth.change_group'):
            vRol=Group.objects.get(id=request.GET['id'])
            vRol.permissions.clear()

            lstPermisos=request.GET.getlist('permisos[]')
            for j in range(len(lstPermisos)):
                vRol.permissions.add(Permission.objects.get(id=lstPermisos[j]))

            acciones("ACTUALIZO LOS PERMISOS DE "+vRol.name, request.user)
            return JsonResponse( {'mensaje':'Permisos de '+ vRol.name+' actualizados correctamente','tipo':'success'} )
        else:
            return JsonResponse({'mensaje':'ocurrio un problema','tipo':'error'})
    else:
        return JsonResponse({'mensaje':'no has iniciado sesion', 'tipo':'error'})

@login_required(login_url='/')
@csrf_exempt
def cambiarUsuarios(request):
    if request.user.is_authenticated():
        if request.user.has_perm('auth.change_group'):
            vRol=Group.objects.get(id=request.GET['id'])
            for j in User.objects.filter(id_empresa=getEmpresa(request)):
                for i in j.groups.all():
                    if i.id == vRol.id:
                        j.groups.remove(vRol)
            lstUsuarios=request.GET.getlist('usuarios[]')
            for j in range(len(lstUsuarios)):
                vUsuario=User.objects.get(id=lstUsuarios[j])
                vUsuario.groups.add(vRol)
            acciones("ACTUALIZO LOS USUARIOS DE "+vRol.name, request.user)
            return JsonResponse({'mensaje':'Usuarios de '+ vRol.name +' actualizados correctamente', 'tipo':'success'})
        else:
            return JsonResponse({'mensaje':'ocurrio un problema','tipo':'error'})
    else:
        return JsonResponse({'mensaje':'no has iniciado sesion', 'tipo':'error'})

# def inhabRoles(request):
#     vRol=seg_rol.objects.get(id=request.GET['id'])
#     if request.GET['activo'] == 'true':
#         vRol.rol_estado=True
#     else:
#         vRol.rol_estado=False
#     vRol.save()
#     acciones("CAMBIO ESTADO DE "+vRol.rol_nombre, request.user)
#     serializado = json.dumps(['Estado de '+ vRol.rol_nombre+' actualizado correctamente','success'])
#     return HttpResponse( serializado, mimetype='application/json' )

# @login_required(login_url='/')
# def formLogs(request):
#     vUsuRol=seg_usuario_rol.objects.filter(usu_id=request.user.id)
#     dict = armaMenu(vUsuRol, request.user)
#     dict['usuario'] = request.user
#     dict['persona'] = obtenerPersona(request)
#     dict['roles'] = obtenerRoles(request)
#     dict['empresa']=getEmpresa(request)
#     if permisosVista(vUsuRol,"Ver Logs") or (request.user.is_superuser and request.user.is_staff):
#         if request.user.is_superuser == False and request.user.is_staff == False :
#             vUsuarios=[]
#             for i in seg_usuario_rol.objects.filter(rol_id=seg_rol.objects.filter(idEmpresa=getEmpresa(request).id)):
#                 vUsuarios.append(i.usu_id)
#             vUsuarios=list(set(vUsuarios))
#             lstLogs=[]
#             for i in vUsuarios:
#                 for j in seg_log_acciones.objects.filter(usu_id=i.id):
#                     lstLogs.append(j)
#             dict['lstLogs']=lstLogs
#         else:
#             dict['lstLogs']=seg_log_acciones.objects.all()
#         return render_to_response("seguridad/logs/lstLogs.html", dict, context_instance = RequestContext(request))
#     else:
#         return render_to_response("seguridad/errores/error_403.html", dict,context_instance = RequestContext(request))

@login_required(login_url='/')
def frmAplicaciones(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('app_seguridad.add_seg_apps'):
            dict['url'] = '/seguridad/aplicaciones/guardar/'
            return render_to_response("seguridad/aplicaciones/frm_apps.html", dict,
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,
                                      context_instance=RequestContext(request))
    else:
        return redirect('/')

def guardarApps(request):
    encrip = False
    if 'chk_encrip' in request.POST:
        encrip= True

    iotmach=False
    if 'chk_iotmach' in request.POST:
        iotmach=True

    logo=''
    if 'img_icono' in request.FILES:
        logo=request.FILES['img_icono']

    seg_apps.objects.create(
        app_nombre=request.POST['txt_nombre'],
        app_descripcion = request.POST['txt_descripcion'],
        app_logo = logo,
        app_iotmach = iotmach,
        app_enlace = request.POST['txt_enlace'],
        app_color = request.POST['txt_color'],
        app_abreviatura = request.POST['txt_abrev'],
        app_encriptar = encrip,
        app_clave = request.POST['txt_clave'],
        app_valor_clave = request.POST['txt_valor']
    )

    messages.add_message(request, messages.SUCCESS, 'Registro de Aplicación Guardado Correctamente')

    return redirect('/seguridad/aplicaciones/crear/')

@login_required(login_url='/')
def listarApps(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('app_seguridad.list_seg_apps'):
            dict['lstApps'] = seg_apps.objects.all().order_by('-app_id')
            return render_to_response("seguridad/aplicaciones/lst_apps.html", dict,
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,
                                      context_instance=RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def editApps(request, codigo):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('app_seguridad.add_seg_apps'):
            dict['datos_app'] = seg_apps.objects.get(app_id=codigo)
            dict['url'] = '/seguridad/aplicaciones/modificar/'+str(codigo)+'/'
            return render_to_response("seguridad/aplicaciones/frm_apps.html", dict,
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,
                                      context_instance=RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def modApps(request, codigo):
    encrip = False
    if 'chk_encrip' in request.POST:
        encrip = True

    iotmach = False
    if 'chk_iotmach' in request.POST:
        iotmach = True

    vApp = seg_apps.objects.get(app_id=codigo)
    vApp.app_nombre=request.POST['txt_nombre']
    vApp.app_descripcion=request.POST['txt_descripcion']

    if 'img_icono' in request.FILES:
        logo = request.FILES['img_icono']
        vApp.app_logo=logo

    vApp.app_iotmach=iotmach
    vApp.app_enlace=request.POST['txt_enlace']
    vApp.app_color=request.POST['txt_color']
    vApp.app_abreviatura=request.POST['txt_abrev']
    vApp.app_encriptar=encrip
    vApp.app_clave=request.POST['txt_clave']
    vApp.app_valor_clave=request.POST['txt_valor']
    vApp.save()

    messages.add_message(request, messages.SUCCESS, 'Registro de Aplicación Actualizado Correctamente')

    return redirect('/seguridad/aplicaciones/listar/')

@login_required(login_url='/')
def eliminarApps(request):
    vApp=seg_apps.objects.get(app_id=request.POST['txt_codigo'])
    vApp.delete()

    messages.add_message(request, messages.SUCCESS, 'Registro de Aplicación Eliminado Correctamente')

    return redirect('/seguridad/aplicaciones/listar/')