var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// var logs = require('./logs.js');
// var console = logs.logNode;

conexionMongo();
function conexionMongo(){
	mongoose.connect("mongodb://localhost/iot", function(err){
		if(err){
			console.log('Error de conexion: no esta inicializado el servidor de mongoDB');
			setTimeout(function(){ 
				conexionMongo();
			}, 2000);
		}else{
			console.log('Se establecio conexion con la bd de mongodb')
		}
	});
}
// mongoose.connect("mongodb://192.168.1.10/db_iotmach");

var lectura = require('./models/iot_lectura');
var modelos = require('./models/db_Iotmach');


module.exports.lectura = lectura;
module.exports.modelos = modelos;