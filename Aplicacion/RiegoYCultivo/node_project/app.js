/**
 * Created by Danilo on 22/05/2016.
 * http://stackoverflow.com/questions/6563885/socket-io-how-do-i-get-a-list-of-connected-sockets-clients
 */

var puerto = 8686;
var namespace = ['/iotmach_node', '/iotmach_django', '/iotmach_actuadores'];
var contadorConexiones = 0;
var evento = 'mensajes';
var rooms = []; // id de los paneles de monitoreo
var con = 0;

// importaciones propias locales
var com_django = require('./comunicacion_django.js');
var metodos = require('./metodos.js');
var db = require('./busquedas.js');

//logs

var logs = require('./logs.js');
var console = logs.logNode;

var envio_datos_django = com_django.envio_datos,
    obtenerMedidas = db.obtenerMedidas,
    conversionArray_Json = metodos.conversionArray_Json,
    recategorizarInformacion = metodos.recategorizarInformacion,
    receteo_dispositivos = metodos.receteo_dispositivos,
    borrado_dispositivos = metodos.borrado_dispositivos;
    ultimasLecturas = metodos.ultimasLecturas;

var io = require('socket.io')(puerto);

function InicializarSistema() {

    receteo_dispositivos(function(data) {
        console.debug(data);
    });

    borrado_dispositivos(function(data){
        console.log(data);
    });

    conexionesExternas();
}

function newEventCallback(eventName, sen_ac_data, dis_mac, id_rooms) {

    //Construccion del json al formato {value,data} a ser enviado

    //le concatenamos la mac como clave para los sensores para evitar mescla de datos entre los dispositivos con el mismo nombre de interfaz (in0)
    var json_lecturas = recategorizarInformacion(sen_ac_data);
    var temp = JSON.stringify(json_lecturas);
    var mac = dis_mac.replace(/:/gi, ""); //expresiones regulares dentro de replace mas info aqui: http://www.w3schools.com/jsref/jsref_replace.asp

    json_lecturas = '{"MAC' + mac + '":' + temp + '}';

    var json = {
        value: dis_mac,
        data: JSON.parse(json_lecturas),
    };

    enviandoDatos(eventName, JSON.stringify(json), id_rooms);
}

function conexionesExternas() {

    //aqui hacer las consultas y contruir el json.
    db.obtenerMedidas(function(medida) {

        // Se envia a consultar en que zona de riego pertenece el sensor enviado la mac del dispositivo y la interfaz del mismo
        if(medida!=null){

            delayLoop(medida,function(mensaje){
                console.debug(mensaje);//termina el for de las interfaces
            });

            db.editarMedida(medida,function(data){});
        }else{
            // Configuracion de seguridad para q no este preguntando demasiado rapido a mongo cuando no hay lecturas nuevas
            setTimeout(function(){
                console.debug('No existe ninguna lectura nueva, esperando nuevos datos......');
                conexionesExternas();
            },1000);
        }
    });
}

function delayLoop(medida,callback) {

    setTimeout(function() { //  call a 0.5s setTimeout when the loop is called

        if (con < medida.datos.length) {
            db.consultarDispositivo(medida.mac)
                .then(id_disp => db.consultarZona(medida.datos[con].interfaz,id_disp,medida.datos[con].valor,medida.mac))
                .then(zona_room => {
                    newEventCallback(evento, medida.datos[con], medida.mac, zona_room);
                    delayLoop(medida, function(mensaje){
                        conexionesExternas();//llamar a una nueva medida guardada
                    });
                    con++;
                })
                .catch(err => {
                    console.debug('HUBO UN ERROR: '+ err);
                    conexionesExternas();
                });
        } else {
            con = 0;
            callback('Se termino la ejecucion:');
        }
    },500);
}

/*
 * Propagacion de los datos
 */

//Propaga los datos a todos los cientes(dashboard) conectados
function enviandoDatos(event, datos, room) { //se le enviaria la room directamete para evitar el for de las rooms

    console.debug('namespace: ' + namespace[0]);
    console.debug('room: ' + room);
    console.debug('evento: ' + event);
    console.debug('json: ' + datos);
    console.debug('__________________________________________');
    io.of(namespace[0]).to(room).emit(event, datos);
}

/*
 * Encargarse de las conexiones al servidor socket.io
 */
//namespace = /iotmach_node
io.of(namespace[0]).on('connection', function(socket) {

    console.debug("Nuevo cliente conectado. ");

    //Cuando se conecta el dashboard
    socket.on('subscribe', function(data) {

        var datos = data.split("-");
        socket.join(datos[0]);
        console.debug('Client join to room: ' + datos[0]);

        ultimasLecturas(datos[0], function(datos_sen_act) {

            for (var i = 0; i < datos_sen_act.length; i++) {

                data = {
                    'interfaz': datos_sen_act[i].interfaz,
                    'valor': datos_sen_act[i].valor,
                };

                newEventCallback(evento, data, datos_sen_act[i].mac, datos[0]);
            }
            console.debug('****** Datos precargados correctamente de la zona: ' + datos[0] + '******');
        });
    });

    //cuando se desconecta el dashboard
    socket.on('disconnect', function(socket) {

        console.debug("Client dìsconect from rooms.");
    });

});


// namespace = /iotmach_actuadores
io.of(namespace[2]).on('connection', function(socket) {

    //Esperando eventos de los actuadores
    socket.on('evento', function(data) {
        var link = '/agricultura/envio_accion_actuador';
        envio_datos_django(data,link,function(datosRespuesta){
            console.debug(datosRespuesta);
        });
    });

});

//Ejecutando el servidor
console.debug("*************************************************************");
console.debug("Servidor node con socket.io iniciado en el puerto=%s", puerto);
InicializarSistema();