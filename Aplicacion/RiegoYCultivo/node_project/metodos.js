
var com_django = require('./comunicacion_django.js');
var CronJob = require('cron').CronJob;
var envio_datos_django = com_django.envio_datos;

function conversionArray_Json(array) {
    //recibe por decir
    //[{"senial":"a","categoria":"s","valor":43,"interfaz":"in0"},{"senial":"d","categoria":"s","valor":"on","interfaz":"out0"},{"senial":"d","categoria":"s","valor":"off","interfaz":"out1"}]
    //{in0:{"senial":"a","categoria":"s","valor":43,"interfaz":"in0"},in1...}
    var acu = '';
    for (var j = 0; j < array.length; j++) {

        var par = JSON.stringify(array[j]);
        acu += '"' + array[j].interfaz + '":' + par + ',';
    }

    acu = acu.substring(0, acu.length - 1);
    acu = JSON.parse('{' + acu + '}');
    return acu;
}

function recategorizarInformacion(data) {
    //recibe: {"senial":"a","categoria":"s","valor":43,"interfaz":"in0"}
    //retorna: {in0: {"senial":"a","categoria":"s","valor":43,"interfaz":"in0"}}
    var temp = JSON.stringify(data);
    var json = '"' + data.interfaz + '":' + temp;
    return JSON.parse('{' + json + '}');
}

function receteo_dispositivos(callback) {

    setInterval(function() {
        var link = '/agricultura/recetearDispositivos';
        envio_datos_django('', link, function(data) {

            callback('*******Tiempo programado de recetar dispositivos activado*******');
        });
    }, 900000); //cada 15 min se recetean los dispositivos
}

function borrado_dispositivos(callback) {

    // Seconds: 0-59
    // Minutes: 0-59
    // Hours: 0-23
    // Day of Month: 1-31
    // Months: 0-11
    // Day of Week: 0-6

    var job = new CronJob({
        cronTime: '00 30 23 27 * *', //se iniciara el 27 de cada mes a las 23:30
        onTick: function() {
            /*
             * Runs every weekday (Monday through Friday)
             * at 11:30:00 AM. It does not run on Saturday
             * or Sunday.
             */
            var link = '/agricultura/borrado_dispositivos_programado';
            envio_datos_django('', link, function(data) {

                callback('*******Tiempo programado de borrar dispositivos no activos*******');
            });
        },
        start: false,
        timeZone: 'America/Los_Angeles'
    });
    job.start();
}

function ultimasLecturas(id_zona, callback) {
    // consultar los sensores_actuadores que tiene la zona
    var link = '/agricultura/ultimasLecturas';
    var datos = {
        'id_zona': id_zona
    };

    envio_datos_django(datos, link, function(datos_sen_act) {
        callback(datos_sen_act);
    });
}

module.exports.conversionArray_Json = conversionArray_Json;
module.exports.recategorizarInformacion = recategorizarInformacion;
module.exports.receteo_dispositivos = receteo_dispositivos;
module.exports.borrado_dispositivos = borrado_dispositivos;
module.exports.ultimasLecturas = ultimasLecturas;