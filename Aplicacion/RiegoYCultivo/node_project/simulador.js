
/**
 * Created by Danilo on 31/05/2016.
 */

var db = require('./controller.js');
var tiempo = 5000;
var mac = ['F0:E1:D2:C3:B4:A5','F1:E2:D3:C4:B5:A6'];

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}

function simulador_lecturas(){

	valor = num_al(0, 1);
	var fecha = new Date();

	fecha = convertDate(fecha);

	estado = false;

	var acumuladorSen = '';
	var acumuladorAct = '';

	// disp 1
	var datosa = [
					{
						'interfaz':'ina0',
						'valor': num_al(0,45)
					},
					{
						'interfaz':'ina1',
						'valor': num_al(0,45)
					},
					{
						'interfaz':'ind0',
						'valor': num_al(0,1)
					}
				];

	var datosb = [
					{
						'interfaz':'ind0',
						'valor': num_al(0,1)
					},
					{
						'interfaz':'outd0',
						'valor': num_al(0,1)
					},
					{
						'interfaz':'outa0',
						'valor': num_al(0,100)
					}
				];

	if(valor==0){
		var newLectura = db.lectura({
			mac : mac[0],
			datos : datosa,
			fecha : fecha,
			estado : estado,
			paquete_id: '001'
		});

		newLectura.save(function(err) {

	        if (err) throw err;
	        for(var i = 0; i < datosa.length; i++){
	        	acumuladorSen = acumuladorSen+''+JSON.stringify(datosa[i]);
	        }
	        console.log("Lectura guardada mac: "+mac[0]+" datos: "+acumuladorSen);
	        acumuladorSen = '';
    	});	
	}else{
		var newLectura = db.lectura({
			mac : mac[1],
			datos : datosb,
			fecha : fecha,
			estado : estado,
			paquete_id: '001'
		});

		newLectura.save(function(err) {
			
	        if (err) throw err;
	        for(var i = 0; i < datosb.length; i++){
	        	acumuladorSen = acumuladorSen+''+JSON.stringify(datosb[i]);
	        }

	        console.log("Lectura guardada mac: "+mac[1]+" datos: "+acumuladorSen);
    		acumuladorSen = '';

    	});	
	}
}

function num_al(desde, hasta){
	var medida = Math.floor((Math.random() * ((hasta+1) - desde))) + desde;
	return medida;
}


//Inicio del simulador
setInterval(function() {
    simulador_lecturas();
}, tiempo);