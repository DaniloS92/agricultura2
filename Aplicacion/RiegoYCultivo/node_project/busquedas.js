/**
 * Created by Danilo on 22/05/2016.
 */
var db = require('./controller.js');
var Promise = require("bluebird");

//logs

var logs = require('./logs.js');
var console = logs.logNode;

function obtenerMedidas(callback) {

    db.lectura.findOne({ estado: false }, {}, { sort: { '_id': 1 } }, function(err, lectura) {

        if (err) throw err;
        callback(lectura);
    });

}

function editarMedida(medida, callback) {

    db.lectura.findById(medida._id, function(err, medida) {

        if (err) throw err;

        var editMedida = medida;
        editMedida.estado = true;

        editMedida.save(function(error, respuesta) {
            if (err) throw err;
            callback('ok');
        });
    });
}

//Consultas a postgre con pool de conexiones
db.modelos.sequelize //pasarlo mejor a controller.js
    .authenticate()
    .then(function(err) {
        console.debug('Connection has been established successfully.');
    })
    .catch(function(err) {
        console.debug('Unable to connect to the database:', err);
    });

function consultarDispositivo(mac) {
    return new Promise(function (resolve,reject){

        db.modelos.dispositivo
            .findOne({
                where: {
                    dis_mac: mac
                }
            }).then(function(dispositivo) {
                if (dispositivo) {
                    dispositivo.updateAttributes({
                        dis_activo: true,
                        dis_fecha_ultima_lectura: new Date()
                    });
                    resolve(dispositivo.dis_id);
                } else {
                    reject('No existe dispositivo '+mac);
                }
            });
    });
}

function consultarZona(interfaz, id_disp, valor, mac) {
    return new Promise(function (resolve,reject){

        db.modelos.sensor_actuador
            .findOne({
                where: {
                    dis_id_id: id_disp,
                    sa_interfaz: interfaz
                }
            }).then(function(sen_act) {
                if (sen_act) {
                    sen_act.updateAttributes({
                        sa_ult_medida: valor
                    }).then(function() {
                        resolve(sen_act.zsa_id_id);
                    });
                } else {
                    resolve(-100);//se enviara una trama dañada a un room q no existe para evitar el catch del promise
                    console.log('No existe sensor_actuador '+interfaz+' vinculado al disp '+mac);
                    // reject('No existe sensor_actuador '+interfaz+' vinculado al disp '+mac);
                }
            });
    });
}


module.exports.obtenerMedidas = obtenerMedidas;
module.exports.editarMedida = editarMedida;
module.exports.consultarZona = consultarZona;
module.exports.consultarDispositivo = consultarDispositivo;
