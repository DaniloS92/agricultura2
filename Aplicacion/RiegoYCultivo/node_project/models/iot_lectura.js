//http://stackoverflow.com/questions/19695058/how-to-define-object-in-array-in-mongoose-schema-correctly-with-2d-geo-index
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var lecturas = new Schema ({
	mac: String,
	datos: { type: Array, "default" : []},
	fecha: String,
	estado : Boolean,
	paquete_id: String,
});


module.exports = mongoose.model('lecturas', lecturas);