var Sequelize = require('Sequelize');
var sequelize = new Sequelize('db_Iotmach', 'iotmachuser', 'iotmach_2016', {
    host: 'localhost',
    dialect: 'postgres',
      // disable logging; default: console.log
  	logging: false,

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },

});

var Dispositivo = sequelize.define('iot_dispositivo', {
    dis_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    dis_nombre: Sequelize.STRING,
    dis_descripcion: Sequelize.STRING,
    dis_mac: Sequelize.STRING,
    dis_fecha_registro: Sequelize.DATE,
    dis_activo: Sequelize.BOOLEAN,
    dis_fecha_ultima_lectura: Sequelize.DATE,
    dis_localizacion: Sequelize.STRING,
    emp_id_id: Sequelize.INTEGER,
    sar_id_id: Sequelize.INTEGER,
}, {
    freezeTableName: true,
	timestamps: false,
});

var Sensor_Actuador = sequelize.define('iot_sensor_actuador', {
    iot_id: {
    	type: Sequelize.INTEGER,
    	primaryKey: true	
    },
    sa_nombre: Sequelize.STRING,
    sa_interfaz: Sequelize.STRING,
    sa_categoria: Sequelize.STRING,
    sa_senial: Sequelize.STRING,
    sa_estado: Sequelize.BOOLEAN,
    sa_ult_medida: Sequelize.FLOAT,
    dis_id_id: {
        type: Sequelize.INTEGER,
        referencies: {
            model: Dispositivo,
            key: 'dis_id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    ted_id_id: Sequelize.INTEGER,
    zsa_id_id: Sequelize.INTEGER,
},{
    freezeTableName: true,
	timestamps: false,
});

module.exports.sequelize = sequelize;
module.exports.dispositivo = Dispositivo;
module.exports.sensor_actuador = Sensor_Actuador;


