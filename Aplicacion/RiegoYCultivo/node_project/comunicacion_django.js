//https://fernetjs.com/2011/12/creando-y-utilizando-callbacks/
//http://stackoverflow.com/questions/19322248/node-js-distinguishing-errors-when-making-http-request
var http = require('http');
var querystring = require('querystring');

//logs

var logs = require('./logs.js');
var console = logs.logNode;

function envio_datos(data, link, callback) {
    var values = querystring.stringify(data);
    var options = {
        hostname: 'localhost',
        port: '8000',
        path: link,
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': values.length,
        }
    };

    var req = http.request(options, function(res) {
        
        res.setEncoding('utf8');
        res.on('data', function(data) {
            try {
                data = JSON.parse(data);
                //regresamos los datos
                callback(data);
            } catch (e) {
                console.debug('Error, no existe o no esta config el sensor en la interfaz de django');
            }
        });
    });

    req.on('error', function(e) {

        console.debug('No se esta conectado el servidor de node a Django:');
        // console.debug(e);
    });

    req.on('timeout', function() {

        console.debug('Se demoro el servidor en responder.');
        req.abort();
    });

    req.setTimeout(10000);
    req.write(values);
    req.end();
}

function envio_datos_json(data, link, callback) {
    var post_req = null,
        post_data = '{"login":"toto","password":"okay","duration":"9999"}';

    var post_options = {
        hostname: 'localhost',
        port: '8000',
        path: link,
        method: 'POST',
        json: true,
        headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            'Content-Length': post_data.length
        }
    };

    post_req = http.request(post_options, function(res) {
        console.debug('STATUS: ' + res.statusCode);
        console.debug('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            console.debug('Response: ', chunk);
        });
    });

    post_req.on('error', function(e) {
        console.debug('problem with request: ' + e.message);
    });

    post_req.write(post_data);
    post_req.end();
}



module.exports.envio_datos = envio_datos;
module.exports.envio_datos_json = envio_datos_json;
