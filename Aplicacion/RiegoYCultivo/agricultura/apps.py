from __future__ import unicode_literals

from django.apps import AppConfig


class AgriculturaConfig(AppConfig):
    name = 'agricultura'
    verbose_name = 'Agricultura'
