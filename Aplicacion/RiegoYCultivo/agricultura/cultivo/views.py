# -*- coding: utf-8 -*-
__author__ = 'Dyuyay'
import json
from datetime import timedelta
import datetime

from socketIO_client import SocketIO, BaseNamespace
from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from django.contrib import messages
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404
from django.contrib.auth.decorators import login_required

from django.views.decorators.csrf import csrf_exempt
from .calculos import *
from agricultura.metodos import *

from app_empresa.models import proy_empresa


class DyuyayNamespace(BaseNamespace):
    def on_aaa_response(self, *args):
        print 'on_aaa_response', args

@login_required(login_url='/')
def cultivo_ver(request):
    print request.POST
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_cultivo') or request.user.has_perm('agrcultura.change_agr_cultivo') or request.user.has_perm('agricultura.list_agr_cultivo'):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            suelos=agr_tipo_suelo.objects.all()
            if (request.method == 'POST'):
                select = request.POST['select_estado']
                fecha = request.POST['start']
                fechaFin = request.POST['end']
                cultivo = request.POST['select_cultivos']
                parcela = request.POST['select_parcelas']
                if (fecha == "-" or fechaFin == "-"):
                    fecha = ""
                datosCultivo = []
                if (select == 'true'):
                    clt = agr_cultivo.objects.filter(parcela=parcelas).exclude(cult_estado=False)
                    if (fecha != "" and cultivo == "todos" and parcela == "todos"):
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        f2 = fechaFin.split('/')
                        fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                datosCultivo.append(fe)
                    elif (fecha == "" and cultivo != "todos" and parcela == "todos"):
                        # 		busqueda por culvito
                        datosCultivo = agr_cultivo.objects.filter(cult_detalle=cultivo)
                    elif (fecha == "" and cultivo == "todos" and parcela != "todos"):
                        datosCultivo = agr_cultivo.objects.filter(parcela__par_id__sar_nombre=parcela)
                    elif (fecha != "" and cultivo != "todos" and parcela == "todos"):
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            f2 = fechaFin.split('/')
                            fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                if (fe.cult_detalle == cultivo):
                                    datosCultivo.append(fe)
                    elif (fecha == "" and cultivo != "todos" and parcela != "todos"):
                        for fe in clt:
                            if (fe.cult_detalle == cultivo and fe.parcela.par_id.sar_nombre == parcela):
                                datosCultivo.append(fe)
                    elif (fecha != "" and cultivo == "todos" and parcela != "todos"):
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            f2 = fechaFin.split('/')
                            fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                if (fe.parcela.par_id.sar_nombre == parcela):
                                    datosCultivo.append(fe)
                    elif (fecha == "" and cultivo == "todos" and parcela == "todos"):
                        datosCultivo = agr_cultivo.objects.filter(parcela=parcelas).exclude(cult_estado=False)
                    else:
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            f2 = fechaFin.split('/')
                            fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                if (fe.cult_detalle == cultivo and fe.parcela.par_id.sar_nombre == parcela):
                                    datosCultivo.append(fe)
                else:
                    clt = agr_cultivo.objects.filter(parcela=parcelas).exclude(cult_estado=True)
                    if (fecha != "" and cultivo == "todos" and parcela == "todos"):
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        f2 = fechaFin.split('/')
                        fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                datosCultivo.append(fe)
                    elif (fecha == "" and cultivo != "todos" and parcela == "todos"):
                        # 		busqueda por culvito
                        datosCultivo = agr_cultivo.objects.filter(cult_detalle=cultivo)
                    elif (fecha == "" and cultivo == "todos" and parcela != "todos"):
                        datosCultivo = agr_cultivo.objects.filter(parcela__par_id__sar_nombre=parcela)
                    elif (fecha != "" and cultivo != "todos" and parcela == "todos"):
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            f2 = fechaFin.split('/')
                            fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                if (fe.cult_detalle == cultivo):
                                    datosCultivo.append(fe)
                    elif (fecha == "" and cultivo != "todos" and parcela != "todos"):
                        for fe in clt:
                            if (fe.cult_detalle == cultivo and fe.parcela.par_id.sar_nombre == parcela):
                                datosCultivo.append(fe)
                    elif (fecha != "" and cultivo == "todos" and parcela != "todos"):
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            f2 = fechaFin.split('/')
                            fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                if (fe.parcela.par_id.sar_nombre == parcela):
                                    datosCultivo.append(fe)
                    elif (fecha == "" and cultivo == "todos" and parcela == "todos"):
                        datosCultivo = agr_cultivo.objects.filter(parcela=parcelas).exclude(cult_estado=True)
                    else:
                        f = fecha.split('/')
                        fechaRe = datetime.datetime(int(f[2]), int(f[1]), int(f[0]))
                        for fe in clt:
                            feCul = fe.cult_fecha.split('/')
                            fCultivo = datetime.datetime(int(feCul[2]), int(feCul[1]), int(feCul[0]))
                            f2 = fechaFin.split('/')
                            fechaFinal = datetime.datetime(int(f2[2]), int(f2[1]), int(f2[0]))
                            if ((fCultivo >= fechaRe) and (fCultivo <= fechaFinal)):
                                if (fe.cult_detalle == cultivo and fe.parcela.par_id.sar_nombre == parcela):
                                    datosCultivo.append(fe)
                clt = datosCultivo
            else:
                clt = agr_cultivo.objects.filter(parcela=parcelas).exclude(cult_estado=False)
            data = []
            for i in clt:
                actual = datetime.date.today()
                #print(i.cult_detalle)
                a = i.cult_fecha.split('/')
                b = i.cult_fecha_fin.split('/')
                c = str(actual).split('-')
                cd = str(c[2]).split(' ')
                actual = datetime.datetime(int(c[0]), int(c[1]), int(cd[0]))
                s = datetime.datetime(int(a[2]), int(a[1]), int(a[0]))
                t = datetime.datetime(int(b[2]), int(b[1]), int(b[0]))
                if (actual >= t):
                    porcentaje = 100
                else:
                    x = actual + timedelta(days=1)
                    #print(x)
                    z = t + timedelta(days=1)
                    #print(x - s)
                    dia1 = str(x - s).split(' ')
                    dia2 = str(z - s).split(' ')
                    # #print(dia1,dia2)
                    try:
                        porcentaje = (int(float(dia1[0]) / float(dia2[0]) * 100))
                    except:
                        porcentaje = (int(float(0) / float(dia2[0]) * 100))
                    #print(porcentaje)
                defaultImagenCultivo=agr_cultivo_imagenes.objects.filter(cult_id=i.cult_id)
                if(defaultImagenCultivo.exists()):
                    imagen=defaultImagenCultivo.get(cim_principal=True).cim_imagen.url
                else:
                    imagen=i.esp_id.esp_imagen.url
                data.append({'id': i.cult_id, 'nombreCultivo': i.cult_detalle, 'descripcion': i.cult_descripcion,
                             'especie': i.esp_id.esp_nombre, 'especieImg': imagen, 'porcentaje': porcentaje})

            if (request.method == "POST"):

                if (select == 'true'):
                    select = 'Activo'
                else:
                    select = 'No activo'

                dic['datosCultivo']= data
                dic['cultivos']= agr_cultivo.objects.filter(parcela=parcelas)
                dic['datosParcelas']= agr_parcela.objects.filter(par_id=subareas)
                dic['med']=r_unidad_medida.objects.all();
                dic['fechaInicio']= fecha
                dic['fechaFin']= fechaFin
                dic['cultivo']= cultivo
                dic['parcela']= parcela
                dic['estado']= select
                dic['datos_subareas']=subareas
                dic['datos_suelo']=agr_tipo_suelo.objects.all()
                    # 'datosValores':agr_cultivo_valor.objects.all(),
                    # 'var':0
            else:
                dic['datosCultivo'] = data
                dic['cultivos'] = agr_cultivo.objects.filter(parcela=parcelas)
                dic['datosParcelas'] = agr_parcela.objects.filter(par_id=subareas)
                dic['datosConsulta'] =  'false'
                dic['datos_subareas']=subareas
                dic['datos_suelo']=agr_tipo_suelo.objects.all()
                    # 'datosValores':agr_cultivo_valor.objects.all(),
                    # 'var':0
            return render_to_response('agricultura/cultivo/templateCultivoVer1.html', dic,
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def cultivo_modelos(request):
    print request.POST
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_cultivo') or request.user.has_perm('agrcultura.change_agr_cultivo') or request.user.has_perm('agricultura.list_agr_cultivo'):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            suelos=agr_tipo_suelo.objects.all()
            clt1 = agr_cultivo.objects.filter(parcela=parcelas)
            if (request.method == 'POST'):
                cultivo = request.POST['select_cultivos']
                parcela = request.POST['select_parcelas']
                datosCultivo = []
                modelos=agr_modelo_cultivo.objects.filter(mcult_cultivo_referencia=clt1)
                if (cultivo == "todos" and parcela == "todos"):
                    for fe in modelos:
                        datosCultivo.append(fe)
                elif (cultivo != "todos" and parcela == "todos"):
                    # busqueda por especie
                    datosCultivo = modelos.filter(mcult_cultivo_referencia__esp_id__esp_nombre=cultivo)
                elif (cultivo == "todos" and parcela != "todos"):
                    datosCultivo = modelos.filter(mcult_cultivo_referencia__riego__rie_nombre=parcela)
                else:
                    for fe in modelos:
                            if (fe.mcult_cultivo_referencia.esp_id.esp_nombre == cultivo and 
                                fe.mcult_cultivo_referencia.riego.rie_nombre == parcela):
                                datosCultivo.append(fe)
                clt = datosCultivo
            else:
                clt = agr_modelo_cultivo.objects.filter(mcult_cultivo_referencia=clt1)
            data_model = []
            if (request.method == "POST"):
                dic['especies']= agr_especie_cultivo.objects.all()
                dic['datosRiegos']= agr_tipo_riego.objects.all()
                dic['med']=r_unidad_medida.objects.all();
                dic['especie']= cultivo
                dic['riego']= parcela
                dic['datos_subareas']=subareas
                dic['datos_suelo']=agr_tipo_suelo.objects.all()
                for i in clt:
                    data_model.append({'id': i.mcult_cultivo_referencia.cult_id, 'nombreCultivo': i.mcult_nombre,
                     'especie': i.mcult_cultivo_referencia.esp_id.esp_nombre, 'especieImg': i.mcult_cultivo_referencia.esp_id.esp_imagen,
                     'id_modelo':i.mcult_id})
                dic['datos_modelos']=data_model
                dic['datosParcelas']= agr_parcela.objects.filter(par_id=subareas)
                    # 'datosValores':agr_cultivo_valor.objects.all(),
                    # 'var':0
            else:
                dic['especies'] = agr_especie_cultivo.objects.all()
                dic['datosRiegos'] = agr_tipo_riego.objects.all()
                dic['datosConsulta'] =  'false'
                dic['datos_subareas']=subareas
                dic['datos_suelo']=agr_tipo_suelo.objects.all()
                data_model = []
                for i in agr_modelo_cultivo.objects.filter(mcult_cultivo_referencia=clt1):
                    data_model.append({'id': i.mcult_cultivo_referencia.cult_id, 'nombreCultivo': i.mcult_nombre,
                     'especie': i.mcult_cultivo_referencia.esp_id.esp_nombre, 'especieImg': i.mcult_cultivo_referencia.esp_id.esp_imagen,
                     'id_modelo':i.mcult_id})
                dic['datos_modelos']=data_model
                dic['datosParcelas']= agr_parcela.objects.filter(par_id=subareas)
                    # 'datosValores':agr_cultivo_valor.objects.all(),
                    # 'var':0
            return render_to_response('agricultura/cultivo/templateCultivoVer2.html', dic,
                                      context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def cambiarImange(request):
    try:
        dic = []
        id= request.POST["id"]
        try:
            imagen = request.FILES['file1']
            cultivo_imagen=agr_cultivo_imagenes.objects.filter(cult_id=id).get(cim_principal=True)
            cultivo_imagen.cim_imagen=imagen
            cultivo_imagen.save()
        except Exception as b:
            #print(b,'dddd111111')
            messages.add_message(request, messages.ERROR, 'No selecciono archivo')
            data=json.dumps(dic)
            return HttpResponse(data, content_type='application/json')
        dic = {
            'result': 'OK'
        }
        messages.add_message(request, messages.SUCCESS, 'Se cambio la imagen')
    except Exception as error:
        #print(error)
        messages.add_message(request, messages.ERROR, 'No coincide el tipo de archivo')
    data=json.dumps(dic)
    return HttpResponse(data, content_type='application/json')

@login_required(login_url='/')
def cambiarImagenes(request):
    try:
        dic = []
        id_imagen=request.POST["id_imagen"]
        id_cultivo=request.POST["id_cultivo"]
        try:
            imagen = request.FILES['file1']
            cultivo_imagen=agr_cultivo_imagenes.objects.filter(cult_id=id_cultivo).get(cim_id=id_imagen)
            cultivo_imagen.cim_imagen=imagen
            cultivo_imagen.save()
        except Exception as b:
            #print(b,'dddd111111')
            messages.add_message(request, messages.ERROR, 'No selecciono archivo')
            data=json.dumps(dic)
            return HttpResponse(data, content_type='application/json')
        dic = {
            'result': 'OK'
        }
        messages.add_message(request, messages.SUCCESS, 'Se cambio la imagen')
    except Exception as error:
        #print(error)
        messages.add_message(request, messages.ERROR, 'No coincide el tipo de archivo')
    data=json.dumps(dic)
    return HttpResponse(data, content_type='application/json')

@login_required(login_url='/')
def agregarImagen(request):
    try:
        dic = []
        id= request.POST["id"]
        try:
            imagen = request.FILES['file1']
            cultivo_imagen=agr_cultivo_imagenes()
            cultivo_imagen.cult_id=agr_cultivo.objects.get(cult_id=id)
            cultivo_imagen.cim_imagen=imagen
            cultivo_imagen.cim_principal=False
            cultivo_imagen.save()
        except Exception as b:
            #print(b,'dddd111111')
            messages.add_message(request, messages.ERROR, 'No selecciono archivo')
            data=json.dumps(dic)
            return HttpResponse(data, content_type='application/json')
        dic = {
            'result': 'OK'
        }
        messages.add_message(request, messages.SUCCESS, 'Se cambio la imagen')
    except Exception as error:
        #print(error)
        messages.add_message(request, messages.ERROR, 'No coincide el tipo de archivo')
    data=json.dumps(dic)
    return HttpResponse(data, content_type='application/json')

@login_required(login_url='/')
def cultivo_agregar(request):
    # #print(calculadora(2,40,1,2,12))
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_cultivo'):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id__emp_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            if (agr_parcela.objects.filter(par_id=subareas).exclude(par_estado=False).exists()):
                dic['datosEspecie'] = agr_especie_cultivo.objects.all().filter(esp_estado=True).order_by('esp_categoria__cat_nombre')
                dic['datosCategoria'] = agr_especie_categoria.objects.all()
                dic['datosSuelo'] = agr_tipo_suelo.objects.filter()
                dic['datosParcela'] = agr_parcela.objects.filter(par_id=subareas).exclude(par_estado=False)
                dic['datosTipoRiego'] = agr_tipo_riego.objects.all()
                return render_to_response('agricultura/cultivo/templateCultivoAgregar3.html', dic,
                                          context_instance=RequestContext(request))
            else:
                messages.add_message(request, messages.ERROR, 'No tiene parcelas en que realizar su cultivo')
                return redirect('/agricultura/inicio/')
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def obtenerEspecie(request):
    id = request.GET['id']
    data = []
    try:
        especie = agr_especie_cultivo.objects.get(esp_id=id)
        data.append({
            'especieNombre': especie.esp_nombre,
            'esp_estado': especie.esp_estado,
            'esp_imagen': especie.esp_imagen.url,
            'categoria': especie.esp_categoria.cat_nombre
        })
        # Buscar todos los valores de los parametros de cada una de las especies
        busquedaQuery = agr_especie_parametros.objects.filter(esp_id=especie)
        #print(busquedaQuery)
        data.append({'contarRegistros': busquedaQuery.count()})
        #print(busquedaQuery.count())
        for ve in busquedaQuery:
            data.append(
                {'valor': ve.valor, 'id': ve.tipo_id.par_id, 'nombreTipo': ve.tipo_id.par_nombre, 'um': ve.tipo_id.par_umedida.umed_abreviatura})
        return JsonResponse(data, safe=False)
    except Exception as error:
        #print(error)
        data = []
    return JsonResponse(data, safe=False)

@login_required(login_url='/')
def obtenerParcela(request):
    id = request.GET['id']
    #print(id,' i d')
    data = []
    try:
        subarea=proy_subarea.objects.get(sar_id=id)
        par = agr_parcela.objects.get(par_id=subarea)
        data.append({
            'parcelaEstado': par.par_estado,
            'parcelaNumero': par.par_hectareas,
            'parcelaDisponible': par.par_disponibles,
            'parcelaNombre': par.par_id.sar_nombre,
            'parcelaEtiqueta': par.par_id.sar_etiqueta,
            'parcelaImagen': par.par_id.sar_imagen.url,
            'suelo_nombre':par.par_suelo.tsue_nombre,
            'suelo_id':par.par_suelo.tsue_id,
            'suelo_imagen':par.par_suelo.tsue_imagen.url
        })
        valoresSuelo=agr_tipo_suelo_parametros.objects.filter(tipo_suelo=par.par_suelo)
        data.append({'contTipos': valoresSuelo.count()
                     })
        for datos in valoresSuelo:
            data.append({
                'tipoPrametro': datos.parametros.par_abreviatura,
                'valor': datos.valor,
                'um':datos.parametros.par_umedida.umed_abreviatura
            })

        return JsonResponse(data, safe=False)
    except Exception as error:
        #print(error)
        data = []
    return JsonResponse(data, safe=False)

@login_required(login_url='/')
def obtenerRiego(request):
    data = []
    try:
        grupo=agr_grupo.objects.get(gru_nombre='Riego')
        tparametro=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
        parametros=agr_parametro.objects.filter(par_tpar=tparametro).order_by('par_id')
        data.append({'contarRegistros': parametros.count()})
        for i in parametros:
                data.append({'id': i.par_id, 'nombreTipo': i.par_nombre,
                             'descripcion': i.par_descripcion,
                             'um': i.par_umedida.umed_abreviatura})
        return JsonResponse(data, safe=False)
    except Exception as error:
        #print(error)
        data = []
    return JsonResponse(data, safe=False)


# CULTIVOS
@login_required(login_url='/')
@transaction.atomic()
def anadir_cultivo(request):
    transaccion = transaction.savepoint()
    dict_suelo = []
    try:
        with transaction.atomic():
            nombre = request.POST['cultivo']
            nhectareas = request.POST['nhectareas']
            fecha = request.POST['fecha']
            descripcion = request.POST['descripcion']
            ubicacion = request.POST['ubicacion']
            especie = request.POST.getlist('select_especie')
            evo=request.POST['select_evo']
            for i in especie:
                esp = i
            parcela = request.POST.getlist('select_parcela')
            for i in parcela:
                par = i
            # #print('PARAMETROS QUE LLEGAN DEL SUELO')
            tiposParametros=agr_parametro.objects.all()
            for t in tiposParametros:
                idVa = '%s_%s' % ('text', t.par_abreviatura)
                if idVa not in request.POST:
                    a=0
                    # #print('no existe ese', idVa)
                else:
                    # #print('parametro: ',t.par_abreviatura)
                    dict_suelo.append({
                    'tipo':  t.par_abreviatura,
                    'valor': request.POST[idVa]
                    })
            riego = request.POST.getlist('select_riego')
            for i in riego:
                rie = i
            ##print(nombre, nhectareas, fecha, esp, par, sue)
            cultivo = agr_cultivo()
            cultivo.riego=agr_tipo_riego.objects.get(rie_id=rie)
            cultivo.cult_detalle = nombre
            cultivo.cult_num_hectareas = nhectareas
            cultivo.cult_fecha = fecha
            cultivo.cult_descripcion = descripcion
            cultivo.cult_ubicacion = ubicacion
            especie=agr_especie_cultivo.objects.get(esp_id=esp)
            cultivo.esp_id =especie
            cultivo.parcela=agr_parcela.objects.get(par_id=par)
            cultivo.cult_fecha_fin = request.POST['fecha']
            cultivo.save()
            fechaInicio = fecha.split('/')
            # #print('PARAMETROS QUE LLEGAN DE ESPECIE')
            dictValoresEspecies=[]
            valor=0
            for t in tiposParametros:
                idVa = '%s-%s' % ('tipoe', t.par_id)
                if idVa not in request.POST:
                    a=0
                    # #print('no existe ese', idVa)
                else:
                    # #print('parametro: ', t.par_abreviatura)
                    dictValoresEspecies.append({
                    'tipo':  t.par_abreviatura,
                    'valor': request.POST[idVa]
                    })
            # PARTE PARA GUARDAR LA IMAGEN DEL CULTIVO
            cultivo_imagen = agr_cultivo_imagenes()
            cultivo_imagen.cim_imagen=especie.esp_imagen
            cultivo_imagen.cim_principal=True
            cultivo_imagen.cult_id=cultivo
            cultivo_imagen.save()
            parcela = agr_parcela.objects.get(par_id=par)
            panel = '{"version":1,"header_image":"","allow_edit":true,"plugins":[],' \
                    '"panes":[{"title":"Wind","width":1,"row":{"2":1,"3":1,"4":1},' \
                    '"col":{"2":2,"3":2,"4":3},"col_width":1,' \
                    '"widgets":[{"type":"text_widget","settings":{"size":"regular","value":"datasources.Weather.wind_speed","sparkline":true,"animate":true,"units":"MPH"}}]},' \
                    '{"title":"Info","width":1,"row":{"2":1,"3":1,"4":1},"col":{"2":1,"3":1,"4":2},"col_width":1,"widgets":[{"type":"text_widget",' \
                    '"settings":{"title":"City","size":"regular","value":"datasources.Weather.place_name","animate":true}},{"type":"text_widget",' \
                    '"settings":{"title":"Conditions","size":"regular","value":"datasources.Weather.conditions","animate":true}}]},' \
                    '{"title":"Humidity","width":1,"row":{"2":7,"3":1,"4":1},"col":{"2":1,"3":3,"4":1},"col_width":1,' \
                    '"widgets":[{"type":"gauge","settings":{"value":"datasources.Weather.humidity","units":"%","min_value":0,"max_value":100}}]},' \
                    '{"title":"datos recibidos MQTT","width":1,"row":{"2":15,"3":1,"4":7},"col":{"2":1,"3":3,"4":3},"col_width":1,' \
                    '"widgets":[{"type":"text_widget","settings":{"title":"Mensajes","size":"regular","value":"datasources.Prueba.mensaje","animate":true}}]},' \
                    '{"title":"Imagen","width":1,"row":{"3":1,"4":1},"col":{"3":1,"4":4},"col_width":1,' \
                    '"widgets":[{"type":"picture","settings":{"src":"'+parcela.par_id.sar_imagen.url+'","refresh":0}}]},{"title":"Localización","width":1,"row":{"3":1,"4":11},"col":{"3":3,"4":3},"col_width":"2",' \
                    '"widgets":[{"type":"google_map","settings":{"lat":"'+str(parcela.par_id.sar_latitud)+'","lon":"'+str(parcela.par_id.sar_longitud)+'"}}]},{"width":1,"row":{"2":15,"3":7,"4":7},"col":{"2":2,"3":1,"4":2},"col_width":1,' \
                    '"widgets":[{"type":"text_widget","settings":{"title":"Sunrise","size":"regular","value":"datasources.Weather.sunrise","animate":true}},{"type":"text_widget",' \
                    '"settings":{"title":"Sunset","size":"regular","value":"datasources.Weather.sunset","animate":true}}]},{"title":"Temperature","width":1,"row":{"2":19,"3":9,"4":9},' \
                    '"col":{"2":1,"3":3,"4":1},"col_width":1,"widgets":[{"type":"text_widget","settings":{"title":"Current","size":"big","value":"datasources.Weather.current_temp",' \
                    '"animate":true,"units":"&deg;C"}},{"type":"text_widget","settings":{"title":"High","size":"regular","value":"datasources.Weather.high_temp",' \
                    '"animate":true,"units":"&deg;C"}},{"type":"text_widget","settings":{"title":"Low","size":"regular","value":"datasources.Weather.low_temp",' \
                    '"animate":true,"units":"&deg;C"}}]},{"title":"Pressure","width":1,"row":{"2":21,"3":15,"4":13},"col":{"2":2,"3":2,"4":2},"col_width":1,' \
                    '"widgets":[{"type":"text_widget","settings":{"size":"regular","value":"datasources.Weather.pressure","sparkline":true,"animate":true,"units":"mb"}}]}],' \
                    '"datasources":[{"name":"Weather","type":"openweathermap","settings":{"api_key":"8a830a7b02e485e3b33d12b095394e16","location":"Machala, EC","units":"metric","refresh":5}},' \
                    '{"name":"MQTT","type":"paho_mqtt","settings":{"topic":"test","name":"MQTT"}}],"columns":4}'

            # agr_monitoreo(idCultivo=cultivo, cult_panel=panel).save()

            #VERIFICAR SI SE PUEDE... POR IDCULTIVO
            # ultimo_panel = agr_cultivo_monitoreo.objects.latest('idCultivo')

            # socketIO = SocketIO('localhost', 8686)
            # dyuyayNamespace = socketIO.define(DyuyayNamespace, '/dyuyay_django')

            # dyuyayNamespace.emit('actualizarRooms',ultimo_panel.id)
            # socketIO.wait(seconds=2)

            parcela = agr_parcela.objects.get(par_id=par)
            disponibles = parcela.par_disponibles
            #print(disponibles)
            parcela.par_disponibles = float(disponibles) - float(nhectareas)
            if ((float(disponibles) - float(nhectareas)) == 0):
                parcela.par_estado = False
            parcela.save()
            # #print('PARAMETROS QUE LLEGAN DE RIEGO')
            for tip in tiposParametros:
                idVa = '%s-%s' % ('tipo', tip.par_id)
                if idVa not in request.POST:
                    # #print('no existe ese', idVa)
                    a=0
                else:
                    # #print('parametro:', tip.par_abreviatura)
                    guardarValoresCultivo = agr_cultivo_parametro()
                    guardarValoresCultivo.cultivo = cultivo
                    guardarValoresCultivo.tipo = tip
                    guardarValoresCultivo.valor = request.POST[idVa]
                    guardarValoresCultivo.save()
            #print 'Llego con exito'
            print(calculadora(cultivo.cult_id, dictValoresEspecies, par, rie, dict_suelo, int(fechaInicio[1]), evo))
            durac=0
            grupo=agr_grupo.objects.get(gru_nombre='Especie')
            tparametro=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)



            medida=r_unidad_medida.objects.get(umed_abreviatura='dias')
            parametros=agr_parametro.objects.filter(par_tpar=tparametro).filter(par_umedida=medida)
            for i in agr_cultivo_parametro.objects.filter(cultivo=cultivo).filter(tipo=parametros):
                durac=durac+i.valor
            cultivo_actualizar=agr_cultivo.objects.get(cult_id=cultivo.cult_id)
            a = fecha.split('/')
            s = datetime.datetime(int(a[2]), int(a[1]), int(a[0]))
            x = s + timedelta(days=durac)
            cultivo_actualizar.cult_fecha_fin = str(str(x.day) + '/' + str(x.month) + '/' + str(x.year))
            cultivo_actualizar.save()
            messages.add_message(request, messages.SUCCESS, 'Cultivo Guardado')
        transaction.savepoint_commit(transaccion)
    except ValueError:
        transaction.savepoint_rollback(transaccion)
        messages.add_message(request, messages.ERROR, 'No ha sido posible covertir los datos a entero.')
        return redirect('/agricultura/cultivo_agregar')
    except ZeroDivisionError as err:
        transaction.savepoint_rollback(transaccion)
        messages.add_message(request, messages.ERROR, 'No se puede dividir para cero.')
        return redirect('/agricultura/cultivo_agregar')
    except ObjectDoesNotExist as e:
        #print(e, '----------------------')
        transaction.savepoint_rollback(transaccion)
        messages.add_message(request, messages.ERROR, 'No existe informacion de la Evotranspiracion')
        return redirect('/agricultura/cultivo_agregar')
    except Exception as e:
        #print(e, '----------------------')
        transaction.savepoint_rollback(transaccion)
        messages.add_message(request, messages.ERROR, 'Error no contemplado.')
        return redirect('/agricultura/cultivo_agregar')
    redirige='/agricultura/detalle/'+str(cultivo.cult_id)
    return redirect(redirige)

@login_required(login_url='/')
def anadir_cultivo_java(request):
    dict_suelo = []
    try:
        fecha= request.POST['fecha']
        especie= request.POST.getlist('select_especie')
        parcela= request.POST.getlist('select_parcela')
        evo=request.POST['select_evo']
        for i in parcela:
            par=i
        # VALORES DE ESPECIES....
        dictValoresEspecies=[]
        tiposParametros=agr_parametro.objects.all()
        for t in tiposParametros:
            idVa = '%s-%s' % ('tipoe', t.par_id)
            if idVa not in request.POST:
                print('no existe ese', idVa)
            else:
                dictValoresEspecies.append({
                'tipo':  t.par_abreviatura,
                'valor': request.POST[idVa]
                })
        for t in tiposParametros:
            idVa = '%s_%s' % ('text', t.par_abreviatura)
            # #print('----------------1------------------')
            if idVa not in request.POST:
                print('no existe ese', idVa)
            else:
                dict_suelo.append({
                'tipo':  t.par_abreviatura,
                'valor': request.POST[idVa]
                })
        riego= request.POST.getlist('select_riego')
        for i in riego:
            rie=i
        a=fecha.split('/')
        tiposCultivo = []
        for tip in tiposParametros:
            idVa='%s-%s'%('tipo',tip.par_id)
            if idVa not in request.POST:
                si=False
            else:
                tiposCultivo.append({'tipo':tip.par_abreviatura, 'valor':request.POST[idVa]})
        #print tiposCultivo
        resultados=(calculadora_java(dictValoresEspecies,par,rie,dict_suelo, int(a[1]), tiposCultivo, evo))
        print(resultados)
        #print('Estos son los resultados: ', resultados)
    except Exception as e:
        #print(e,'Hay una excecion')
        data = json.dumps(resultados)
        return HttpResponse(data, content_type="aplication/json")
    data = json.dumps(resultados)
    #print('terminoo con exito')
    #print(resultados)
    return HttpResponse(data, content_type="aplication/json")
#
# @login_required(login_url='/')
# def detalle_cultivo(request):
#     try:
#         #print('oooooooooo')
#         cultivo = agr_cultivo.objects.get(id=request.GET['id'])
#         tipos_valores = agr_cultivo_valor.objects.filter(cultivo=cultivo)
#         formulas_valores = agr_cultivo_formulas.objects.filter(cultivo=cultivo)
#         data = []
#         data.append({'contador': 0})
#         for i in tipos_valores:
#             tipos = agr_tipo_parametro.objects.get(tpar_nombre=i.tipo)
#             data.append({'nombre': tipos.tpar_nombre, 'descripcion': tipos.tpar_descripcion, 'valor': i.valor,
#                          'um': tipos.umed_id.umed_abreviatura})
#         for i in formulas_valores:
#             tipos = agr_tipo_parametro.objects.get(tpar_nombre=i.tipo)
#             data.append({'nombre': tipos.tpar_nombre, 'descripcion': tipos.tpar_descripcion, 'valor': i.valor,
#                          'um': tipos.umed_id.umed_abreviatura})
#         data[0]['contador'] = len(data) - 1
#         #print(data)
#         dic = {
#             'result': 'OK'
#         }
#
#     # #print('--------',calculadora(cultivo,esp,par,rie))
#     except Exception as e:
#         dic = {
#             'result': 'Error al guardar'
#         }
#         #print('---------------', e)
#     data = json.dumps(data)
#     return HttpResponse(data, content_type='application/json')

@login_required(login_url='/')
def detallado(request, id):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_cultivo') or request.user.has_perm('agricultura.change_agr_cultivo') or request.user.has_perm('agricultura.list_agr_cultivo'):
            try:
                cultivo = agr_cultivo.objects.get(cult_id=id)
                imagenesCultivo = agr_cultivo_imagenes.objects.filter(cult_id=cultivo)
                especie=agr_especie_cultivo.objects.get(esp_id=cultivo.esp_id.esp_id)
                parcela = cultivo.parcela
                evotranspiracion=agr_evotranspiracion.objects.filter(par_id=parcela)
                dataRiegos = []
                dataFormulas = []
                dataEspecies = []
                dataSuelos=[]
                grupo = agr_grupo.objects.get(gru_nombre='Riego')
                tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                parametros=agr_parametro.objects.filter(par_tpar=tparametro)
                datos_riego=agr_cultivo_parametro.objects.filter(tipo=parametros).filter(cultivo=cultivo)
                for i in datos_riego:
                    dataRiegos.append({'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                                       'um': i.tipo.par_umedida.umed_abreviatura})
                resultadosRiego=agr_formulas_riego.objects.filter(tipo_riego=cultivo.riego).filter(for_general=False).order_by('pk')
                for j in resultadosRiego:
                    print ('......', j.for_nombre, '-----------')
                    parametros = agr_parametro.objects.get(par_abreviatura=j.for_nombre)
                    i = agr_cultivo_parametro.objects.filter(cultivo=cultivo).get(tipo=parametros)
                    dataFormulas.append({'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                                       'um': i.tipo.par_umedida.umed_abreviatura})

                grupo = agr_grupo.objects.get(gru_nombre='Especie')
                tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                parametros=agr_parametro.objects.filter(par_tpar=tparametro)
                tipos_especies=agr_cultivo_parametro.objects.filter(tipo=parametros).filter(cultivo=cultivo)
                for i in tipos_especies:
                    dataEspecies.append(
                        {'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                         'um': i.tipo.par_umedida.umed_abreviatura})

                notas = agr_notas_cultivo.objects.filter(not_cultivo=cultivo)

                grupo = agr_grupo.objects.get(gru_nombre='Tipo de Suelo')
                tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                parametros=agr_parametro.objects.filter(par_tpar=tparametro)
                tipos_suelos=agr_cultivo_parametro.objects.filter(tipo=parametros).filter(cultivo=cultivo)

                for i in tipos_suelos:
                    dataSuelos.append(
                        {'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                         'um': i.tipo.par_umedida.umed_abreviatura})
                if (cultivo):
                    dic['datosCultivo'] = cultivo
                    dic['datosImagenes'] = imagenesCultivo
                    dic['datosFormulas'] = dataFormulas
                    dic['datosEspecie'] = especie
                    dic['datoSuelo'] = agr_tipo_suelo.objects.get(tsue_id=parcela.par_suelo.tsue_id)
                    dic['datosEspecies'] = dataEspecies
                    dic['datosValores'] = dataRiegos
                    dic['datosSuelo'] = dataSuelos
                    dic['datosNotas'] = notas
                    dic['datosFases']=agr_fase.objects.all().exclude(fas_nombre='No aplica')
                    dic['med']=r_unidad_medida.objects.all()
                    if(evotranspiracion.exists()):
                        dic['datosEvo']=evotranspiracion[0]
                    else:
                        dic['datosEvo']='No asignado'
                    grupo=agr_grupo.objects.get(gru_nombre='Especie')
                    tparametro=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                    medida=r_unidad_medida.objects.get(umed_abreviatura='dias')
                    parametros=agr_parametro.objects.filter(par_tpar=tparametro).filter(par_umedida=medida)
                    fases=agr_fase.objects.all().exclude(fas_nombre='No aplica')
                    generalesFase=agr_cultivo_parametro.objects.filter(fase=fases).filter(cultivo=cultivo).exclude(tipo=parametros).order_by('id')
                    generalesDuracion=agr_cultivo_parametro.objects.filter(fase=fases).filter(cultivo=cultivo).filter(tipo=parametros).order_by('id')
                    dic['datosFasesValores']=generalesFase
                    dic['datosDuracionFases']=generalesDuracion
                    dic['var'] = porcentaje_me(cultivo.cult_id)
                    dic['tipo_plaga'] = agr_tipo_plaga_enfermedad.objects.all()
                    dic['plagas'] = agr_plaga_enfermedad.objects.filter(cul_id=cultivo).order_by("tpe_id","pe_fecha_identificacion")
                    return render_to_response('agricultura/cultivo/templateDetalleCultivo.html', dic,
                                              context_instance=RequestContext(request))
                else:
                    messages.add_message(request, messages.ERROR, 'No existe información')
            except Exception as e:
                messages.add_message(request, messages.ERROR, 'Información solicitada no existe')
                print(e, 'Error')
            return redirect('/agricultura/cultivo_ver')
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def detalleModelo(request, id):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_cultivo') or request.user.has_perm('agricultura.change_agr_cultivo') or request.user.has_perm('agricultura.list_agr_cultivo'):
            try:
                cultivo = agr_cultivo.objects.get(cult_id=id)
                imagenesCultivo = agr_cultivo_imagenes.objects.filter(cult_id=cultivo)
                especie=agr_especie_cultivo.objects.get(esp_id=cultivo.esp_id.esp_id)
                parcela = cultivo.parcela
                evotranspiracion=agr_evotranspiracion.objects.filter(par_id=parcela)
                dataRiegos = []
                dataFormulas = []
                dataEspecies = []
                dataSuelos=[]
                grupo = agr_grupo.objects.get(gru_nombre='Riego')
                tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                parametros=agr_parametro.objects.filter(par_tpar=tparametro)
                datos_riego=agr_cultivo_parametro.objects.filter(tipo=parametros).filter(cultivo=cultivo)
                for i in datos_riego:
                    dataRiegos.append({'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                                       'um': i.tipo.par_umedida.umed_abreviatura})

                resultadosRiego=agr_formulas_riego.objects.filter(tipo_riego=cultivo.riego).filter(for_general=False).order_by('pk')
                for j in resultadosRiego:
                    print ('......', j.for_nombre, '-----------')
                    parametros = agr_parametro.objects.get(par_abreviatura=j.for_nombre)
                    i = agr_cultivo_parametro.objects.filter(cultivo=cultivo).get(tipo=parametros)
                    dataFormulas.append({'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                                       'um': i.tipo.par_umedida.umed_abreviatura})

                grupo = agr_grupo.objects.get(gru_nombre='Especie')
                tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                parametros=agr_parametro.objects.filter(par_tpar=tparametro)
                tipos_especies=agr_cultivo_parametro.objects.filter(tipo=parametros).filter(cultivo=cultivo)
                for i in tipos_especies:
                    dataEspecies.append(
                        {'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                         'um': i.tipo.par_umedida.umed_abreviatura})
                notas = agr_notas_cultivo.objects.filter(not_cultivo=cultivo)

                grupo = agr_grupo.objects.get(gru_nombre='Tipo de Suelo')
                tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                parametros=agr_parametro.objects.filter(par_tpar=tparametro)
                tipos_suelos=agr_cultivo_parametro.objects.filter(tipo=parametros).filter(cultivo=cultivo)
                for i in tipos_suelos:
                    dataSuelos.append(
                        {'nombre': i.tipo.par_nombre, 'descripcion': i.tipo.par_descripcion, 'valor': i.valor,
                         'um': i.tipo.par_umedida.umed_abreviatura})
                if (cultivo):
                    dic['datosCultivo'] = cultivo
                    dic['datosImagenes'] = imagenesCultivo
                    dic['datosFormulas'] = dataFormulas
                    dic['datosEspecie'] = especie

                    dic['datoSuelo'] = agr_tipo_suelo.objects.get(tsue_id=parcela.par_suelo.tsue_id)
                    dic['datosEspecies'] = dataEspecies
                    dic['datosValores'] = dataRiegos
                    dic['datosSuelo'] = dataSuelos
                    dic['datosNotas'] = notas
                    dic['datosFases']=agr_fase.objects.all().exclude(fas_nombre='No aplica')
                    dic['med']=r_unidad_medida.objects.all();
                    if(evotranspiracion.exists()):
                        dic['datosEvo']=evotranspiracion[0]
                    else:
                        dic['datosEvo']='No asignado'
                    grupo=agr_grupo.objects.get(gru_nombre='Especie')
                    tparametro=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                    medida=r_unidad_medida.objects.get(umed_abreviatura='dias')
                    parametros=agr_parametro.objects.filter(par_tpar=tparametro).filter(par_umedida=medida)
                    fases=agr_fase.objects.all().exclude(fas_nombre='No aplica')
                    generalesFase=agr_cultivo_parametro.objects.filter(fase=fases).filter(cultivo=cultivo).exclude(tipo=parametros).order_by('id')
                    generalesDuracion=agr_cultivo_parametro.objects.filter(fase=fases).filter(cultivo=cultivo).filter(tipo=parametros).order_by('id')
                    dic['datosFasesValores']=generalesFase
                    dic['datosDuracionFases']=generalesDuracion
                    dic['var'] = porcentaje_me(cultivo.cult_id)
                    dic['tipo_plaga'] = agr_tipo_plaga_enfermedad.objects.all()
                    dic['plagas'] = agr_plaga_enfermedad.objects.filter(cul_id=cultivo).order_by("tpe_id","pe_fecha_identificacion")
                    dic['modelo'] = agr_modelo_cultivo.objects.get(mcult_cultivo_referencia=cultivo)
                    empresa=request.user.id_persona.emp_id.emp_id            
                    areas = proy_area.objects.filter(emp_id_id=empresa)
                    subareas = proy_subarea.objects.filter(ar_id=areas)
                    dic['datosParcelas'] = agr_parcela.objects.filter(par_id=subareas)
                    return render_to_response('agricultura/cultivo/templateDetalleCultivoModelo.html', dic,
                                              context_instance=RequestContext(request))
                else:
                    messages.add_message(request, messages.ERROR, 'No existe informacion')
            except Exception as e:
                messages.add_message(request, messages.ERROR, 'No existe informacion')
                print(e, 'Error')
            return redirect('/agricultura/cultivo_modelos')
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')


def porcentaje_me(id):
    try:
        clt = agr_cultivo.objects.get(cult_id=id)
        actual = datetime.date.today()
        a = clt.cult_fecha.split('/')
        b = clt.cult_fecha_fin.split('/')
        s = datetime.datetime(int(a[2]), int(a[1]), int(a[0]))
        t = datetime.datetime(int(b[2]), int(b[1]), int(b[0]))
        c = str(actual).split('-')
        cd = str(c[2]).split(' ')
        actual = datetime.datetime(int(c[0]), int(c[1]), int(cd[0]))
        if (actual >= t):
            porcentaje = 100
        else:
            x = actual + timedelta(days=1)
            z = t + timedelta(days=1)
            dia1 = str(x - s).split(' ')
            dia2 = str(z - s).split(' ')
            porcentaje = (int(float(dia1[0]) / float(dia2[0]) * 100))
    except Exception as e:
        #print(e, 'lllllllllllllll')
        return 0
    return porcentaje

def lista_cosecha(request):
    data=[]
    try:
        cultivo =agr_cultivo.objects.get(cult_id=request.GET['id'])
        cosechas = agr_cosecha.objects.filter(cult_id=cultivo.cult_id).order_by('-cos_fecha')
        if cosechas.exists():
            # for cos in cosechas:
            #     print("codigo:"+str(cos.pk)+
            #         "-fecha:"+str(cos.cos_fecha)+
            #         "-cantidad_cosechada:"+str(cos.cos_cantidad_cosechada)+
            #         "-observacion:"+str(cos.cos_observacion)+
            #         "-precio_venta:"+str(cos.cos_precio_venta)+
            #         "-costo_produccion:"+str(cos.cos_costo_produccion)+
            #         "-cant_hect_cosechadas:"+str(cos.cos_cant_hect_cosechadas)+
            #         "-medida:"+str(cos.medida_id.umed_abreviatura))

            dic = {'result': 'OK'}
            cose=[]
            for cos in cosechas:
                cose.append({'id':cos.pk,'fecha':str(cos.cos_fecha),
                    'cantidad_cosechada': cos.cos_cantidad_cosechada,
                    'observacion': cos.cos_observacion,
                    'precio_venta': cos.cos_precio_venta,
                    'costo_produccion': cos.cos_costo_produccion,
                    'cant_hect_cosechadas': cos.cos_cantidad_hect_cosechadas,
                    'medida': cos.medida_id.umed_abreviatura})
                
            data.append(dic)
            data.append(cose)

        else:
            dic = {'result': 'No'}
            data.append(dic)
    except Exception as e:
        print(e)
        dic = {'result': 'Error'}
        data.append(dic)
    d = json.dumps(data)
    print d
    return HttpResponse(d, content_type="aplication/json")


def guarda_cosecha(request):
    data=""
    print request.GET['cant_hect_cosechadas']
    try:
        cul_id =agr_cultivo.objects.get(cult_id=request.GET['id']) 
        fecha=request.GET['fecha']
        cantidad_cosechada=request.GET['cantidad_cosechada']
        observacion = request.GET['observacion']
        precio_venta = request.GET['precio_venta']
        costo_produccion = request.GET['costo_produccion']
        cant_hect_cosechadas=float(request.GET['cant_hect_cosechadas'])
        medida_id=r_unidad_medida.objects.get(umed_id=request.GET['medida_id'])
        # print(cul_id)
        # print(fecha)
        # print(cantidad_cosechada)
        # print(observacion)
        # print(precio_venta)
        # print(costo_produccion)
        # print(cant_hect_cosechadas)
        # print(medida_id)

        cosecha=agr_cosecha(cult_id=cul_id,cos_cantidad_cosechada=cantidad_cosechada,
                                                   cos_observacion=observacion,
                                                   cos_precio_venta=precio_venta,
                                                   cos_costo_produccion=costo_produccion,
                                                   cos_cantidad_hect_cosechadas=cant_hect_cosechadas,
                                                   medida_id=medida_id)
        cosecha.save()

        cosecha.cos_fecha=fecha
        cosecha.save()
        print("guardado")
        dic = {
            'result': 'OK'
        }
        data = json.dumps(dic)
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error'
        }
        data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

def consulta_cosecha(request):
    data=[]
    try:
        cos=agr_cosecha.objects.get(cos_id=request.GET['id'])
        data.append({'id':cos.pk,'fecha':str(cos.cos_fecha),
                    'cantidad_cosechada': cos.cos_cantidad_cosechada,
                    'observacion': cos.cos_observacion,
                    'precio_venta': cos.cos_precio_venta,
                    'costo_produccion': cos.cos_costo_produccion,
                    'cant_hect_cosechadas': cos.cos_cantidad_hect_cosechadas,
                    'medida': cos.medida_id.umed_id})
    except Exception, e:
        print e
    return JsonResponse(data, safe=False)





def edita_cosecha(request):
    try:
        cos_id = request.GET['id']
        fecha=request.GET['fecha']
        cantidad_cosechada=request.GET['cantidad_cosechada']
        observacion = request.GET['observacion']
        precio_venta = request.GET['precio_venta']
        costo_produccion = request.GET['costo_produccion']
        cant_hect_cosechadas=request.GET['cant_hect_cosechadas']
        medida_id=r_unidad_medida.objects.get(umed_id=request.GET['medida_id'])
        agr_cosecha.objects.filter(cos_id=cos_id).update(cos_cantidad_cosechada=cantidad_cosechada,
           cos_observacion=observacion,cos_precio_venta=precio_venta,cos_costo_produccion=costo_produccion,
           cos_cantidad_hect_cosechadas=cant_hect_cosechadas,medida_id=medida_id)
        # cosecha.cos_cantidad_cosechada=cantidad_cosechada
        # cosecha.cos_observacion=observacion        
        # cosecha.cos_precio_venta=precio_venta
        # cosecha.cos_costo_produccion=costo_produccion
        # cosecha.cos_cant_hect_cosechadas=cant_hect_cosechadas
        # cosecha.medida_id=medida_id
        dic = {
            'result': 'OK'
        }
        
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al Editar'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")


def cosechar(request):
    id = request.GET['id']
    try:
        bajaCultivo = agr_cultivo.objects.get(cult_id=id)
        bajaCultivo.cult_estado = False
        bajaCultivo.save()
        parcela = agr_parcela.objects.get(par_id=bajaCultivo.parcela.par_id.sar_id)
        disponiblesBD = parcela.par_disponibles
        ocupadasBD = bajaCultivo.cult_num_hectareas
        parcela.par_disponibles = (int(disponiblesBD) + int(ocupadasBD))
        parcela.par_estado = True
        parcela.save()
        dic = {'result': 'OK'}
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja la cultivo'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def agregar_nota(request):
    try:
        id = request.GET['id']
        #print(id)
        nota = request.GET['nota']
        #print(nota)
        fecha = datetime.datetime.now()
        #print(fecha)
        cultivo = agr_cultivo.objects.get(cult_id=id)
        #print(cultivo)
        notaCultivo = agr_notas_cultivo(not_cultivo=cultivo, not_nota=nota, not_fecha=fecha)
        notaCultivo.save()
        dic = {'result': 'OK'}
    except Exception as e:
        #print(e)
        dic = {
            'result': 'Error'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

def eliminar_nota(request):
    try:
        id = request.GET['id']
        notaCultivo = agr_notas_cultivo.objects.get(not_id=id)
        notaCultivo.delete()
        dic = {'result': 'OK'}
    except Exception as e:
        #print(e)
        dic = {
            'result': 'Error'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def tiene_parcela(request):
    try:
        id = request.GET['id']
        et = agr_evotranspiracion.objects.filter(par_id__par_id=id).get(mes='Enero')
        dic = {'result': 'OK'}
    except Exception as e:
        #print(e)
        dic = {
            'result': 'Error'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")


@login_required(login_url='/')
def obtener_datosSuelo(request):
    try:
        dict = []
        id = request.GET['id']
        tsue=agr_tipo_suelo.objects.get(tsue_id=id)
        datosSuelo = agr_tipo_suelo_parametros.objects.filter(tipo_suelo=tsue)
        for datos in datosSuelo:
            dict.append({
                'tipoPrametro': datos.agr_tipo_parametros.tpar_abreviatura,
                'valor': datos.valor,
            })
    except Exception as e:
        #print(e)
        dict = {
            'result': 'Error'
        }
    data = json.dumps(dict)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def copiar_cultivo(request):
    if request.user.is_authenticated():
        if (request.method == 'POST'):
            try:
                cultivo = request.POST['busq']
                nombre = request.POST['txt_nombre_copiar']
                parcela = request.POST['select_parcela_copiar']
                fecha = request.POST['fecha_copiar']
                hectareas = request.POST['txt_hectareas_copiar']
                cultivo_copiado=agr_cultivo.objects.get(cult_id=cultivo)

                parcela_nueva=agr_parcela.objects.get(par_id__sar_id=parcela)
                parcela_nueva.par_disponibles=parcela_nueva.par_disponibles-float(hectareas)
                parcela_nueva.save()

                cultivo_nuevo=agr_cultivo()
                cultivo_nuevo.cult_detalle=nombre
                cultivo_nuevo.cult_descripcion=cultivo_copiado.cult_descripcion
                cultivo_nuevo.cult_fecha=fecha
                cultivo_nuevo.cult_fecha_fin=fecha
                cultivo_nuevo.cult_num_hectareas=hectareas
                cultivo_nuevo.cul_estado=True
                cultivo_nuevo.esp_id=cultivo_copiado.esp_id
                cultivo_nuevo.parcela=parcela_nueva
                cultivo_nuevo.riego=cultivo_copiado.riego
                cultivo_nuevo.save()
                parametros=agr_cultivo_parametro.objects.filter(cultivo=cultivo_copiado)
                for i in parametros:
                    par_nuevo=agr_cultivo_parametro()
                    par_nuevo.cultivo=cultivo_nuevo
                    par_nuevo.tipo=i.tipo
                    par_nuevo.fase=i.fase
                    par_nuevo.valor=i.valor
                    par_nuevo.save()
                durac=0
                grupo=agr_grupo.objects.get(gru_nombre='Especie')
                tparametro=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
                medida=r_unidad_medida.objects.get(umed_abreviatura='dias')
                parametros=agr_parametro.objects.filter(par_tpar=tparametro).filter(par_umedida=medida)
                for i in agr_cultivo_parametro.objects.filter(cultivo=cultivo_nuevo).filter(tipo=parametros):
                    durac=durac+i.valor
                cultivo_actualizar=agr_cultivo.objects.get(cult_id=cultivo_nuevo.cult_id)
                a = fecha.split('/')
                s = datetime.datetime(int(a[2]), int(a[1]), int(a[0]))
                x = s + timedelta(days=durac)
                cultivo_actualizar.cult_fecha_fin = str(str(x.day) + '/' + str(x.month) + '/' + str(x.year))
                cultivo_actualizar.save()
                cultivo_imagen = agr_cultivo_imagenes()
                cultivo_imagen.cim_imagen=cultivo_copiado.esp_id.esp_imagen
                cultivo_imagen.cim_principal=True
                cultivo_imagen.cult_id=cultivo_nuevo
                cultivo_imagen.save()                
                messages.add_message(request, messages.SUCCESS, 'Cultivo configurado correctamente')
                return redirect('/agricultura/detalle/'+str(cultivo_nuevo.cult_id))
            except Exception as e:
                print e
                messages.add_message(request, messages.ERROR, 'Error')
                return redirect('/agricultura/cultivo_ver/')
        else:
            return redirect('/agricultura/cultivo_ver/')
    else:
        return redirect('/')


@login_required(login_url='/')
def crear_modelo(request):
    id = request.GET['id']
    print('id', id)
    nombre = request.GET['nombre']
    try:
        cultivo = agr_cultivo.objects.get(cult_id=id)
        modelo=agr_modelo_cultivo.objects.filter(mcult_cultivo_referencia=cultivo)
        if(modelo.exists()):
            modelo=agr_modelo_cultivo.objects.get(mcult_cultivo_referencia=cultivo)
            modelo.mcult_nombre=nombre
            modelo.save()
            dic = {'result': 'OK'}
        else:
            modelo=agr_modelo_cultivo()
            modelo.mcult_cultivo_referencia=cultivo
            modelo.mcult_nombre=nombre
            modelo.save()
            dic = {'result': 'OK'}
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al crear el modelo'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")