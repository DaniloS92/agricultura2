__author__ = 'DYUYAY'
from ..models import *
from decimal import *
import openpyxl
import datetime
from Equation import Expression

def calculadora(cultivo,especie,parcela,riego,suelo, fecha, evo_select):
    riego=agr_tipo_riego.objects.get(rie_id=riego)
    hoja=agr_formulas_riego.objects.filter(tipo_riego=riego).exclude(for_general=True).order_by('for_id')
    vectorDatos=[]
    for i in especie:
        vectorDatos.append({'tipo':i['tipo'], 'valor':i['valor']})
    for i in suelo:
        vectorDatos.append({'tipo':i['tipo'], 'valor':i['valor']})
    parcelaQuery = agr_parcela.objects.get(par_id__sar_id=parcela)
    evo=agr_evotranspiracion.objects.get(par_id=parcelaQuery, tipo=evo_select, mes=obtenerMes(fecha))
    evo1= evo_tipo_valor.objects.filter(evo_id=evo)
    for i in evo1:
        vectorDatos.append({'tipo':i.tipo, 'valor':i.valor})
    parcela= agr_parametros_parcela.objects.filter(parcela=parcelaQuery)
    for i in parcela:
        vectorDatos.append({'tipo':i.par_tipo, 'valor':i.par_valor})
    cultivoQuery=agr_cultivo.objects.get(cult_id=cultivo)
    cultivo_valor=agr_cultivo_parametro.objects.filter(cultivo=cultivoQuery)
    for i in cultivo_valor:
        vectorDatos.append({'tipo':i.tipo.par_abreviatura, 'valor':i.valor})
    rptaV=[]
    j=0
    resultados=[]
    for row in hoja:
        datos=[]
        try:
            tipos=row.for_tipos.split(',')
            for i in tipos:
                datos.append(str(i))
        except Exception as e:
            #print('eError',e)
            datos.append(row.for_tipos)
        rptaV.append(row.for_nombre)
        valores=[]
        for i in vectorDatos:
            for a in datos:
                if (recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])) == i['tipo']:
                    valores.append({(recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])):i['valor']})
        # #print(row.for_formula)
        f=Expression(row.for_formula,datos)
        if (len(datos)==1):
            #print('Estoy en 1')
            rpta = caso1(f, valores, datos[0])
            resultados.append({'tipo':rptaV[j], 'valor':rpta})
            vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
            #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
        if (len(datos)==2):
            #print('Estoy en 2')
            rpta = caso2(f, valores, datos[0], datos[1])
            resultados.append({'tipo':rptaV[j], 'valor':rpta})
            vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
            #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
        if (len(datos)==3):
            #print('Estoy en 3')
            rpta = caso3(f, valores, datos[0], datos[1], datos[2])
            resultados.append({'tipo':rptaV[j], 'valor':rpta})
            vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
            #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
        if (len(datos)==4):
            #print('Estoy en 4')
            rpta = caso4(f, valores, datos[0], datos[1], datos[2], datos[3])
            resultados.append({'tipo':rptaV[j], 'valor':rpta})
            vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
            #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
        if (len(datos)==5):
            #print('Estoy en 5')
            rpta = caso5(f, valores, datos[0], datos[1], datos[2], datos[3], datos[4] )
            resultados.append({'tipo':rptaV[j], 'valor':rpta})
            vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
            #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
        j=j+1
    #print 'Finalizo Operaciones con exito'
    for i in especie:
        guardarParametrosEspecieValores=agr_cultivo_parametro()
        guardarParametrosEspecieValores.cultivo=cultivoQuery
        parametro=agr_parametro.objects.get(par_abreviatura=i['tipo'])
        guardarParametrosEspecieValores.tipo=parametro
        guardarParametrosEspecieValores.valor=i['valor']
        guardarParametrosEspecieValores.save()
    #print 'Especies Guardadas'
    for i in suelo:
        guardarSueloValores=agr_cultivo_parametro()
        guardarSueloValores.cultivo=cultivoQuery
        parametro=agr_parametro.objects.get(par_abreviatura=i['tipo'])
        guardarSueloValores.tipo = parametro
        guardarSueloValores.valor=i['valor']
        guardarSueloValores.save()
    #print 'Suelos Guardados'
    for i in resultados:
        guardarFormulas=agr_cultivo_parametro()
        guardarFormulas.cultivo=cultivoQuery
        parametro=agr_parametro.objects.get(par_abreviatura=i['tipo'])
        guardarFormulas.tipo = parametro
        guardarFormulas.valor=i['valor']
        guardarFormulas.save()
    #print 'Primera Parte Exito'
    hoja=agr_formulas_riego.objects.filter(tipo_riego=riego).exclude(for_general=False).order_by('pk')
    fases = agr_fase.objects.all().exclude(fas_nombre='No aplica')
    vGenerales=[]
    for i in vectorDatos:
        vGenerales.append(i)
    for fas in fases:
        j=0
        rptaV=[]
        del vectorDatos
        vectorDatos=[]
        for i in vGenerales:
            vectorDatos.append(i)
        for row in hoja:
            medida = r_unidad_medida.objects.get(umed_abreviatura='dias')
            dato = agr_parametro.objects.filter(par_fase=fas).exclude(par_umedida=medida)
            datos=[]
            try:
                tipos=row.for_tipos.split(',')
                for i in tipos:
                    if(i=='fase'):
                        # #print(i)
                        datos.append(dato[0].par_abreviatura)
                    else:
                        datos.append(str(i))
            except Exception as e:
                # #print('eError',e)
                if(i=='fase'):
                    # #print(i)
                    datos.append(dato[0].par_abreviatura)
                else:
                    datos.append(row.for_tipos)
            rptaV.append(row.for_nombre)
            valores=[]
            for i in vectorDatos:
                for a in datos:
                    if (recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])) == i['tipo']:
                        valores.append({(recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])):i['valor']})
            variableTemporar=row.for_formula.replace('fase',dato[0].par_abreviatura)
            f=Expression(variableTemporar,datos)
            # #print(datos,'4-1')
            # #print(len(datos),'5')
            if (len(datos)==1):
                # #print('Estoy en 1')
                resultados.append({'tipo':'type', 'resultado':rptaV[j], 'formula':"="+row.for_formula, 'value':
                    datos[0]+'='+str(obtener(valores,(datos[0], datos[0].split('9'))[datos[0].endswith('9')],(False, True)[datos[0].endswith('9')]))})

                # CASO 1
                rpta = caso1(f, valores, datos[0] )

                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                guardarFase(fas, rptaV[j], cultivo, rpta )
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
            if (len(datos)==2):
                #print('Estoy en 2')
                html=""
                for ll in range(0,2):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+variableTemporar,'value':html})

                # CASO 2
                rpta = caso2(f, valores, datos[0], datos[1])

                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                guardarFase(fas, rptaV[j], cultivo, rpta )
            if (len(datos)==3):
                #print('Estoy en 3')
                html=""
                for ll in range(0,3):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+variableTemporar,'value':html})
                # CASO 3
                rpta = caso3(f, valores, datos[0], datos[1], datos[2] )

                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                guardarFase(fas, rptaV[j], cultivo, rpta )
            if (len(datos)==4):
                #print('Estoy en 4')
                html=""
                for ll in range(0,4):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+variableTemporar,'value':html})
                # CASO 4
                rpta = caso4(f, valores, datos[0], datos[1], datos[2], datos[3])
                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                guardarFase(fas, rptaV[j], cultivo, rpta )
            if (len(datos)==5):
                #print('Estoy en 5')
                html=""
                for ll in range(0,5):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+variableTemporar,'value':html})
                # CASO 5
                rpta = caso5(f, valores, datos[0], datos[1], datos[2], datos[3], datos[4])
                # #print('Sin llegar')
                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                guardarFase(fas, rptaV[j], cultivo, rpta)
            j=j+1
            #print(vectorDatos)
        for i in vectorDatos:
            if(i['tipo']=='etc'):
                i['tipo']='n/a'
                i['valor']='n/a'
    return 'Ok'

def obtener(diccionario, tipo, ajustado):
    # #print('1. Se va a obtener el parametro Diccionario: ',diccionario,' Tipo: ',tipo)
    if(ajustado):
        # #print('2. Se verifica si es ajustado')
        tipo=str(tipo[0])
    for i in diccionario:
        # #print('entro al for')
        # #print(i.get(tipo))
        if(i.get(tipo)):
            # #print('entro al 1ewr if')
            # #print('3. Encontro el tipo: ',i.get(tipo))
            if(ajustado):
                return(round(i.get(tipo)))
            else:
                # #print('Antes del float',(i.get(tipo)))
                return(float(i.get(tipo)))
        if(i.get(tipo)==0):
            return(float(i.get(tipo)))
def obtenerTipo(diccionario, tipo):
    #print('1. Se va a obtener el parametro Diccionario: ',diccionario,' Tipo: ',tipo)
    for i in diccionario:
        if(i.get(tipo)):
            # #print(i)
            return(float(i.get(tipo)))
def obtenerMes(mes):
    meses=agr_mes.objects.all()
    contadorMes=1
    data=[]
    for i in meses:
        data.append({contadorMes:i.mes_nombre})
        contadorMes=contadorMes+1
    for i in data:
        if(i.get(mes)):
            return(i.get(mes))
def recortarTipo(vector, ajustado):
    try:
        if(ajustado):
            return  vector[0]
        else:
            return vector
    except Exception as e:
        print(e)

def calculadora_java(especie,parcela,riego,suelo, fecha, tiposCultivo, evo_select):
    try:
        riego=agr_tipo_riego.objects.get(rie_id=riego)
        hoja=agr_formulas_riego.objects.filter(tipo_riego=riego).exclude(for_general=True).order_by('for_id')
        vectorDatos=[]
        for i in especie:
            vectorDatos.append({'tipo':i['tipo'], 'valor':i['valor']})
        for i in suelo:
            vectorDatos.append({'tipo':i['tipo'], 'valor':i['valor']})
        parcelaQuery=agr_parcela.objects.get(par_id__sar_id=parcela)
        evo=agr_evotranspiracion.objects.get(par_id=parcelaQuery, tipo=evo_select, mes=obtenerMes(fecha))
        evo1= evo_tipo_valor.objects.filter(evo_id=evo)
        for i in evo1:
            vectorDatos.append({'tipo':i.tipo, 'valor':i.valor})
        parcela= agr_parametros_parcela.objects.filter(parcela=parcelaQuery)
        for i in parcela:
            vectorDatos.append({'tipo':i.par_tipo, 'valor':i.par_valor})
        for i in tiposCultivo:
            vectorDatos.append({'tipo':i['tipo'], 'valor':i['valor']})
        rptaV=[]
        j=0
        resultados=[]
        respuesta=[]
        # #print(vectorDatos)
        for row in hoja:
            datos=[]
            try:
                tipos=row.for_tipos.split(',')
                for i in tipos:
                    datos.append(str(i))
            except Exception as e:
                # #print('eError',e)
                datos.append(row.for_tipos)
            rptaV.append(row.for_nombre)
            valores=[]
            for i in vectorDatos:
                for a in datos:
                    if (recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])) == i['tipo']:
                        valores.append({(recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])):i['valor']})
            f=Expression(row.for_formula,datos)
            if (len(datos)==1):
                resultados.append({'tipo':'type', 'resultado':rptaV[j], 'formula':"="+row.for_formula, 'value':
                    datos[0]+'='+str(obtener(valores,(datos[0], datos[0].split('9'))[datos[0].endswith('9')],(False, True)[datos[0].endswith('9')]))})

                # CASO 1
                rpta = caso1(f, valores, datos[0] )

                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
            if (len(datos)==2):
                # #print('Estoy en 2')
                html=""
                for ll in range(0,2):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+row.for_formula,'value':html})

                # CASO 2
                rpta = caso2(f, valores, datos[0], datos[1])

                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
            if (len(datos)==3):
                # #print('Estoy en 3')
                html=""
                for ll in range(0,3):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+row.for_formula,'value':html})
                # CASO 3
                rpta = caso3(f, valores, datos[0], datos[1], datos[2] )

                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
            if (len(datos)==4):
                # #print('Estoy en 4')
                html=""
                for ll in range(0,4):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+row.for_formula,'value':html})
                # CASO 4
                rpta = caso4(f, valores, datos[0], datos[1], datos[2], datos[3])
                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
            if (len(datos)==5):
                # #print('Estoy en 5')
                html=""
                for ll in range(0,5):
                    html+=datos[ll]+'='+str(round(obtener(valores,(datos[ll], datos[ll].split('9'))[datos[ll].endswith('9')],(False, True)[datos[ll].endswith('9')])))+"; "
                resultados.append({'tipo':'type','resultado':rptaV[j],'formula':"="+row.for_formula,'value':html})
                # CASO 5
                rpta = caso5(f, valores, datos[0], datos[1], datos[2], datos[3], datos[4])
                # #print('Sin llegar')
                resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
            j=j+1
        #    METODO PARA EL CALCULO DE LAS ETAPAS
        #print(vectorDatos, 'aki termina')
        hoja=agr_formulas_riego.objects.filter(tipo_riego=riego).exclude(for_general=False).order_by('pk')
        fases=agr_fase.objects.all().exclude(fas_nombre='No aplica')
        vGenerales=[]
        for i in vectorDatos:
            vGenerales.append(i)
        for fas in fases:
            #print(fas.fas_nombre, 'ETAPA-------------------------------***************')
            j=0
            rptaV=[]
            del vectorDatos
            vectorDatos=[]
            for i in vGenerales:
                vectorDatos.append(i)
            # vectorDatos=vGenerales
            for row in hoja:
                banderaFase=False
                medida=r_unidad_medida.objects.get(umed_abreviatura='dias')
                dato=agr_parametro.objects.filter(par_fase=fas).exclude(par_umedida=medida)
                datos=[]
                try:
                    tipos=row.for_tipos.split(',')
                    for i in tipos:
                        if(i=='fase'):
                            # #print(i)
                            datos.append(dato[0].par_abreviatura)
                            banderaFase=True
                        else:
                            datos.append(str(i))
                except Exception as e:
                    #print('eError',e)
                    if(i=='fase'):
                        # #print(i)
                        datos.append(dato[0].par_abreviatura)
                        banderaFase=True
                    else:
                        datos.append(row.for_tipos)
                medida=r_unidad_medida.objects.get(umed_abreviatura='dias')
                dato2=agr_parametro.objects.filter(par_umedida=medida).get(par_fase=fas)
                for ik in vectorDatos:
                    if(ik['tipo']==dato2.par_abreviatura):
                        valor=ik['valor']
                rptaV.append(row.for_nombre)
                valores=[]
                for i in vectorDatos:
                    for a in datos:
                        if (recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])) == i['tipo']:
                            valores.append({(recortarTipo((a, a.split('9'))[a.endswith('9')],(False, True)[a.endswith('9')])):i['valor']})
                variableTemporar=row.for_formula.replace('fase',dato[0].par_abreviatura)
                #print(variableTemporar)
                f=Expression(variableTemporar,datos)
                if (len(datos)==1):
                    rpta = caso1(f, valores, datos[0] )
                    vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                    #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                if (len(datos)==2):
                    rpta = caso2(f, valores, datos[0], datos[1])
                    vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                    # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                if (len(datos)==3):
                    rpta = caso3(f, valores, datos[0], datos[1], datos[2] )
                    vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                    # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                if (len(datos)==4):
                    rpta = caso4(f, valores, datos[0], datos[1], datos[2], datos[3])
                    vectorDatos.append({'tipo':rptaV[j], 'valor':rpta})
                    # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                if (len(datos)==5):
                    rpta = caso5(f, valores, datos[0], datos[1], datos[2], datos[3], datos[4])
                    # resultados.append({'tipo':rptaV[j], 'valor':round(rpta)})
                    # #print(rpta,' - ',rptaV[j],'Esta es:-----------------')
                j=j+1
                # #print('-')
                # #print(vectorDatos)
                # #print('-')
    except ZeroDivisionError as cero:
        #print('Hubo una division para cero, por favor ajuste los datos ',cero)
        resultados.append({'tipo':'error', 'valor':'No se puede dividir para cero'})
        return resultados
    except ValueError as e:
        #print ("Error de conversion ",e)
        resultados.append({'tipo':'error', 'valor':'ZeroDivisionError'})
        return resultados
    except TypeError as t:
        #print ("No coincide los tipos de datos... Verifique los datos ingresados ",t)
        resultados.append({'tipo':'error', 'valor':'No coincide los tipos de datos... Verifique los datos ingresados'})
        return resultados
    except Exception as e:
        #print('Hubo un exceptio de tipo: ',e)
        resultados.append({'tipo':'error', 'valor':e.message})
        return resultados
    resultados.append({'tipo': 'ok', 'valor': 'El proceso de calculo se efectuo con Exito'})
    return resultados

def caso1(f, valores ,valor):
    rpta=f(obtener(valores,(valor, valor.split('9'))[valor.endswith('9')],(False, True)[valor.endswith('9')]))
    return rpta
def caso2(f, valores ,valor1, valor2):
    rpta=f(obtener(valores,(valor1, valor1.split('9'))[valor1.endswith('9')],(False, True)[valor1.endswith('9')]),
            obtener(valores,(valor2, valor2.split('9'))[valor2.endswith('9')],(False, True)[valor2.endswith('9')]))
    return rpta
def caso3(f, valores ,valor1, valor2, valor3):
    rpta=f(obtener(valores,(valor1, valor1.split('9'))[valor1.endswith('9')],(False, True)[valor1.endswith('9')]),
           obtener(valores,(valor2, valor2.split('9'))[valor2.endswith('9')],(False, True)[valor2.endswith('9')]),
           obtener(valores,(valor3, valor3.split('9'))[valor3.endswith('9')],(False, True)[valor3.endswith('9')]))
    return rpta
def caso4(f, valores ,valor1, valor2, valor3, valor4):
    rpta=f(obtener(valores,(valor1, valor1.split('9'))[valor1.endswith('9')],(False, True)[valor1.endswith('9')]),
           obtener(valores,(valor2, valor2.split('9'))[valor2.endswith('9')],(False, True)[valor2.endswith('9')]),
           obtener(valores,(valor3, valor3.split('9'))[valor3.endswith('9')],(False, True)[valor3.endswith('9')]),
           obtener(valores,(valor4, valor4.split('9'))[valor4.endswith('9')],(False, True)[valor4.endswith('9')]))
    return rpta
def caso5(f, valores ,valor1, valor2, valor3, valor4, valor5):
    rpta=f(obtener(valores,(valor1, valor1.split('9'))[valor1.endswith('9')],(False, True)[valor1.endswith('9')]),
           obtener(valores,(valor2, valor2.split('9'))[valor2.endswith('9')],(False, True)[valor2.endswith('9')]),
           obtener(valores,(valor3, valor3.split('9'))[valor3.endswith('9')],(False, True)[valor3.endswith('9')]),
           obtener(valores,(valor4, valor4.split('9'))[valor4.endswith('9')],(False, True)[valor4.endswith('9')]),
           obtener(valores,(valor5, valor5.split('9'))[valor5.endswith('9')],(False, True)[valor5.endswith('9')]))
    return rpta
def guardarFase(fase, tipo, cultivo, valor):
    try:
        guardarFase=agr_cultivo_parametro()
        guardarFase.fase=fase
        tip=agr_parametro.objects.get(par_abreviatura=tipo)
        guardarFase.tipo=tip
        cultivo=agr_cultivo.objects.get(cult_id=cultivo)
        guardarFase.cultivo=cultivo
        guardarFase.valor=valor
        guardarFase.save()
    except Exception as e:
        print('error salvaje',e)