from audioop import max

from django.db import models
from app_empresa.models import proy_subarea, proy_empresa

# Create your models here.
#\django\contrib\auth\migrations  --> Carpeta de migraciones
class agr_especie_categoria(models.Model):
    cat_id=models.AutoField(primary_key=True,verbose_name="ID")
    cat_nombre= models.CharField("Nombre",max_length=100)
    cat_observacion= models.CharField("Observacion",max_length=100)
    cat_estado= models.BooleanField("Estado")

    class Meta:
        default_permissions = ()
        db_table = "agr_especie_categoria"
        verbose_name = 'Especie Categoria'
        verbose_name_plural = 'Especies Categorias'
        permissions = (
            ('add_agr_especie_categoria', "Puede agregar Especie Categoria"),
            ('change_agr_especie_categoria', "Puede editar Especie Categoria"),
            ('list_agr_especie_categoria', "Puede listar Especie Categoria"),
        )

class agr_especie_cultivo(models.Model):
    esp_id = models.AutoField("ID",primary_key=True)
    esp_nombre = models.CharField("Nombre",max_length=100)
    esp_imagen = models.ImageField("Imagen",upload_to='img_especies', blank=True, null=False)
    esp_estado = models.BooleanField("Estado",default=True)
    esp_categoria=models.ForeignKey(agr_especie_categoria,verbose_name="Categoria")

    class Meta:
        default_permissions = ()
        db_table = "agr_especie"
        verbose_name = 'Especie Cultivo'
        verbose_name_plural = 'Especies Cultivos'
        permissions = (
            ('add_agr_especie_cultivo', "Puede agregar Especie Cultivo"),
            ('change_agr_especie_cultivo', "Puede editar Especie Cultivo"),
            ('list_agr_especie_cultivo', "Puede listar Especie Cultivo"),
        )

class r_unidad_medida(models.Model):
    umed_id=models.AutoField("ID",primary_key=True)
    umed_nombre = models.CharField('Nombre',max_length=100, blank=True,null=True)
    umed_abreviatura = models.CharField('Abreviatura', max_length=100, default='')

    class Meta:
        default_permissions = ()
        db_table = "e_unidad_medida"
        verbose_name = 'Unidades de Medidas'
        verbose_name_plural = 'Unidades de Medidas'
        permissions = (
            ('add_r_unidad_medida', "Puede agregar Unidad Medida"),
            ('change_r_unidad_medida', "Puede editar Unidad Medida"),
            ('delete_r_unidad_medida', "Puede eliminar Unidad Medida"),
        )

    def __unicode__(self):
        return self.umed_abreviatura

class agr_grupo(models.Model):
    gru_id=models.AutoField("ID",primary_key=True)
    gru_nombre=models.CharField("Nombre",max_length=100)

    class Meta:
        default_permissions = ()
        db_table = "agr_grupo"
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'

    def __unicode__(self):
        return self.gru_nombre


class agr_tipo_parametro(models.Model):
    tpar_id=models.AutoField("ID",primary_key=True)
    tpar_nombre = models.CharField("Nombre",max_length=100)
    tpar_abreviatura = models.CharField("Abreviatura",max_length=100)
    tpar_descripcion = models.CharField("Descripcion",max_length=1000)
    tpar_grupo=models.ForeignKey(agr_grupo,verbose_name="Grupo")
    # tpar_tipo=models.BooleanField(default=True)
    # umed_id = models.ForeignKey(r_unidad_medida)

    class Meta:
        default_permissions=()
        db_table = "agr_tipo_parametro"
        verbose_name = 'Tipo de Parametro'
        verbose_name_plural = 'Tipos de Parametros'
        permissions = (
            ('add_agr_tipo_parametro', "Puede agregar Tipo de Parametro"),
            ('change_agr_tipo_parametro', "Puede editar Tipo de Parametro"),
            ('delete_agr_tipo_parametro', "Puede eliminar Tipo de Parametro"),
            ('list_agr_tipo_parametro', "Puede listar Tipo de Parametro"),
        )

    def __unicode__(self):
        return self.tpar_nombre

class agr_fase(models.Model):
    fas_id=models.AutoField("ID",primary_key=True)
    fas_nombre=models.CharField("Nombre",max_length=100)
    fas_descripcion=models.CharField("Descripcion",max_length=100)

    class Meta:
        default_permissions=()
        db_table = "agr_fase"
        verbose_name = 'Fase'
        verbose_name_plural = 'Fases'
        permissions = (
            ('add_agr_fase', "Puede agregar Fase"),
            ('change_agr_fase', "Puede editar Fase"),
            ('delete_agr_fase', "Puede eliminar Fase"),
            ('list_agr_fase', "Puede listar Fase"),
        )
    def __unicode__(self):
        return self.fas_nombre

class agr_parametro(models.Model):
    par_id=models.AutoField("ID",primary_key=True)
    par_nombre = models.CharField("Nombre",max_length=100)
    par_descripcion = models.CharField("Descripcion",max_length=1000)
    par_abreviatura = models.CharField("Abreviatura",max_length=10)
    par_valor_minimo = models.FloatField("Valor Minimo")
    par_valor_maximo = models.FloatField("Valor Maximo")
    par_valor = models.FloatField("Valor")
    par_estado=models.BooleanField("Estado")
    par_umedida=models.ForeignKey(r_unidad_medida,verbose_name="Unidad de Medida")
    par_fase=models.ForeignKey(agr_fase,verbose_name="Fase")
    par_tpar=models.ForeignKey(agr_tipo_parametro,verbose_name="Tipo de Parametro")

    class Meta:
        default_permissions = ()
        db_table = "agr_parametro"
        verbose_name = 'Parametro'
        verbose_name_plural = 'Parametros'
        permissions = (
            ('add_agr_parametro', "Puede agregar Parametro"),
            ('change_agr_parametro', "Puede editar Parametro"),
            ('delete_agr_parametro', "Puede eliminar Parametro"),
            ('list_agr_parametro', "Puede listar Parametro"),
        )

    def __unicode__(self):
        return self.par_nombre

class agr_tipo_riego(models.Model):
    rie_id=models.AutoField("ID",primary_key=True)
    rie_nombre= models.CharField("Nombre",max_length=100)
    rie_descripcion = models.CharField("Descripcion",max_length=1000)
    # rie_formulas=models.FileField(upload_to='archivo_formulas', blank=True, null=True)

    class Meta:
        default_permissions = ()
        db_table =  "agr_tipo_riego"
        verbose_name = 'Tipo de Riego'
        verbose_name_plural = 'Tipos de Riesgos'
        permissions = (
            ('add_agr_tipo_riego', "Puede agregar Tipo Riego"),
            ('change_agr_tipo_riego', "Puede editar Tipo Riego"),
            ('delete_agr_tipo_riego', "Puede eliminar Tipo Riego"),
            ('list_agr_tipo_riego', "Puede listar Tipo Riego"),
        )

    def __unicode__(self):
        return self.rie_nombre;

class agr_especie_parametros(models.Model):
    valor=models.FloatField("Valor")
    tipo_id=models.ForeignKey(agr_parametro,verbose_name="Id Tipo de Parametro")
    esp_id=models.ForeignKey(agr_especie_cultivo,verbose_name="ID Especie")

    class Meta:
        default_permissions=()
        db_table = "agr_esp_parametro"
        verbose_name = 'Especie de Parametro'
        verbose_name_plural = 'Especies de Parametros'
        permissions = (
            ('add_agr_especie_parametros', "Puede agregar Parametros de Especie"),
            ('change_agr_especie_parametros', "Puede editar Parametros de Especie"),
            ('delete_agr_especie_parametros', "Puede eliminar Parametros de Especie"),
            ('list_agr_especie_parametros', "Puede listar Parametros de Especie"),
        )

    def __unicode__(self):
        return self.esp_id.esp_nombre


class agr_tipo_suelo(models.Model):
    tsue_id=models.AutoField("ID",primary_key=True)
    tsue_nombre=models.CharField("Nombre",max_length=100)
    tsue_imagen = models.ImageField("Imagen",upload_to='img_suelo', blank=True, null=True, default='img_suelo/sin_imagen.gif')
    tsue_estado=models.BooleanField("Estado",default=True)

    def __unicode__(self):
        return unicode(self.tsue_nombre)

    class Meta:
        default_permissions=()
        db_table = "agr_tipo_suelo"
        verbose_name = 'Tipo de suelo'
        verbose_name_plural = 'Tipos de suelos'
        permissions = (
            ('add_agr_tipo_suelo', "Puede agregar Tipos de Suelo"),
            ('change_agr_tipo_suelo', "Puede editar Tipos de Suelo"),
            ('delete_agr_tipo_suelo', "Puede eliminar Tipos de Suelo"),
            ('list_agr_tipo_suelo', "Puede listar Tipos de Suelo"),
        )



class agr_parcela(models.Model):
    par_id = models.OneToOneField(proy_subarea, primary_key=True,verbose_name="Id")
    par_area_neta = models.FloatField("Area Neta")
    par_area_bruta = models.FloatField("Area Bruta")
    par_hectareas = models.FloatField("Hectareas")
    par_disponibles = models.FloatField("Disponibles")
    par_estado = models.BooleanField("Estado",default=True)
    par_suelo=models.ForeignKey(agr_tipo_suelo,verbose_name="Suelo")

    class Meta:
        default_permissions = ()
        db_table = "agr_parcela"
        verbose_name = 'Parcela'
        verbose_name_plural = 'Parcelas'
        permissions = (
            ('add_agr_parcela', "Puede agregar Parcela"),
            ('change_agr_parcela', "Puede editar Parcela"),
            ('delete_agr_parcela', "Puede eliminar Parcela"),
            ('list_agr_parcela', "Puede listar Parcela"),
        )

    def __unicode__(self):
        return self.par_id.sar_nombre

class agr_parametros_parcela(models.Model):
    par_tipo = models.CharField("Tipo",max_length=10)
    par_valor = models.FloatField("Valor")
    parcela = models.ForeignKey(agr_parcela,verbose_name="Parcela")

    class Meta:
        default_permissions = ()
        db_table = "agr_parametros_parcela"
        verbose_name = 'Parametros de parcela'
        verbose_name_plural = 'Parametros de parcelas'
        permissions = (
            ('add_agr_parametros_parcela', "Puede agregar Parametros de Parcela"),
            ('change_agr_parametros_parcela', "Puede editar Parametros de Parcela"),
            ('delete_agr_parametros_parcela', "Puede eliminar Parametros de Parcela"),
        )

class agr_cultivo(models.Model):
    cult_id=models.AutoField("ID",primary_key=True)
    cult_detalle = models.CharField("Detalle",max_length=50)
    cult_descripcion = models.CharField("Descripcion",max_length=500)
    cult_fecha = models.CharField("Fecha",max_length=100)
    cult_fecha_fin = models.CharField("Fecha Fin",max_length=100)
    cult_num_hectareas = models.FloatField("Numero de hectareas")
    cult_ubicacion = models.CharField("Ubicacion",max_length=100)
    cult_estado=models.BooleanField("Estado",default=True)
    esp_id = models.ForeignKey(agr_especie_cultivo,verbose_name="ID Especie")
    parcela = models.ForeignKey(agr_parcela,verbose_name="Parcela")
    riego=models.ForeignKey(agr_tipo_riego,verbose_name="Riego")

    class Meta:
        default_permissions = ()
        db_table = "agr_cultivo"
        verbose_name = 'Cultivo'
        verbose_name_plural = 'Cultivos'
        permissions = (
            ('add_agr_cultivo', "Puede agregar Cultivo"),
            ('change_agr_cultivo', "Puede editar Cultivo"),
            ('list_agr_cultivo', "Puede listar Cultivo"),
            ('print_agr_cultivo', "Puede imprimir Cultivo"),
        )

    def __unicode__(self):
        return self.cult_detalle
class agr_modelo_cultivo(models.Model):
    mcult_id=models.AutoField("ID",primary_key=True)
    mcult_cultivo_referencia=models.ForeignKey(agr_cultivo,verbose_name="Referencia Cultivo")
    mcult_nombre=models.CharField("Nombre",max_length=100)

class agr_cultivo_imagenes(models.Model):
    cim_id=models.AutoField("ID",primary_key=True)
    cim_imagen = models.ImageField("Imagen",upload_to='img_cultivos', blank=True, null=False)
    cim_principal = models.BooleanField("Principal",default=False)
    cult_id = models.ForeignKey(agr_cultivo,verbose_name="Id cultivo")

    class Meta:
        default_permissions=()
        db_table = "agr_cultivo_imagenes"
        verbose_name = 'Cultivo de imagen'
        verbose_name_plural = 'Cultivos de Imagenes'

    def __unicode__(self):
        return self.cult_id.cult_descripcion

class agr_zona_sensor_actuador(models.Model):
    zsa_id = models.AutoField("ID",primary_key=True)
    zsa_nombre = models.CharField("Nombre",max_length=100)
    zsa_descripcion = models.CharField("Descripcion",max_length=500)
    zsa_localizacion = models.CharField("Localizacion",max_length=100)
    zsa_estado = models.BooleanField("Estado",default=True)
    zsa_imagen = models.ImageField("Imagen",upload_to='img_zona',null=True,blank=True,default='img_zona/dashboard.png')
    zsa_nec_act = models.BooleanField(default=False)
    sar_id = models.ForeignKey(proy_subarea,verbose_name="Id Subarea")

    class Meta:
        default_permissions=()
        db_table = "iot_zona_sensor_actuador"
        verbose_name = 'Zona de Sensor Actuador'
        verbose_name_plural = 'Zonas de Sensores Actuadores'
        permissions = (
            ('add_agr_zona_sensor_actuador', "Puede agregar Zona de Sensores y Actuadores"),
            ('change_agr_zona_sensor_actuador', "Puede editar Zona de Sensores y Actuadores"),
            ('list_agr_zona_sensor_actuador', "Puede listar Zona de Sensores y Actuadores"),
        )

    def __unicode__(self):
        return self.zsa_nombre

class agr_monitoreo(models.Model):
    zsa_id = models.OneToOneField(agr_zona_sensor_actuador, primary_key=True,verbose_name="Id")
    mon_panel=models.TextField("Panel Monitoreo",default='no')

    class Meta:
        default_permissions = ()
        db_table = "iot_monitoreo"
        verbose_name = 'Monitoreo'
        verbose_name_plural = 'Monitoreos'
        permissions = (
            ('add_agr_monitoreo', "Puede agregar Monitoreo"),
            ('change_agr_monitoreo', "Puede editar Monitoreo"),
            ('list_agr_monitoreo', "Puede listar Monitoreo"),
        )

    def __unicode__(self):
        return self.zsa_id.zsa_nombre

#utilizado para Hargravens
class agr_latitud(models.Model):
    lat_valor = models.IntegerField("Latitud",default=0)

    class Meta:
        default_permissions = ()
        db_table = "agr_latitud"
        verbose_name = 'Latitud'
        verbose_name_plural = 'Latitudes'
        permissions = (
            ('add_agr_latitud', "Puede agregar Latitud"),
            ('change_agr_latitud', "Puede editar Latitud"),
            ('delete_agr_latitud', "Puede eliminar Latitud"),
            ('list_agr_latitud', "Puede listar Latitud"),
        )

    def __unicode__(self):
        return str(self.lat_valor)


#utilizado para Hargravens
class agr_mes(models.Model):
    mes_nombre = models.CharField("Nombre",max_length=10)

    class Meta:
        default_permissions=()
        db_table = "agr_mes"
        verbose_name = 'Mes'
        verbose_name_plural = 'Meses'

    def __unicode__(self):
        return self.mes_nombre

#utilizado para Hargravens
class agr_lat_mes(models.Model):
    latitud = models.ForeignKey(agr_latitud,verbose_name="Latitud")
    mes = models.ForeignKey(agr_mes,verbose_name="Mes")
    valor = models.FloatField("Valor")
    lat_hemisferio = models.CharField("Latitud Hemisferio",max_length=1)

    class Meta:
        default_permissions=()
        db_table = "agr_lat_mes"
        verbose_name = 'Latitud Mes'
        verbose_name_plural = 'Latitudes Meses'

    def __unicode__(self):
        return str(self.latitud.lat_valor)+'-'+self.mes.mes_nombre+'-'+str(self.valor)+'-'+self.lat_hemisferio

class agr_tipo_suelo_parametros(models.Model):
    tipo_suelo=models.ForeignKey(agr_tipo_suelo,verbose_name="Tipo suelo")
    parametros=models.ForeignKey(agr_parametro,verbose_name="Parametros")
    valor=models.FloatField("Valor")

    class Meta:
        default_permissions = ()
        db_table = "agr_suelo_parametro"
        verbose_name = 'Tipo de Suelo Parametro'
        verbose_name_plural = 'Tipos de suelos parametros'
        permissions = (
            ('change_agr_tipo_suelo_valores', "Puede agregar Valores de Tipo Suelo"),
        )

    def __unicode__(self):
        return self.tipo_suelo.tsue_nombre+self.parametros.par_abreviatura+str(self.valor)

#tablas para el metodo de Penman monteith-Horas luz

#utilizado para Penman
class agr_latitud_horas_luz(models.Model):
    lat_valor = models.IntegerField("Latitud Valor",default=0)

    class Meta:
        default_permissions=(['add','change','delete','list'])
        verbose_name = 'Latitud hora luz'
        verbose_name_plural = 'Latitudes hora luZ'

    def __unicode__(self):
        return str(self.lat_valor)


#utilizado para Penman
class agr_mes_horas_luz(models.Model):
    mes_nombre = models.CharField("Nombre Mes",max_length=10)

    class Meta:
        default_permissions=()
        verbose_name = 'Mes hora luz'
        verbose_name_plural = 'Meses horas luz'

    def __unicode__(self):
        return self.mes_nombre

#utilizado para Penman
class agr_lat_mes_horas_luz(models.Model):
    latitud = models.ForeignKey(agr_latitud_horas_luz,verbose_name="Latitud")
    mes = models.ForeignKey(agr_mes_horas_luz,verbose_name="Mes")
    valor = models.FloatField("Valor")
    lat_hemisferio = models.CharField("Latitud Hemisferio",max_length=1)

    class Meta:
        default_permissions=()
        verbose_name = 'Latitud mes hora'
        verbose_name_plural = 'Latitudes meses horas'

    def __unicode__(self):
        return str(self.latitud.lat_valor)+'-'+self.mes.mes_nombre+'-'+str(self.valor)+'-'+self.lat_hemisferio






#tablas para el metodo de Penman monteith-radiacion

#utilizado para Penman
class agr_latitud_radiacion(models.Model):
    lat_valor = models.IntegerField("Latitud Valor",default=0)

    class Meta:
        default_permissions=(['add','change','delete','list'])
        verbose_name = 'Latitud Radiacion'
        verbose_name_plural = 'Latitudes Radiaciones'

    def __unicode__(self):
        return str(self.lat_valor)

#utilizado para Penman
class agr_mes_radiacion(models.Model):
    mes_nombre = models.CharField("Mes",max_length=10)

    class Meta:
        default_permissions=()
        verbose_name = 'Mes radiacion'
        verbose_name_plural = 'Meses Radiaciones'

    def __unicode__(self):
        return self.mes_nombre

#utilizado para Penman
class agr_lat_mes_radiacion(models.Model):
    latitud = models.ForeignKey(agr_latitud_radiacion,verbose_name="Latitud")
    mes = models.ForeignKey(agr_mes_radiacion,verbose_name="Mes")
    valor = models.FloatField("Valor")
    lat_hemisferio = models.CharField("Latitud Hemisferio",max_length=1)

    class Meta:
        default_permissions=()
        verbose_name = 'Latitud Mes Rsdiacion'
        verbose_name_plural = 'Latitudes Mes Radiacion'

    def __unicode__(self):
        return str(self.latitud.lat_valor)+'-'+self.mes.mes_nombre+'-'+str(self.valor)+'-'+self.lat_hemisferio

class agr_evotranspiracion(models.Model):
    evo_id = models.AutoField("Id",primary_key=True)
    mes = models.CharField("Mes",max_length=100)
    par_id = models.ForeignKey(agr_parcela,verbose_name="ID Parcela")
    tipo = models.CharField("Tipo",max_length=100,default="Hargravens")
    class Meta:
        default_permissions=()
        db_table = "agr_evotranspiracion"
        verbose_name = 'Evotranspiracion'
        verbose_name_plural = 'Evotranspiraciones'
        permissions = (
            ('add_agr_evotranspiracion', "Puede agregar Evotranspiracion"),
            ('change_agr_evotranspiracion', "Puede editar Evotranspiracion"),
            ('delete_agr_evotranspiracion', "Puede eliminar Evotranspiracion"),
            ('list_agr_evotranspiracion', "Puede listar Evotranspiracion"),
        )

    def __unicode__(self):
        return self.mes+'-'+self.par_id.par_id.sar_nombre+'-'+self.tipo

class evo_tipo_valor(models.Model):
    tip_id = models.AutoField("Id Tipo",primary_key=True)
    valor = models.FloatField("Valor")
    evo_id = models.ForeignKey(agr_evotranspiracion,verbose_name="Id Evotranspiracion")
    tipo = models.CharField("Tipo",max_length=50)

    class Meta:
        default_permissions=()
        db_table = "agr_evo_tipo_valor"
        verbose_name = 'Evotranspiracion Tipo Valor'
        verbose_name_plural = 'Evotranspiraciones Tipo Valor'

    def __unicode__(self):
        return self.evo_id.mes+'-'+self.tipo

class agr_cultivo_parametro(models.Model):
    cultivo=models.ForeignKey(agr_cultivo,verbose_name="Cultivo")
    tipo=models.ForeignKey(agr_parametro,verbose_name="Tipo")
    fase=models.ForeignKey(agr_fase, null=True,verbose_name="Fase")
    valor = models.FloatField("Valor")

    class Meta:
        default_permissions=()
        db_table = "agr_cultivo_parametro"
        verbose_name = 'Cultivo Parametro'
        verbose_name_plural = 'Cultivos Parametros'

    def __unicode__(self):
        return self.tipo.par_abreviatura+(str(self.valor))

class agr_notas_cultivo(models.Model):
    not_id = models.AutoField("Id notas",primary_key=True)
    not_cultivo=models.ForeignKey(agr_cultivo,verbose_name="Cultivo")
    not_fecha=models.CharField("Fecha",max_length=100)
    not_nota=models.CharField("Nota",max_length=300)

    class Meta:
        default_permissions=()
        db_table = "agr_notas_cultivo"
        verbose_name = 'Nota Cultivo'
        verbose_name_plural = 'Notas Cultivos'
        permissions = (
            ('add_agr_notas_cultivo', "Puede agregar Notas de Cultivo"),
            ('list_agr_notas_cultivo', "Puede listar Notas de Cultivo"),
        )

class agr_formulas_riego(models.Model):
    for_id=models.AutoField("Id",primary_key=True)
    for_nombre=models.CharField("Nombre",max_length=100)
    for_descripcion=models.CharField("Descripcion",max_length=100)
    for_formula=models.CharField("Formula",max_length=100)
    for_tipos=models.CharField("Tipos",max_length=200)
    for_estado= models.BooleanField("Estado",default=True)
    for_general=models.BooleanField("General",default=False)
    for_formato=models.CharField("Formato",max_length=10, default='ninguno')
    for_dato=models.CharField("Dato",max_length=10, default='')
    for_operador=models.CharField("Operador",max_length=5, default='')
    u_medida=models.ForeignKey(r_unidad_medida,verbose_name="Unidad de medida")
    tipo_riego=models.ForeignKey(agr_tipo_riego,verbose_name="Tipo de Riego")

    class Meta:
        default_permissions = ()
        db_table = "agr_formulas_riego"
        verbose_name = 'Formula Riego'
        verbose_name_plural = 'Formulas Riegos'
        permissions = (
            ('add_agr_formulas_riego', "Puede agregar Formulas de Riego"),
            ('change_agr_formulas_riego', "Puede editar Formulas de Riego"),
            ('delete_agr_formulas_riego', "Puede eliminar Formulas de Riego"),
            ('list_agr_formulas_riego', "Puede listar Formulas de Riego"),
        )

    def __unicode__(self):
        return self.for_formula

class agr_cosecha(models.Model):
    cos_id=models.AutoField("Id",primary_key=True)
    cos_fecha=models.DateTimeField("Fecha",auto_now=True)
    cos_cantidad_cosechada=models.FloatField("Cantidad Cosechada")
    cos_observacion = models.CharField("Observacion",max_length=400)
    cos_precio_venta = models.FloatField("Precio de venta")
    cos_costo_produccion = models.FloatField("Costo de produccion")
    cos_cantidad_hect_cosechadas = models.FloatField("Cantidad Hectareas Cosechadas")
    medida_id=models.ForeignKey(r_unidad_medida,verbose_name="ID Medida")
    cult_id=models.ForeignKey(agr_cultivo,verbose_name="ID Cultivo")


    class Meta:
        default_permissions = ()
        db_table = "agr_cosecha"
        verbose_name = 'Cosecha'
        verbose_name_plural = 'Cosechas'

class agr_tipo_plaga_enfermedad(models.Model):
    tpe_id = models.AutoField("Id",primary_key=True)
    tpe_nombre = models.CharField("Nombre",max_length=100)

    class Meta:
            default_permissions = ()
            db_table = "agr_tipo_plaga_enfermedad"
            verbose_name = 'Tipo de plaga enfermedad'
            verbose_name_plural = 'Tipos de plagas enfermedades'
            permissions = (
                ("add_agr_tipo_plaga_enfermedad", "Puede agregar Plaga Enfermedad"),
                ("change_agr_tipo_plaga_enfermedad", "Puede editar Plaga Enfermedad"),
                ("list_agr_tipo_plaga_enfermedad", "Puede listar Plaga Enfermedad")
            )

    def __unicode__(self):
        return unicode(self.tpe_nombre)

class agr_plaga_enfermedad(models.Model):
    pe_id = models.AutoField("Id",primary_key=True)
    pe_fecha_identificacion = models.DateField("Fecha de identificacion")
    pe_cant_plantas_afectadas = models.CharField("Cantidad Plantas Afectadas",max_length=100)
    pe_cant_hectareas = models.FloatField("Cantidad de hectareas")
    pe_observaciones = models.TextField("Observaciones")
    pe_estado = models.BooleanField("Estado",default=True)
    tpe_id = models.ForeignKey(agr_tipo_plaga_enfermedad,verbose_name="Id tipo plaga enfermedad")
    cul_id = models.ForeignKey(agr_cultivo,verbose_name="Id cultivo")

    class Meta:
        default_permissions = ()
        db_table = "agr_plaga_enfermedad"
        verbose_name = 'Plaga Enfermedad'
        verbose_name_plural = 'Plagas Enfermedades'
        permissions = (
            ("add_agr_plaga_enfermedad", "Puede agregar Plaga Enfermedad"),
            ("change_agr_plaga_enfermedad", "Puede editar Plaga Enfermedad"),
            ("list_agr_plaga_enfermedad", "Puede listar Plaga Enfermedad")
        )

    def __unicode__(self):
        return unicode(self.pe_fecha_identificacion)

class agr_tipo_archivo(models.Model):
    tarc_id = models.AutoField("Id",primary_key=True)
    tarc_nombre = models.CharField("Nombre",max_length=100)
    tarc_icono = models.CharField("Icono",max_length=100)

    class Meta:
        default_permissions = ()
        verbose_name = 'Tipo de Archivo'
        verbose_name_plural = 'Tipos de Archivos'
    def __unicode__(self):
        return unicode(self.tarc_nombre)

class agr_archivo(models.Model):
    arc_id = models.AutoField("Id",primary_key=True)
    arc_nombre = models.CharField("Nombre",max_length=100)
    arc_estado = models.BooleanField("Estado",default=True)
    arc_uri = models.FileField("URI",upload_to='archivos')
    par_id = models.ForeignKey(agr_parcela,verbose_name="Id parcela")
    tarc_id = models.ForeignKey(agr_tipo_archivo,verbose_name="Id Tipo Archivo")

    class Meta:
        default_permissions = ()
        db_table = "agr_archivos"
        verbose_name = 'Archivo'
        verbose_name_plural = 'Archivos'
        permissions = (
            ("add_agr_archivo", "Puede agregar Archivos"),
            ("change_agr_archivo", "Puede editar Archivos"),
            ("list_agr_archivo", "Puede listar Archivos")
        )

    def __unicode__(self):
        return unicode(self.arc_nombre)
