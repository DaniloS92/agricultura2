# -*- coding: utf-8 -*-
__author__ = 'Dyuyay'
import json
from datetime import timedelta, date
import datetime
import time
import random

from django.utils import timezone

from socketIO_client import SocketIO, BaseNamespace
from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from django.contrib import messages
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from django.views.decorators.csrf import csrf_exempt
from agricultura.metodos import *

from app_empresa.models import proy_empresa
from agricultura.models import *
from iot.models import *
from app_eventos.models import *

# Monitoreo y control
@login_required(login_url='/')
def panel_monitoreo(request, id):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if request.user.has_perm('agricultura.list_agr_zona_sensor_actuador'):
            if agr_zona_sensor_actuador.objects.filter(zsa_id=id,zsa_estado=True).exists():
                empresa=request.user.id_persona.emp_id.emp_id            
                areas = proy_area.objects.filter(emp_id_id=empresa)
                subareas = proy_subarea.objects.filter(ar_id=areas)
                
                findZona = agr_zona_sensor_actuador.objects.get(zsa_id = id)
                vMonitoreo =  agr_monitoreo.objects.get(zsa_id__zsa_id=id)

                listZonas = agr_zona_sensor_actuador.objects.filter(sar_id = subareas).exclude(zsa_estado=False)

                if vMonitoreo.mon_panel=='no':
                    dic['id_zona'] = int(id)
                    dic['estado'] = 'vacio'
                    dic['datosPanel'] = ''
                    dic['nombreZona'] = findZona.zsa_nombre
                    dic['lisDashboards'] = listZonas
                else:
                    dic['id_zona'] = int(id)
                    dic['estado'] = 'guardado'
                    panel = vMonitoreo.mon_panel
                    dic['datosPanel'] = json.dumps(panel)
                    dic['nombreZona'] = findZona.zsa_nombre
                    dic['lisDashboards'] = listZonas
                return render_to_response('agricultura/monitoreo/monitoreo_y_control.html',dic, context_instance=RequestContext(request))
            else:
                return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        redirect('/')

@login_required(login_url='/')
def guardarPanel(request):
    try:
        datosPanel = request.POST.get('dato')
        id = request.POST.get('id')
        vPanel = agr_monitoreo.objects.get(zsa_id__zsa_id = id)
        vPanel.mon_panel = datosPanel
        vPanel.save()
        dict = {
            'result': 'OK'
        }
    except Exception as e:
        print (e)
    return JsonResponse(dict)


@csrf_exempt
def recetearDispositivos(request):
    
    print '*******************************RECETEO DISPOSITIVOS************************************'
    vecResp = []
    findDispositivos = iot_dispositivo.objects.all()

    for dispositivo in findDispositivos:
        dispositivo.dis_activo = False
        dispositivo.save()

    return HttpResponse(json.dumps(vecResp))

@csrf_exempt
def borrado_dispositivos_programado(request):

    print '****************************BORRADO DISPOSITIVOS*********************************'
    vecResp = []

    try:

        findDispositivos = iot_dispositivo.objects.filter(dis_activo=False)
        now = timezone.now()
        for dispositivo in findDispositivos:
            fecha_inicial =now - dispositivo.dis_fecha_ultima_lectura
            if(abs(fecha_inicial).days > 28):
                dispositivo.delete()
            else:
                print 'El dispositivo aun es estable'
    
    except Exception as e:
        print e

    return HttpResponse(json.dumps(vecResp))

@csrf_exempt
def cargarZonas(request):

    # print '*******************************LOAD ZONAS*******************************'
    vectorZonas= []
    try:
        mac = request.POST['mac']
        lec_interfaz = request.POST['interfaz']
        lec_valor = request.POST['valor']

        # print 'ESTA LLEGANDO MAC: ',mac

        dispositivo=iot_dispositivo.objects.get(dis_mac=mac)

        dispositivo.dis_activo = True
        dispositivo.dis_fecha_ultima_lectura = date.today()
        dispositivo.save()

        # print dispositivo

        findSensorAct=iot_sensor_actuador.objects.filter(dis_id=dispositivo.dis_id,sa_interfaz=lec_interfaz)
        
        if findSensorAct.exists():

            for sen_act in findSensorAct:
                sen_act.sa_ult_medida = lec_valor
                findZona = agr_zona_sensor_actuador.objects.get(zsa_id=sen_act.zsa_id.zsa_id)

                # findZonas = agr_zona_sensor_actuador.objects.filter(iot_id = sen_act.iot_id)
                # for zona in findZonas:
                #     vectorZonas.append(zona.zsa.id)
                    
                vectorZonas.append(findZona.zsa_id)
                sen_act.save()

        # print vectorZonas
    except Exception as e:
        print (e)
    return HttpResponse(json.dumps(vectorZonas))

@csrf_exempt
def ult_lecturas(request):

    print '*******************************LOAD ULTIMAS MEDIDAS************************************'
    vectorZonas= []
    try:
        id_zona = request.POST['id_zona']

        print 'ESTA LLEGANDO LA ZONA: ', id_zona

        findSensorAct=iot_sensor_actuador.objects.filter(zsa_id=id_zona)

        for sen_act in findSensorAct:
            ultValor = sen_act.sa_ult_medida
            vectorZonas.append(
                {
                    'interfaz' : sen_act.sa_interfaz,
                    'valor' : sen_act.sa_ult_medida,
                    'mac' : sen_act.dis_id.dis_mac,
                }
            )

        print vectorZonas
    except Exception as e:
        print (e)
    return HttpResponse(json.dumps(vectorZonas))

@csrf_exempt
@transaction.atomic()
def envio_accion_actuador(request):
    transaccion = transaction.savepoint()
    print '*******************************ENVIADO ACCION************************************'
    try:
        with transaction.atomic():
            id_actuador     = request.POST['idActuador']
            estado          = request.POST['estado']

            accion = 'accion'

            if (estado==0):
                accion = 'Apagar'
            else:
                accion = 'Encender'

            # buscando el actuador
            findSensorAct = iot_sensor_actuador.objects.get(iot_id = id_actuador)
            # creando la accion
            now = time.strftime("%H:%M:%S")
            # sumaMin = int(now.minute + 1)
            # hora_inicio = str(now.hour) +':'+str(sumaMin)+':'+str(now.second)
            # Buscando la empresa donde pertenece el actuador
            empresa_id = findSensorAct.dis_id.emp_id.emp_id
            modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id

            new_accion = MAccion(
                        acc_nombre = accion+' '+findSensorAct.sa_interfaz,
                        acc_fecha_inicio = date.today(),
                        acc_fecha_fin = date.today(),
                        acc_descripcion = 'Enviando : '+accion+' '+findSensorAct.sa_interfaz+' '+findSensorAct.ted_id.ted_name,
                        acc_estado = 'Activado',
                        acc_interval_ejec = '0',
                        acc_interval_espera = '0',
                        acc_num_intervalo = '0',
                        acc_hora_inicio = now,
                        acc_hora_fin = now,
                        acc_tipo = 'Inmediata',
                        mod_id=modelo_id
                        )
            new_accion.save()

            # creando la relacion m_accion_actuador
            new_accion_act = MAccionActuador(
                            acc_valor = estado,
                            acc = new_accion,
                            iot = findSensorAct
                            )
            new_accion_act.save()

            new_paquetes_ctrl = MPaqueteControl(
                            pc_paquete = 'Paquete de control',
                            pc_fecha = date.today(),
                            pc_hora = now,
                            pc_estado = 'Pendiente',
                            pc_mac = findSensorAct.dis_id.dis_mac,
                            pc_datos = '{}',
                            acc = new_accion)
            new_paquetes_ctrl.save()

            estructuraJson = json.loads('{"paquete_id":"","mac":"","interfaz":""}')
            estructuraJson["paquete_id"] = str(random.randint(1, 65459))
            estructuraJson["mac"] = new_paquetes_ctrl.pc_mac
            estructuraJson["interfaz"] = findSensorAct.sa_interfaz
            estructuraJson["valor"] = int(new_accion_act.acc_valor)

            new_paquetes_ctrl.pc_datos = json.dumps(estructuraJson)
            new_paquetes_ctrl.save()

            res = {'result':'Ok'}
        transaction.savepoint_commit(transaccion)
    except Exception, e:
        res = {'result':'Fail'}
        transaction.savepoint_rollback(transaccion)
        print e
        
    return HttpResponse(json.dumps(res))