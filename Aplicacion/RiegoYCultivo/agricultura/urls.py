__author__ = 'Dyuyay'

from django.conf.urls import patterns, url

from archivos.views import *
from especie.views import *
from configuracion.views import *
from parcelas.views import *
from plagas.views import *
from cultivo.views import *
from reportes.views import *
from monitoreo.views import *
from django.views.generic import RedirectView
from django.conf import settings
from views import *

# SECCION TIPO DE SUELO
from configuracion.views_suelo import *

urlpatterns = patterns('',
    url(r'^inicio/$', inicio, name="url_inicio"),
    #url-especies
    url(r'^especie/$', especie, name="url_especie"),
    url(r'^categoria_especie/$', categoria_especie, name="url_categoria_especie"),
    #url-especies-operaciones
    url(r'^categoria_especie_agregar/$', agregar_categoria, name="url_categoria_agregar"),
    url(r'^bajaCategoria/$', baja_categoria),
    url(r'^ver_datos_categoria/$', ver_datos_categoria),
    url(r'^modificar_datos_categoria/$', modificar_datos_categoria),
    url(r'^especie_agregar/$', agregar_especie, name="url_especie_agregar"),
    url(r'^especie_valores/$', valores_especie, name="url_especie_valores"),
    url(r'^bajaEspecie/$', baja_especie),
    url(r'^ver_datos_especie/$', ver_datos_especie),
    url(r'^modificar_datos_especie/$', modificar_datos_especie),
    url(r'^tipos_parametros_especie/$', tipos_parametros_especie),
    # url(r'^verTipos/$', verTipos),

    # url(r'^verParametros/$', verParametros),
    url(r'^parametros_agregar/$', agregar_parametros, name="url_parametros_agregar"),
    url(r'^bajaParametros/$', baja_parametros),
    url(r'^verParametros/(\d+)/$', ver_datos_parametros),
    # url(r'^ingresarNuevoTipoParametro/$', ingresar_datos_tipoParametros),

    url(r'^riego_guardar/$', guardar_riego, name="url_riego_guardar"),
    url(r'^riego_archivo/$', guardar_archivo_formulas, name="url_riego_archivo"),
    url(r'^riego_formulas/$', tipo_riego_formulas, name="url_riego_formulas"),
    url(r'^formula_agregar/$', agregar_formula, name="url_formula_agregar"),
    url(r'^eliminar_formula/$', eliminar_formula),
    url(r'^bajaFormula/$', baja_formula),
    url(r'^tiporiegoformula/$', verTipoRiegoFormula),
    url(r'^ver_datos_riego/$',ver_datos_riego),
    url(r'^modificar_datos_riego/$',modificar_datos_riego),
    url(r'^descargarExcel/(\w+)/$', descargarExcel),


    url(r'^parametros_cultivo/$', parametros_cultivo, name="url_parametros_cultivo"),
    url(r'^copiar_cultivo/$', copiar_cultivo, name="url_copiar_cultivo"),
    url(r'^crear_modelo/$', crear_modelo, name="url_crear_modelo"),

    # TIPOS DE PARAMETROS
    url(r'^parametros_tipo/$', tipos_parametros, name="url_parametros_tipo"),
    url(r'^obtenerTipo/(\d+)/$', ver_datos_tipo),
    url(r'^editarTipo/(\d+)/$', editar_tipo),
    url(r'^bajaTipo/$', baja_tipo),
    url(r'^tipo_agregar/$', tipo_agregar, name="url_tipo_agregar"),

    url(r'^tipo_riego/$', tipo_riego, name="url_tipo_riego"),
    url(r'^cultivo_agregar/$', cultivo_agregar, name="url_cultivo_agregar"),
    url(r'^cultivo_ver/$', cultivo_ver, name="url_cultivo_ver"),
    url(r'^cultivo_modelos/$', cultivo_modelos, name="url_cultivo_modelos"),
    url(r'^restaurar/$', default_config, name="url_restaurar"),

    url(r'^tiposSuelo/$', tipoSuelo, name="url_tipo_suelo"),
    url(r'^eliminar_suelo/$', eliminar_suelo),
    url(r'^tipo_suelo_agregar/$', agregar_tipo_suelo, name="url_tipo_suelo_agregar"),
    url(r'^bajaSuelo/$', baja_suelo),
    url(r'^verSuelos/(\d+)/$', ver_datos_suelo),
    url(r'^suelo_valores/$', valores_suelo, name="url_suelo_valores"),
    url(r'^tipos_parametros_suelo/$', tipos_parametros_suelo),



    url(r'^detalle_especie/$', obtenerEspecie),
    url(r'^detalle_parcela/$', obtenerParcela),
    url(r'^detalle_riego/$', obtenerRiego),
    url(r'^evotranspiracionInicial/$', confEt0, name="url_et0"),
    url(r'^datosParcela/$', detalleParcela),    #para metodo de hargravens
    url(r'^datosParcela_P/$', detalleParcela_P), #para metodo de Penmant
    url(r'^guardarDatoEvo/$', guardarDatoEvo),  #guardado para metodo de Hargravens
    url(r'^guardarDatoEvoPenman/$', guardarDatoEvoPenman),  #guardado para metodo de Penman Monteith
    url(r'^evotranspiracionInicialP/$', confEt0Penman, name="url_et0P"),

#   parcelas
    url(r'^parcelas/$', parcelas, name="url_parcelas"),
    url(r'^guardarParcela/$', guardarParcela, name="url_guardar_parcela"),
    url(r'^ver_datos_parcela/$', ver_datos_parcela, name="url_datos_parcela"),
    url(r'^modificar_datos_parcela/$', modificar_datos_parcela, name="url_modificar_datos_parcela"),
    url(r'^bajaParcela/$', dar_dbaja_parcela, name="url_dbaja_parcela"),

# zona sensor actuador
    url(r'^zona_riego/(?P<id>.*)/$', zona_riego),
    url(r'^guardarZona/$', guardarZona, name="url_zona_agregar"),
    url(r'^ver_datos_zona/$', ver_datos_zona, name="url_datos_zona"),
    url(r'^modificar_datos_zonas/$', modificar_datos_zonas, name="url_modificar_datos_zonas"),
    url(r'^bajaZona/$', dar_dbaja_zona, name="url_dbaja_zona"),
    url(r'^consulta_sen_act/$', consulta_sen_act),
    url(r'^actualizar_sen_act_zona/$', act_sen_act_zona),

#   archivos de parcelas
    url(r'^parcela/archivos/(?P<parcela_id>.*)/$', archivos, name="url_archivos_por_parcela"),
    url(r'^guardarArchivo/$',guardarArchivo, name="url_guardar_archivo"),
    url(r'^ver_datos_archivo/$', ver_datos_archivo, name="url_datos_archivo"),
    url(r'^modificar_datos_archivo/$', modificar_datos_archivo, name="url_modificar_datos_archivo"),
    url(r'^bajaArchivo/$', dar_dbaja_archivo, name="url_dbaja_archivo"),

#   plagas
    url(r'^guardar_plaga/$', guardar_plaga, name="url_cultivo_plaga"),
    url(r'^bajaPlaga/$', dar_dbaja_plaga, name="url_dbaja_plaga"),

#   cultivos
    url(r'^anadirCultivo/$', anadir_cultivo, name="url_cultivo_anadir"),
    url(r'^anadirCultivo_java/$', anadir_cultivo_java, name="url_cultivo_anadir_java"),
    # url(r'^detalleCultivo/$', detalle_cultivo, name="url_cultivo_detalle"),
    url(r'^detalle/(?P<id>.*)/$', detallado, name="url_cultivo_detalle"),
    url(r'^detalleModelo/(?P<id>.*)/$', detalleModelo, name="url_cultivo_detalle_modelo"),
    url(r'^cosechar/$', cosechar, name="url_cultivo_cosechar"),
    url(r'^agregar_nota/$', agregar_nota, name="url_cultivo_nota"),
    url(r'^eliminar_nota/$', eliminar_nota, name="url_cultivo_nota_e"),
    url(r'^tiene_parcela/$', tiene_parcela, name="url_cultivo_tiene_parcela"),
    url(r'^obtener_datos_suelo/$', obtener_datosSuelo, name="url_cultivo_datosSuelo"),
    url(r'^cambiar_imagen_cultivo/$', cambiarImange, name="url_cambiar_imagen_cultivo"),
    url(r'^editar_imagen_cultivo/$', cambiarImagenes, name="editar_imagen_cultivo"),
    url(r'^agregar_imagen_cultivo/$', agregarImagen, name="editar_imagen_cultivo"),

# Monitoreo y Control
    url(r'^monitoreo/(?P<id>.*)/$',panel_monitoreo, name="url_monitoreo_y_control"),
    url(r'^guardar/$',guardarPanel, name="url_prueba"),
    url(r'^recetearDispositivos$',recetearDispositivos),
    url(r'^consultar_zonas$',cargarZonas),
    url(r'^ultimasLecturas$',ult_lecturas),
    url(r'^envio_accion_actuador$',envio_accion_actuador),
    url(r'^borrado_dispositivos_programado$',borrado_dispositivos_programado),

# reportes a ver si sale :v
    #url(r'^reporte/$', ver_reporte, name="url_reporte"),#reportes
    url(r'^reportes_cultivo/$', reportes_cultivo, name="url_reportes_cultivo"),
    url(r'^formulario/(\d+)/$', frmReporteCultivo),
    url(r'^reporteC/datos/(\d+)/$', datos_Cultivo),

#cosechas
    url(r'^listar_cosechas/$',lista_cosecha),
    url(r'^guardar_cosecha/$',guarda_cosecha),
    url(r'^editar_cosecha/$',edita_cosecha),
    url(r'^consulta_cosecha/$',consulta_cosecha),
    url(r'^arbol/$', templateArbol),
)#+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
