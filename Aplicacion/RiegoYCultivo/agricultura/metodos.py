from django.contrib.auth.models import Permission, ContentType
from django.db.models import Q

from app_seguridad.models import *
from app_empresa.models import *
import openpyxl

def precargar_Permisos():
    for j in Permission.objects.all():
        for i in ContentType.objects.all():
            if j.content_type.model == i.model:
                if not Permission.objects.filter(content_type=i, codename__icontains='list').exists():
                    Permission.objects.create(codename='list_'+i.model,
                                              name='Can List '+i.model,
                                              content_type=i)


def getEmpresa(request):
    return request.user.id_persona.emp_id

def cargarMenus():
    documento = openpyxl.load_workbook('bd.xlsx')
    hoja = documento.get_sheet_by_name('menus')
    for row in range(2, hoja.get_highest_row() + 1):
        if hoja['A' + str(row)].value != None:
            estado_url = False
            if hoja['G' + str(row)].value == 'True':
                estado_url = True
            if hoja['F' + str(row)].value == None:
                if (hoja['J' + str(row)].value == None):
                    seg_menu.objects.create(
                        men_id=hoja['A' + str(row)].value,
                        men_nombre=hoja['B' + str(row)].value,
                        men_icono=hoja['C' + str(row)].value,
                        men_tipo=hoja['E' + str(row)].value,
                        men_ruta=hoja['D' + str(row)].value,
                        men_estado_url=estado_url,
                        men_plantilla=hoja['H' + str(row)].value,
                        men_id_active=hoja['I' + str(row)].value
                    )
                else:
                    seg_menu.objects.create(
                        men_id=hoja['A' + str(row)].value,
                        men_nombre=hoja['B' + str(row)].value,
                        men_icono=hoja['C' + str(row)].value,
                        men_ruta=hoja['D' + str(row)].value,
                        men_tipo=hoja['E' + str(row)].value,
                        men_plantilla=hoja['H' + str(row)].value,
                        men_id_active=hoja['I' + str(row)].value,
                        men_estado_url=estado_url,
                        permisos=Permission.objects.get(codename=hoja['J' + str(row)].value)
                    )
            else:
                if (hoja['J' + str(row)].value != None):
                    seg_menu.objects.create(
                        men_id=hoja['A' + str(row)].value,
                        men_nombre=hoja['B' + str(row)].value,
                        men_icono=hoja['C' + str(row)].value,
                        men_ruta=hoja['D' + str(row)].value,
                        men_tipo=hoja['E' + str(row)].value,
                        men_id_padre=seg_menu.objects.get(pk=hoja['F' + str(row)].value),
                        men_plantilla=hoja['H' + str(row)].value,
                        men_id_active=hoja['I' + str(row)].value,
                        men_estado_url=estado_url,
                        permisos=Permission.objects.get(codename=hoja['J' + str(row)].value)
                    )
                else:
                    seg_menu.objects.create(
                        men_id=hoja['A' + str(row)].value,
                        men_nombre=hoja['B' + str(row)].value,
                        men_icono=hoja['C' + str(row)].value,
                        men_ruta=hoja['D' + str(row)].value,
                        men_tipo=hoja['E' + str(row)].value,
                        men_id_padre=seg_menu.objects.get(pk=hoja['F' + str(row)].value),
                        men_plantilla=hoja['H' + str(row)].value,
                        men_id_active=hoja['I' + str(row)].value
                    )

def crearMenus():
    if seg_menu.objects.all().count() == 0:
        cargarMenus()

    if not proy_empresa.objects.filter(emp_ruc='012345678', emp_nombre_comercial='IOTMACH').exists():
        vEmpresa = proy_empresa.objects.create(emp_ruc="012345678",
                                               emp_nombre_comercial="IOTMACH",
                                               emp_nombre_juridico="IOTMACH GROUP RESEARCH",
                                               emp_grupo_empresa="Desarrolladora de Software",
                                               emp_direccion="Machala - El Oro",
                                               emp_email="iotmach@gmail.com",
                                               emp_web="iotmach.utmachala.edu.ec",
                                               emp_dominio_correo="@iotmach.com")

        rol = Group.objects.create(name='Administrador')
        for i in Permission.objects.all().exclude(
                                        Q(content_type__model='permission') | Q(content_type__model='contenttype') |
                                        Q(content_type__model='group') |  Q(content_type__model='logentry') |
                                        Q(content_type__model='session')| Q(content_type__model='seg_apps')):
            rol.permissions.add(i)

        persona=proy_persona.objects.create(per_nombres='Admin',
                                    per_apellidos='IOTMACH',
                                    per_direccion='----',
                                    per_telef_movil='000000',
                                    per_email='iotmach@gmail.com',
                                    per_telef_fijo='0000',
                                    emp_id=vEmpresa,
                                    )
        vUsuario = User.objects.get(username='iotmach',is_superuser=True)
        vUsuario.first_name = 'Admin'
        vUsuario.last_name = 'IOTMACH'
        vUsuario.id_persona= persona
        vUsuario.groups.add(rol)
        vUsuario.save()

        rol=Group.objects.create(name='Agricultor')
        for i in Permission.objects.filter(Q(content_type__app_label='agricultura') | Q(content_type__app_label='app_evento')):
            rol.permissions.add(i)

        rol=Group.objects.create(name='Tecnico')
        for i in Permission.objects.filter(Q(content_type__app_label='iot') | Q(content_type__app_label='app_evento')):
            rol.permissions.add(i)

        rol = Group.objects.create(name='Reportes')
        rol.permissions.add(Permission.objects.get(codename='print_agr_cultivo'))
        rol.permissions.add(Permission.objects.get(codename='print_iot_sensor_actuador'))


def acciones(accion, usuario):
    vLog = seg_log_acciones(la_accion=accion, usu_id=usuario)
    vLog.save()

def armaMenu(permisos):
    menu = []
    submenu = []
    items=[]
    list_menus=seg_menu.objects.all().order_by('men_id')
    for j in permisos:
        for i in list_menus:
            if i.permisos != None:
                perm = j.split('.')
                if i.men_estado:
                    if i.permisos.codename == perm[1]:
                        if(i.men_tipo=='item'):
                            items.append(i)
                            if(i.men_id_padre!=None and i.men_id_padre.men_tipo=='submenu'):
                                submenu.append(i.men_id_padre)
                                if(i.men_id_padre.men_id_padre!=None and i.men_id_padre.men_id_padre.men_tipo=='menu'):
                                    menu.append(i.men_id_padre.men_id_padre)
                            else:
                                if(i.men_id_padre!=None and i.men_id_padre.men_tipo=='menu'):
                                    menu.append(i.men_id_padre)
                        elif(i.men_tipo=='submenu'):
                            submenu.append(i)
                            if(i.men_id_padre!=None and i.men_id_padre.men_tipo=='menu'):
                                menu.append(i.men_id_padre)
                        else:
                            menu.append(i)

    items = list(set(items))
    menu = list(set(menu))
    submenu = list(set(submenu))
    return {'menu' : menu,'submenu': submenu,'items': items}


def obtenerPersona(request):
    return request.user.id_persona

def obtenerRoles(request):
    lstRoles=[]
    for i in request.user.groups.values_list():
        lstRoles.append(i[1])
    return lstRoles

def darPermisos(grupo):
    for i in Permission.objects.all().exclude(Q(content_type__model='permission')|
                                                      Q(content_type__model='contenttype')|
                                                      Q(content_type__model='logentry')|
                                                      Q(content_type__model='session')):
        grupo.permissions.add(i)