__author__ = 'Dyuyay'
import json

from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from django.contrib import messages
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from ..models import *
from agricultura.metodos import *


#LLAMADA A PLANTILLAS
@login_required(login_url='/')
def especie(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if request.user.has_perm('agricultura.add_agr_especie_cultivo') or request.user.has_perm('agricultura.change_agr_especie_cultivo') or request.user.has_perm('agricultura.list_agr_especie_cultivo'):
            dic['datos_especie']=agr_especie_cultivo.objects.all()
            dic['datos_categoria']=agr_especie_categoria.objects.all().filter(cat_estado=True)
            print(dic)
            return render_to_response('agricultura/especie/templateEspecie.html',dic, context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", context_instance = RequestContext(request))
    else:
        return redirect('/')
@login_required(login_url='/')
def categoria_especie(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.is_ajax():
            datos = agr_especie_categoria.objects.all().order_by("-cat_id").values('cat_id','cat_nombre')
            return HttpResponse(json.dumps({"data":list(datos)},cls=DjangoJSONEncoder),content_type = "application/json; charset=utf8") 
        if request.user.has_perm('agricultura.add_agr_especie_categoria') or request.user.has_perm('agricultura.change_agr_especie_categoria') or request.user.has_perm('agricultura.list_agr_especie_categoria'):
            dic['datos_categoria']=agr_especie_categoria.objects.all()
            return render_to_response('agricultura/especie/templateEspecieCat.html',dic, context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html",context_instance = RequestContext(request))
    else:
        return redirect('/')

#OPERACIONESf
@login_required(login_url='/')
def agregar_categoria(request):
    if request.method == "POST":
        msj={'codigo':0,'msj':'Se guardo'}
        try:
            nombre= request.POST["txt_cat_nombre"]
            observacion= request.POST["txt_cat_observacion"]
            estado = request.POST.getlist('check_cat_estado')
            if(estado):
                estado=True
            else:
                estado=False
            if request.is_ajax():
                guardarCategoria=agr_especie_categoria(cat_nombre=nombre, cat_observacion=observacion, cat_estado=True)
                guardarCategoria.save()
                return HttpResponse(json.dumps({"mensaje":"Guardado Correctamente", "id":guardarCategoria.cat_id}),content_type = "application/json; charset=utf8")
            else:
                guardarCategoria=agr_especie_categoria(cat_nombre=nombre, cat_observacion=observacion, cat_estado=estado)
                guardarCategoria.save()
                messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
        except Exception as e:
            msj['codigo']=1
            msj['msj']="Error inesperado"
            messages.add_message(request, messages.ERROR, e.message)
        return redirect('/agricultura/categoria_especie')

@login_required(login_url='/')
def baja_categoria(request):
    id = request.GET['id']
    estado = request.GET['estado']
    try:
        eliminar=agr_especie_categoria.objects.get(cat_id=id)
        eliminar.estado=estado
        if(estado=='true'):
            eliminar.cat_estado=estado
        else:
            eliminar.cat_estado=False
        eliminar.save()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def ver_datos_categoria(request):
    id = request.GET['id']
    data = []
    try:
        categoria = agr_especie_categoria.objects.get(cat_id=id)
        data.append({'id': categoria.cat_id, 'nombre': categoria.cat_nombre, 'observacion': categoria.cat_observacion,
                     'estado':categoria.cat_estado})
        return HttpResponse(json.dumps(data), content_type="application/json")
    except Exception as error:
        data = []
    return JsonResponse(data, safe=False)

@login_required(login_url='/')
def modificar_datos_categoria(request):
    id = request.GET['id']
    nombre = request.GET['txt_cat_nombre']
    observacion = request.GET['txt_cat_observacion']
    dic = []
    try:

        categoria = agr_especie_categoria.objects.get(cat_id=id)
        categoria.cat_nombre=nombre
        categoria.cat_observacion=observacion
        categoria.save()
        dic = {
            'result': 'OK'
        }
    except Exception as error:
        dic = {
            'result': 'Error al modificar'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type='application/json')


#OPERACIONES
@login_required(login_url='/')
def agregar_especie(request):
    if request.method == "POST":
        try:
            nombre= request.POST["txt_especie"]
            imagen = request.FILES['file1']
            estado = request.POST.getlist('check_estado')
            categoria = request.POST["select_categoria_especie"]
            if(estado):
                estado=True
            else:
                estado=False
            guardarEspecie=agr_especie_cultivo(esp_nombre=nombre, esp_imagen=imagen, esp_estado=estado, esp_categoria=agr_especie_categoria.objects.get(cat_id=categoria))
            guardarEspecie.save()
            messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
        except Exception as e:
            messages.add_message(request, messages.ERROR, e.message)
        return redirect('/agricultura/especie/')

@login_required(login_url='/')
def baja_especie(request):
    id = request.GET['id']
    estado = request.GET['estado']
    #print(estado)
    try:
        eliminar=agr_especie_cultivo.objects.get(esp_id=id)
        if(estado=='true'):
            eliminar.esp_estado=True
        else:
            eliminar.esp_estado=False
        eliminar.save()
        print('est',eliminar.esp_estado)
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def ver_datos_especie(request):
    id = request.GET['id']
    data = []
    try:
        especie_model = agr_especie_cultivo.objects.get(esp_id=id)
        data.append({'id': especie_model.esp_id, 'nombre': especie_model.esp_nombre,
                     'imagen': especie_model.esp_imagen.url,
                     'categoria_nombre': especie_model.esp_categoria.cat_nombre,
                     'categoria_id':especie_model.esp_categoria.cat_id})
        contCategorias=0
        cat=agr_especie_categoria.objects.all().filter(cat_estado=True).exclude(cat_id=especie_model.esp_categoria.cat_id)
        cont=cat.count()
        data.append({'contCategorias': cont})
        for s in cat:
            data.append({'categorias_id': s.cat_id,'categorias_nombre': s.cat_nombre})
        return JsonResponse(data, safe=False)
    except Exception as error:
        print(error)
        data = []
    return JsonResponse(data, safe=False)

@login_required(login_url='/')
def modificar_datos_especie(request):
    try:
        id= request.POST["id"]
        estado = request.POST.getlist('check_estado')
        categoria = request.POST["select_categoria_especie"]
        nombre= request.POST["txt_especie"]
        guardarEspecie=agr_especie_cultivo.objects.get(esp_id=id)
        try:
            imagen = request.FILES['file1']
            guardarEspecie.esp_imagen=imagen
        except:
            print('no llega img')
        dic = []
        guardarEspecie.esp_nombre=nombre
        guardarEspecie.esp_categoria=agr_especie_categoria.objects.get(cat_id=categoria)
        guardarEspecie.save()
        dic = {
            'result': 'OK'
        }
    except Exception as error:
        print(error)
        dic = {
            'result': 'Error al modificar'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type='application/json')

@login_required(login_url='/')
def valores_especie(request):
    if request.method == "POST":
        try:
            id= request.POST["id"]
            tipos = agr_parametro.objects.all()
            esp = agr_especie_cultivo.objects.get(esp_id=id)
            for tip in tipos:
                idVa='%s-%s'%('tipo',tip.par_id)
                if idVa not in request.POST:
                    print('no existe ese',idVa)
                else:
                    existeEspecieParametro=agr_especie_parametros.objects.filter(esp_id=esp, tipo_id=tip)
                    if(existeEspecieParametro):#Mando a actualizar los valores
                        if(request.POST[idVa]!=""):
                                existe=agr_especie_parametros.objects.get(esp_id=esp, tipo_id=tip)
                                existe.valor=request.POST[idVa]
                                existe.save()
                    else:#Mando a guardar los valores
                        if(request.POST[idVa]!=""):
                            guardar=agr_especie_parametros(esp_id=esp, tipo_id=tip, valor=request.POST[idVa])
                            guardar.save()
            messages.add_message(request, messages.SUCCESS, 'Se guardo con exito')
        except Exception as e:
            print(e)
            messages.add_message(request, messages.ERROR, e.message)
        return redirect('/agricultura/especie/')

@login_required(login_url='/')
def tipos_parametros_especie(request):
    data=[]
    id = request.GET['id']
    especieQuery=agr_especie_cultivo.objects.get(esp_id=id)
    try:
        #SABER LOS PARAMETROS DE ESPECIE
        grupo=agr_grupo.objects.get(gru_nombre='Especie')
        tipos_parametros=agr_tipo_parametro.objects.filter(tpar_grupo=grupo).order_by('tpar_id')
        data.append({'contParametrosEspecie': tipos_parametros.count()})
        for i in tipos_parametros:
            data.append({'id': i.tpar_id,
                         'nombre': i.tpar_nombre})
            parametros = agr_parametro.objects.filter(par_tpar=i.tpar_id).order_by('par_id')
            data.append({'contTipos': parametros.count()})
            for j in parametros:
                try:
                    parametros_valores = agr_especie_parametros.objects.filter(esp_id=especieQuery).get(tipo_id=j.par_id)
                    data.append(
                        {'nombreTipo': j.par_nombre, 'idTipo': j.par_id, 'valor': parametros_valores.valor})
                except:
                    data.append(
                    {'nombreTipo': j.par_nombre, 'idTipo': j.par_id, 'valor': ''})
    except Exception as error:
        print(error)
    data=json.dumps(data)
    return HttpResponse(data, content_type='application/json')