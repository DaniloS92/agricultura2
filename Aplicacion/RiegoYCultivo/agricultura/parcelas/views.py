#-*- coding: utf-8 -*-
__author__ = 'Dyuyay'
import json

from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import JsonResponse

from ..models import *
from agricultura.metodos import *
from iot.models import *
from django.db import transaction
from iot.metodos import *


@login_required(login_url='/')
def parcelas(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if request.user.has_perm('agricultura.add_agr_parcela') or request.user.has_perm('agricultura.list_agr_parcela'):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            suelos=agr_tipo_suelo.objects.all()
            dic['datos_parcela']=parcelas
            dic['datos_subareas']=subareas
            dic['datos_suelo']=agr_tipo_suelo.objects.all()
            dic['regiones']=reg_coef_expe.objects.all()
            return render_to_response('agricultura/parcelas/templateParcelas.html',dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def guardarParcela(request):
    print request.POST
    if request.user.is_authenticated():
        if request.method == "POST":
            try:
                subarea = request.POST["select_local"]
            except:
                subarea = request.POST["select_local1"]
            suelo = request.POST["select_suelo"]
            gparcela = agr_parcela()
            gparcela.par_id_id=subarea
            gparcela.par_hectareas = request.POST['numHectareas']
            gparcela.par_area_neta = request.POST['txtAreaNeta']
            gparcela.par_area_bruta = request.POST['txtAreaBruta']
            gparcela.par_estado = True
            gparcela.par_disponibles = request.POST['numHectareas']
            suelo=agr_tipo_suelo.objects.get(tsue_id=suelo)
            gparcela.par_suelo=suelo
            gparcela.save()

            param_parcela = agr_parametros_parcela(
                par_tipo = 'areaneta',
                par_valor = request.POST['txtAreaNeta'],
                parcela = gparcela)
            param_parcela.save()
            param_parcela2 = agr_parametros_parcela(
                par_tipo = 'areabruta',
                par_valor = request.POST['txtAreaBruta'],
                parcela = gparcela
            )
            param_parcela2.save()
            if(request.POST['tipo_envio']=='parcela'):
                messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
                messages.add_message(request, messages.WARNING, 'Proceder a guardar datos en Evotranspiracion')
                return redirect('/agricultura/parcelas/')
            if(request.POST['tipo_envio']=='har'):
                messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
                return redirect('/agricultura/evotranspiracionInicial/')
            if(request.POST['tipo_envio']=='pen'):
                messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
                return redirect('/agricultura/evotranspiracionInicialP/')                
    else:
        return redirect('/')

# SE USA PARA RETORNAR EL JSONRESPONSE ESTE METODO YA FUNCIONA
def ver_datos_parcela(request):
    id = request.GET['id']
    data = {}
    try:
        vparcela = agr_parcela.objects.get(par_id = id)
        data = {
            'id' : vparcela.par_id.sar_id,
            'parcelaNombre' : vparcela.par_id.sar_nombre,
            'parcelaImagen' : vparcela.par_id.sar_imagen.url,
            'areaNeta' : vparcela.par_area_neta,
            'areaBruta' : vparcela.par_area_bruta,
            'hectareas' : vparcela.par_hectareas,
            'estado' : vparcela.par_estado,
            'suelo':vparcela.par_suelo.tsue_id
            }
        return JsonResponse(data, safe=False)
    except Exception as error:
        print(error)
        data = []
    return JsonResponse(data, safe=False)

def modificar_datos_parcela(request):
    try:
        id = request.POST['select_local']
        suelo = request.POST['select_suelo']
        dic = []
        suelo=agr_tipo_suelo.objects.get(tsue_id=suelo)
        subarea=proy_subarea.objects.get(sar_id=id)
        guardarParcela = agr_parcela.objects.get(par_id = subarea)
        guardarParcela.par_suelo=suelo
        guardarParcela.par_area_neta = request.POST['txtAreaNeta']
        guardarParcela.par_area_bruta = request.POST['txtAreaBruta']
        hectareasBase=float(guardarParcela.par_hectareas)
        disponiblesBase=float(guardarParcela.par_disponibles)
        if((agr_cultivo.objects.filter(parcela=guardarParcela).exclude(cult_estado=False).exists())):
            if(hectareasBase<float(request.POST['numHectareas'])):
                messages.add_message(request, messages.ERROR, 'No se puede actualizar las hectareas, parcela no disponible')
        else:
            guardarParcela.par_hectareas = request.POST['numHectareas']
            guardarParcela.par_disponibles = request.POST['numHectareas']
        guardarParcela.save()
        dic = {'result':'OK'}

        param_parcela = agr_parametros_parcela.objects.filter(parcela = guardarParcela)
        for datos in param_parcela:
            print(datos.par_tipo)
            if datos.par_tipo == "Area Bruta":
                datos.par_valor=request.POST['txtAreaBruta']
                datos.save()
            else:
                datos.par_valor=request.POST['txtAreaNeta']
                datos.save()
                messages.add_message(request, messages.SUCCESS, 'Registro Actualizado')
    except Exception as error:
        print(error)
        dic = {'result':'Error al modificar'}
    # data = json.dumps(dic)
    return JsonResponse(dic)

def dar_dbaja_parcela(request):
    id = request.GET['id']
    estado = request.GET['estado']
    try:
        bajaParcela = agr_parcela.objects.get(par_id = id)
        if (estado=='true'):
            bajaParcela.par_estado=True
        else:
            bajaParcela.par_estado=False
        bajaParcela.save()
        dic = {'result': 'OK'}
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja la parcela'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def zona_riego(request, id):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        empresa=request.user.id_persona.emp_id.emp_id            
        areas = proy_area.objects.filter(emp_id_id=empresa)
        subareas = proy_subarea.objects.filter(ar_id=areas)
        verificar_update(request)
        if (id=='all'):
            if request.user.has_perm('agricultura.change_agr_zona_sensor_actuador') or request.user.has_perm('agricultura.list_agr_zona_sensor_actuador'):
                
                dic['datos_sub_area'] = subareas
                dic['datos_zonas'] = agr_zona_sensor_actuador.objects.filter(sar_id = subareas)
                dic['esGeneral'] = True
                dic['regiones']=reg_coef_expe.objects.all()
                return render_to_response("agricultura/cultivo/templateListaZonaRiego.html",dic,context_instance = RequestContext(request))
            else:
                return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
        else:
            if request.user.has_perm('agricultura.change_agr_zona_sensor_actuador') or request.user.has_perm('agricultura.list_agr_zona_sensor_actuador'):
          
                dic['datos_sub_area'] = subareas
                dic['subarea'] = proy_subarea.objects.get(sar_id = id)
                dic['datos_zonas'] = agr_zona_sensor_actuador.objects.filter(sar_id__sar_id = id)
                dic['esGeneral'] = False
                return render_to_response("agricultura/cultivo/templateListaZonaRiego.html",dic,context_instance = RequestContext(request))
            else:
                return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

def guardarZona(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            try:
                estado = request.POST.getlist('check_estado')
                if(estado):
                    estado=True
                else:
                    estado=False
                newZona = agr_zona_sensor_actuador()
                newZona.zsa_nombre=request.POST['txt_zona']
                newZona.zsa_descripcion = request.POST['txt_descripcion']
                newZona.zsa_localizacion = request.POST['txt_localizacion']
                newZona.zsa_estado = estado
                newZona.zsa_imagen=request.FILES['file1']
                bsubarea = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
                newZona.sar_id = bsubarea
            except Exception, e:
                newZona.zsa_imagen='img_zona/dashboard.png'
                bsubarea = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
                newZona.sar_id = bsubarea

            newZona.save()
            # Creamos un panel por defecto para la zona de riego
            agr_monitoreo(zsa_id=newZona).save();

    return redirect('/agricultura/zona_riego/all')

def ver_datos_zona(request):
    id = request.GET['id']
    data = {}
    vector = []
    try:
        findZonas = agr_zona_sensor_actuador.objects.get(zsa_id = id)
        find_sub_areas = proy_subarea.objects.exclude(sar_id = findZonas.sar_id.sar_id)
        for sub_ar in find_sub_areas:
            vector.append({'sar_id': sub_ar.sar_id,'sar_nombre' : sub_ar.sar_nombre})
        data = {
            'id' : findZonas.zsa_id,
            'zsa_nombre' : findZonas.zsa_nombre,
            'zsa_descripcion' : findZonas.zsa_descripcion,
            'zsa_localizacion' : findZonas.zsa_localizacion,
            'zsa_estado' : findZonas.zsa_estado,
            'zsa_imagen' : findZonas.zsa_imagen.url,
            'sar_id' : findZonas.sar_id.sar_id,
            'sar_nombre' : findZonas.sar_id.sar_nombre,
            'sub_areas' : vector,
            }
        response = JsonResponse(data)
        return HttpResponse(response.content)
    except Exception as error:
        print(error)
        data = {}
        response = JsonResponse(data)
    return HttpResponse(response.content)

def modificar_datos_zonas(request):
    try:

        id = request.POST["id"]
        nombre= request.POST["txt_zona"]
        descripcion = request.POST['txt_descripcion']
        localizacion = request.POST["txt_localizacion"]
        sub_area_id = request.POST["select_sub_area"]
        
        sub_area = proy_subarea.objects.get(sar_id = sub_area_id)

        saveZona = agr_zona_sensor_actuador.objects.get(zsa_id = id)

        saveZona.zsa_nombre=nombre
        saveZona.zsa_descripcion=descripcion
        saveZona.zsa_localizacion=localizacion

        try:
            imagen = request.FILES["file1"]
            saveZona.zsa_imagen = imagen
        except:
            print('no llega img')
        saveZona.sar_id = sub_area;
        saveZona.save()

        dic = {
            'result': 'OK'
        }
    except Exception as error:
        print(error)
        dic = {
            'result': 'Error al modificar'
        }
    response = JsonResponse(dic)
    return HttpResponse(response.content)

def dar_dbaja_zona(request):
    id = request.GET['id']
    estado = request.GET['estado']
    try:
        bajaZonaRiego = agr_zona_sensor_actuador.objects.get(zsa_id = id)
        if (estado=='true'):
            bajaZonaRiego.zsa_estado=True
        else:
            bajaZonaRiego.zsa_estado=False
        bajaZonaRiego.save()
        dic = {'result': 'OK'}
    except Exception as e:
        print(e)
        dic = {'result': 'Error al dar de baja la parcela'}
    response = JsonResponse(dic)
    return HttpResponse(response.content)

def consulta_sen_act(request):

    id = request.GET['id']
    vec_sensores_actuadores = []
    vec_sen_act_zona = []
    try:

        empresa = request.user.id_persona.emp_id.emp_id
        dispositivos = iot_dispositivo.objects.filter(emp_id = empresa)
        find_Sen_Act = iot_sensor_actuador.objects.filter(dis_id = dispositivos)
        find_Sen_Act_Zona = iot_sensor_actuador.objects.filter(zsa_id__zsa_id = id)
        find_Zona = agr_zona_sensor_actuador.objects.get(pk = id)

        for sen_act in find_Sen_Act:
            estado = sen_act.sa_estado
            zona_sa = ''
            if sen_act.zsa_id == find_Zona:#true es igual a que el sensor o actuador esta disponible
                estado = True
            if sen_act.zsa_id != None:
                zona_sa = sen_act.zsa_id.zsa_nombre

            vec_sensores_actuadores.append({
                'id' : sen_act.iot_id,
                'nombre' : sen_act.ted_id.ted_name,
                'interfaz' : sen_act.sa_interfaz,
                'categoria' : sen_act.sa_categoria,
                'dispositivo' : sen_act.dis_id.dis_nombre,
                'estado' : estado,
                'zona' : zona_sa,
            })

        for sen_act_z in find_Sen_Act_Zona:
            vec_sen_act_zona.append({
                'id' : sen_act_z.iot_id,
                'categoria' : sen_act_z.sa_categoria,
            })

        dic = {
            'sensoresActuadores' : vec_sensores_actuadores,
            'sensoresActuadoresZona' : vec_sen_act_zona,
        }

    except Exception, e:
        print(e)
        dic = {'result': 'Error en la busqueda de los sensores y actuadores'}

    response = JsonResponse(dic)
    return HttpResponse(response.content)

@transaction.atomic()
def act_sen_act_zona(request):
    transaccion = transaction.savepoint()
    sensores = request.POST.getlist('sen[]')
    actuadores = request.POST.getlist('act[]')
    idZona = request.POST['id']

    findZona = agr_zona_sensor_actuador.objects.get(zsa_id = idZona)

    vec_sensores_actuadores = []
    vec_senial_digitales = []
    vec_accion_actuador_dig = []
    vec_accion_actuador_an = []

    # Estructuras base de los sensores y actuadores del dashboard
    plugin_dashboard = json.loads('{"version":1,"allow_edit":true,"plugins":[],"panes":[],"datasources":[{"name":"Node","type":"node_js","settings":{"url":"http://localhost:8686/iotmach_node","events":[{"eventName":"mensajes"}],"rooms":[{"roomName":"1","roomEvent":"subscribe"}]}}],"columns":3}')
    # estructura_sensor = json.loads('{"title": "","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[]}')
    estructura_senial_digital = json.loads('{"title":"Sensores Digitales","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[]}')
    estructura_accion_actuador_digital = json.loads('{"title":"Actuadores Digitales","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[]}')
    estructura_accion_actuador_analogico = json.loads('{"title":"Actuadores Analógicos","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[]}')
    
    # Bandera para saber si existen sensores digitales al momento de graficar en el dashboard
    sen_dig = False

    try:
        with transaction.atomic():
            findSen_Act = iot_sensor_actuador.objects.filter(zsa_id = findZona)

            if findSen_Act.exists():
                for sen_act in findSen_Act:
                    sen_act.zsa_id = None
                    sen_act.sa_estado = True
                    sen_act.save()

            for id_sensor in sensores:
                print 'entro a los sensores'
                find_sen = iot_sensor_actuador.objects.get(iot_id = id_sensor)
                mac = find_sen.dis_id.dis_mac
                find_sen.zsa_id = findZona
                find_sen.sa_estado = False

                value = mac.replace(':','')+'.'+find_sen.sa_interfaz

                if find_sen.sa_senial.upper() == 'A':

                    pluginSensor = construir_pluginSensor('Sensor', find_sen.dis_id.dis_nombre+' - '+find_sen.sa_nombre, value, find_sen.ted_id.um_id.umed_abreviatura,find_sen.ted_id.ted_name)
                    vec_sensores_actuadores.append(pluginSensor)
                else:
                    sen_dig = True
                    vec_senial_digitales.append(construir_widget_senial_digital(find_sen.dis_id.dis_nombre+' - '+find_sen.sa_nombre+' - '+find_sen.sa_interfaz, value));

                find_sen.save()

            # preguntar si hay sensores digitales para agregar los widgets
            if sen_dig:
                estructura_senial_digital['widgets'] = vec_senial_digitales
                vec_sensores_actuadores.append(estructura_senial_digital)

            for id_act in actuadores:
                print 'entro a los actuadores'
                find_act = iot_sensor_actuador.objects.get(iot_id = id_act)
                mac = find_act.dis_id.dis_mac
                find_act.zsa_id = findZona
                find_act.sa_estado = False

                value = mac.replace(':','')+'.'+find_act.sa_interfaz

                # array de widgets
                if find_act.sa_senial.upper() == 'D': 
                    vec_accion_actuador_dig.append(construir_plugin_accion_actuador(find_act.dis_id.dis_nombre+' - '+find_act.sa_nombre,find_act.iot_id,value))
                else:
                    vec_accion_actuador_an.append(construir_plugin_accion_actuador_an(find_act.dis_id.dis_nombre+' - '+find_act.sa_nombre,find_act.iot_id,value))
                find_act.save()

            # paneles de actuadores
            if actuadores.__len__() > 0:
                # estructura_estado_actuador['widgets'] = vec_estado_actuador
                if vec_accion_actuador_dig.__len__() > 0:
                    widgetBotonMaster = json.loads('{"type":"boton_master","settings":{"titulo":"","labelEncendido":"on","labelApagado":"off"}}')
                    vec_accion_actuador_dig.insert(0,widgetBotonMaster)
                    estructura_accion_actuador_digital['widgets'] = vec_accion_actuador_dig
                    vec_sensores_actuadores.append(estructura_accion_actuador_digital)
                if len(vec_accion_actuador_an) > 0:
                    estructura_accion_actuador_analogico['widgets'] = vec_accion_actuador_an
                    vec_sensores_actuadores.append(estructura_accion_actuador_analogico)

            # agregando los paneles al json general de dashboard
            plugin_dashboard['panes'] = vec_sensores_actuadores
            plugin_dashboard['datasources'][0]['settings']['rooms'][0]['roomName'] = idZona

            # Agregando extras: imagen y mapa
            panel_imagen = json.loads('{"title":"Imagen","width":1,"row":{"3":1,"5":11},"col":{"3":1,"5":1},"col_width":1,"widgets":[{"type":"picture","settings":{"src":"/media/img_zona/zona_aYJQi3p.jpg","refresh":0}}]}')
            
            # configurando el panel de la imagen
            panel_imagen['widgets'][0]['settings']['src'] = findZona.zsa_imagen.url

            plugin_dashboard['panes'].append(panel_imagen)
            find_monitoreo = agr_monitoreo.objects.get(zsa_id__zsa_id = idZona)
            find_monitoreo.mon_panel = json.dumps(plugin_dashboard)

            find_monitoreo.save()

            act_estado_disp(idZona)
        transaction.savepoint_commit(transaccion)
        dic = {'result': 'OK'}
    except Exception, e:
        transaction.savepoint_rollback(transaccion)
        dic = {'result': 'ERROR'}
        print e

    response = JsonResponse(dic)
    return HttpResponse(response)

def construir_pluginSensor(titlePanel,titleWidget,value,umedida,nombre_ted):
    
    plugin = json.loads('{"title": "","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[{"type":"text_widget","settings":{"title": "","size":"regular","value":"","animate":true,"units":""}},{"type":"sparkline","settings":{"title":"","value":[],"include_legend":false,"legend":""}}]}')

    if nombre_ted.upper() == 'TEMPERATURA':

        plugin = json.loads('{"title": "","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[{"type":"text_widget","settings":{"title": "","size":"regular","value":"","sparkline": true,"animate":true,"units":"oC"}}]}')
        plugin['title'] = titlePanel.upper()
        plugin['widgets'][0]['settings']['title'] = titleWidget
        plugin['widgets'][0]['settings']['value'] = 'datasources.Node.data.MAC'+value+'.valor'
        plugin['widgets'][0]['settings']['units'] = umedida

    elif nombre_ted.upper() == 'HUMEDAD':

        plugin = json.loads('{"title": "","width":1,"row":{"3":1},"col":{"3":1},"col_width":1,"widgets":[{"type":"gauge","settings":{"title":"","value":"","units":"%","min_value":0,"max_value":100}}]}')
        plugin['title'] = titlePanel.upper()
        plugin['widgets'][0]['settings']['title'] = titleWidget
        plugin['widgets'][0]['settings']['value'] = 'datasources.Node.data.MAC'+value+'.valor'
        plugin['widgets'][0]['settings']['units'] = umedida

    else:
        
        plugin['title'] = titlePanel.upper()
        plugin['widgets'][0]['settings']['title'] = titleWidget
        plugin['widgets'][0]['settings']['value'] = 'datasources.Node.data.MAC'+value+'.valor'
        plugin['widgets'][0]['settings']['units'] = umedida
        plugin['widgets'][1]['settings']['value'].append('datasources.Node.data.MAC'+value+'.valor')

    
    return plugin

def construir_widget_senial_digital(titleWidget,value):
    # me va a retornar un widget
    widgetIndicador = json.loads('{"type":"indicator","settings":{"title":"","value":"","on_text":"ON","off_text":"OFF"}}')
    widgetIndicador['settings']['title'] = titleWidget
    widgetIndicador['settings']['value'] = 'if(datasources.Node.data.MAC'+value+'.valor == "1"){\n    return true;\n} else{\n    return false;\n}'

    return widgetIndicador

def construir_plugin_accion_actuador(titleWidget,value,url_estado):
    # me va a retornar un widget
    widgetActuador = json.loads('{"type":"boton","settings":{"titulo":"","labelEncendido":"ON","labelApagado":"OFF","url":"http://localhost:8686/iotmach_actuadores","actuador":"","estado_actuador":"","tiempo":1}}')
    # widgetActuador = json.loads('{"type":"boton","settings":{"titulo":"","labelEncendido":"ON","labelApagado":"OFF","url":"http://localhost:8686/iotmach_actuadores","actuador":"","tiempo":1}}')
    widgetActuador['settings']['titulo'] = titleWidget
    widgetActuador['settings']['actuador'] = value
    widgetActuador['settings']['estado_actuador'] = 'datasources.Node.data.MAC'+url_estado+'.valor'

    return widgetActuador

def construir_plugin_accion_actuador_an(titleWidget,value,url_estado):
    # me va a retornar un widget
    widgetActuador = json.loads('{"type":"boton_analogico","settings":{"titulo":"","url":"http://localhost:8686/iotmach_actuadores","actuador":"","estado_actuador":"","tiempo":1}}')
    # widgetActuador = json.loads('{"type":"boton","settings":{"titulo":"","labelEncendido":"ON","labelApagado":"OFF","url":"http://localhost:8686/iotmach_actuadores","actuador":"","tiempo":1}}')
    widgetActuador['settings']['titulo'] = titleWidget
    widgetActuador['settings']['actuador'] = value
    widgetActuador['settings']['estado_actuador'] = 'datasources.Node.data.MAC'+url_estado+'.valor'

    return widgetActuador