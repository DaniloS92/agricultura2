from django import template
from urlparse import urlparse
from os.path import splitext, basename

register = template.Library()

@register.filter(name="ext")
def extension(value):
    dissasembled = urlparse(value)
    filename, file_ext = splitext(basename(dissasembled.path))
    return file_ext