from django.contrib import admin
from models import *


class agr_lat_mesAdmin(admin.ModelAdmin):
    list_display = ('id', 'valor', 'lat_hemisferio', 'latitud_id', 'mes_id')
    list_filter= ['valor']
    search_fields = ['valor']
    list_per_page = 10

class agr_cosechaAdmin(admin.ModelAdmin):
    list_display = ('cos_id', 'cos_fecha', 'cos_cantidad_cosechada', 'cos_observacion', 'cos_precio_venta', 'cos_costo_produccion','cos_cantidad_hect_cosechadas')
    list_filter= ['cos_fecha']
    search_fields = ['cos_fecha']
    list_per_page = 10

class agr_cultivo_parametroAdmin(admin.ModelAdmin):
    list_display=('cultivo','tipo','fase','valor')
    list_filter= ['tipo']
    search_fields = ['tipo']
    list_per_page = 10

class arg_parametroAdmin(admin.ModelAdmin):
    list_display=('par_id','par_nombre','par_descripcion','par_abreviatura','par_valor_minimo','par_valor_maximo','par_valor','par_estado','par_umedida_id','par_fase_id','par_tpar_id')
    list_filter= ['par_nombre']
    search_fields = ['par_nombre']
    list_per_page = 10

class agr_tipo_archivoAdmin(admin.ModelAdmin):
    list_display=('tarc_id','tarc_nombre','tarc_icono')
    list_filter= ['tarc_nombre']
    search_fields = ['tarc_nombre']
    list_per_page = 10

class agr_archivoAdmin(admin.ModelAdmin):
    list_display=('arc_id','arc_nombre','arc_estado','arc_uri','par_id_id','tarc_id_id')
    list_filter= ['arc_nombre']
    search_fields = ['arc_nombre']
    list_per_page = 10

class agr_tipo_plaga_enfermedadAdmin(admin.ModelAdmin):
    list_display=('tpe_id','tpe_nombre')
    list_filter= ['tpe_nombre']
    search_fields = ['tpe_nombre']
    list_per_page = 10

class agr_plaga_enfermedadAdmin(admin.ModelAdmin):
    list_display=('pe_id','pe_fecha_identificacion','pe_cant_plantas_afectadas','pe_cant_hectareas','pe_observaciones','pe_estado','tpe_id','cul_id')
    list_filter= ['pe_fecha_identificacion']
    search_fields = ['pe_fecha_identificacion']
    list_per_page = 10

class agr_especie_categoriaAdmin(admin.ModelAdmin):
    list_display=('cat_id','cat_nombre','cat_observacion','cat_estado')
    list_filter= ['cat_nombre']
    search_fields = ['cat_nombre']
    list_per_page = 10

class agr_tipo_riegoAdmin(admin.ModelAdmin):
    list_display=('rie_id','rie_nombre','rie_descripcion')
    list_filter= ['rie_nombre']
    search_fields = ['rie_nombre']
    list_per_page = 10

class r_unidad_medidaAdmin(admin.ModelAdmin):
    list_display=('umed_id','umed_nombre','umed_abreviatura')
    list_filter= ['umed_nombre']
    search_fields = ['umed_nombre']
    list_per_page = 10

class agr_tipo_parametroAdmin(admin.ModelAdmin):
    list_display=('tpar_id','tpar_nombre','tpar_abreviatura','tpar_descripcion','tpar_grupo')
    list_filter= ['tpar_nombre']
    search_fields = ['tpar_nombre']
    list_per_page = 10

class agr_especie_cultivoAdmin(admin.ModelAdmin):
    list_display=('esp_id','esp_nombre','esp_imagen','esp_estado','esp_categoria')
    list_filter= ['esp_nombre']
    search_fields = ['esp_nombre']
    list_per_page = 10

class agr_latitudAdmin(admin.ModelAdmin):
    #list_display=('lat_valor')
    list_filter= ['lat_valor']
    search_fields = ['lat_valor']
    list_per_page = 10

class agr_mesAdmin(admin.ModelAdmin):
    #list_display=('mes_nombre')
    list_filter= ['mes_nombre']
    search_fields = ['mes_nombre']
    list_per_page = 10

class agr_tipo_sueloAdmin(admin.ModelAdmin):
    list_display=('tsue_id','tsue_nombre','tsue_imagen','tsue_estado')
    list_filter= ['tsue_nombre']
    search_fields = ['tsue_nombre']
    list_per_page = 10

class agr_parcelaAdmin(admin.ModelAdmin):
    list_display=('par_id','par_area_neta','par_area_bruta','par_hectareas','par_disponibles','par_estado','par_suelo')
    list_filter= ['par_disponibles']
    search_fields = ['par_area_neta']
    list_per_page = 10

class agr_evotranspiracionAdmin(admin.ModelAdmin):
    list_display=('evo_id','mes','par_id','tipo')
    list_filter= ['tipo']
    search_fields = ['mes','tipo']
    list_per_page = 10

class evo_tipo_valorAdmin(admin.ModelAdmin):
    list_display=('tip_id','valor','evo_id','tipo')
    list_filter= ['tipo']
    search_fields = ['tipo']
    list_per_page = 10

class agr_cultivoAdmin(admin.ModelAdmin):
    list_display=('cult_id','cult_detalle','cult_descripcion','cult_fecha','cult_fecha_fin','cult_num_hectareas')
    list_filter= ['cult_fecha']
    search_fields = ['cult_fecha']
    list_per_page = 10

class agr_modelo_cultivoAdmin(admin.ModelAdmin):
    list_display=('mcult_id','mcult_cultivo_referencia','mcult_nombre')
    list_filter= ['mcult_nombre']
    search_fields = ['mcult_nombre']
    list_per_page = 10

class agr_formulas_riegoAdmin(admin.ModelAdmin):
    list_display=('for_id','for_nombre','for_descripcion','for_formula','for_tipos','for_estado','for_general','for_formato','for_dato','for_operador','u_medida','tipo_riego')
    list_filter= ['for_tipos']
    search_fields = ['for_nombre']
    list_per_page = 10

class agr_parametros_parcelaAdmin(admin.ModelAdmin):
    list_display=('par_tipo','par_valor','parcela')
    list_filter= ['par_tipo']
    search_fields = ['par_tipo']
    list_per_page = 10

class agr_monitoreoaAdmin(admin.ModelAdmin):
    list_display=('zsa_id','mon_panel')
    list_filter= ['mon_panel']
    search_fields = ['mon_panel']
    list_per_page = 10

class agr_cultivo_imagenesAdmin(admin.ModelAdmin):
    list_display=('cim_id','cim_imagen','cim_principal','cult_id')
    list_filter= ['cim_imagen']
    search_fields = ['cim_imagen']
    list_per_page = 10

class agr_faseAdmin(admin.ModelAdmin):
    list_display=('fas_id','fas_nombre','fas_descripcion')
    list_filter= ['fas_nombre']
    search_fields = ['fas_nombre']
    list_per_page = 10

class agr_zona_sensor_actuadorAdmin(admin.ModelAdmin):
    list_display=('zsa_id','zsa_nombre','zsa_descripcion','zsa_localizacion','zsa_estado','zsa_nec_act','zsa_imagen','sar_id')
    list_filter= ['zsa_localizacion']
    search_fields = ['zsa_nombre']
    list_per_page = 10

class agr_latitud_radiacionAdmin(admin.ModelAdmin):
    #list_display=('lat_valor')
    list_filter= ['lat_valor']
    search_fields = ['lat_valor']
    list_per_page = 10

class agr_mes_radiacionAdmin(admin.ModelAdmin):
    #list_display=('mes_nombre')
    list_filter= ['mes_nombre']
    search_fields = ['mes_nombre']
    list_per_page = 10

class agr_lat_mes_radiacionAdmin(admin.ModelAdmin):
    list_display=('latitud','mes','valor','lat_hemisferio')
    list_filter= ['mes']
    search_fields = ['mes']
    list_per_page = 10

class agr_latitud_horas_luzAdmin(admin.ModelAdmin):
    #list_display=('lat_valor')
    list_filter= ['lat_valor']
    search_fields = ['lat_valor']
    list_per_page = 10

class agr_mes_horas_luzAdmin(admin.ModelAdmin):
    #list_display=('mes_nombre')
    list_filter= ['mes_nombre']
    search_fields = ['mes_nombre']
    list_per_page = 10

class agr_lat_mes_horas_luzAdmin(admin.ModelAdmin):
    list_display=('latitud','mes','valor','lat_hemisferio')
    list_filter= ['mes']
    search_fields = ['mes']
    list_per_page = 10

# Register your models here.
admin.site.register(agr_tipo_archivo,agr_tipo_archivoAdmin)
admin.site.register(agr_archivo,agr_archivoAdmin)
admin.site.register(agr_tipo_plaga_enfermedad,agr_tipo_plaga_enfermedadAdmin)
admin.site.register(agr_plaga_enfermedad,agr_plaga_enfermedadAdmin)
admin.site.register(agr_especie_categoria,agr_especie_categoriaAdmin)
admin.site.register(agr_tipo_riego,agr_tipo_riegoAdmin)
admin.site.register(agr_parametro,arg_parametroAdmin)
admin.site.register(r_unidad_medida,r_unidad_medidaAdmin)
admin.site.register(agr_tipo_parametro,agr_tipo_parametroAdmin)
admin.site.register(agr_especie_cultivo,agr_especie_cultivoAdmin)
admin.site.register(agr_latitud,agr_latitudAdmin)
admin.site.register(agr_mes,agr_mesAdmin)
admin.site.register(agr_lat_mes,agr_lat_mesAdmin)
admin.site.register(agr_tipo_suelo,agr_tipo_sueloAdmin)
admin.site.register(agr_parcela,agr_parcelaAdmin)
admin.site.register(agr_evotranspiracion,agr_evotranspiracionAdmin)
admin.site.register(evo_tipo_valor,evo_tipo_valorAdmin)
admin.site.register(agr_cultivo,agr_cultivoAdmin)
admin.site.register(agr_modelo_cultivo,agr_modelo_cultivoAdmin)
admin.site.register(agr_formulas_riego,agr_formulas_riegoAdmin)

admin.site.register(agr_parametros_parcela,agr_parametros_parcelaAdmin)
admin.site.register(agr_monitoreo,agr_monitoreoaAdmin)
admin.site.register(agr_cultivo_imagenes,agr_cultivo_imagenesAdmin)
admin.site.register(agr_fase,agr_faseAdmin)
admin.site.register(agr_zona_sensor_actuador,agr_zona_sensor_actuadorAdmin)
admin.site.register(agr_latitud_radiacion,agr_latitud_radiacionAdmin)
admin.site.register(agr_mes_radiacion,agr_mes_radiacionAdmin)
admin.site.register(agr_lat_mes_radiacion,agr_lat_mes_radiacionAdmin)
admin.site.register(agr_latitud_horas_luz,agr_latitud_horas_luzAdmin)
admin.site.register(agr_mes_horas_luz,agr_mes_horas_luzAdmin)
admin.site.register(agr_lat_mes_horas_luz,agr_lat_mes_horas_luzAdmin)
admin.site.register(agr_cosecha,agr_cosechaAdmin)
#@admin.register(agr_lat_mes)
#class tablaAdmin(admin.ModelAdmin):
#    list_display = ('latitud','mes','valor',)
