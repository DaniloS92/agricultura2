__author__ = 'JENNIFER'
from ..models import *
from decimal import *

def cargar_har(mes,minima,maxima,et0,parcela):
    print parcela
    try:
        parcelaQuery=agr_parcela.objects.get(par_id__sar_id=parcela)
        et0Query=agr_evotranspiracion.objects.filter(par_id=parcelaQuery,mes=mes,tipo='Hargravens')
        if et0Query.exists():
            for i in et0Query:
                evo=i.evo_id
            evotrans=agr_evotranspiracion.objects.get(evo_id=evo)
            evo_valor=evo_tipo_valor.objects.filter(evo_id_id=evotrans)
            evo_valor.delete()
            print('Mes:',mes,'-Minima:',minima,'-Maxima:',maxima,'-Et0:',et0,'-Evo',evo)
            guardar_har(evo,minima,maxima,et0)
        else:
            print('---no existe')
            print('Mes:',mes,'-Minima:',minima,'-Maxima:',maxima,'-Et0:',et0,'-Parcela',parcela)
            evotranspiracion=agr_evotranspiracion(mes=mes, par_id=parcelaQuery,tipo='Hargravens')
            evotranspiracion.save()
            guardar_har(evotranspiracion.evo_id,minima,maxima,et0)
    except Exception as e:
        print e


def guardar_har(evotranspiracion,minima,maxima,et0):
    evotrans = agr_evotranspiracion.objects.get(evo_id=evotranspiracion)
    evo_valor=evo_tipo_valor(valor=minima, tipo='TempMinima', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=maxima, tipo='TempMaxima', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=et0, tipo='et0', evo_id=evotrans)
    evo_valor.save()



#para metodo de penman
def cargar(mes,minima,maxima,horas_luz,vel_viento,humedad,parcela,et0):
    try:
        parcelaQuery=agr_parcela.objects.get(par_id__sar_id=parcela)
        et0Query=agr_evotranspiracion.objects.filter(par_id=parcela,mes=mes,tipo='Penman')
        if et0Query.exists():
            for i in et0Query:
                evo=i.evo_id
            evotrans=agr_evotranspiracion.objects.get(evo_id=evo)
            evo_valor=evo_tipo_valor.objects.filter(evo_id_id=evotrans)
            evo_valor.delete()
            print('Mes:',mes,'-Minima:',minima,'-Maxima:',maxima,'-horas_luz:',horas_luz,'-vel_viento:',vel_viento,'-humedad:',humedad,'-Et0:',et0,'-Parcela',parcela)
            guardar(evo,minima,maxima,et0,horas_luz,vel_viento,humedad)
        else:
            print('---no existe')
            print('Mes:',mes,'-Minima:',minima,'-Maxima:',maxima,'-horas_luz:',horas_luz,'-vel_viento:',vel_viento,'-humedad:',humedad,'-Et0:',et0,'-Parcela',parcela)
            evotranspiracion=agr_evotranspiracion(mes=mes, par_id=parcelaQuery,tipo='Penman')
            evotranspiracion.save()
            guardar(evotranspiracion.evo_id,minima,maxima,et0,horas_luz,vel_viento,humedad)
    except Exception as e:
        print e

def guardar(evotranspiracion,minima,maxima,et0,horas_luz,vel_viento,humedad):
    evotrans = agr_evotranspiracion.objects.get(evo_id=evotranspiracion)
    evo_valor=evo_tipo_valor(valor=minima, tipo='TempMinima', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=maxima, tipo='TempMaxima', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=et0, tipo='et0', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=horas_luz, tipo='horas_luz', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=vel_viento, tipo='vel_viento', evo_id=evotrans)
    evo_valor.save()
    evo_valor=evo_tipo_valor(valor=humedad, tipo='humedad', evo_id=evotrans)
    evo_valor.save()
