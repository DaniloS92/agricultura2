__author__ = 'Dyuyay'
import json

from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from django.contrib import messages
import openpyxl
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from hastamientras import *
from agricultura.metodos import *


#SECCION:
    # PARAMETROS DE CULTIVO
@login_required(login_url='/')
def parametros_cultivo(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_parametro') or request.user.has_perm('agricultura.list_agr_parametro'):
            ''' Mostrar plantilla templateCultivo.html
            Devuelve un diccionario de datos
            :param request:
            :return dic:
            '''
            dic['datos_parametros']=agr_parametro.objects.all()
            dic['unidades_medida']=r_unidad_medida.objects.all()
            dic['fase']=agr_fase.objects.all()
            dic['tparametro'] = agr_tipo_parametro.objects.all()
            return render_to_response('agricultura/configuracion/templateParametros.html', dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))


    #OPERACIONES
@login_required(login_url='/')
def agregar_parametros(request):
    '''Agregar parametros a la Base de datos
    Redirecciona a la pagina de parametros de cultivo
    :param request: Informacion de la pagina de donde viene la peticion
    :return Redicciona a la pagina de parametros de cultivo
    :exception Si existe un error al momento de guardar
    '''
    if request.user.is_authenticated():
        if request.method == "POST":
            try:
                id = request.POST["id"]
                local_nombre= request.POST["txt_nombre"]
                local_descripcion= request.POST["txt_descripcion"]
                local_abreviatura= request.POST["txt_abreviatura"]
                local_valorMinimo= request.POST["txt_minimo"]
                local_valorMaximo= request.POST["txt_maximo"]
                local_valor= request.POST["txt_valor"]
                local_medida = request.POST['select_medida']
                local_fase=request.POST["fase"]
                local_tipo= request.POST["tparametro"]
                medida=r_unidad_medida.objects.get(umed_id=local_medida)
                fase=agr_fase.objects.get(fas_id=local_fase)
                tipo_parametro=agr_tipo_parametro.objects.get(tpar_id=local_tipo)
                if (id == ''):
                    guardarParametro=agr_parametro(par_nombre=local_nombre,par_descripcion=local_descripcion,
                                               par_abreviatura=local_abreviatura,par_valor_minimo=local_valorMinimo,
                                               par_valor_maximo=local_valorMaximo,par_valor=local_valor,
                                               par_estado=True, par_umedida=medida, par_fase=fase, par_tpar=tipo_parametro)
                    guardarParametro.save()
                    messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
                else:
                    guardarParametro = agr_parametro.objects.get(par_id=id)
                    guardarParametro.par_nombre = local_nombre
                    guardarParametro.par_descripcion = local_descripcion
                    guardarParametro.par_abreviatura = local_abreviatura
                    guardarParametro.par_valor_minimo = local_valorMinimo
                    guardarParametro.par_valor_maximo = local_valorMaximo
                    guardarParametro.par_valor = local_valor
                    guardarParametro.par_umedida = medida
                    guardarParametro.par_fase=fase
                    guardarParametro.par_tpar=tipo_parametro
                    guardarParametro.save()
                    messages.add_message(request, messages.SUCCESS, 'Registro Actualizado')
            except Exception as e:
                messages.add_message(request, messages.ERROR, e.message)
            return redirect('/agricultura/parametros_cultivo')
    else:
        return redirect('/')

@login_required(login_url='/')
def baja_parametros(request):
    '''Cambiar de estado a el parametro
    Devuelve datos serializados
    :param request: Informacion de la pagina de donde viene la peticion
    :return dic: Tipo JSON
    :exception Si existe un error cuando se realiza la busquedad y se da de baja
    '''
    try:
        id = request.GET['id']
        estado = request.GET['estado']
        darBaja=agr_parametro.objects.get(par_id=id)
        if(estado=='true'):
            darBaja.par_estado=True
        else:
            darBaja.par_estado=False
        darBaja.save()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def ver_datos_parametros(request, id):
    data = []
    try:
        parametros_model = agr_parametro.objects.get(par_id=id)
        data.append({'id': parametros_model.par_id, 'nombre': parametros_model.par_nombre,
                     'descripcion': parametros_model.par_descripcion,
                     'abreviatura': parametros_model.par_abreviatura,
                     'minimo': parametros_model.par_valor_minimo,
                     'maximo':parametros_model.par_valor_maximo,
                     'valor':parametros_model.par_valor,
                     'estado':parametros_model.par_estado,
                     'medida':parametros_model.par_umedida.umed_id,
                     'fase':parametros_model.par_fase.fas_id,
                     'tipo_parametro':parametros_model.par_tpar.tpar_id})
        return JsonResponse(data, safe=False)
    except Exception as error:
        print(error)
        data = []
    return JsonResponse(data, safe=False)




#SECCION
    # TIPOS DE PARAMETROS
@login_required(login_url='/')
def tipos_parametros(request):
    ''' Mostrar plantilla templateTipos.html
    Devuelve un diccionario de datos
    :param request:
    :return dic:
    '''
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if request.user.has_perm('agricultura.add_agr_tipo_parametro') or request.user.has_perm('agricultura.list_tipo_parametro'):
            dic['datos_parametros']=agr_tipo_parametro.objects.all()
            dic['grupo']=agr_grupo.objects.all()
            return render_to_response('agricultura/configuracion/templateTipos.html', dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

    #OPERACIONES

@login_required(login_url='/')
def ver_datos_tipo(request, id):
    data = []
    try:
        tipos = agr_tipo_parametro.objects.get(tpar_id=id)
        data.append({'id': tipos.tpar_id, 'nombre': tipos.tpar_nombre, 'descripcion': tipos.tpar_descripcion,
                     'grupo':tipos.tpar_grupo.gru_id, 'abreviatura':tipos.tpar_abreviatura})
    except Exception as error:
        data = []
    return HttpResponse(json.dumps(data), content_type="application/json")

def editar_tipo(request, id):
    data = []
    try:
        tipos = agr_tipo_parametro.objects.get(tpar_id=id)
        data.append({'id': tipos.tpar_id, 'nombre': tipos.tpar_nombre, 'descripcion': tipos.tpar_descripcion,
                     'grupo':tipos.tpar_grupo.gru_id, 'abreviatura':tipos.tpar_abreviatura})
        # data.append({'id': tipos.id, 'nombre': tipos.tpar_nombre, 'descripcion': tipos.tpar_descripcion,
        #              'um':tipos.umed_id.id, 'abreviatura':tipos.tpar_abreviatura})
        return HttpResponse(json.dumps(data), content_type="application/json")
    except Exception as error:
        data = []
    return JsonResponse(data, safe=False)

def tipo_agregar(request):
    ''' Mostrar plantilla templateTipos.html
    Devuelve un diccionario de datos
    :param request:
    :return dic:
    '''
    if request.user.is_authenticated():
        if request.method == "POST":
            id=request.POST["id"]
            abreviatura=request.POST["abreviatura"]
            nombre=request.POST["nombre"]
            descripcion=request.POST["descripcion"]
            grupoGet=request.POST["grupo"]
            if(id==''):
                tipo=agr_tipo_parametro()
                tipo.tpar_abreviatura=abreviatura
                tipo.tpar_nombre=nombre
                tipo.tpar_descripcion=descripcion
                grupo=agr_grupo.objects.get(gru_id=grupoGet)
                tipo.tpar_grupo=grupo
                tipo.save()
                messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
                return redirect('/agricultura/parametros_tipo/')
            else:
                tipo=agr_tipo_parametro.objects.get(tpar_id=id)
                tipo.tpar_abreviatura=abreviatura
                tipo.tpar_nombre=nombre
                tipo.tpar_descripcion=descripcion
                tipo.tpar_grupo=agr_grupo.objects.get(gru_id=grupoGet)
                tipo.save()
                messages.add_message(request, messages.SUCCESS, 'Registro Actualizado')
                return redirect('/agricultura/parametros_tipo/')
        return redirect('/agricultura/parametros_tipo/')
    else:
        return redirect('/')

@login_required(login_url='/')
def baja_tipo(request):
    id = request.GET['id']
    estado = request.GET['estado']
    #print(estado)
    try:
        eliminar=agr_tipo_parametro.objects.get(tpar_id=id)
        if(estado=='true'):
            eliminar.tpar_tipo=True
        else:
            eliminar.tpar_tipo=False
        eliminar.save()
        print('est',eliminar.tpar_tipo)
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

#SECCION:
    # TIPO DE RIEGO
@login_required(login_url='/')
def tipo_riego(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_agr_tipo_riego') or request.user.has_perm('agricultura.list_agr_tipo_riego'):
            dic['parametros']=agr_parametro.objects.all()
            dic['riegos']=verParametrosRiegoPlantilla()
            dic['tipo_riego']=agr_tipo_riego.objects.all()
            return render_to_response('agricultura/configuracion/templateTipoRiego.html',dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def tipo_riego_formulas(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.add_formulas_riego') or request.user.has_perm('agricultura.list_agr_formulas_riego'):
            grupo=agr_grupo.objects.filter(gru_nombre='Resultados de Riego')
            tparametro=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
            dic['parametros']=agr_parametro.objects.all()
            dic['tipo_riego']=agr_tipo_riego.objects.all()
            dic['unidades_medida']=r_unidad_medida.objects.all()
            dic['formulas']=agr_formulas_riego.objects.all()
            dic['archivo']='/static/agricultura/img/formato.xlsx'
            return render_to_response('agricultura/configuracion/templateTipoRiegoFormulas.html',dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def agregar_formula(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            msj={'codigo':0,'msj':'Se guardo'}
            try:
                nombre= request.POST["nombre"]
                buscarTipo=agr_parametro.objects.get(par_id=nombre)
                descripcion= request.POST["descripcion"]
                formula= request.POST["formula"]
                tipos=request.POST["tipos"]
                um=request.POST["um"]
                riego=request.POST["riego"]
                tipo=request.POST["tipoF"]
                tipoG=request.POST["tipoG"]
                operador=request.POST["operador"]
                valor=request.POST["valor"]
                print(nombre,descripcion,formula,um,riego,tipos)
                guardar_formula=agr_formulas_riego()
                guardar_formula.for_nombre=buscarTipo.par_abreviatura
                guardar_formula.for_descripcion=descripcion
                guardar_formula.for_formula=formula
                guardar_formula.for_tipos=tipos[:len(tipos)-1]
                guardar_formula.u_medida_id=um
                guardar_formula.tipo_riego=agr_tipo_riego.objects.get(rie_id=riego)
                if(tipo!='ninguno'):
                    guardar_formula.for_operador=operador
                    guardar_formula.for_dato=valor
                    guardar_formula.for_formato=tipo
                if(tipoG=='fase'):
                    guardar_formula.for_general=True
                guardar_formula.save()
                messages.add_message(request, messages.SUCCESS, 'Formula Guardada')
            except Exception as e:
                print(e,' Este es mi error')
                msj['codigo']=1
                msj['msj']="Error inesperado"
                messages.add_message(request, messages.ERROR, e.message)
            return redirect('/agricultura/riego_formulas')
    else:
        return redirect('/')


@login_required(login_url='/')
def eliminar_formula(request):
    '''Eliminar Formula
    Devuelve datos serializados
    :param request: Informacion de la pagina de donde viene la peticion
    :return dic: Tipo JSON
    :exception Si existe un error cuando se realiza la busquedad y se da de baja
    '''
    try:
        id = request.GET['id']
        formula=agr_formulas_riego.objects.get(for_id=id)
        formula.delete()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al eliminar'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def baja_formula(request):
    id = request.GET['id']
    estado = request.GET['estado']
    #print(estado)
    try:
        eliminar=agr_formulas_riego.objects.get(for_id=id)
        if(estado=='true'):
            eliminar.for_estado=True
        else:
            eliminar.for_estado=False
        eliminar.save()
        print('est',eliminar.estado)
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def verTipoRiegoFormula(request):
    id = request.GET['id']
    try:
        grupo = agr_grupo.objects.filter(gru_nombre='Resultados de Riego')
        tparametro = agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
        buscarTipo=agr_parametro.objects.filter(par_tpar=tparametro)
        diccionario=[]
        for i in buscarTipo:
            try:
                if (agr_formulas_riego.objects.filter(for_nombre=i.par_abreviatura).get(tipo_riego=id)):
                    print(i.par_nombre,' Existe')
            except Exception as e:
                diccionario.append({'id':i.par_id, 'tipo':i.par_abreviatura,'descripcion':i.par_descripcion})
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(diccionario)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def guardar_riego(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            par = request.POST.getlist('listaParam')
            tipo = request.POST.get('tipoRiego')
            descripcion = request.POST.get('descripcionRiego')
            guardarTipo = agr_tipo_riego(rie_nombre=tipo,rie_descripcion=descripcion)
            guardarTipo.save()
        return redirect('/agricultura/tipo_riego')
    else:
        return redirect('/')


@login_required(login_url='/')
def guardar_archivo_formulas(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            tipo = request.POST.get('riego2')
            if 'formulas' in request.FILES:
                archivo=request.FILES['formulas']
                documento=openpyxl.load_workbook(archivo)
                hoja=documento.get_sheet_by_name('Formulas')
                for row in range(2, hoja.get_highest_row()+1):
                    print(hoja['C' + str(row)].value)
                    guardar_formula=agr_formulas_riego()
                    guardar_formula.for_nombre=hoja['B' + str(row)].value
                    guardar_formula.for_descripcion=hoja['c' + str(row)].value
                    guardar_formula.for_formula=hoja['D' + str(row)].value
                    guardar_formula.for_tipos=hoja['F' + str(row)].value
                    guardar_formula.u_medida=r_unidad_medida.objects.get(umed_abreviatura=hoja['E' + str(row)].value)
                    guardar_formula.tipo_riego=agr_tipo_riego.objects.get(rie_id=tipo)
                    if(hoja['G' + str(row)].value!='ninguno'):
                        guardar_formula.operador=hoja['H' + str(row)].value
                        guardar_formula.dato=hoja['I' + str(row)].value
                        guardar_formula.formato=hoja['G' + str(row)].value
                    guardar_formula.save()
                messages.add_message(request, messages.ERROR, 'El archivo se subio con exito')
            else:
                messages.add_message(request, messages.ERROR, 'No ha seleccionado archivo')
        return redirect('/agricultura/riego_formulas/')
    else:
        return redirect('/')


@login_required(login_url='/')
def ver_datos_riego(request):
    id = request.GET['id']
    data = []
    try:
        riego_model = agr_tipo_riego.objects.get(rie_id=id)
        data.append({'id': riego_model.rie_id, 'tipoRiego': riego_model.rie_nombre,
                     'descripcion': riego_model.rie_descripcion})
        return JsonResponse(data, safe=False)
    except Exception as error:
        print(error)
        data = []
    return JsonResponse(data, safe=False)

@login_required(login_url='/')
def modificar_datos_riego(request):
    try:
        id= request.POST["id"]
        nombre= request.POST["tipoRiego"]
        descripcion= request.POST["descripcionRiego"]
        guardarRiego=agr_tipo_riego.objects.get(rie_id=id)
        try:
            formula = request.FILES['formulas']
            guardarRiego.rie_formulas=formula
        except:
            print('Sin archivo')
        dic = []
        guardarRiego.rie_nombre=nombre
        guardarRiego.rie_descripcion=descripcion
        guardarRiego.save()
        dic = {
            'result': 'OK'
        }
    except Exception as error:
        print(error)
        dic = {
            'result': 'Error al modificar'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type='application/json')

from math import floor
@login_required(login_url='/')
def confEt0(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.list_agr_parcela') or request.user.has_perm('agricultura.add_agr_evotranspiracion'):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            dic['datos_parcela']=parcelas
            dic['datos_subareas']=subareas
            dic['datos_suelo']=agr_tipo_suelo.objects.all()
            dic['regiones']=reg_coef_expe.objects.all()
            return render_to_response('agricultura/configuracion/templateEt0.html',dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')



#carga inicial de Parcelas al seleccionar (metodo de hargravens)

@login_required(login_url='/')
def detalleParcela(request):
    try: 
        cod = request.GET['id']
        data=[]
        lat=agr_parcela.objects.get(par_id=cod)
        redondeado=floor(lat.par_id.sar_latitud)
        if (redondeado%2 != 0):
            redondeado=(floor(lat.par_id.sar_latitud)-1)
        latitud=agr_latitud.objects.get(lat_valor=redondeado)
        radiacion=agr_lat_mes.objects.filter(latitud=latitud,lat_hemisferio=lat.par_id.sar_hem_lat)
        data.append({'latitud':lat.par_id.sar_latitud})
        for i in radiacion:
            data.append({'mes':i.mes.mes_nombre, 'valor':str(i.valor)})
        evotranspiracion = agr_evotranspiracion.objects.filter(par_id = lat,tipo='Hargravens')
        cant = evotranspiracion.count()
        data.append({'cant':cant})
        print(data)
        for et in evotranspiracion:
            print(et.mes)
            vtipoValor = evo_tipo_valor.objects.filter(evo_id=et)
            #print(vtipoValor,'-----------------')
            if (vtipoValor.exists()):
                for i in vtipoValor:
                    print(i.tipo)
                    if(i.tipo=='TempMaxima'):
                        print('maxima---')
                        maxima=str(i.valor)
                    if(i.tipo=='TempMinima'):
                        print('min---')
                        minima=str(i.valor)
                    if(i.tipo=='et0'):
                        print('et0---')
                        et0=str(i.valor)
                    #data.append({'mes':et.mes,'temMin':str(vtipoValor[0].valor),'temMax':str(vtipoValor[1].valor),'et0':str(vtipoValor[2].valor)})
                data.append({'id':et.evo_id,'mes':et.mes,'temMin':minima,'temMax':maxima,'et0':et0})
                #data.append({'id':et.id,'mes':et.mes,'temMin':str(vtipoValor[0].valor),'temMax':str(vtipoValor[1].valor),'et0':str(vtipoValor[2].valor)})
        data=json.dumps(data)
    except Exception as e:
        print(e)
        print 'aaa'
    return HttpResponse(data, content_type='application/json')



#carga inicial de Parcelas al seleccionar (metodo de Penmant)

@login_required(login_url='/')
def detalleParcela_P(request):
    try: 
        cod = request.GET['id']
        data=[]
        lat=agr_parcela.objects.get(par_id=cod)
        lat_parcela=lat.par_id.sar_latitud
        tipo_region=lat.par_id.sar_region
        altura_snm=lat.par_id.sar_asnm

        #cargando los valores de radiacion maxima de dia sin nubes en tabla segun su latitud de la parcela
        
        radiacion_latitud=agr_latitud_radiacion.objects.filter(lat_valor=lat_parcela)
        if not(radiacion_latitud.exists()):
            latitud_menor=agr_latitud_radiacion.objects.filter(lat_valor__lt=lat_parcela).first()
            latitud_mayor=agr_latitud_radiacion.objects.filter(lat_valor__gte=lat_parcela).last()
            print(str(latitud_menor)+">>"+str(latitud_mayor))
            radiacion_menor=agr_lat_mes_radiacion.objects.filter(latitud=latitud_menor,lat_hemisferio=lat.par_id.sar_hem_lat)
            radiacion_mayor=agr_lat_mes_radiacion.objects.filter(latitud=latitud_mayor,lat_hemisferio=lat.par_id.sar_hem_lat)
            print(radiacion_mayor)
            data=Promediar(radiacion_menor,radiacion_mayor,latitud_menor,latitud_mayor,lat_parcela) 
            
        else:
            data.append({'latitud':lat.par_id.sar_latitud})
            radiacion=agr_lat_mes_radiacion.objects.filter(latitud=radiacion_latitud,lat_hemisferio=lat.par_id.sar_hem_lat)
            for i in radiacion:
                data.append({'mes':i.mes.mes_nombre, 'valor':str(i.valor)})
            print data        

        
        #cargando los valores de horas maximas de luz en tabla segun la latitud de la parcela
        
        horas_luz_latitud=agr_latitud_horas_luz.objects.filter(lat_valor=lat_parcela)
        if not(horas_luz_latitud.exists()):
            latitud_menor=agr_latitud_horas_luz.objects.filter(lat_valor__lt=lat_parcela).first()
            latitud_mayor=agr_latitud_horas_luz.objects.filter(lat_valor__gte=lat_parcela).last()
            print(str(latitud_menor)+">>"+str(latitud_mayor))
            horas_luz_menor=agr_lat_mes_horas_luz.objects.filter(latitud=latitud_menor,lat_hemisferio=lat.par_id.sar_hem_lat)
            horas_luz_mayor=agr_lat_mes_horas_luz.objects.filter(latitud=latitud_mayor,lat_hemisferio=lat.par_id.sar_hem_lat)
            print(horas_luz_mayor)
            data.append(Promediar(horas_luz_menor,horas_luz_mayor,latitud_menor,latitud_mayor,lat_parcela))
            
        else:
            datos=[]
            datos.append({'latitud':lat.par_id.sar_latitud})
            horas_luz=agr_lat_mes_horas_luz.objects.filter(latitud=horas_luz_latitud,lat_hemisferio=lat.par_id.sar_hem_lat)
            for i in horas_luz:
                datos.append({'mes':i.mes.mes_nombre, 'valor':str(i.valor)})
            data.append(datos)
            print data
        if tipo_region:
            data.append({'zona': tipo_region.region, 'valor_a1': tipo_region.valor_a1,'valor_b1': tipo_region.valor_b1})
        if altura_snm:
            data.append({'altura_parcela':float(altura_snm)})
#valores antes guardados en la tabla de calculo de evotranspiracion

        evotranspiracion = agr_evotranspiracion.objects.filter(par_id = lat,tipo='Penman')   
        if evotranspiracion.exists():
            cant = evotranspiracion.count()
            data.append({'cant':cant})

            carga=[]
            for et in evotranspiracion:
                print(et.mes)
                vtipoValor = evo_tipo_valor.objects.filter(evo_id=et)
                #print(vtipoValor,'-----------------')
                if (vtipoValor.exists()):
                    for i in vtipoValor:
                        print(i.tipo)
                        if(i.tipo=='TempMaxima'):
                            print('maxima---')
                            maxima=str(i.valor)
                        if(i.tipo=='TempMinima'):
                            print('min---')
                            minima=str(i.valor)
                        if(i.tipo=='et0'):
                            print('et0---')
                            et0=str(i.valor)
                        if(i.tipo=='horas_luz'):
                            print('horas_luz---')
                            horas_luz=str(i.valor)
                        if(i.tipo=='vel_viento'):
                            print('vel_viento---')
                            vel_viento=str(i.valor)
                        if(i.tipo=='humedad'):
                            print('humedad---')
                            humedad=str(i.valor)
                        #data.append({'mes':et.mes,'temMin':str(vtipoValor[0].valor),'temMax':str(vtipoValor[1].valor),'et0':str(vtipoValor[2].valor)})
                    print('id',et.evo_id,'mes',et.mes,'temMin',minima,'temMax',maxima,'et0',et0,'horas_luz',horas_luz,'vel_viento',vel_viento,'humedad',humedad)
                    carga.append({'id':et.evo_id,'mes':et.mes,'temMin':minima,'temMax':maxima,'et0':et0,'horas_luz':horas_luz,'vel_viento':vel_viento,'humedad':humedad})
                    #data.append({'id':et.id,'mes':et.mes,'temMin':str(vtipoValor[0].valor),'temMax':str(vtipoValor[1].valor),'et0':str(vtipoValor[2].valor)})
            data.append(carga)
            print(data[17])
        data=json.dumps(data)

    except Exception as e:
        print(e)
    return HttpResponse(data, content_type='application/json')





def Promediar(arreglomenor,arreglomayor,base_menor,base_mayor,base):
    data=[]
    diferencia=float(str(base_mayor))-float(str(base_menor))
    diferencia_menor=float(str(base))-float(str(base_menor))
    constante=diferencia_menor/diferencia    
    data.append({'latitud':str(base)})  
    a=0
    for i in arreglomenor:
        dif=float(float(str(arreglomayor[a].valor))-float(str(i.valor)))
        data.append({'mes':i.mes.mes_nombre,'valor':str(float(dif*float(constante))+float(str(i.valor)))})
        print('mes: '+i.mes.mes_nombre+'  valor:'+str(float(dif*float(constante))+float(str(i.valor))))
        a+=1
    return data

#guardar datos para Hargravens
@login_required(login_url='/')
def guardarDatoEvo(request):
    print request.GET
    mes = request.GET['mes']
    minima = request.GET['minima']
    maxima = request.GET['maxima']
    et0 = request.GET['et0']
    parcela = request.GET['parcela']
    print(mes,minima,maxima,et0,parcela)
    cargar_har(mes,minima,maxima,et0,parcela)
    data=[]
    data.append({'latitud':1,'mes':mes})
    data=json.dumps(data)
    return HttpResponse(data, content_type='application/json')

#guardar datos para Penman
@login_required(login_url='/')
def guardarDatoEvoPenman(request):
    mes = request.GET['mes']
    minima = request.GET['minima']
    maxima = request.GET['maxima']
    horas_luz=request.GET['horas_luz']
    vel_viento=request.GET['vel_viento']
    humedad=request.GET['humedad']
    parcela = request.GET['parcela']
    et0 = request.GET['et0']
    print(mes,minima,maxima,horas_luz,vel_viento,humedad,parcela,et0)
    cargar(mes,minima,maxima,horas_luz,vel_viento,humedad,parcela,et0)
    data=[]
    data.append({'latitud':1,'mes':mes})
    data=json.dumps(data)
    return HttpResponse(data, content_type='application/json')



#SECCION:
    # EVOTRANSPIRACION P
@login_required(login_url='/')
def confEt0Penman(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if request.user.has_perm('agricultura.add_agr_parcela') or request.user.has_perm('agricultura.list_agr_parcela'):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            dic['datos_subareas']=subareas
            dic['datos_parcela']=parcelas
            dic['datos_suelo']=agr_tipo_suelo.objects.all()
            dic['regiones']=reg_coef_expe.objects.all()
            return render_to_response('agricultura/configuracion/templateEt0Pemman.html',dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

def verParametrosRiegoPlantilla():
    '''Ver todos los tipos de parametros que existen
    Devuelve datos serializados
    :param request: Informacion de la pagina de donde viene la peticion
    :return data: Tipo JSON
    :exception Si existe un error cuando se realiza la busquedad
    '''
    riego=agr_tipo_riego.objects.all()
    tiposParametro=[]
    riegoP=[]
    general=[]
    dict=[]
    for i in riego:
        try:
            nombreArchivo=str(i.rie_formulas.url).split('/media/archivo_formulas/')
            archivo=nombreArchivo[1]
        except:
            archivo='Sin cargar'
        riegoP.append({'riego_id':i.rie_id, 'riego_nombre':i.rie_nombre,
                          'riego_formula':archivo})
        dict={
            'riego':riegoP,
            'tipos':tiposParametro
        }
        riegoP=[]
        general.append(dict)
    for i in general:
        print(i)
    return general


import os, mimetypes
from StringIO import StringIO

@login_required(login_url='/')
def descargarExcel(request, id):
    try:
        if(id=='f'):
            image = StringIO(file('static/agricultura/img/formato.xlsx', "rb").read())
            mimetype = mimetypes.guess_type(os.path.basename('static/agricultura/img/formato.xlsx'))[0]
        else:
            descargar=agr_tipo_riego.objects.get(rie_id=id).rie_formulas.path
            image = StringIO(file(descargar, "rb").read())
            mimetype = mimetypes.guess_type(os.path.basename(descargar))[0]
        return JsonResponse(image.read(), safe=True)
        # return response
    except Exception as error:
        
        dic = {
            'result': 'Error al descargar'
        }
        data=json.dumps(dic)
        return HttpResponse(data, content_type='application/json')
