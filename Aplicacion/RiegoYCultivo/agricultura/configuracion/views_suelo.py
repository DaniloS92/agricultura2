__author__ = 'Dyuyay'
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect
from ..models import *
import json
from django.contrib import messages
from hastamientras import *
import openpyxl
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from agricultura.metodos import *

#SECCION
    #Tipo de Suelo

@login_required(login_url='/')
def tipoSuelo(request):
    ''' Mostrar todos los tipos de Suelo
    Devuelve un diccionario de datos
    :param request:
    :return dic:
    '''
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.is_ajax():
            datos = agr_tipo_suelo.objects.all().order_by("-tsue_id").values('tsue_id','tsue_nombre')
            return HttpResponse(json.dumps({"data":list(datos)},cls=DjangoJSONEncoder),content_type = "application/json; charset=utf8")
        if request.user.has_perm('agricultura.add_agr_tipo_suelo') or request.user.has_perm('agricultura.list_agr_tipo_suelo'):
            dic['datos_parametros']=agr_parametro.objects.all()
            dic['datos_tipos']=agr_tipo_parametro.objects.all()
            dic['datos_suelo']=agr_tipo_suelo.objects.all()
            return render_to_response('agricultura/configuracion/templateTipoSuelo.html', dic,context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

def agregar_tipo_suelo(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            try:
                id = request.POST["id_suelo"]
                nombreSuelo_local=request.POST['txt_nombre']
                if (id == ''):
                    try:
                        guardarTipoSuelo = agr_tipo_suelo(tsue_nombre=nombreSuelo_local)
                        guardarTipoSuelo.tsue_imagen = request.FILES['file1']
                    except Exception as error:
                        print('error',error)
                    guardarTipoSuelo.save()
                    if request.is_ajax():    
                        return HttpResponse(json.dumps({"mensaje":"Guardado Correctamente", "id":guardarTipoSuelo.tsue_id}),content_type = "application/json; charset=utf8")
                    else:
                        messages.add_message(request, messages.SUCCESS, 'Guardado con exito')
                else:
                    try:
                        buscarSuelo = agr_tipo_suelo.objects.get(tsue_id=id)
                        buscarSuelo.tsue_nombre=nombreSuelo_local
                        imagen = request.FILES['file1']
                        buscarSuelo.tsue_imagen = imagen
                    except Exception as error:
                        print('error', error)
                    buscarSuelo.save()
                    messages.add_message(request, messages.SUCCESS, 'Registro Actualizado')
            except Exception as e:
                print(e)
                messages.add_message(request, messages.ERROR, e.message)
            return redirect('/agricultura/tiposSuelo/')
    else:
        return redirect('/')

def eliminar_suelo(request):
    '''Eliminar Tipo de Suelo
    Devuelve datos serializados
    :param request: Informacion de la pagina de donde viene la peticion
    :return dic: Tipo JSON
    :exception Si existe un error cuando se realiza la busquedad y se da de baja
    '''
    try:
        id = request.GET['id']
        suelo=agr_tipo_suelo.objects.get(tsue_id=id)
        suelo.delete()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

def baja_suelo(request):
    id = request.GET['id']
    estado = request.GET['estado']
    try:
        eliminar=agr_tipo_suelo.objects.get(tsue_id=id)
        if(estado=='true'):
            eliminar.tsue_estado=True
        else:
            eliminar.tsue_estado=False
        eliminar.save()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

def ver_datos_suelo(request, id):
    data = []
    try:
        vsuelo = agr_tipo_suelo.objects.get(tsue_id = id)
        data.append({
            'id' : vsuelo.tsue_id,
            'nombre' : vsuelo.tsue_nombre,
            'imagen' : vsuelo.tsue_imagen.url,
            })
        return JsonResponse(data, safe=False)
    except Exception as error:
        print(error)
        data = []
    return JsonResponse(data, safe=False)

def valores_suelo(request):
    if request.method == "POST":
        try:
            id= request.POST["id"]
            tipos = agr_parametro.objects.all()
            for tip in tipos:
                idVa='%s-%s'%('tipo',tip.par_id)
                if idVa not in request.POST:
                    print('no existe ese',idVa)
                else:
                    existeSueloParametro=agr_tipo_suelo_parametros.objects.filter(tipo_suelo__tsue_id=id, parametros=tip)
                    if(existeSueloParametro):#Mando a actualizar los valores
                        if(request.POST[idVa]!=""):
                                existe=agr_tipo_suelo_parametros.objects.get(tipo_suelo__tsue_id=id, parametros=tip)
                                existe.valor=request.POST[idVa]
                                existe.save()
                    else:#Mando a guardar los valores
                        if(request.POST[idVa]!=""):
                            guardar=agr_tipo_suelo_parametros(tipo_suelo__tsue_id=id, parametros__par_id=tip.par_id, valor=request.POST[idVa])
                            guardar.save()
            messages.add_message(request, messages.SUCCESS, 'Se guardo con exito')
        except Exception as e:
            print(e)
            messages.add_message(request, messages.ERROR, e.message)
        return redirect('/agricultura/tiposSuelo/')


def tipos_parametros_suelo(request):
    data=[]
    id = request.GET['id_suelo']
    sueloQuery=agr_tipo_suelo.objects.get(tsue_id=id)
    try:
        #SABER LOS PARAMETROS DE ESPECIE
        grupo=agr_grupo.objects.get(gru_nombre='Tipo de Suelo')
        tipos_parametros=agr_tipo_parametro.objects.filter(tpar_grupo=grupo)
        data.append({'contParametrosSuelo': tipos_parametros.count()})
        for i in tipos_parametros:
            data.append({'id': i.tpar_id,
                         'nombre': i.tpar_nombre})
            parametros = agr_parametro.objects.filter(par_tpar__tpar_id=i.tpar_id)
            data.append({'contTipos': parametros.count()})
            for j in parametros:
                try:
                    parametros_valores = agr_tipo_suelo_parametros.objects.filter(tipo_suelo=sueloQuery).get(parametros=j)
                    data.append(
                        {'nombreTipo': j.par_nombre, 'idTipo': j.par_id, 'valor': parametros_valores.valor})
                except Exception as e:
                    print e
                    data.append(
                    {'nombreTipo': j.par_nombre, 'idTipo': j.par_id, 'valor': ''})
    except Exception as error:
        print(error)
    data=json.dumps(data)
    return HttpResponse(data, content_type='application/json')