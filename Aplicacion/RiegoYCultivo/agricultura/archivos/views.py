__author__ = 'Dyuyay'
import json

from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from urlparse import urlparse

from ..models import *
from agricultura.metodos import *

@login_required(login_url="/")
def archivos(request, parcela_id):
    if request.user.is_authenticated():
        dic = armaMenu(request.user.get_all_permissions())
        dic['usuario'] = request.user
        dic['persona'] = obtenerPersona(request)
        dic['roles'] = obtenerRoles(request)
        dic['empresa'] = getEmpresa(request)
        dic['tipo_archivos'] = agr_tipo_archivo.objects.all()
        areas = proy_area.objects.filter(emp_id_id=dic['empresa'])
        subareas = proy_subarea.objects.filter(ar_id=areas)
        dic['parcelas'] = agr_parcela.objects.filter(par_id=subareas)
        if(parcela_id == "all"):
            if request.user.has_perm('agricultura.add_agr_archivo') or request.user.has_perm(
                    'agricultura.list_agr_archivo'):
                dic['archivos'] = agr_archivo.objects.all()
                dic['esGeneral'] = True
                return render(request, "agricultura/archivos/templateArchivos.html", dic)
            else:
                return render_to_response("seguridad/errores/error_403.html", dic,
                                          context_instance=RequestContext(request))
        else:
            if request.user.has_perm('agricultura.add_agr_archivo') or request.user.has_perm(
                    'agricultura.list_agr_archivo'):
                dic['parcela'] = get_object_or_404(agr_parcela, pk=parcela_id)
                dic['archivos'] = agr_archivo.objects.filter(par_id = parcela_id)
                return render(request, 'agricultura/archivos/templateArchivos.html', dic)
            else:
                return render_to_response("seguridad/errores/error_403.html", dic,
                                          context_instance=RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url="/")
def guardarArchivo(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            tipo_archivo = agr_tipo_archivo.objects.get(pk=request.POST.get("select_archivo",""))
            archivo = agr_archivo()
            archivo.arc_estado = request.POST.get("estArchivo","")
            archivo.arc_nombre = request.POST.get("nomArchivo","")
            archivo.arc_uri = request.FILES.get("fileArchivo","")
            parcela = agr_parcela.objects.get(pk=request.POST.get("select_parcela", ""))
            archivo.par_id = parcela
            archivo.tarc_id = tipo_archivo
            archivo.save()
            messages.add_message(request, messages.SUCCESS, "Archivo Guardado")
            return redirect(reverse("agricultura:url_archivos_por_parcela", args=(archivo.par_id.pk,)))
    else:
        return redirect("/")

@login_required(login_url="/")
def ver_datos_archivo(request):
    id = request.GET.get("id","")
    data = {}
    try:
        archivo = agr_archivo.objects.get(pk=id)
        data = {
            'pk':archivo.pk,
            'uri':archivo.arc_uri.url,
            'nombre':archivo.arc_nombre,
            'tipo':archivo.tarc_id.pk,
            'estado':archivo.arc_estado
        }
        return JsonResponse(data, safe=False)
    except Exception as err:
        print err
        data = []
    return JsonResponse(data, safe=False)

@login_required(login_url="/")
def modificar_datos_archivo(request):
    try:
        dic = []
        archivoeditar = agr_archivo.objects.get(pk=request.POST.get("id",""))
        archivoeditar.arc_nombre = request.POST.get("nomArchivo","")
        archivoeditar.tarc_id = agr_tipo_archivo.objects.get(pk=request.POST.get("select_archivo", ""))
        archivoeditar.arc_uri = request.FILES.get("fileArchivo", "")
        archivoeditar.save()
        dic = {'result': 'OK'}
        messages.add_message(request, messages.SUCCESS, 'Registro Actualizado')
    except Exception as error:
        print(error)
        dic = {'result': 'Error al modificar'}
    # data = json.dumps(dic)
    return JsonResponse(dic)

@login_required(login_url="/")
def dar_dbaja_archivo(request):
    id = request.GET.get('id',"")
    estado = request.GET.get('estado', "")
    try:
        bajaArchivo = agr_archivo.objects.get(pk=id)
        if (estado == 'true'):
            bajaArchivo.arc_estado = True
        else:
            bajaArchivo.arc_estado = False
        bajaArchivo.save()
        dic = {'result': 'OK'}
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja el archivo'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="application/json")