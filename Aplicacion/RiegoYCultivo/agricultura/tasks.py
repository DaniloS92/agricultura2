# -*- coding: utf-8 -*-
#from celery import shared_task
from models import *
from iot.models import *
from .metodos import cargarMenus
from app_seguridad.models import seg_menu
import openpyxl
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect
from django.contrib import messages

#@shared_task()
def defaultConfiguracion():
    try:
        seg_menu.objects.all().delete()
        cargarMenus()

        #Abrir el excel
        documento=openpyxl.load_workbook('bd.xlsx')
    #   Categoria Especie
        hoja=documento.get_sheet_by_name('categoria')
        bc=agr_especie_categoria.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            guardarCategoria=agr_especie_categoria(cat_nombre=hoja['B' + str(row)].value, cat_observacion=hoja['C' + str(row)].value, cat_estado=hoja['D' + str(row)].value)
            guardarCategoria.save()
        print('Se guardo Categoria---------------------------------------')
        #Especie
        hoja=documento.get_sheet_by_name('especie')
        bc=agr_especie_cultivo.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            categoriaBusquedad=agr_especie_categoria.objects.get(cat_nombre=hoja['F' + str(row)].value)
            guardarEspecie=agr_especie_cultivo(esp_nombre=hoja['B' + str(row)].value, esp_imagen=hoja['C' + str(row)].value, esp_estado=hoja['D' + str(row)].value,
                                               esp_categoria=categoriaBusquedad )
            guardarEspecie.save()
        print('Se guardo Especie---------------------------------------')
        #Unidad de Medida
        hoja=documento.get_sheet_by_name('medida')
        bc=r_unidad_medida.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            medida=r_unidad_medida(umed_abreviatura=hoja['B' + str(row)].value)
            medida.save()
        print('Se guardo Unidad Medida')
        #Grupo
        hoja=documento.get_sheet_by_name('grupo')
        bc=agr_grupo.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            tipos=agr_grupo(gru_nombre=hoja['B' + str(row)].value)
            tipos.save()
        print('Se guardo Grupos')

        #Fases
        hoja=documento.get_sheet_by_name('fases')
        bc=agr_fase.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            tipos=agr_fase(fas_nombre=hoja['B' + str(row)].value,fas_descripcion=hoja['C' + str(row)].value)
            tipos.save()
        print('Se guardo Fases')

        #Tipos Parametros
        hoja=documento.get_sheet_by_name('tipos_parametros')
        bc=agr_tipo_parametro.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            buscar=agr_grupo.objects.get(gru_nombre=hoja['E' + str(row)].value)
            tipos=agr_tipo_parametro(tpar_nombre=hoja['B' + str(row)].value,tpar_abreviatura=hoja['C' + str(row)].value,
                                     tpar_descripcion=hoja['D' + str(row)].value,tpar_grupo=buscar)
            tipos.save()
        print('Se guardo Tipos de Parametros')

        #Parametros
        hoja=documento.get_sheet_by_name('parametros')
        bc=agr_parametro.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            buscar=r_unidad_medida.objects.get(umed_abreviatura=hoja['H' + str(row)].value)
            buscarTipo=agr_tipo_parametro.objects.get(tpar_nombre=hoja['J' + str(row)].value)
            buscarFase = agr_fase.objects.get(fas_nombre=hoja['I' + str(row)].value)
            parametros=agr_parametro(par_nombre=hoja['B' + str(row)].value,par_descripcion=hoja['D' + str(row)].value,
                                     par_abreviatura=hoja['C' + str(row)].value,par_estado=hoja['K' + str(row)].value,
                                     par_valor_minimo=hoja['E' + str(row)].value,par_valor_maximo=hoja['F' + str(row)].value,
                                     par_valor=hoja['G' + str(row)].value, par_umedida=buscar,
                                     par_tpar=buscarTipo, par_fase=buscarFase)
            parametros.save()
        print('Se guardo Parametros')

        #Riego
        hoja=documento.get_sheet_by_name('riego')
        bc=agr_tipo_riego.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            riego=agr_tipo_riego(rie_nombre=hoja['B' + str(row)].value,rie_descripcion=hoja['C' + str(row)].value)
            riego.save()
        print('Se guardo Tipos de riego')

        #Parametros Valores ESPECIE
        hoja=documento.get_sheet_by_name('pe_valores')
        bc=agr_especie_parametros.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            buscarE=agr_especie_cultivo.objects.get(esp_nombre=hoja['D' + str(row)].value)
            buscarT=agr_parametro.objects.get(par_nombre=hoja['C' + str(row)].value)
            pev=agr_especie_parametros(valor=hoja['B' + str(row)].value,tipo_id=buscarT, esp_id=buscarE)
            pev.save()
        print('Se guardo Especie Parametros Valores')

        #Tipo de Suelo
        hoja=documento.get_sheet_by_name('suelo')
        bc=agr_tipo_suelo.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            tsuelo=agr_tipo_suelo(tsue_nombre=hoja['B' + str(row)].value)
            tsuelo.save()
        print('Se guardo Tipo de suelo')

        #Suelo Valores
        hoja=documento.get_sheet_by_name('sueloValores')
        bc=agr_tipo_suelo_parametros.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            buscarT=agr_parametro.objects.get(par_nombre=hoja['C' + str(row)].value)
            buscarS=agr_tipo_suelo.objects.get(tsue_nombre=hoja['B' + str(row)].value)
            suelov=agr_tipo_suelo_parametros(valor=hoja['D' + str(row)].value,parametros=buscarT, tipo_suelo=buscarS)
            suelov.save()
        print('Se Tipo de suelo valores')
        #latitud
        hoja=documento.get_sheet_by_name('latitud')
        bc=agr_latitud.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            vLatitud = agr_latitud(lat_valor=hoja['B' + str(row)].value)
            vLatitud.save()
        print('Se guardo las latitudes por defecto')
        #meses
        hoja=documento.get_sheet_by_name('mes')
        bc=agr_mes.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            vMes = agr_mes(mes_nombre=hoja['B' + str(row)].value)
            vMes.save()
        print('Se guardo los meses')
        #relacion latitud con mes
        hoja=documento.get_sheet_by_name('radiacion')
        bc=agr_lat_mes.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            buscarM=agr_mes.objects.get(mes_nombre=hoja['C' + str(row)].value)
            buscarL=agr_latitud.objects.get(lat_valor=hoja['B' + str(row)].value)
            vMesLat = agr_lat_mes(latitud=buscarL,mes=buscarM,valor=hoja['D' + str(row)].value,lat_hemisferio=hoja['E' + str(row)].value)
            vMesLat.save()
        print('Se guardo la latitud con el mes')
        #Formulas
        hoja=documento.get_sheet_by_name('formulas')
        bc=agr_formulas_riego.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            riego_busqueda=agr_tipo_riego.objects.get(rie_nombre=hoja['F' + str(row)].value)
            medida_busqueda=r_unidad_medida.objects.get(umed_abreviatura=hoja['D' + str(row)].value)
            guardarFormula=agr_formulas_riego(for_nombre=hoja['B' + str(row)].value, for_descripcion=hoja['G' + str(row)].value, for_formula=hoja['C' + str(row)].value,
                                              for_tipos=hoja['E' + str(row)].value, u_medida=medida_busqueda, tipo_riego=riego_busqueda, for_general=hoja['H' + str(row)].value)
            guardarFormula.save()
        # Dispositivos
        hoja=documento.get_sheet_by_name('dispositivos')
        bc=iot_dispositivo.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            # tecnologia = iot_tecnologia.objects.get(tec_id=hoja['H' + str(row)].value)
            empresa = proy_empresa.objects.get(emp_nombre_comercial='IOTMACH')
            vDis = iot_dispositivo(
                dis_nombre=hoja['B' + str(row)].value,
                dis_descripcion = hoja['C' + str(row)].value,
                dis_mac = hoja['D' + str(row)].value,
                dis_fecha_registro = hoja['E' + str(row)].value,
                dis_activo = hoja['F' + str(row)].value,
                dis_localizacion = hoja['H' + str(row)].value,
                emp_id = empresa,
                dis_fecha_ultima_lectura = hoja['G' + str(row)].value)
            vDis.save()
        print('Se guardo los dispositivos')
        # Teds
        hoja=documento.get_sheet_by_name('iot_ted')
        bc=Ted.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            queryU=r_unidad_medida.objects.get(umed_abreviatura=hoja['D' + str(row)].value)
            vTec = Ted(
                ted_name = hoja['B' + str(row)].value,
                ted_template = hoja['C' + str(row)].value,
                um_id=queryU)
            vTec.save()
        print('Se guardo los teds') 
        # Sensores y Actuadores
        hoja=documento.get_sheet_by_name('sensores_actuadores')
        bc=iot_sensor_actuador.objects.all()
        bc.delete()
        for row in range(2, hoja.get_highest_row() + 1):
            findTed = Ted.objects.get(ted_name = hoja['H' + str(row)].value)
            dispositivo = iot_dispositivo.objects.get(dis_nombre=hoja['G' + str(row)].value)
            vTec = iot_sensor_actuador(
                sa_nombre = hoja['I' + str(row)].value,
                sa_interfaz = hoja['B' + str(row)].value,
                sa_categoria = hoja['C' + str(row)].value,
                sa_senial = hoja['D' + str(row)].value,
                sa_estado = hoja['E' + str(row)].value,
                sa_ult_medida = hoja['F' + str(row)].value,
                dis_id = dispositivo,
                ted_id = findTed)
            vTec.save()
        print('Se guardo los sensores y actuadores')                
        

        

#horas luz para calculo de Penman
        hoja=documento.get_sheet_by_name('horas_luz_mensual')
        bc=agr_lat_mes_horas_luz.objects.all()
        if bc.exists():
            bc.delete()
        bc=agr_mes_horas_luz.objects.all()
        if bc.exists():
            bc.delete()
        bc=agr_latitud_horas_luz.objects.all()
        if bc.exists():
            bc.delete()
        for colum in hoja.rows[0]:     
            if (colum.value !="hemisferio" and colum.value !="latitud"):
                mes = agr_mes_horas_luz(mes_nombre = colum.value,)
                mes.save()
        for col in hoja['B2':'B19']:
            latitud_existe=agr_latitud_horas_luz.objects.filter(lat_valor=col[0].value)
            if latitud_existe.exists()==False:
                lat=agr_latitud_horas_luz(lat_valor=col[0].value)
                lat.save()
        for rows in hoja['A2':'N19']:
            hemisferio=rows[0].value
            latitud=rows[1].value
            for celda in rows:
                if openpyxl.utils.column_index_from_string(celda.column)>2:
                    mes=hoja.cell(row = 1, column = openpyxl.utils.column_index_from_string(celda.column)).value
                    lat_obj=agr_latitud_horas_luz.objects.filter(lat_valor=int(latitud))
                    mes_obj=agr_mes_horas_luz.objects.filter(mes_nombre=mes)                    
                    porcentaje=agr_lat_mes_horas_luz(latitud = lat_obj[0], mes=mes_obj[0],valor=celda.value ,lat_hemisferio=hemisferio,)
                    porcentaje.save()
        print('Se guardaron las tablas de maximas horas luz') 





#radiacion para calculo de Penman
        hoja=documento.get_sheet_by_name('radiacion_mensual')
        bc=agr_lat_mes_radiacion.objects.all()
        if bc.exists():
            bc.delete()
        bc=agr_mes_radiacion.objects.all()
        if bc.exists():
            bc.delete()
        bc=agr_latitud_radiacion.objects.all()
        if bc.exists():
            bc.delete()
        for colum in hoja.rows[0]:     
            if (colum.value !="hemisferio" and colum.value !="latitud"):
                mes = agr_mes_radiacion(mes_nombre = colum.value,)
                mes.save()
        for col in hoja['B2':'B27']:
            latitud_existe=agr_latitud_radiacion.objects.filter(lat_valor=col[0].value)
            if latitud_existe.exists()==False:
                lat=agr_latitud_radiacion(lat_valor=col[0].value)
                lat.save()
        for rows in hoja['A2':'N27']:
            hemisferio=rows[0].value
            latitud=rows[1].value
            for celda in rows:
                if openpyxl.utils.column_index_from_string(celda.column)>2:
                    mes=hoja.cell(row = 1, column = openpyxl.utils.column_index_from_string(celda.column)).value
                    lat_obj=agr_latitud_radiacion.objects.filter(lat_valor=int(latitud))
                    mes_obj=agr_mes_radiacion.objects.filter(mes_nombre=mes)                    
                    "guadar= latitud:"+str(latitud) +"-mes:"+mes+"-valor:"+str(celda.value)+"-hemisferio:"+hemisferio
                    porcentaje=agr_lat_mes_radiacion(latitud = lat_obj[0], mes=mes_obj[0],valor=celda.value ,lat_hemisferio=hemisferio,)
                    porcentaje.save()
        print('Se guardaron las tablas de radiacion') 


#coeficiente experimental de region para penman
        bc=reg_coef_expe.objects.all()
        if bc.exists():
            bc.delete()
        hoja=documento.get_sheet_by_name('coeficientes_experimentales')
        rango=hoja['A2':'C4']
        for fila in rango:
            coeficiente=reg_coef_expe(region=fila[0].value,valor_a1=fila[1].value,valor_b1=fila[2].value,)
            coeficiente.save()
        print ("Se guardaron las coeficientes experimentales para tipos de suelo almacenados")


        hoja=documento.get_sheet_by_name('tipo_archivo')
        bc=agr_tipo_archivo.objects.all()
        bc.delete()
        rango=hoja['A2':'B6']
        for fila in rango:
            archivo=agr_tipo_archivo(tarc_nombre=fila[0].value,tarc_icono=fila[1].value)
            archivo.save()
        print('Se guardo los tipos de archivo por defecto')                

        hoja=documento.get_sheet_by_name('tipo_plagas')
        bc=agr_tipo_plaga_enfermedad.objects.all()
        bc.delete()
        rango=hoja['A2':'A7']
        for fila in rango:
            plaga=agr_tipo_plaga_enfermedad(tpe_nombre=fila[0].value)
            plaga.save()
        print('Se guardo las plaga por defecto')      

        print('La restauracion se efectuo con exito')
    except Exception as e:
        print(e)
