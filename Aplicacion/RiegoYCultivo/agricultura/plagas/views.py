# -*- coding: utf-8 -*-
__author__ = 'Dyuyay'
import json
from datetime import timedelta
import datetime

from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from django.contrib import messages
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, Http404
from django.contrib.auth.decorators import login_required

from django.views.decorators.csrf import csrf_exempt
from agricultura.metodos import *
from agricultura.models import *

from app_empresa.models import proy_empresa

@csrf_exempt
@login_required(login_url="/")
def guardar_plaga(request):
    if request.is_ajax():
        cultivo = request.POST.get("cultivo_id","")
        tipo = request.POST.get("tipo_plaga","")
        fecha = request.POST.get("fecha","")
        cant_plantas = request.POST.get("pa_cant_plantas","")
        cant_hectareas = request.POST.get("pa_cant_hectareas","")
        observaciones = request.POST.get("pa_observaciones","")
        accion = request.POST.get("accion","")
        id = request.POST.get("plaga_id","")
        if tipo == "" or fecha == "" or cant_plantas == "" or cant_hectareas == "" or observaciones == "" \
            or cultivo == "":
            dic = {'result': 'No ha sido ingresados todos los datos'}
        else:
            try:
                plaga_crear = agr_plaga_enfermedad() if accion == "0" and id == "" else agr_plaga_enfermedad.objects.get(pk=id)
                plaga_crear.pe_fecha_identificacion = fecha
                plaga_crear.pe_cant_plantas_afectadas = cant_plantas
                plaga_crear.pe_cant_hectareas = cant_hectareas
                plaga_crear.pe_observaciones = observaciones
                cultivo_obj = agr_cultivo.objects.get(pk=cultivo)
                tipo_plaga = agr_tipo_plaga_enfermedad.objects.get(pk=tipo) 
                plaga_crear.cul_id = cultivo_obj
                plaga_crear.tpe_id = tipo_plaga
                plaga_crear.save()
                dic = {'result': 'OK'}
            except Exception as e:
                print e
                dic = {'result': 'Error al dar de baja la cultivo'}
            data = json.dumps(dic)
            return HttpResponse(data, content_type="aplication/json")
    else:
        raise Http404("Nothing to see here")

@login_required(login_url="/")
def dar_dbaja_plaga(request):
    id = request.GET['id']
    estado = request.GET['estado']
    try:
        bajaPlaga = agr_plaga_enfermedad.objects.get(pk = id)
        if (estado=='true'):
            bajaPlaga.pe_estado=True
        else:
            bajaPlaga.pe_estado=False
        bajaPlaga.save()
        dic = {'result': 'OK'}
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja la parcela'
        }
    data = json.dumps(dic)
    return HttpResponse(data, content_type="application/json")