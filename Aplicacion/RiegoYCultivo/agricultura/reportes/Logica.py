#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from agricultura.views import *
import json
from django.contrib import messages
from django.db.models import Q #para hacer filtros or en un queryset
from agricultura.models import *
import os
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_LEFT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, Flowable
from reportlab.lib.utils import ImageReader
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, inch
from reportlab.lib import colors
from agricultura.metodos import *


##### variables globales
estilos = getSampleStyleSheet()

estilos.add(ParagraphStyle(name="centrado",
                           alignment=TA_CENTER
))
estilos.add(ParagraphStyle(name="izquierda",
                           alignment=TA_LEFT
))

styles = getSampleStyleSheet()

styleN = styles["BodyText"]
styleN.alignment = TA_LEFT
styleN.fontSize=6.2

styleBH = styles["Normal"]
styleBH.alignment = TA_CENTER
styleBH.fontSize=8

PAGE_WIDTH = A4[0]
PAGE_HEIGHT = A4[1]
logoEmpresa = ''
vEmpresa = ''

# REPORTES DE ESPECIES
@login_required(login_url='/')
def lista_cultivos(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        dict['epecies']=agr_cultivo.objects.all()
        print(request.user.get_all_permissions())
        return render_to_response('reportes/templateLista.html', dict, context_instance = RequestContext(request))
    else:
        return redirect('/')
'''
def frmReporteProyecto(request, codigo):
    vUsuRol=seg_usuario_rol.objects.filter(usu_id=request.user.id)
    dict = armaMenu(vUsuRol, request.user)
    dict['usuario'] = request.user
    dict['persona'] = obtenerPersona(request)
    dict['roles'] = obtenerRoles(request)
    dict['empresa']=getEmpresa(request)
    if not p_proyecto.objects.filter(emp_id=dict['empresa'].id).exists():
        return render_to_response('sincronizacion/msj_sincronizacion.html', dict,context_instance = RequestContext(request))
    else:
        dict['tituloReporte']='DATOS INFORMATIVOS DE '+p_proyecto.objects.get(id=codigo).proy_nombre.upper()
        dict['urlReporte'] = '/reportes/proyecto/datos/'+str(codigo)
        return render_to_response('reportes/reporte.html', dict, context_instance = RequestContext(request))

def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.line(30, A4[1] - 50, 560, A4[1] - 50)
    canvas.line(30, 0.80*inch, 560, 0.80*inch)
    canvas.drawString(535, 0.65 * inch, "| %d" %(doc.page))
    nuevoLogo = ImageReader('static/recursos/images/ico/focoDYuyay.ico')
    canvas.drawImage(nuevoLogo, 30,0.55 * inch, width=15, height=15)

    canvas.restoreState()

#Definimos disposiciones alternas para las caracteristicas de las otras páginas
def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.line(30, 0.80*inch, 560, 0.80*inch)
    canvas.line(30, A4[1] - 50, 560, A4[1] - 50)
    nuevoLogo = ImageReader('media/'+logoEmpresa)
    canvas.drawImage(nuevoLogo, 30,A4[1] - 40, width=20, height=20)
    canvas.drawString(55, A4[1]-40, vEmpresa.emp_nombre_juridico)
    canvas.drawString(450, A4[1]-40, vEmpresa.emp_ruc)
    canvas.drawString(535, 0.65 * inch, "| %d" %(doc.page))
    nuevoLogo = ImageReader('static/recursos/images/ico/focoDYuyay.ico')
    canvas.drawImage(nuevoLogo, 30,0.55 * inch, width=15, height=15)


@login_required(login_url='/')
def datos_empresa(request):
    global vEmpresa
    vEmpresa=getEmpresa(request)

    response = HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'filename=reporte'+vEmpresa.emp_nombre_comercial+'-'+vEmpresa.emp_ruc+'.pdf'


    doc = SimpleDocTemplate(response, pagesize=A4,
                            rightMargin=30, leftMargin=30,
                            topMargin=55, bottomMargin=55 )


    documento = []

    #documento.append(Spacer(1, 20))

    global logoEmpresa
    logoEmpresa=vEmpresa.emp_logo.name

    f1=Image('media/'+vEmpresa.emp_logo.name, width=None, height=None)
    f1.drawHeight = 2*cm
    f1.drawWidth = 2*cm
    documento.append(f1)
    documento.append(Spacer(1, 5))

    texto = """<font size=15><b>"""+vEmpresa.emp_nombre_juridico.upper()+"""</b></font>"""
    documento.append(Paragraph(texto, estilos['centrado']))
    documento.append(Spacer(1, 5))
    texto = """<font size=8><b>R. U. C.:"""+vEmpresa.emp_ruc.upper()+"""</b></font>"""
    documento.append(Paragraph(texto, estilos['centrado']))
    documento.append(Spacer(1, 25))

    texto = """<font size=12><b>1. <u>Datos de la Empresa</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 15))

    nombresJ=Paragraph("""<font size=10><b>NOMBRE JURIDICO:</b> <br/>"""+vEmpresa.emp_nombre_juridico+"""</font>""", estilos['izquierda'])
    nombresC=Paragraph("""<font size=10><b>NOMBRE COMERCIAL:</b> <br/>"""+vEmpresa.emp_nombre_comercial+"""</font>""", estilos['izquierda'])
    direccion = Paragraph("""<font size=10><b>DIRECCION:</b> <br/>"""+vEmpresa.emp_direccion+"""</font>""", estilos['izquierda'])
    correo = Paragraph("""<font size=10><b>CORREO ELECTRONICO:</b> <br/> """+vEmpresa.emp_email+"""</font>""", estilos['izquierda'])
    direccion_web = Paragraph("""<font size=10><b>PAGINA WEB:</b> <br/>"""+vEmpresa.emp_web+"""</font>""", estilos['izquierda'])
    ruc = Paragraph("""<font size=10><b>R. U. C.:</b> <br/> """+vEmpresa.emp_ruc+"""</font>""", estilos['izquierda'])

    data= [[ruc,direccion],
           [nombresJ, nombresC],
           [correo,direccion_web],
           ]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey),
        #('SPAN',(1,1),(0,1)),
        ],colWidths=[263,263])
    documento.append(t)

    documento.append(Spacer(1, 20))

    texto = """<font size=12><b>2. <u>Datos del Representante</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 15))

    vRepresentante = proy_representante.objects.get(emp_id=vEmpresa.id)

    nombres=Paragraph("""<font size=10><b>NOMBRES:</b> <br/>"""+vRepresentante.per_id.per_nombres+"""</font>""", estilos['izquierda'])
    apellidos=Paragraph("""<font size=10><b>APELLIDOS:</b> <br/>"""+vRepresentante.per_id.per_apellidos+"""</font>""", estilos['izquierda'])
    direccion=Paragraph("""<font size=10><b>DIRECCION:</b> <br/>"""+vRepresentante.per_id.per_direccion+"""</font>""", estilos['izquierda'])
    email=Paragraph("""<font size=10><b>CORREO ELECTRONICO:</b> <br/>"""+vRepresentante.per_id.user_id.email+"""</font>""", estilos['izquierda'])
    tlfono_movil = Paragraph("""<font size=10><b>TELEFONO MOVIL:</b> <br/>"""+vRepresentante.per_id.per_telef_movil+"""</font>""", estilos['izquierda'])
    tlfono_fijo = Paragraph("""<font size=10><b>TELEFONO FIJO:</b> <br/>"""+vRepresentante.per_id.per_telef_fijo+"""</font>""", estilos['izquierda'])
    fInicio = Paragraph("""<font size=10><b>FECHA DE INICIO:</b> <br/>"""+vRepresentante.rep_fecha_inicio.isoformat()+"""</font>""", estilos['izquierda'])
    fFin = Paragraph("""<font size=10><b>FECHA DE FINALIZACION:</b> <br/>"""+vRepresentante.rep_fecha_fin.isoformat()+"""</font>""", estilos['izquierda'])
    descripcion = Paragraph("""<font size=10><b>DESCRIPCION:</b> <br/>"""+vRepresentante.rep_descripcion+"""</font>""", estilos['izquierda'])
    data= [[nombres,apellidos],
           [direccion,email],
           [tlfono_movil, tlfono_fijo],
           [fInicio,fFin],
           [descripcion,'']
           ]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey),
        ('SPAN',(0,4),(-1,-1)),
        ],colWidths=[263,263])
    documento.append(t)
    documento.append(Spacer(1, 20))

    doc.build(documento, onFirstPage = myFirstPage, onLaterPages = myLaterPages)
    return response


vProyecto=''

def datos_proyecto(request, codigo):
    global vProyecto
    global vEmpresa

    vEmpresa=getEmpresa(request)

    vProyecto=p_proyecto.objects.get(id=codigo)

    response = HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'filename=reporte'+vProyecto.proy_nombre+'-'+vEmpresa.emp_nombre_comercial+'.pdf'


    doc = SimpleDocTemplate(response, pagesize=A4,
                            rightMargin=30, leftMargin=30,
                            topMargin=55, bottomMargin=55 )


    documento = []

    #documento.append(Spacer(1, 20))

    global logoEmpresa
    logoEmpresa=vEmpresa.emp_logo.name

    documento.append(Spacer(1, 10))
    f1=Image('media/'+vEmpresa.emp_logo.name, width=None, height=None)
    f1.drawHeight = 2*cm
    f1.drawWidth = 2*cm
    documento.append(f1)
    documento.append(Spacer(1, 5))

    texto = """<font size=15><b>"""+vEmpresa.emp_nombre_juridico.upper()+"""</b></font>"""
    documento.append(Paragraph(texto, estilos['centrado']))
    documento.append(Spacer(1, 5))
    texto = """<font size=8><b>R. U. C.:"""+vEmpresa.emp_ruc.upper()+"""</b></font>"""
    documento.append(Paragraph(texto, estilos['centrado']))
    documento.append(Spacer(1, 30))
    texto = """<font size=13><b> PROYECTO """+vProyecto.proy_nombre.upper()+"""</b></font>"""
    documento.append(Paragraph(texto, estilos['centrado']))
    documento.append(Spacer(1, 30))
    texto = """<font size=12><b>1. <u>Datos del Proyecto</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    nombresProy=Paragraph("""<font size=10><b>NOMBRE DE PROYECTO:</b> <br/>"""+vProyecto.proy_nombre+"""</font>""", estilos['izquierda'])
    descripcion=Paragraph("""<font size=10><b>DESCRIPCION:</b> <br/>"""+vProyecto.proy_descripcion+"""</font>""", estilos['izquierda'])
    fInicio = Paragraph("""<font size=10><b>FECHA DE INICIO:</b> <br/>"""+vProyecto.proy_fecha_ini.isoformat()+"""</font>""", estilos['izquierda'])
    fFin= Paragraph("""<font size=10><b>FECHA DE FINALIZACION:</b> <br/> """+vProyecto.proy_fecha_fin.isoformat()+"""</font>""", estilos['izquierda'])
    duracion= Paragraph("""<font size=10><b>DURACION:</b> <br/>"""+str(vProyecto.proy_duracion)+""" dias</font>""", estilos['izquierda'])
    tipo = Paragraph("""<font size=10><b>TIPO DE PROYECTO:</b> <br/> """+vProyecto.tproy_id.tproy_nombre+"""</font>""", estilos['izquierda'])
    sector = Paragraph("""<font size=10><b>SECTOR:</b> <br/> """+vProyecto.sec_id.sec_nombre+"""</font>""", estilos['izquierda'])
    direccion = Paragraph("""<font size=10><b>DIRECCION:</b> <br/> """+vProyecto.proy_direccion+"""</font>""", estilos['izquierda'])
    data= [[nombresProy,descripcion],
           [fInicio,fFin],
           [duracion,tipo],
           [sector, direccion]
           ]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey),
        #('SPAN',(1,1),(0,1)),
        ],colWidths=[263,263])
    documento.append(t)

    documento.append(Spacer(1, 30))

    texto = """<font size=12><b>2. <u>Datos del Administrador</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    vAdministrador = p_administrador.objects.get(proy_id=vProyecto.id)

    nombres=Paragraph("""<font size=10><b>NOMBRES:</b> <br/>"""+vAdministrador.per_id.per_nombres+"""</font>""", estilos['izquierda'])
    apellidos=Paragraph("""<font size=10><b>APELLIDOS:</b> <br/>"""+vAdministrador.per_id.per_apellidos+"""</font>""", estilos['izquierda'])
    direccion=Paragraph("""<font size=10><b>DIRECCION:</b> <br/>"""+vAdministrador.per_id.per_direccion+"""</font>""", estilos['izquierda'])
    email=Paragraph("""<font size=10><b>CORREO ELECTRONICO:</b> <br/>"""+vAdministrador.per_id.user_id.email+"""</font>""", estilos['izquierda'])
    tlfono_movil = Paragraph("""<font size=10><b>TELEFONO MOVIL:</b> <br/>"""+vAdministrador.per_id.per_telef_movil+"""</font>""", estilos['izquierda'])
    tlfono_fijo = Paragraph("""<font size=10><b>TELEFONO FIJO:</b> <br/>"""+vAdministrador.per_id.per_telef_fijo+"""</font>""", estilos['izquierda'])
    fInicio = Paragraph("""<font size=10><b>FECHA DE INICIO DE ADMINISTRACION:</b> <br/>"""+vAdministrador.adm_fecha_ini.isoformat()+"""</font>""", estilos['izquierda'])
    fFin = Paragraph("""<font size=10><b>FECHA DE FINALIZACION DE ADMINISTRACION:</b> <br/>"""+vAdministrador.adm_fecha_fin.isoformat()+"""</font>""", estilos['izquierda'])
    descripcion = Paragraph("""<font size=10><b>DESCRIPCION:</b> <br/>"""+vAdministrador.adm_descripcion+"""</font>""", estilos['izquierda'])
    data= [[nombres,apellidos],
           [direccion,email],
           [tlfono_movil, tlfono_fijo],
           [fInicio,fFin],
           [descripcion,'']
           ]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey),
        ('SPAN',(0,4),(-1,-1)),
        ],colWidths=[263,263])
    documento.append(t)
    documento.append(Spacer(1, 30))

    texto = """<font size=12><b>3. <u>Fases del Proyecto</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    for i in p_fase_proyecto.objects.filter(proy_id=vProyecto.id).order_by('tfas_id__tfas_orden'):
        texto = '<font size=10><b><i>'+str(i.tfas_id.tfas_orden)+') '+i.tfas_id.tfas_nombre+'</i></b></font>'
        documento.append(Paragraph(texto, estilos['izquierda']))
        documento.append(Spacer(1, 15))
        f_inicio=Paragraph("""<font size=10><b>FECHA DE INICIO:</b> <br/>"""+i.fas_fecha_inicio.isoformat()+"""</font>""", estilos['izquierda'])
        f_fin=Paragraph("""<font size=10><b>FECHA DE FINALIZACION:</b> <br/>"""+i.fas_fecha_fin.isoformat()+"""</font>""", estilos['izquierda'])
        descripcion = Paragraph("""<font size=10><b>DESCRIPCION:</b> <br/>"""+i.fas_descripcion+"""</font>""", estilos['izquierda'])
        data= [[f_inicio,f_fin],
               [descripcion,'']
               ]

        t=Table(data,style=[
            ('GRID',(0,0),(-1,-1),0.5,colors.grey),
            ('SPAN',(0,1),(-1,-1)),
            ],colWidths=[263,263])
        documento.append(t)
        documento.append(Spacer(1, 30))


    doc.build(documento, onFirstPage = myFirstPage, onLaterPages = myLaterPages)
    return response
'''