#-*- coding: utf-8 -*-
__author__ = 'Dyuyay'
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse,redirect
from ..models import *
# from ..htmltopdf import render_to_pdf
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_LEFT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, Flowable
from reportlab.lib.utils import ImageReader
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, inch
from reportlab.lib import colors
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from agricultura.metodos import *
from unipath import Path
import os
import requests

estilos = getSampleStyleSheet()

estilos.add(ParagraphStyle(name="centrado",
                           alignment=TA_CENTER
))
estilos.add(ParagraphStyle(name="izquierda",
                           alignment=TA_LEFT
))

styles = getSampleStyleSheet()

styleN = styles["BodyText"]
styleN.alignment = TA_LEFT
styleN.fontSize=6.2

styleBH = styles["Normal"]
styleBH.alignment = TA_CENTER
styleBH.fontSize=8

PAGE_WIDTH = A4[0]
PAGE_HEIGHT = A4[1]
logoEspecie = ''
vCultivo = ''
vEmpresa = ''




@login_required(login_url='/')
def reportes_cultivo(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if (request.user.has_perm('agricultura.print_agr_cultivo') or request.user.has_perm('agricultura.list_agr_cultivo')):
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            parcelas = agr_parcela.objects.filter(par_id=subareas)
            dic['datosCultivo']=agr_cultivo.objects.filter(parcela=parcelas)
            return render_to_response('agricultura/reportes/templateCultivoR.html',dic, context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def frmReporteCultivo(request, codigo):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if (request.user.has_perm('agricultura.print_agr_cultivo') or request.user.has_perm('agricultura.list_arg_cultivo')):
            dic['tituloReporte']='DATOS INFORMATIVOS DE '+str(agr_cultivo.objects.get(cult_id=codigo).cult_detalle.upper())
            dic['urlReporte'] = '/agricultura/reporteC/datos/'+str(codigo)
            return render_to_response('agricultura/reportes/templateReportes.html', dic, context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", dic,context_instance = RequestContext(request))
    else:
        return redirect('/')

# def ver_reporte(request):
# 	return render_to_pdf('agricultura/reportes/prueba.html',{'titulo' : 'Reportes con pdf'})

def datos_Cultivo(request, codigo):
    global vCultivo
    global vParametros
    global vEmpresa


    # global logoEmpresa
    # logoEmpresa=str(vEmpresa.emp_logo.name)
    param   = {'cult_id':codigo, '$format':'json'}
    ws      = requests.get('http://localhost:9090/server/iotmach/ws_reporte_encabezado/views/reporte_encabezado', params=param);
    data    = ws.json()
    datos   = data["elements"]
    #print datos
    empresa =request.user.id_persona.emp_id.emp_id    
    vEmpresa= proy_empresa.objects.get(emp_id=empresa)

    vEspecie=agr_cultivo.objects.get(cult_id=codigo).esp_id

    vCultivo=agr_cultivo.objects.get(cult_id=codigo)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=reporte-'+datos[0]['cult_detalle']+'.pdf'

    doc = SimpleDocTemplate(response, pagesize=A4,
                            rightMargin=30, leftMargin=30,
                            topMargin=55, bottomMargin=55 )
    documento = []

    #documento.append(Spacer(1, 20))

    global logoEspecie
    logoEspecie=vEspecie.esp_imagen.name
    try:
        print(logoEspecie[0:],'*-------------')
        documento.append(Spacer(1, 10))
        f1=Image('media/'+logoEspecie[0:],width=40, height=40)#     f1.drawHeight = 2*cm
        print('x aki en try')
    except:
        print('x aki')
        f1 = Image(logoEspecie[1:], width=50, height=50)  # f1.drawHeight = 2*cm
    f1.drawWidth = 2*cm
    documento.append(f1)
    documento.append(Spacer(1, 5))

    texto = """<font size=15><b>"""+datos[0]['cult_detalle'].upper()+"""</b></font>"""
    documento.append(Paragraph(texto, estilos['centrado']))
    documento.append(Spacer(1, 5))
    texto = """<font size=12><b>1. <u>Datos del Cultivo</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    # DATOS DEL CULTIVO
    nombre=Paragraph("""<font size=10><b>NOMBRE DE CULTIVO:</b> <br/>"""+datos[0]['cult_detalle']+"""</font>""", estilos['izquierda'])
    descripcion=Paragraph("""<font size=10><b>DESCRIPCION:</b> <br/>"""+datos[0]['cult_descripcion']+"""</font>""", estilos['izquierda'])
    fInicio = Paragraph("""<font size=10><b>FECHA DE INICIO:</b> <br/>"""+datos[0]['cult_fecha']+"""</font>""", estilos['izquierda'])
    fFin= Paragraph("""<font size=10><b>FECHA DE FINALIZACION:</b> <br/> """+datos[0]['cult_fecha_fin']+"""</font>""", estilos['izquierda'])
    ubicacion= Paragraph("""<font size=10><b>UBICACION EN PARCELA:</b> <br/>"""+datos[0]['cult_ubicacion']+"""</font>""", estilos['izquierda'])
    parcela = Paragraph("""<font size=10><b>PARCELA:</b> <br/> """+datos[0]['sar_nombre']+"""</font>""", estilos['izquierda'])
    especie = Paragraph("""<font size=10><b>ESPECIE:</b> <br/> """+datos[0]['esp_nombre']+"""</font>""", estilos['izquierda'])
    hectareas = Paragraph("""<font size=10><b>NUM HECTAREAS:</b> <br/> """+str(datos[0]['cult_num_hectareas'])+"""</font>""", estilos['izquierda'])
    data= [[nombre,descripcion],
           [fInicio,fFin],
           [ubicacion,parcela],
           [especie, hectareas]
           ]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey),
        #('SPAN',(1,1),(0,1)),
        ],colWidths=[263,263])
    documento.append(t)

    documento.append(Spacer(1, 30))

    texto = """<font size=12><b>2. <u>Datos de Especie</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    #*********************TABLA DE ESPECIE ************************************
    #vEspecieN = agr_especie_cultivo.objects.get(esp_id=vCultivo.esp_id.esp_id)

    nombres=Paragraph("""<font size=10><b>NOMBRE:</b> <br/>"""+datos[0]['esp_nombre']+"""</font>""", estilos['izquierda'])
    categoria=Paragraph("""<font size=10><b>CATEGORIA:</b> <br/>"""+datos[0]['cat_nombre']+"""</font>""", estilos['izquierda'])
    data= [[nombres,categoria]]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey)],colWidths=[263,263])
    documento.append(t)
    documento.append(Spacer(1, 30))

    texto = """<font size=12><b>3. <u>Datos de Parcela</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    #*******TABLA PARCELA***************************************************************
    #vParcelaN = agr_parcela.objects.get(par_id=vCultivo.parcela.par_id_id)

    nombres=Paragraph("""<font size=10><b>NOMBRE:</b> <br/>"""+datos[0]['sar_nombre']+"""</font>""", estilos['izquierda'])
    latitud=Paragraph("""<font size=10><b>LATITUD: </b></font>"""+str(datos[0]['sar_latitud'])+' '+datos[0]['sar_hem_lat']+"""<br/>"""
                        +'<font size=10><b>LONGITUD: </b></font>'+str(datos[0]['sar_longitud'])+' '+datos[0]['sar_hem_lon']
                        +"""""", estilos['izquierda'])
    area=Paragraph("""<font size=10><b>AREA NETA: </b></font>"""+str(datos[0]['par_area_neta'])+"""<br/>"""
                        +'<font size=10><b>AREA BRUTA: </b></font>'+str(datos[0]['par_area_bruta'])
                        +"""""", estilos['izquierda'])
    hectareas=Paragraph("""<font size=10><b>Nº DE HECTAREAS TOTALES: </b></font>"""+str(datos[0]['par_hectareas'])+"""<br/>"""
                        +'<font size=10><b>Nº DE HECTAREAS DISPONIBLES: </b></font>'+str(datos[0]['par_disponibles'])
                        +"""""", estilos['izquierda'])
    data= [[nombres,latitud],
           [area,hectareas]]

    t=Table(data,style=[
        ('GRID',(0,0),(-1,-1),0.5,colors.grey)
        ],colWidths=[263,263])
    documento.append(t)
    documento.append(Spacer(1, 20))

    #TABLA GENERALES    
    riego=agr_tipo_riego.objects.get(rie_id=vCultivo.riego.rie_id)
    #FIN TABLAS GENERALES
    
    texto = """<font size=12><b>4. <u>Datos de Riego por: """+riego.rie_nombre.upper()+""" </u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))
    
    p   = {'cultivo_id':codigo , '$format':'json'}
    dr  = requests.get('http://localhost:9090/server/iotmach/ws_reporte_riego/views/reporte_riego',params=p)
    datos_riego = dr.json()
    print("---- ------DATOS RIEGO -----------")    
    #*******************************TABLA DE RIEGOS***************************************
    for i in datos_riego["elements"]:
        f_inicio=Paragraph("""<font size=10>"""+str(i['par_nombre'])+'- </font> <font size=7>'+str(i['par_descripcion'])+"""</font>""", estilos['izquierda'])
        descripcion = Paragraph("""<font size=10>"""+str(i['valor'])+' '+str(i['umed_abreviatura']).encode("utf-8")+"""</font>""", estilos['izquierda'])
        data= [[f_inicio,descripcion]
               ]

        t=Table(data,style=[
            ('GRID',(0,0),(-1,-1),0.0,colors.grey)
            ],colWidths=[263,263])
        documento.append(t)
        documento.append(Spacer(1, 0))
    
    documento.append(Spacer(1, 40))
    texto = """<font size=12><b>5. <u>Diseño agronomico del Cultivo</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))

    #--------------------------------TABLA DE FORMULAS DE RIEGO-> ws_reporte_formulas_riego ------------------------------------------------------------------
    pdfr    = {'cultivo_id':codigo, '$format':'json'}
    dfr     = requests.get('http://localhost:9090/server/iotmach/ws_reporte_formulas_riego/views/reporte_formulas_riego',params=pdfr)
    resultadosRiego = dfr.json()    
    print ("---------- LOS RESULTADOS DEL RIEGO ---------------")
    
    for i in resultadosRiego["elements"]:

        f_inicio=Paragraph("""<font size=10>"""+str(i['par_nombre'])+'- </font> <font size=7>'+str(i['par_descripcion'])+"""</font>""", estilos['izquierda'])
        descripcion = Paragraph("""<font size=10>"""+str(i['valor'])+' '+str(i['umed_abreviatura']).encode("utf-8")+"""</font>""", estilos['izquierda'])
        data= [[f_inicio,descripcion]
               ]

        t=Table(data,style=[
            ('GRID',(0,0),(-1,-1),0.0,colors.grey)
            ],colWidths=[263,263])
        documento.append(t)
        documento.append(Spacer(1, 0))
    
    
    #-------------------FASES EN EL DOCUMENTO------------------------------------------------------------------------------
    fases=agr_fase.objects.all().exclude(fas_nombre='No aplica')
    texto = """<font size=12><b>6. <u>Fases del Cultivo</u></b></font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))
    for j in fases:
        texto = """<font size=12><b><u>FASE: """+j.fas_nombre+"""</u></b></font>"""
        documento.append(Paragraph(texto, estilos['izquierda']))
        documento.append(Spacer(1, 20))

        #************************* WEB SERVICE ws_reporte_fases DENODO ************************************
        
        p = {'fas_nombre':j.fas_nombre, 'cultivo_id':codigo,'$format':'json'} #parametros 
        d = requests.get('http://localhost:9090/server/iotmach/ws_reporte_fases/views/reporte_fases',params=p)
        generalesDuracion = d.json()        
        
        for i in generalesDuracion["elements"]:

            f_inicio=Paragraph("""<font size=10>"""+str(i['par_nombre'])+'- </font> <font size=7>'+str(i['par_descripcion'])+"""</font>""", estilos['izquierda'])
            descripcion = Paragraph("""<font size=10>"""+str(i['valor'])+' '+str(i['umed_abreviatura']).encode("utf-8")+"""</font>""", estilos['izquierda'])
            data= [[f_inicio,descripcion]]
            t=Table(data,style=[
                ('GRID',(0,0),(-1,-1),0.0,colors.grey)
                ],colWidths=[263,263])
            documento.append(t)
            documento.append(Spacer(1, 0))
        documento.append(Spacer(1, 20))
    doc.build(documento, onFirstPage = myFirstPage, onLaterPages = myLaterPages)
    return response

def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.line(30, A4[1] - 50, 560, A4[1] - 50)
    canvas.line(30, 0.80*inch, 560, 0.80*inch)

    # nuevoLogo = ImageReader('media/'+vEmpresa.emp_logo.name)
    # canvas.drawImage(nuevoLogo, 30, A4[1] - 40, width=20, height=20)
    canvas.drawString(55, A4[1] - 40, vEmpresa.emp_nombre_juridico)
    canvas.drawString(450, A4[1] - 40, vEmpresa.emp_ruc)

    canvas.drawString(535, 0.65 * inch, "| %d" %(doc.page))
    nuevoLogo = ImageReader('static/agricultura/img/iotmach.ico')
    canvas.drawImage(nuevoLogo, 30,0.55 * inch, width=15, height=15)

    canvas.restoreState()

def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.line(30, 0.80*inch, 560, 0.80*inch)
    canvas.line(30, A4[1] - 50, 560, A4[1] - 50)
    # nuevoLogo = ImageReader('media/'+logoEmpresa)
    # canvas.drawImage(nuevoLogo, 30,A4[1] - 40, width=20, height=20)

    # nuevoLogo = ImageReader('media/'+vEmpresa.emp_logo.name)
    # canvas.drawImage(nuevoLogo, 30, A4[1] - 40, width=20, height=20)
    canvas.drawString(55, A4[1] - 40, vEmpresa.emp_nombre_juridico)
    canvas.drawString(450, A4[1] - 40, vEmpresa.emp_ruc)
    canvas.drawString(535, 0.65 * inch, "| %d" % (doc.page))

    nuevoLogo = ImageReader('static/agricultura/img/iotmach.ico')
    canvas.drawImage(nuevoLogo, 30,0.55 * inch, width=15, height=15)

