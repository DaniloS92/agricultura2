from django.shortcuts import render_to_response, RequestContext
from models import *
import openpyxl
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User
from .tasks import defaultConfiguracion
from agricultura.metodos import *
from .models import *
from django.views.decorators.csrf import *
from app_seguridad.encriptacion import *
from app_seguridad.views import socketLDAP
# Create your views here.
from Equation import Expression
# def inicio(request):
#     return render_to_response('agricultura/base.html', context_instance=RequestContext(request))
def inicio(request):
    if request.user.is_authenticated():
        dict=armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['empresa']=getEmpresa(request)
        dict['persona'] = obtenerPersona(request)
        # dict['roles'] = obtenerRoles(request)
        return render_to_response("agricultura/base_agricultura.html", dict, context_instance = RequestContext(request))
    else:
        return redirect('/')
def base(request):
    # dict=get_credenciales(request)
    # user = dict['user']
    # passwd = dict['passwd']
    # if user!='' and passwd!='':
    if request.user.is_authenticated():
        dict=armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['empresa']=getEmpresa(request)
        dict['persona'] = obtenerPersona(request)
        # dict['roles'] = obtenerRoles(request)
        return render_to_response("base.html", dict, context_instance = RequestContext(request))
        # else:
            # return render_to_response("index.html", context_instance = RequestContext(request))
            # return HttpResponseRedirect('http://192.168.1.30:6001')
            # return redirect('/iniciosesion/')
    else:
        # return redirect('/iniciosesion/')
        return render_to_response("index.html", context_instance = RequestContext(request))
        # return redirect('/')
        # return HttpResponseRedirect('http://192.168.1.30:6001')
def obtener(diccionario, tipo):
    for i in diccionario:
        if(i.get(tipo)):
            print(i)
            return(i.get(tipo))
def conf_tema(request):
    return render_to_response('agricultura/skin-config.html', context_instance=RequestContext(request))
#Configuracion Inicial
def default_config(request):
    try:
        defaultConfiguracion()
        messages.add_message(request, messages.SUCCESS, 'Finalizo la conf por defecto')
    except Exception as e:
        messages.add_message(request, messages.ERROR, e)
    return redirect('/agricultura/inicio/')
#nn
def templateArbol(request):
    parcela=agr_parcela.objects.all()
    cultivo=agr_cultivo.objects.all()
    print(cultivo)
    dic={
        'cultivo':cultivo,
    }
    return render_to_response('agricultura/pruebas/templateArbol.html', dic,context_instance=RequestContext(request))
### -------------- Seguridad ----------
def get_credenciales(request):
    dict={'user': '', 'passwd':'', 'permiso': False}
    for i in request.COOKIES:
        try:
            print(decrypt_node(i))
            if decrypt_node(i) == '2uth1ld2p3':
                dict['user'] = decrypt_node(request.COOKIES[i])
            if decrypt_node(i) == '9s3r0dAd1s':
                dict['passwd'] = decrypt_node(request.COOKIES[i])
            if(decrypt_node(i) == '10tw4tch53rV3r'):
                dict['permiso'] = True
        except Exception as e:
            print('cookie ignorada')
    return dict
##Metodo para la iniciar sesion en el sistema siempre y cuando el usuario no se encuentre previamente logeado en el sistema \n
##authenticate(): metodo que provee django para comprobar la existencia de un usuario\n
##is_authenticated(): metodo que provee django en el modelo User para comprobar si un usuario ha iniciado sesion\n
##login(): metodo que provee django para iniciar sesion
##redirect(): metodo que provee django para redireccionar hacia una URL o hacia una pagina de otro sitio web
# @param request es un objeto de peticion
# @see acciones() Descripcion dentro del enlace...
# @see precargar_Permisos() Descripcion dentro del enlace...
# @see crearMenus() Descripcion dentro del enlace...
# @return redirige con hacia la pagina principal del sistema
def inicioSesion(request):
    # dict = get_credenciales(request)
    # user = dict['user']
    # passwd = dict['passwd']
    # permiso= dict['permiso']
    # print(dict)
    # if permiso:
        # if user != '' and passwd != '':
    if request.user.is_authenticated():
        # if user != request.user.username:
        #     login(request, user)
        #     crearMenus()
        return redirect('/')
    else:
        # if request.POST:
        username =request.POST['txtUsuario']
        password =request.POST['txtPassword']
        # cUser=authenticate(username=user, password=passwd)
        cUser=authenticate(username=username, password=password)
        if cUser is not None and cUser.is_active:
            login(request, cUser)
            vUser = User.objects.get(username=username)
            acciones('Inicio de Sesion', vUser)
            crearMenus()
            precargar_Permisos()
            return redirect('/')
        else:
            messages.add_message(request, messages.ERROR, 'Nombre de usuario o contrasenia incorrectos')
            print('Nombre de usuario o contrasenia incorrectos')
            # return redirect('http://192.168.1.30:6001/')
            return redirect('/')
    #     # else:
    #         return cerrarSesion(request)
    # else:
    #     return redirect('http://192.168.1.30:6001/')
def cerrarSesion(request):
    try:
        # vUser = User.objects.get(username=request.user.username)
        logout(request)
        # acciones('Cerro Sesion', vUser)
        # return redirect('http://192.168.1.30:6001/')
        return redirect('/')
    except Exception as e:
        print(e)
        # return redirect('http://192.168.1.30:6001/')
        return redirect('/')

@login_required(login_url='/')
def cierre_node(request):
    logout(request)
    # return redirect('http://192.168.1.30:6001/logout')
    return redirect('/')

@csrf_exempt
def cambiar_password(request):
    if request.method=="POST":
        txt_username=request.POST['usuario']
        txt_email=request.POST['id_email']
        txt_password=request.POST['password']
        txt_password_confirm=request.POST['password_confirm']
        response = redirect('http://127.0.0.1:6001/')

        if txt_password == txt_password_confirm:
            try:
                usuario_reset=User.objects.get(username=txt_username, email=txt_email)
                usuario_reset.set_password(txt_password)
                datos = {'usuario': txt_username,
                         'pass': txt_password,
                         'empresa': usuario_reset.id_persona.emp_id.emp_ruc,
                         }
                socketLDAP('actualizar_pass', datos)
                usuario_reset.save()
                response.set_cookie('messages', 'Recuperacion de cuenta exitosa. Inicie sesion con su nueva contrasenia-success')
                # messages.add_message(request, messages.SUCCESS, "Recuperacion de cuenta exitosa. Inicie sesion con su nueva contrasenia")
                return response
            except:
                messages.add_message(request, messages.ERROR, "Ingrese su nombre de usuario o correo electronico correctamente")
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            messages.add_message(request, messages.ERROR, "Las contrasenias ingresadas no coinciden")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return redirect('/')

def verifica_recuperacion_cuenta(request, username_user, email_user):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')
    else:
        if User.objects.filter(username=username_user, email=email_user, is_active=True).exists():
            dict={
                'title': 'Recuperar cuenta IOTMACH'
            }
            return render_to_response('seguridad/recuperarcuenta.html', dict, context_instance=RequestContext(request))
        else:
            return HttpResponseRedirect('/')

def error404(request):
    # print('....')
    return render_to_response("seguridad/errores/error_404.html", {'usuario':request.user},
                              context_instance=RequestContext(request))

def error500(request):
    return render_to_response("seguridad/errores/error_403.html",{'usuario':request.user},
                              context_instance=RequestContext(request))