from __future__ import unicode_literals

from django.apps import AppConfig


class ModularConfig(AppConfig):
    name = 'app_modular'
    verbose_name = 'Modular'