from django.db import models
from django.contrib.auth.models import User, Group

class mod_modulo(models.Model):
    mod_name = models.CharField("Nombre",max_length=100)
    mod_fecha_carga=models.DateField("Fech de carga",auto_now=True)
    mod_fecha_instalacion=models.DateField("Fecha de instalacion")
    mod_hora_instalacion=models.TimeField("Hora de instalacion")
    mod_archivo=models.FileField("Archivo")
    mod_descripcion=models.TextField("Descripcion")
    id_usuario=models.ForeignKey(User, verbose_name="Id usuario")

    class Meta:
        db_table='modulo'
        verbose_name = 'Modulo'
        verbose_name_plural = 'Modulos'
        default_permissions=()
        permissions = (
            ('add_mod_modulo', "Puede agregar Modulo"),
            ('change_mod_modulo', "Puede editar Modulo"),
            ('delete_mod_modulo', "Puede eliminar Modulo"),
            ('list_mod_modulo', "Puede listar Modulo"),
        )

    def __unicode__(self):
        return self.mod_name + ' - ' + self.id_usuario.get_full_name()

class tipo_modificacion(models.Model):
    t_mod_nombre=models.TextField(max_length=50)
    t_mod_icono=models.TextField(max_length=50)

    class Meta:
        default_permissions=()
        verbose_name = 'Tipo de Modificacion'
        verbose_name_plural = 'Tipos de Modificaciones'

    def __unicode__(self):
        return self.t_mod_nombre

class mod_notificaciones(models.Model):
    not_mensaje = models.TextField("Mensaje")
    not_estado = models.BooleanField("Estado")
    not_fecha = models.DateField("Fecha")
    id_modulo = models.ForeignKey(mod_modulo, blank=True,null=True, verbose_name="Modulo")
    id_t_mod = models.ForeignKey(tipo_modificacion, verbose_name="Id Modulo")

    class Meta:
        db_table='notificaciones'
        verbose_name = 'Notificacion'
        verbose_name_plural = 'Notificaciones'
        default_permissions=()
        permissions = (
            ('add_mod_notificaciones', "Puede agregar Notificaciones"),
            ('change_mod_notificaciones', "Puede editar Notificaciones"),
            ('delete_mod_notificaciones', "Puede eliminar Notificaciones"),
            ('list_mod_notificaciones', "Puede listar Notificaciones"),
        )

    def __unicode__(self):
        return  self.not_mensaje

class mod_tecnologias(models.Model):
    tec_nombre=models.TextField("Nombre")
    tec_estado = models.BooleanField("Estado")
    tec_fecha = models.DateField("Fecha",auto_now=True)
    id_usuario = models.ForeignKey(User, verbose_name="Usuario")

    class Meta:
        db_table='tecnologias'
        verbose_name = 'Tecnologia'
        verbose_name_plural = 'Tecnologias'
        default_permissions=()
        permissions = (
            ('add_mod_tecnologias', "Puede agregar Tecnologias"),
            ('change_mod_tecnologias', "Puede editar Tecnologias"),
            ('delete_mod_tecnologias', "Puede eliminar Tecnologias"),
            ('list_mod_tecnologias', "Puede listar Tecnologias"),
        )

    def __unicode__(self):
        return self.tec_nombre