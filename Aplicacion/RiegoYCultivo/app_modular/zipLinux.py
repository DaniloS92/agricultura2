from django.template.defaulttags import csrf_token
from django.core.management import call_command #PARA SINCRONIZAR BASE DE DATOS
import shutil                       #HACER PROCESOS ENTRE DIRECTORIOS EN PYTHON
import os                           #COGER RUTAS DE ARCHIVOS
from os.path import dirname         #COGER RUTAS DE ARCHIVOS
import zipfile                      #PARA ARCHIVOS.ZIP
from zipfile import ZipFile         #PARA ARCHIVOS.ZIP
def capturaRuta():
    PATH=dirname(__file__)          #OBTENGO LA RUTA DEL PROYECTO
    datos=PATH.split(slash())       #WINDOWS: QUITAR LOS \\ PARA HACERLO ARRAY
    ruta=''                         #CREO UNA VARIABLE PARA COGER LA RUTA PRINCIPAL DEL PROYECTO
    for i in datos:                 #RECORRE MI ARRAY
        ruta+=i+slash()             #CONCATENANDO LA RUTA CON EL \\
        if(i=='dyuyay_server'):            #SI ENCONTRO LA RUTA PRINCIPAL DEL PROYECTO SALIR DEL FOR EJEM: XXX\XXX\DYUYAY
            break                   #SALIR DEL FORM
    return ruta                     #RETORNO LA RUTA DEL PROYECTO

#METODO PARA MOVER ARCHIVOS(src--ruta donde se encuentra , dst--destino a mover)
def move(src, dest):
    shutil.move(src, dest)

#PARA SABER SI EXISTE LA APLICACION INSTALADA
def existeApp(filename):
    nombreApp=(filename.name).split('.zip') #COGER EL NOMBRE DE LA APP A COMPARAR
    d = os.path.dirname(capturaRuta()+nombreApp[0]+slash())
    if not os.path.exists(d):
        return 0   #NO EXISTE
    else:
        print('Si existe tengo que elminar')
        return 1   #SI EXISTE

def eliminarAplicaciones(filename):
    os.system('cd '+capturaRuta()+' \n cd .. \n chmod -R 777 dyuyay_server/')
    nombreApp=(filename.name).split('.zip')
    os.system('rm -r '+capturaRuta()+nombreApp[0])
    os.system('rm -r '+capturaRuta()+'static'+slash()+nombreApp[0])
    os.system('rm -r '+capturaRuta()+'templates'+slash()+nombreApp[0])

#METODO PARA DESCOMPRIMIR LOS ARCHIVOS(NOMBRE DE ARCHIVO)
def descomprime(filename):
    with ZipFile(filename) as myzip: #CON EL ARCHIVO ZIPFILE
        myzip.extractall(capturaRuta()+'media'+slash()) #DESCOMPRIMIR TODOS LOS ARCHIVOS (RUTA) EN LA CARPETA MEDIA

#INSTALAR LA APLICACION ...COPIAR,MOVER,ELIMINAR,INSERTAR
def instalaApp(filename):
    nombreApp=(filename.name).split('.zip')  #COGER SOLO EL NOMBRE DE DE LA APLICACION
    rutaProyecto=capturaRuta()+'media'+slash()+nombreApp[0]+slash()+nombreApp[0]+slash()#COGER LA RUTA DE LA APLICACION DESCOMPRIMIDA
    rutaStatic=capturaRuta()+'media'+slash()+nombreApp[0]+slash()+'static'+slash()+nombreApp[0]+slash() #COGER LA RUTA DE LA CARPETA STATIC
    rutaTemplates=capturaRuta()+'media'+slash()+nombreApp[0]+slash()+'templates'+slash()+nombreApp[0]+slash() #COGER LA RUTA DE LA CARPETA TEMPLATES
    #MOVER LOS ARCHIVOS DESCOMPRIMIDOS A EL PROYECTO
    move(rutaProyecto,capturaRuta())
    move(rutaStatic,capturaRuta()+'static'+slash())
    move(rutaTemplates,capturaRuta()+'templates'+slash())

#METODO PARA SOBRESCRIBIR LAS URLS.PY
def escribirurls(filename):
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'urls.py','r') #ABRIR EL ARCHIVO DE LAS URLS.PY
    n_App=(filename.name).split('.zip') #COGER EL NOMBRE DE LA APP A INSTALAR
    nombreApp=n_App[0].split('_')
    contador=0 #CREAR BANDERA PARA CONTAR LAS LINEAS
    linea=0    #CREAR BANDERA PARA COGER LA POSICION EN LA LINEA QUE SE VAYA A INSERTAR
    lineas=[]  #IR GUARDANDO TODAS LAS LINEAS EN UN ARRAY PARA SU POSTERIOR INGRESO
    for i in settings.readlines(): #RECORRER TODAS LAS LINEAS DEL ARCHIVO
        cortar=i.replace(' ','')   #REEMPLAZAR LOS SPACIOS POR NINGUNO PARA QUE NO HAYA INCOVENIENTES PARA COMPARAR
        if(cortar[0]==')'):        #SI ENCUENTRA UN ) QUE ES LA ULTIMA LINEA DE URLS.PY CAPTURAR EN QUE NUM DE LINEA ESTA
            linea=contador         #SE GUARDA EL NUMERO DE LINEA
            break                 #SALE DEL FOR UNA VEZ ENCONTRADO
        contador=contador+1        #CONTADOR DE LINEAS
    contador=0
    settings.close()               #SE CIERRA EL ARCHIVO
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'urls.py','r') #ABRIR EL ARCHIVO DE LAS URLS.PY
    for i in settings.readlines(): #RECORRER TODAS LAS LINEAS DEL ARCHIVO
        lineas.append(i)           #INSERTAR EN EL ARRAY LINEA POR LINEA SIN REALIZAR NINGUN CAMBIO
        if(contador==linea-1):     #CUANDO ENTRE EN LA LINEA -1 CAPTURADA
            #lineas.append("""    url(r'^', include('"""+nombreApp[0]+""".urls')),\n""") #INGRESAR LA NUEVA LINEA PARA QUE RECONOZCA LAS URL
            lineas.append("""    url(r'^"""+nombreApp[1]+"""/', include('"""+n_App[0]+""".urls', namespace='URL"""+nombreApp[1]+"""')),\n""") #INGRESAR LA NUEVA LINEA PARA QUE RECONOZCA LAS URL
        contador=contador+1
    settings.close()                    #CERRAR EL ARCHIVO ABIERTO
    os.system('cd ' +capturaRuta()+'dyuyay_server \n rm urls.py')
    settings=open(capturaRuta()+'urls.py','w') #ABRIR EL ARCHIVO DE LAS URLS.PY
    for i in lineas:                           #RECORRER LAS EL ARRAY PARA ESCRIBIR EL ARCHIVO
        settings.write(i)                      #ESCRIBIR LINEA POR LINEA
    settings.close()                           #CERRAR EL ARCHIVO ABIERTO
    move(capturaRuta()+'urls.py',capturaRuta()+'dyuyay_server'+slash()+'urls.py')  #MOVER EL ARHCHIVO REESCRITO AL PROYECTO

#METODO PARA SOBRESCRIBIR EL SETTINGS.PY
def escribirSettings(filename):
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'settings.py','r') #ABRIR SCRIPT PARA HACER CAMBIOS DE ARCHIVOS
    nombreApp=(filename.name).split('.zip')                         #COGER EL NOMBRE DE LA APP A INSTALAR
    lineas=[]                                                       #IR GUARDANDO TODAS LAS LINEAS EN UN ARRAY PARA SU POSTERIOR INGRESO
    contador=0                                                      #CREAR BANDERA PARA CONTAR LAS LINEAS
    linea1=0
    linea2=0                                                        #CREAR BANDERA PARA COGER LA POSICION EN LA LINEA DONDE EMPIEZA LAS APPS
    for i in settings.readlines():                                  #RECORRER TODAS LAS LINEAS DEL ARCHIVO
        cortar=i.split(' ')                                         #CORTAR TODOS LOS ESPACIOS Y HACERLO VECTOR PARA SU MEJOR MANEJO
        if(cortar[0]=='INSTALLED_APPS'):                            #SI ENCUENTRA UN 'INSTALLED_APPS' QUE ES LA PRIMERA LINEA DE CONFIGURACION PARA LOS ARCHIVOS
            linea1=contador                                         #CAPTURA EN NUMERO DE LINEA
            #print("#NUMERO DE LINEA",linea1)
            break
        contador=contador+1                                     #CONTADOR SIGUE SUMANDO
    contador=0
    settings.close()                                                #CERRAR EL ARHIVO ABIERTO
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'settings.py','r') #ABRIR EL SETTINGS.PY
    linea2=0                                                        #CREAR BANDERA PARA COGER LA POSICION EN LA LINEA DONDE TERMINA LAS APPS
    for i in settings.readlines():                                  #RECORRER TODAS LAS LINEAS DEL ARCHIVO
        cortar=i.split(' ')                                    #REEMPLAZAR LOS ESPACIOS POR NADA
        if(contador>=linea1):                                       #SI LLEGO A LA LINEA DONDE EMPEZABA INSTALL_APPS
            if(cortar[0]==')'):                                      #SI ENCONTRO EL CIERRE DE LAS APP
                linea2=contador                                     #CAPTURA LA ULTIMA LINEA DE LAS APPS
		#print('linea2',linea2)
                break                                               #SALIR DEL FOR
        contador=contador+1
    contador=0
    settings.close()                                                #CERRAR EL ARCHIVO ABIERTO
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'settings.py','r') #ABRIR EL SETTINGS.PY
    #ESCRIBIR LA NUEVA LINEA DE LA APLICACION
    for i in settings.readlines():                                   #RECORRER TODAS LAS LINEAS DEL ARCHIVO
        lineas.append(i)                                             #INSERTAR EN EL ARRAY LINEA POR LINEA SIN REALIZAR NINGUN CAMBIO
        if(contador==linea2-1):                                      #CUANDO ENTRE EN LA LINEA2 -1 CAPTURADA
            lineas.append("""    '"""+nombreApp[0]+"""',\n""")       #INGRESAR LA NUEVA LINEA DE LA APLICACION
            #print("#NUMERO DE LINEA 2",linea2,"-1 ",linea2-1)
        contador=contador+1
    contador=0
    settings.close()                                                #CERRAR EL ARCHIVO ABIERTO
    os.system('cd ' +capturaRuta()+'dyuyay_server \n rm settings.py')                        #EJECUTAR SCRIPT
    settings=open(capturaRuta()+'settings.py','w')                  #ABRIR EL SETTINGS.PY
    for i in lineas:                                                #RECORRER LAS EL ARRAY PARA ESCRIBIR EL ARCHIVO
        settings.write(i)                                           #ESCRIBIR LINEA POR LINEA
    settings.close()                                                #CERRAR EL ARCHIVO ABIERTO
    move(capturaRuta()+'settings.py',capturaRuta()+'dyuyay_server'+slash()+'settings.py')#MOVER EL ARHCHIVO REESCRITO AL PROYECTO
    #os.system('cd '+capturaRuta()+' python manage.py syncdb')                          #ABRIR SCRIPT PARA HACER CAMBIOS EN EL PROYECTO
    call_command('syncdb',interactive=True) #LLAMADA A COMANDO SYNCDB  PARA SINCRONIZAR MODELOS DE APP NUEVA
    #call_command('runserver')
    #os.system('service apache2 reload')                          #ABRIR SCRIPT PARA HACER CAMBIOS EN EL PROYECTO

def slash():
    return "/"

