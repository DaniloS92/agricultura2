#-*- coding: utf-8 -*-
from django.contrib import messages
from django.shortcuts import render, render_to_response, RequestContext, redirect, HttpResponse
from django.template.defaulttags import csrf_token
import shutil                       #HACER PROCESOS ENTRE DIRECTORIOS EN PYTHON
import os                           #COGER RUTAS DE ARCHIVOS
from os.path import dirname         #COGER RUTAS DE ARCHIVOS
import zipfile                      #PARA ARCHIVOS.ZIP
from zipfile import ZipFile         #PARA ARCHIVOS.ZIP
#from .zipLinux import *
from .zipWindows import *
from .models import *
from django.contrib.auth.decorators import login_required
from agricultura.metodos import *
from django.core.management import call_command

@login_required(login_url='/')
def formModulos(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['roles'] = obtenerRoles(request)
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('app_modular.add_mod_modulo'):
            return render_to_response("modular/modulo/frmModulo.html", dict, context_instance = RequestContext(request))
        else:
            return render_to_response('seguridad/errores/error_403.html', dict,context_instance = RequestContext(request))
    else:
        return redirect('/')

def cargar(request):
    mj=''
    try:
        if request.method == "POST":
            filename=request.FILES['archivo']
            print(filename)
            if(zipfile.is_zipfile(filename)):                           #SABER SI EL ARCHIVO ES ZIP
#                logging.warning('ddd')
                if(existeApp(filename)==1):
                    print('Enviar si realmente desea eliminar')
                    mj=capturaRuta()
                    #En caso de que si quiera eliminar
                    eliminarAplicaciones(filename)
                    # descomprime(filename)
                    # instalaApp(filename)
                else:
#                     mj='no exixste se creara'
                    descomprime(filename)
                    instalaApp(filename)
                    escribirurls(filename) #LLAMAR A METODO PARA SOBRESCRIBIR LAS URLS.PY
                    escribirSettings(filename) #LLAMAR A METODO PARA SOBRESCRIBIR SETTINGS.PY
#                     print('ya sta')
                    messages.add_message(request, messages.SUCCESS, 'El nuevo modulo fue programado'.upper())
                return redirect('/')
#                     print('ola')
            else:
                messages.add_message(request, messages.ERROR, 'El archivo no es zip'.upper())
                return redirect('/modular/modulo/agregar/')
    except(Exception)as e:
        print(e)
        messages.add_message(request, messages.ERROR, e)
        # return redirect('/seguridad/modular/cargar/')
        return redirect('/')

@login_required(login_url='/')
def lista_modulos(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['roles'] = obtenerRoles(request)
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('app_modular.list_mod_modulo'):
            dict['modulos'] = mod_modulo.objects.all()
            return render_to_response("modular/modulo/list_modulos.html", dict, context_instance = RequestContext(request))
        else:
            return render_to_response('seguridad/errores/error_403.html', dict, context_instance = RequestContext(request))
    else:
        return redirect('/')

def listar_modulos(request):
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'settings.py','r')
    contador=0
    linea1=0
    linea2=0
    for i in settings.readlines():
        cortar=i.split(' ')
        if(cortar[0]=='INSTALLED_APPS'):
            linea1=contador
        if(cortar[0]==')'):
            linea2=contador
            break
        contador=contador+1
    contador=0
    settings.close()
    settings=open(capturaRuta()+'dyuyay_server'+slash()+'settings.py','r')

    aplicaciones=[]
    for i in settings.readlines():
        if(contador >= linea1+1):
            aplicaciones.append(i)
        if(contador == linea2-1):
            break
        contador=contador+1
    settings.close()
    return HttpResponse(aplicaciones)


############## Tecnologias
@login_required(login_url='/')
def frm_tecnologias(request):
    if request.user.is_authenticated():
        dict=armaMenu(request.user.get_all_permissions())
        dict['roles'] = obtenerRoles(request)
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['empresa'] = getEmpresa(request)
        if request.user.has_perm('app_modular.add_mod_tecnologias'):
            os.system('pip list > list_paq.nvk')
            archivo=open(capturaRuta()+'list_paq.nvk','r')
            resultados=[]
            for i in archivo.readlines():
                resultados.append(i)
            dict['paquetes']=resultados
            return render_to_response('modular/tecnologias/frmTecnologias.html',dict,context_instance = RequestContext(request))
        else:
            return render_to_response('seguridad/errores/error_403.html',dict,context_instance = RequestContext(request))
    else:
        return redirect('/')