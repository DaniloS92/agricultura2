from django.conf.urls import patterns, include, url
from app_modular.views import *
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # modulos
    url(r'modulo/agregar/$', formModulos, name="URLcrear"),
    url(r'modulo/guardar/$', cargar, name="URLcrear"),
    url(r'modulo/listar/$', lista_modulos),

    # tecnologias
    url(r'tecnologia/agregar/$', frm_tecnologias, name="URLcrear"),


)
