from django.contrib import admin
from .models import *


class mod_moduloAdmin(admin.ModelAdmin):
    list_display = ('mod_name', 'mod_fecha_carga', 'mod_fecha_instalacion','mod_hora_instalacion','mod_archivo','mod_descripcion')
    list_filter= ['mod_fecha_instalacion']
    search_fields = ['mod_fecha_instalacion']
    list_per_page = 10

class mod_notificacionesAdmin(admin.ModelAdmin):
    list_display = ('not_mensaje', 'not_estado', 'not_fecha','id_modulo','id_t_mod')
    list_filter= ['not_fecha']
    search_fields = ['not_fecha']
    list_per_page = 10


class mod_tecnologiasAdmin(admin.ModelAdmin):
    list_display = ('tec_nombre', 'tec_estado', 'tec_fecha','id_usuario')
    list_filter= ['tec_nombre']
    search_fields = ['tec_nombre']
    list_per_page = 10


# admin.site.register(mod_modulo, mod_moduloAdmin)
# admin.site.register(mod_notificaciones, mod_notificacionesAdmin)
# admin.site.register(mod_tecnologias, mod_tecnologiasAdmin)