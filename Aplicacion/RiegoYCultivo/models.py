# # Modelos de Riego y Cultivo
# # Modelos de Agricultura
# class agr_especie_categoria(models.Model):
#     nombre= models.CharField(max_length=100)
#     observacion= models.CharField(max_length=100)
#     estado= models.BooleanField()

#     class Meta:
#         default_permissions=(['add','change','list'])

# class agr_especie_cultivo(models.Model):
#     esp_nombre = models.CharField(max_length=100)
#     esp_imagen = models.ImageField(upload_to='img_especies', blank=True, null=False)
#     esp_altura_cultivo = models.FloatField()
#     esp_estado = models.BooleanField()
#     esp_categoria=models.ForeignKey(agr_especie_categoria)
#     esp_estado = models.BooleanField(default=True)

#     class Meta:
#         default_permissions=(['add','change','list'])

# class r_unidad_medida(models.Model):
#     umed_nombre = models.CharField(max_length=100)

#     class Meta:
#         default_permissions=(['add','change','delete'])

#     def __unicode__(self):
#         return self.umed_nombre

# class agr_parametro(models.Model):
#     par_nombre = models.CharField(max_length=100)
#     par_descripcion = models.CharField(max_length=1000)
#     par_abreviatura = models.CharField(max_length=10)
#     par_valor_minimo = models.FloatField()
#     par_valor_maximo = models.FloatField()
#     par_valor = models.FloatField()
#     par_estado=models.BooleanField()

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.par_nombre

# class agr_tipo_parametro(models.Model):
#     tpar_nombre = models.CharField(max_length=100)
#     tpar_abreviatura = models.CharField(max_length=10)
#     tpar_descripcion = models.CharField(max_length=1000)
#     tpar_tipo=models.BooleanField(default=True)
#     umed_id = models.ForeignKey(r_unidad_medida)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.tpar_nombre

# class agr_tipo_par_par(models.Model):
#     tipo_parametro=models.ForeignKey(agr_tipo_parametro)
#     parametro=models.ForeignKey(agr_parametro)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.tipo_parametro.tpar_nombre

# class agr_tipo_riego(models.Model):
#     rie_nombre= models.CharField(max_length=100)
#     rie_descripcion = models.CharField(max_length=1000)
#     rie_formulas=models.FileField(upload_to='archivo_formulas', blank=True, null=True)
#     # par_id = models.ForeignKey(agr_parametro)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.rie_nombre;

# class agr_tipo_par_riego(models.Model):
#     rie_id = models.ForeignKey(agr_tipo_riego)
#     par_id = models.ForeignKey(agr_parametro)

#     class Meta:
#         default_permissions=()

# class agr_especie_parametro(models.Model):
#     ep_valor_min = models.FloatField()
#     ep_valor_max = models.FloatField()
#     ep_valor_estandar = models.FloatField()

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

# class agr_esp_par_par(models.Model):
#     ep_id=models.ForeignKey(agr_especie_parametro)
#     par_id=models.ForeignKey(agr_parametro)
#     fase=models.BooleanField(default=False)

#     class Meta:
#         default_permissions=()

# class agr_especie_parametros_valores(models.Model):
#     epv_valor=models.FloatField()
#     tipo_id=models.ForeignKey(agr_tipo_parametro)
#     esp_id=models.ForeignKey(agr_especie_cultivo)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.esp_id.esp_nombre


# class agr_parcela(models.Model):
#     par_id=models.OneToOneField(proy_subarea, primary_key=True)
#     par_area_neta = models.FloatField()
#     par_area_bruta = models.FloatField()
#     par_hectareas = models.FloatField()
#     par_disponibles=models.FloatField()
#     par_estado = models.BooleanField(default=True)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.par_id.sar_nombre

# class agr_parametros_parcela(models.Model):
#     par_tipo = models.CharField(max_length=10)
#     par_valor = models.FloatField()
#     parcela = models.ForeignKey(agr_parcela)

#     class Meta:
#         default_permissions=(['add','change','delete'])

#     def __unicode__(self):
#         return self.par_tipo+'-'+self.parcela.par_nombre

# class agr_cultivo(models.Model):
#     cult_detalle = models.CharField(max_length=50)
#     cult_descripcion = models.CharField(max_length=500)
#     cult_modo = models.CharField(max_length=100)
#     cult_fecha = models.CharField(max_length=100)
#     cult_fecha_fin = models.CharField(max_length=100)
#     cult_num_hectareas = models.FloatField()
#     cult_ubicacion = models.CharField(max_length=100)
#     cul_estado=models.BooleanField(default=True)
#     esp_id = models.ForeignKey(agr_especie_cultivo)
#     parcela = models.ForeignKey(agr_parcela)

#     class Meta:
#         default_permissions=(['add','change','list','print'])

#     def __unicode__(self):
#         return self.cult_detalle

# class agr_cultivo_imagenes(models.Model):
#     imagen = models.ImageField(upload_to='img_cultivos', blank=True, null=False)
#     principal = models.BooleanField(default=False)
#     cultivo = models.ForeignKey(agr_cultivo)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.cultivo.cult_descripcion

# class agr_cultivo_monitoreo(models.Model):
#     cult_panel=models.TextField(default='no')
#     idCultivo = models.OneToOneField(agr_cultivo)

#     class Meta:
#         default_permissions=(['add','change','list'])

#     def __unicode__(self):
#         return self.idCultivo.cult_descripcion

# class agr_centro_consumo(models.Model):
#     cen_nombre = models.CharField(max_length=100)
#     cen_telefono =models.CharField(max_length=10)
#     cen_email = models.EmailField()
#     cen_descripcion = models.CharField(max_length=1000)
#     cen_longitud = models.FloatField()
#     cen_latitud = models.FloatField()

#     class Meta:
#         default_permissions=()

# class agr_cultivo_cent_consumo(models.Model):
#     cc_detalle = models.CharField(max_length=1000)
#     cc_distancia = models.CharField(max_length=10)
#     cc_tiempo = models.CharField(max_length=10)
#     cc_transportes = models.CharField(max_length=10)
#     cult_id = models.ForeignKey(agr_cultivo)
#     cen_id = models.ForeignKey(agr_centro_consumo)

#     class Meta:
#         default_permissions=()

# class agr_latitud(models.Model):
#     lat_valor = models.IntegerField(default=0)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return str(self.lat_valor)

# class agr_mes(models.Model):
#     mes_nombre = models.CharField(max_length=10)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.mes_nombre

# class agr_lat_mes(models.Model):
#     latitud = models.ForeignKey(agr_latitud)
#     mes = models.ForeignKey(agr_mes)
#     valor = models.FloatField()
#     lat_hemisferio = models.CharField(max_length=1)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return str(self.latitud.lat_valor)+'-'+self.mes.mes_nombre+'-'+str(self.valor)+'-'+self.lat_hemisferio

# class agr_tipo_suelo(models.Model):
#     nombre=models.CharField(max_length=100)
#     imagen = models.ImageField(upload_to='img_suelo', blank=True, null=True, default='img_suelo/sin_imagen.gif')
#     agr_parametro=models.ForeignKey(agr_parametro)
#     estado=models.BooleanField(default=True)
#     def __unicode__(self):
#         return str(self.nombre)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

# class agr_tipo_suelo_valores(models.Model):
#     agr_tipo_suelo=models.ForeignKey(agr_tipo_suelo)
#     agr_tipo_parametros=models.ForeignKey(agr_tipo_parametro)
#     valor=models.FloatField()

#     class Meta:
#         default_permissions=(['change'])

#     def __unicode__(self):
#         return self.agr_tipo_suelo.nombre+self.agr_tipo_parametros.tpar_nombre+str(self.valor)

# class agr_evotranspiracion(models.Model):
#     mes = models.CharField(max_length=100)
#     parcela = models.ForeignKey(agr_parcela)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.mes+'-'+self.parcela.par_nombre

# class evo_tipo_valor(models.Model):
#     valor = models.FloatField()
#     evo_id = models.ForeignKey(agr_evotranspiracion)
#     tipo = models.CharField(max_length=50)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.evo_id.mes+'-'+self.tipo

# class agr_cultivo_valor(models.Model):
#     cultivo=models.ForeignKey(agr_cultivo)
#     valor = models.FloatField()
#     tipo=models.CharField(max_length=50)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.tipo+(str(self.valor))

# class agr_cultivo_suelo_valores(models.Model):
#     cultivo=models.ForeignKey(agr_cultivo)
#     valor = models.FloatField()
#     tipo=models.CharField(max_length=50)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.tipo+(str(self.valor))

# class agr_cultivo_especie_parametros_valores(models.Model):
#     cultivo=models.ForeignKey(agr_cultivo)
#     valor = models.FloatField()
#     tipo=models.CharField(max_length=50)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.tipo+(str(self.valor))

# class agr_cultivo_formulas(models.Model):
#     cultivo=models.ForeignKey(agr_cultivo)
#     valor = models.FloatField()
#     tipo=models.CharField(max_length=50)

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.tipo+(str(self.valor))

# class agr_notas_cultivo(models.Model):
#     cultivo=models.ForeignKey(agr_cultivo)
#     fecha=models.CharField(max_length=100)
#     nota=models.CharField(max_length=300)

#     class Meta:
#         default_permissions=(['add','list'])

# class agr_formulas_riego(models.Model):
#     nombre=models.CharField(max_length=100)
#     descripcion=models.CharField(max_length=100)
#     formula=models.CharField(max_length=100)
#     tipos=models.CharField(max_length=200)
#     u_medida=models.ForeignKey(r_unidad_medida)
#     tipo_riego=models.ForeignKey(agr_tipo_riego)
#     estado= models.BooleanField(default=True)
#     general=models.BooleanField(default=False)
#     formato=models.CharField(max_length=10, default='ninguno')
#     dato=models.CharField(max_length=10, default='')
#     operador=models.CharField(max_length=5, default='')

#     class Meta:
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.formula


# class agr_fase(models.Model):
#     nombre=models.CharField(max_length=100)
#     descripcion=models.CharField(max_length=100)
#     def __unicode__(self):
#         return self.nombre

# class agr_fase_tpar(models.Model):
#     fase=models.ForeignKey(agr_fase)
#     tpar=models.ForeignKey(agr_tipo_parametro)
#     def __unicode__(self):
#         return self.fase.nombre

# class agr_fase_cultivo(models.Model):
#     fase=models.ForeignKey(agr_fase)
#     tipo=models.ForeignKey(agr_tipo_parametro)
#     valor = models.FloatField()
#     cultivo=models.ForeignKey(agr_cultivo)

# # Modelos app_empresa
# class proy_empresa(models.Model):
#     emp_ruc = models.CharField(max_length=50, unique=True)
#     emp_nombre_comercial = models.CharField(max_length=200)
#     emp_nombre_juridico = models.CharField(max_length=200)
#     emp_grupo_empresa = models.CharField(max_length=200)
#     emp_direccion = models.CharField(max_length=200)
#     emp_email = models.CharField(max_length=500, unique=True)
#     emp_web = models.CharField(max_length=500)
#     emp_logo = models.ImageField(upload_to='empresa_logos', blank=True, null=True)
#     is_active = models.BooleanField(default=True)
#     emp_dominio_correo = models.CharField(max_length=500)
#     emp_descripcion = models.CharField(max_length=500, blank=True, null=True)

#     class Meta:
#         db_table = 'empresa'
#         default_permissions = (['change'])
#         # permissions = ("list_empresa","Can list empresa")


#     def __unicode__(self):
#         return self.emp_nombre_comercial

# #class proy_emp_telefono(models.Model):
# #    telef_numero = models.CharField(max_length=10)
# #    telef_proveedor = models.CharField(max_length=10)
# #    emp_id = models.ForeignKey(proy_empresa)

# class proy_persona(models.Model):
#     per_nombres = models.CharField(max_length=200)
#     per_apellidos = models.CharField(max_length=200)
#     per_direccion = models.CharField(max_length=500)
#     per_telef_movil = models.CharField(max_length=10)
#     per_email = models.CharField(max_length=200)
#     per_foto = models.ImageField(upload_to="fotos_personas", blank=True, null=True)
#     per_telef_fijo = models.CharField(max_length=10)
#     emp_id =models.ForeignKey(proy_empresa)
#     user_id = models.ForeignKey(User)

#     class Meta:
#         db_table = 'persona'
#         # permissions = (("list_persona", "Can list persona"),)
#         default_permissions = ()

#     def __unicode__(self):
#         return self.per_nombres+" "+self.per_apellidos

# class proy_representante(models.Model):
#     emp_id = models.ForeignKey(proy_empresa)
#     per_id = models.ForeignKey(proy_persona)
#     rep_fecha_inicio = models.DateField()
#     rep_fecha_fin = models.DateField()
#     rep_descripcion = models.CharField(max_length=800)
#     is_active = models.BooleanField(default=True)

#     class Meta:
#         db_table = 'representante'
#         # permissions = (("list_representante", "Can list representante"),)
#         default_permissions = (['change','list'])


#     def __unicode__(self):
#         return self.per_id

# class proy_area(models.Model):
#     ar_nombre = models.CharField(max_length=100)
#     ar_descripcion = models.CharField(max_length=100)
#     ar_etiqueta=models.CharField(max_length=100)
#     ar_estado = models.BooleanField(default=True)
#     ar_direccion = models.CharField(max_length=100)
#     ar_logo= models.ImageField(upload_to='areas_logos', blank=True, null=True, default='img_parcelas/sin_imagen.gif')
#     emp_id= models.ForeignKey(proy_empresa)
#     class Meta:
#         default_permissions=(['add','change','delete','list'])

# class proy_subarea(models.Model):
#     sar_nombre = models.CharField(max_length=100)
#     sar_etiqueta=models.CharField(max_length=100)
#     sar_imagen = models.ImageField(upload_to='img_parcelas', blank=True, null=True, default='img_parcelas/sin_imagen.gif')
#     sar_latitud = models.FloatField(max_length=100)
#     sar_longitud = models.FloatField(max_length=50)
#     sar_hem_lat = models.CharField(max_length=50)
#     sar_hem_lon = models.CharField(max_length=50)
#     ar_id= models.ForeignKey(proy_area)
#     class Meta:
#         default_permissions=(['add','change','delete','list'])

# class iot_tipo_sensor(models.Model):
#     ts_nombre   = models.CharField(max_length=100)

# class iot_fabricante(models.Model):
#     fac_nombre  = models.CharField(max_length=100)

# class iot_unidad_medida(models.Model):
#     umed_nombre         = models.CharField(max_length=100)
#     umed_abreviatura    = models.CharField(max_length=100)

# class iot_ted(models.Model):
#     ted_formula     = models.CharField(max_length=50)
#     ted_serie       = models.CharField(max_length=50)
#     ted_min         = models.BigIntegerField()
#     ted_max         = models.BigIntegerField()
#     ted_descripcion = models.CharField(max_length=100)
#     umed_id         = models.ForeignKey(iot_unidad_medida)
#     fac_id          = models.ForeignKey(iot_fabricante)
#     ts_id           = models.ForeignKey(iot_tipo_sensor)

# class iot_tecnologia(models.Model):
#     tec_nombre      = models.CharField(max_length=100)
#     tec_descripcion = models.CharField(max_length=50)
#     tec_icono       = models.CharField(max_length=50)

# class iot_dispositivo(models.Model):
#     dis_nombre          = models.CharField(max_length=100)
#     dis_descripcion     = models.CharField(max_length=100)
#     dis_token           = models.CharField(max_length=100)
#     dis_mac             = models.CharField(max_length=50)
#     dis_emp_codigo      = models.CharField(max_length=50)
#     dis_fecha_registro  = models.DateField()
#     dis_activo          = models.BooleanField(default=True)
#     tec_id              = models.ForeignKey(iot_tecnologia)

# class iot_sensor(models.Model):
#     sen_nombre      = models.CharField(max_length=100)
#     sen_serie       = models.CharField(max_length=100)
#     sen_interfaz    = models.CharField(max_length=50)
#     ted_id          = models.ForeignKey(iot_ted)
#     dis_id          = models.ForeignKey(iot_dispositivo)

# class iot_tipo_actuador(models.Model):
#     tact_nombre     = models.CharField(max_length=100)

# class iot_actuador(models.Model):
#     act_nombre          = models.CharField(max_length=50)
#     act_descripcion     = models.CharField(max_length=200)
#     act_serie           = models.CharField(max_length=100)
#     act_estado          = models.CharField(max_length=100)
#     act_interfaz        = models.BigIntegerField()
#     tact_id             = models.ForeignKey(iot_tipo_actuador)
#     dis_id              = models.ForeignKey(iot_dispositivo)

# User.add_to_class('id_empresa', models.ForeignKey(proy_empresa, blank=True, null=True))
# Group.add_to_class('id_empresa', models.ForeignKey(proy_empresa, blank=True, null=True))

# # Modelos app_modular
# class mod_modulo(models.Model):
#     mod_name = models.CharField(max_length=100)
#     mod_fecha_carga=models.DateField(auto_now=True)
#     mod_fecha_instalacion=models.DateField()
#     mod_hora_instalacion=models.TimeField()
#     mod_archivo=models.FileField()
#     mod_descripcion=models.TextField()
#     id_usuario=models.ForeignKey(User)

#     class Meta:
#         db_table='modulo'
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.mod_name + ' - ' + self.id_usuario.get_full_name()

# class tipo_modificacion(models.Model):
#     t_mod_nombre=models.TextField(max_length=50)
#     t_mod_icono=models.TextField(max_length=50)

#     class Meta:
#         default_permissions=()

#     def __unicode__(self):
#         return self.t_mod_nombre

# class mod_notificaciones(models.Model):
#     not_mensaje = models.TextField()
#     not_estado = models.BooleanField()
#     not_fecha = models.DateField()
#     id_modulo = models.ForeignKey(mod_modulo, blank=True,null=True)
#     id_t_mod = models.ForeignKey(tipo_modificacion)

#     class Meta:
#         db_table='notificaciones'
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return  self.not_mensaje

# class mod_tecnologias(models.Model):
#     tec_nombre=models.TextField()
#     tec_estado = models.BooleanField()
#     tec_fecha = models.DateField(auto_now=True)
#     id_usuario = models.ForeignKey(User)

#     class Meta:
#         db_table='tecnologias'
#         default_permissions=(['add','change','delete','list'])

#     def __unicode__(self):
#         return self.tec_nombre

# # Modelos app_seguridad
# class seg_log_acciones(models.Model):
#     la_fecha_hora = models.DateTimeField(auto_now=True)
#     la_accion = models.CharField(max_length=100)
#     usu_id = models.ForeignKey(User)

#     class Meta:
#         db_table = 'logs'
#         # permissions = (("list_logs", "Can list logs"),)
#         default_permissions = ()

#     def __unicode__(self):
#         return str(self.la_accion+" - "+self.usu_id.get_full_name())

# class seg_menu(models.Model):
#     men_nombre =  models.CharField(max_length=100)
#     men_estado = models.BooleanField(default=True)
#     men_icono = models.CharField(max_length=100, blank=True, null=True)
#     men_ruta = models.CharField(max_length=100)
#     men_tipo = models.CharField(max_length=20)
#     men_id_padre = models.ForeignKey('self', blank=True, null=True)
#     permisos = models.OneToOneField(Permission, blank=True, null=True)

#     class Meta:
#         db_table = 'menu'
#         # permissions = (("list_menu", "Can list menus"),)
#         default_permissions = ()

#     def __unicode__(self):
#         return self.men_nombre