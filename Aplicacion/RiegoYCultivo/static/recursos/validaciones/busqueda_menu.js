function buscar_menu(){
	var busqueda=$('#txt_busqueda').val();
	if (busqueda.trim().length>0) {
		 $('.texto_menu').each(function(i, obj) {
		 	var nombre_menu=$(this).html();
		 	if(nombre_menu.toLowerCase().indexOf(busqueda.toLowerCase())>-1){	 		
		 		$('#'+nombre_menu.split(' ').join('')).css('display', '');
		 	}else{
		 		$('#'+nombre_menu.split(' ').join('')).css('display', 'none');
		 	}
		});
	}else{
		$('.sidebar-menu > li').css('display', '');
	}
}

function buscar_(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 13 )buscar_menu();
}