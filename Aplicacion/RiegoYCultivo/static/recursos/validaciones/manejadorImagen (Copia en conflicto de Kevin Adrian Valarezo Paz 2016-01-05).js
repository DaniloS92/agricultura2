//controla k el archivo sea imagen
function esImagen(tField) {
    file=tField.value;
    extArray = new Array(".jpg",".png", ".jpeg");
    //valor booleano que será true mientras el archivo sea una imagen y false si no lo es #}
    allowSubmit = false;
    //Seguira con el proceso siempre que se haya seleccionado un archivo #}
    if (!file) return;
    // El archivo debe contener contrabarras en su nombre #}
        while (file.indexOf("\\") != -1)
            file = file.slice(file.indexOf("\\") + 1);
            //convierte en minusculas la extension del archivo #}
            ext = file.slice(file.indexOf(".")).toLowerCase();
            for (var i = 0; i < extArray.length; i++) {
                //Compara que la extension sea la misma que alguna posicion del arreglo #}
                if (extArray[i] == ext) {
                    allowSubmit = true;
                    break;
                    }
                }
              //{# En caso de que la extension no sea una de imagen, lanza un mensaje #}
                if (!allowSubmit) {
                    tField.value="";
                    var notification = alertify.notify('El archivo seleccionado no es una imagen\n' +
                            'Por favor seleccione un nuevo archivo','success',
                                5, function(){  console.log('dismissed'); });
                    tField.focus();

                }
}

