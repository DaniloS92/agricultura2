    function guardar_rol(){
        //agregar fecha_fin de descripcion
        var txtRol=$("#txtRol").val();
        var listPermisos=[];
        for(i=0; i<document.getElementById("listPermisos").length; i++){
            if (document.getElementById("listPermisos")[i].selected)
                listPermisos.push(document.getElementById("listPermisos")[i].value)
        }
        $("#alerta_usuario").empty();
        $("#id_usuario_rol").empty();
        if(txtRol.trim() == "" || listPermisos.length == 0){
            $("#id_usuario_rol").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Llene los campos obligatorios' +
                '</div>');
            $('html,body').animate({
                scrollTop: $("#id_usuario_rol").offset().top
            }, 500);
        }else{
            var formData = new FormData();
            var agregado = $("#id_tpro_nombre").val();
            formData.append('txtRol', txtRol);
            formData.append('listPermisos', JSON.stringify(listPermisos));
            $.ajax({
                data: formData,
                url: '/seguridad/usuarios/agregar/rol/',
                processData: false,
                contentType: false,
                cache: false,
                type: 'post',
                //beforeSend: function(){
                //    var opts = {
                //        lines: 15, // The number of lines to draw
                //        length: 24, // The length of each line
                //        width: 15, // The line thickness
                //        radius: 29, // The radius of the inner circle
                //        corners: 1, // Corner roundness (0..1)
                //        rotate: 6, // The rotation offset
                //        direction: 1, // 1: clockwise, -1: counterclockwise
                //        color: '#000', // #rgb or #rrggbb or array of colors
                //        speed: 0.8, // Rounds per second
                //        trail: 41, // Afterglow percentage
                //        shadow: false, // Whether to render a shadow
                //        hwaccel: false, // Whether to use hardware acceleration
                //        className: 'spinner', // The CSS class to assign to the spinner
                //        zIndex: 2e9, // The z-index (defaults to 2000000000)
                //        top: '50%', // Top position relative to parent
                //        left: '50%' // Left position relative to parent
                //    };
                //    var target = document.getElementById('foo');
                //    spinner = new Spinner(opts).spin(target);
                //},
                //complete: function(){
                //    spinner.stop();
                //},
                success: function (data) {
                    $("#id_tipo").load(location.href+" #id_tipo>*","");
                    $("#id_tipo_proyecto").load(location.href+" #id_tipo_proyecto>*","");
                    //$("#id_tipo option[text="+agregado+"]").attr('selected', 'selected');

                    if (data.result=="OK"){
                        $('#modal_rol').modal('hide');////////////////////////////////////////////////////////////7
                        $("#alerta_usuario").append('<div class="alert alert-success fade in">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        '<strong>Correcto: </strong>'+data.mensaje +
                        '</div>');
                        $('html,body').animate({ 
                            scrollTop: $("#alerta_usuario").offset().top
                        }, 500);

                        $("#id_usuario_rol").append('<div class="alert alert-success fade in">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        '<strong>Correcto: </strong>'+data.mensaje +
                        '</div>');
                        $('html,body').animate({ 
                            scrollTop: $("#id_usuario_rol").offset().top
                        }, 500);

                        $("#txtRol").val("");
                        $("#listPermisos").val("");
                        $("#listRoles").append($('<option>', {
                            value: data.vRol_id,
                            text: data.vRol_nombre
                        }));
                    }else{
                        $('#btn_modificar_tipo_pro').attr("disabled", false);////////////////////////////////////
                        $("#id_usuario_rol").append('<div class="alert alert-danger fade in">' +////////////////////
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        '<strong>Error: </strong>'+data.mensaje +
                        '</div>');
                        $('html,body').animate({
                            scrollTop: $("#id_usuario_rol").offset().top
                        }, 500);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#btn_modificar_tipo_pro').attr("disabled", false);//////////////////////////////////
                    alert(jqXHR.responseText);
                    $("#id_usuario_rol").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong>Error: </strong>Dyuyay sufrio un error inesperado, vuelva a intentar.' +
                            '</div>');
                        $('html,body').animate({
                            scrollTop: $("#id_usuario_rol").offset().top
                        }, 500);
                }
            });
        }
    }

  function validar(emp_nombre){

      // console.log(datos);

    return true;
    }

function mostrar(){
    // Limpiar campos de texto
    $("#modal_rol").modal("show");
    $("#txtRol").val("");
    $("#listPermisos").val("");
}