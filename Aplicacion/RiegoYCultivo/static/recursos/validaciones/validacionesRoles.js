function validar(){
    lstPermisos=[]
    for(i=0; i<document.getElementById("listPermisos").length; i++){
        if (document.getElementById("listPermisos")[i].selected)
            lstPermisos.push(document.getElementById("listPermisos")[i].value)
    }
    if (lstPermisos.length==0){
         $("#alerta_rol").append('<div class="alert alert-danger fade in">' +
            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
            '<strong>Error: </strong>Escoja al menos un permiso para el rol...' +
            '</div>');
        $('html,body').animate({
            scrollTop: $("#alerta_rol").offset().top
        }, 500);
        return false;
    }
    return true;
}