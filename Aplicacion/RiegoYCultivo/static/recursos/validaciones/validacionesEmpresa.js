function validarVacios1(){
    var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var regexUrl = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;

    if($.trim($('#txt_ruc').val()) == "" || $.trim($('#txt_correo_e').val()) == "" || $.trim($('#txt_nombre_c').val()) == "" || $.trim($('#txt_nombre_j').val()) == "" || $.trim($('#txt_dominio').val()) == "" ||
        $.trim($('#txt_web').val()) == "" || $.trim($('#txt_grupo').val()) == "" || $.trim($('#txt_direccion_e').val()) == ""){
        $("#alerta_empresa").empty();
        $("#alerta_empresa").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>LLene los campos obligatorios' +
                '</div>');
        return false;
    }else if(!regexEmail.test($.trim($('#txt_correo_e').val()))) {
        $("#alerta_empresa").empty();
        $("#alerta_empresa").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Ingrese un correo válido' +
                '</div>');
        return false;
    }else if(!regexUrl.test($.trim($('#txt_web').val()))) {
        $("#alerta_empresa").empty();
        $("#alerta_empresa").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Ingrese una URL válida' +
                '</div>');
        return false;
    }
    else{
        return true;
    }
}

function validarVacios2(){
    var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contrasena1 = $("#txt_password").val();
    $("#alerta_empresa2").empty();

    if($.trim($('#txt_nombre_p').val()) == "" || $.trim($('#txt_apellido_p').val()) == "" || $.trim($('#txt_telefono_f').val()) == "" || $.trim($('#txt_telefono_m').val()) == "" || $.trim($('#txt_fecha_i').val()) == "" || $.trim($('#txt_fecha_f').val()) == "" ||
        $.trim($('#txt_correo_p').val()) == "" || $.trim($('#txt_usuario_p').val()) == "" || $.trim($('#txt_password').val()) == "" || $.trim($('#txt_password_v').val()) == "" || $.trim($('#txt_direccion_p').val()) == "" || $.trim($('#txt_descripcion_p').val()) == ""){
        $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>LLene los campos obligatorios' +
                '</div>');
        return false;
    }else if(!regexEmail.test($.trim($('#txt_correo_p').val()))){
        $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Ingrese un correo válido' +
                '</div>');
        return false;
    }else if ($("#txt_password").val() != $("#txt_password_v").val()){
        $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
        '<strong>Error: </strong>Las contraseñas no coinciden' +
        '</div>');
        return false
    }else if(contrasena1 == null || contrasena1.length == 0 || contrasena1.length < 6 || /^\s+$/.test(contrasena1)){
        $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
        '<strong>Error: </strong>La contraseña deber ser mayor a 6 caracteres' +
        '</div>');
        return false
    }else{
        return true;
    }
}

function validaSoloNumeros() {
    if ((event.keyCode < 48) || (event.keyCode > 57))
        event.returnValue = false;
}

function validaSoloLetras() {
    if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122) && (event.keyCode != 180) && (event.keyCode != 241)
         && (event.keyCode != 250)&& (event.keyCode != 243) && (event.keyCode != 237) && (event.keyCode != 225) && (event.keyCode != 233))
        event.returnValue = false;
}

function fechaMininaFase(inicio, fin){
    var sFecha = $("#"+inicio).val();
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fechaInicio = aFecha[0]+'-'+aFecha[1]+'-'+aFecha[2];
    fechaInicio= new Date(fechaInicio);
    fechaInicio.setDate(fechaInicio.getDate()+2);
    var anno=fechaInicio.getFullYear();
    var mes= fechaInicio.getMonth()+1;
    var dia= fechaInicio.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaF = anno+sep+mes+sep+dia;
    document.getElementById(fin).setAttribute("min",fechaF);
    document.getElementById(fin).value=fechaF;
}
function fechaActualFase(inicio, fin){
    var d=1;
    var Fecha = new Date();
    var sFecha = (Fecha.getDate() + "-" + (Fecha.getMonth() +1) + "-" + Fecha.getFullYear());
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fechaInicio = aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0];
    fechaInicio= new Date(fechaInicio);
    fechaInicio.setDate(fechaInicio.getDate());
    var anno=fechaInicio.getFullYear();
    var mes= fechaInicio.getMonth()+1;
    var dia= fechaInicio.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaF = anno+sep+mes+sep+dia;
    document.getElementById(inicio).value=fechaF;
    var fechaFinal = aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0];
    fechaFinal= new Date(fechaFinal);
    fechaFinal.setDate(fechaFinal.getDate()+parseInt(d));
    var anno=fechaFinal.getFullYear();
    var mes= fechaFinal.getMonth()+1;
    var dia= fechaFinal.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaF = anno+sep+mes+sep+dia;
    document.getElementById(fin).setAttribute("min",fechaF);
    document.getElementById(fin).value=fechaF;
}

//Guardar datos de empresa y representante
function guardar_empresa_representante(url){
    $("#alerta_empresa").empty();
    var form = document.forms.namedItem("form_registro_empresa");
    datos = new FormData(form);
    $.ajax({
        type: "POST",
        url:url,
        dataType: "json",
        async: true,
        data: datos,
        processData: false,
        contentType: false,
        beforeSend: function(){
            //var opts = {
            //    lines: 15, // The number of lines to draw
            //    length: 24, // The length of each line
            //    width: 15, // The line thickness
            //    radius: 29, // The radius of the inner circle
            //    corners: 1, // Corner roundness (0..1)
            //    rotate: 6, // The rotation offset
            //    direction: 1, // 1: clockwise, -1: counterclockwise
            //    color: '#000', // #rgb or #rrggbb or array of colorsm
            //    speed: 0.8, // Rounds per second
            //    trail: 41, // Afterglow percentage
            //    shadow: false, // Whether to render a shadow
            //    hwaccel: false, // Whether to use hardware acceleration
            //    className: 'spinner', // The CSS class to assign to the spinner
            //    zIndex: 2e9, // The z-index (defaults to 2000000000)
            //    top: '50%', // Top position relative to parent
            //    left: '50%' // Left position relative to parent
            //};
            //var target = document.getElementById('foo');
            //spinner = new Spinner(opts).spin(target);
        },
        complete: function(){
            //spinner.stop();
        },
        success: function(json){
            if (json.result == "OK"){
                $("#alerta_empresa2").append('<div class="alert alert-success fade in">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    '<strong>Correcto: </strong>'+json.message +
                    '</div>');
                $('html,body').animate({
                    scrollTop: $("#alerta_empresa2").offset().top
                }, 500);

                //Redireccion a la url para autenticar al usuario
                window.location.href = "/empresa/datos/completo/?usuario="+$("#txt_usuario_p").val()+"&&clave="+$("#txt_password").val();

            }else if (json.result == "XRUC"){
                btnAtras();
                $("#alerta_empresa").append('<div class="alert alert-danger fade in">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    '<strong>Error: </strong>'+json.message +
                    '</div>');
                $('html,body').animate({
                    scrollTop: $("#alerta_empresa").offset().top
                }, 500);
            }else if (json.result == "XREP"){
                $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    '<strong>Error: </strong>'+json.message +
                    '</div>');
                $('html,body').animate({
                    scrollTop: $("#alerta_empresa2").offset().top
                }, 500);
            }else{
                $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    '<strong>Error: </strong>'+json.message +
                    '</div>');
                $('html,body').animate({
                    scrollTop: $("#alerta_empresa2").offset().top
                }, 500);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#alerta_empresa2").append('<div class="alert alert-danger fade in">' +
                    '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                    '<strong>Error: </strong>Dyuyay sufrio un error inesperado, vuelva a intentar.' +
                    '</div>');
                $('html,body').animate({
                    scrollTop: $("#alerta_empresa2").offset().top
                }, 500);
                alert(jqXHR.status+"::"+jqXHR.responseText)
        }
    });
}

//Actualizar datos de la empresa
function actualizar_empresa(url){
    $("#alerta_empresa").empty();
    var form = document.forms.namedItem("form_registro_empresa");
    datos = new FormData(form);
    if(validarVacios1()){
            $.ajax({
            type: "POST",
            url:url,
            dataType: "json",
            async: true,
            data: datos,
            processData: false,
            contentType: false,
            beforeSend: function(){
                //var opts = {
                //    lines: 15, // The number of lines to draw
                //    length: 24, // The length of each line
                //    width: 15, // The line thickness
                //    radius: 29, // The radius of the inner circle
                //    corners: 1, // Corner roundness (0..1)
                //    rotate: 6, // The rotation offset
                //    direction: 1, // 1: clockwise, -1: counterclockwise
                //    color: '#000', // #rgb or #rrggbb or array of colorsm
                //    speed: 0.8, // Rounds per second
                //    trail: 41, // Afterglow percentage
                //    shadow: false, // Whether to render a shadow
                //    hwaccel: false, // Whether to use hardware acceleration
                //    className: 'spinner', // The CSS class to assign to the spinner
                //    zIndex: 2e9, // The z-index (defaults to 2000000000)
                //    top: '50%', // Top position relative to parent
                //    left: '50%' // Left position relative to parent
                //};
                //var target = document.getElementById('foo');
                //spinner = new Spinner(opts).spin(target);
            },
            complete: function(){
                //spinner.stop();
            },
            success: function(json){
                if (json.result == "OK"){
                    $("#alerta_empresa").append('<div class="alert alert-success fade in">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        '<strong>Correcto: </strong>'+json.message +
                        '</div>');
                    $('html,body').animate({
                        scrollTop: $("#alerta_empresa").offset().top
                    }, 500);
                }else{
                    $("#alerta_empresa").append('<div class="alert alert-danger fade in">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        '<strong>Error: </strong>'+json.message +
                        '</div>');
                    $('html,body').animate({
                        scrollTop: $("#alerta_empresa").offset().top
                    }, 500);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#alerta_empresa").append('<div class="alert alert-danger fade in">' +
                        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                        '<strong>Error: </strong>Dyuyay sufrio un error inesperado, vuelva a intentar.' +
                        '</div>');
                    $('html,body').animate({
                        scrollTop: $("#alerta_empresa").offset().top
                    }, 500);
                    alert(jqXHR.status+"::"+jqXHR.responseText)
            }
        });
    }
}

function btnSiguiente(){
    $("#alerta_empresa").empty();
    $("#alerta_empresa2").empty();
    if(validarVacios1() && $.trim($('#txt_logo').val()) != ""){
        $("#id_form_representante").show();
        $("#id_form_empresa").hide();
        $("#btn_siguiente").hide();
        $("#btn_atras").show();
        $("#btn_guardar").show();
    }
}

function btnAtras(){
    $("#alerta_empresa").empty();
    $("#alerta_empresa2").empty();
    $("#id_form_representante").hide();
    $("#id_form_empresa").show();
    $("#btn_siguiente").show();
    $("#btn_atras").hide();
    $("#btn_guardar").hide();
}

function btnGuardar(){
    $("#alerta_empresa2").empty();
    if(validarVacios2() && validarVacios1()){
        //Guardar a la BD
        guardar_empresa_representante("/empresa/datos/guardar/")
    }
}