/**
 * Created by jefferson on 24/1/2016.
 */
function valida_formulario_gis(){
    $("#alerta_modal_nuevo").empty();
    var direccion=frames.frameNuevoGis.document.getElementById("nuevo_mi_direccion").value;
    var  latitud=frames.frameNuevoGis.document.getElementById("nuevo_mi_latitud").value;
    var longitud= frames.frameNuevoGis.document.getElementById("nuevo_mi_longitud").value;
    var observacion= frames.frameNuevoGis.document.getElementById("nuevo_observacion").value;
    var  parcela= frames.frameNuevoGis.document.getElementById("nuevo_parcelas").value;
    return($.trim(direccion) == "" || latitud == "" || $.trim(longitud)== "" ||$.trim(observacion) == ""|| parcela == "");
   }
function valida_formulario_modificar(){
    $("#alerta_modal_nuevo").empty();
    var idModificarGis= document.getElementById("idModificarGis").value;
    var direccion= frames.frameModifcarGis.document.getElementById("modificar_mi_direccion").value;
    var latitud= frames.frameModifcarGis.document.getElementById("modificar_mi_latitud").value;
    var longitud =frames.frameModifcarGis.document.getElementById("modificar_mi_longitud").value;
    var observacion= frames.frameModifcarGis.document.getElementById("modificar_observacion").value;
    return($.trim(direccion) == "" || latitud == "" || $.trim(longitud)== "" ||$.trim(observacion) == "");
   }
function guardarPunto() {
    if (!valida_formulario_gis()) {
        $.ajax({
            // la URL para la petición
            url: '/app_gmaps/gis/guardar/',
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data: {
                direccion: frames.frameNuevoGis.document.getElementById("nuevo_mi_direccion").value,
                latitud: frames.frameNuevoGis.document.getElementById("nuevo_mi_latitud").value,
                longitud: frames.frameNuevoGis.document.getElementById("nuevo_mi_longitud").value,
                observacion: frames.frameNuevoGis.document.getElementById("nuevo_observacion").value,
                parcela: frames.frameNuevoGis.document.getElementById("nuevo_parcelas").value
            },
            type: 'GET',
            success: function (data) {
                if (data.result == "OK") {
                    location.reload();
                }
            }
        })
    } else {
        $("#alerta_modal_nuevo").append('<div class="alert alert-danger fade in">' +
        '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
        '<strong>Error: </strong>Llene los campos vacios'+
        '</div>');
        $('html,body').animate({
            scrollTop: $("#alerta_modal_modificar").offset().top
            }, 500);
             return false;
    }
}
    function guardarPuntoModificado() {

        if (!valida_formulario_modificar()) {
            $.ajax({
                // la URL para la petición
                url: '/app_gmaps/gis/actualizar/',
                // la información a enviar
                // (también es posible utilizar una cadena de datos)

                data: {
                    idModificarGis: document.getElementById("idModificarGis").value,
                    mi_direccion: frames.frameModifcarGis.document.getElementById("modificar_mi_direccion").value,
                    mi_latitud: frames.frameModifcarGis.document.getElementById("modificar_mi_latitud").value,
                    mi_longitud: frames.frameModifcarGis.document.getElementById("modificar_mi_longitud").value,
                    observacion: frames.frameModifcarGis.document.getElementById("modificar_observacion").value
                },
                type: 'GET',
                success: function (data) {
                    location.reload()
                }
            })
         } else {
                $("#alerta_modal_modificar").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Llene los campos vacios'+
                '</div>');
                $('html,body').animate({
                    scrollTop: $("#alerta_modal_modificar").offset().top
                    }, 500);
                     return false;
            }
    }
    function borrarFoto(id){
        document.getElementById("idAnexo").value=id;
    }
    function agregarPunto(){
        $('#modalNuevoMapa').modal('show');
    }
    function ver_Anexos(idGis){
        $('#idGis').hide();
        $('#idAnexo').hide();
        $("#modalAnexos").modal('show');
        //parent.frameAnexos.location.href="/app_gmaps/gis/anexo/"+idGis;
        //$('#anexos').load("/app_gmaps/gis/anexo/"+idGis,"_self")
        document.getElementById("idGis").value=idGis;
    }
    function OpenInNewTab(url) {
         $('#verpdf').modal('show');
          parent.MyIFrame.location.href=url;
    }
    idPglobal=0
    function eliminar(idP){
        document.getElementById("idGis").value=idP;
        $.ajax({
            // la URL para la petición
            url : '/app_gmaps/gis/eliminar/',
            // la información a enviar
            // (también es posible utilizar una cadena de datos)

            data : {
                idPunto:idP
            },
            success : function(data) {
                location.reload()
            }
        })
    }
    function modificarPunto(idP) {
            $.ajax({
                // la URL para la petición
                url: '/app_gmaps/gis/ir/',
                // la información a enviar
                // (también es posible utilizar una cadena de datos)

                data: {
                    idPunto: idP
                },
                // especifica si será una petición POST o GET
                type: 'GET',
                // código a ejecutar si la petición es satisfactoria;
                // la respuesta es pasada como argumento a la función
                success: function (data) {
                    $('#idModificarGis').hide();
                    document.getElementById("idModificarGis").value = idP;
                    $('#modalModificarMapa').modal('show');
                    frames.frameModifcarGis.document.getElementById("modificar_mi_direccion").value = data[3];
                    frames.frameModifcarGis.document.getElementById("modificar_mi_latitud").value = data[0];
                    frames.frameModifcarGis.document.getElementById("modificar_mi_longitud").value = data[1];
                    frames.frameModifcarGis.document.getElementById("modificar_observacion").value = data[2];
                    frames.frameModifcarGis.verMensaje(data[0], data[1]);
                }
            })
    }
    function validadFormulario(){
         document.getElementById("frmArchivos").onsubmit=function() {
             var imagen = document.getElementById("files").files;
             if (imagen.length == 0) {
                $("#alerta_documentos_add").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Seleccione al menos un archivo' +
                '</div>');
                return false;
             } else {
                 for (x = 0; x < imagen.length; x++) {
                     if (imagen[x].type != "image/png" && imagen[x].type != "image/jpg" && imagen[x].type != "image/jpeg" && imagen[x].type != "image/gif" && imagen[x].type != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && imagen[x].type != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && imagen[x].type != "application/pdf") {
                        $("#alerta_documentos_add").append('<div class="alert alert-danger fade in">' +
                            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                            '<strong>Error: </strong>El archivo ' + imagen[x].name + " no es compatible "+
                            '</div>');
                        $('html,body').animate({
                            scrollTop: $("#alerta_documentos_add").offset().top
                        }, 500);
                         return false;
                     }
                 }
             }
         }
    }
