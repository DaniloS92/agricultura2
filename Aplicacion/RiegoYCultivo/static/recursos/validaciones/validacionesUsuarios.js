//Validar contraseñas
function validarContrasena(c1, c2){
    if(c1 == c2)
        return true;
    else
        return false;
}

//Funcion que permite solo Numeros
function validaSoloNumeros() {
 if ((event.keyCode < 48) || (event.keyCode > 57))
    event.returnValue = false;
}

//Valida solo texto o letras
function validaSoloTexto() {
 if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
    event.returnValue = false;
}

//Validaciones varias
function informacionCorrecta(){
    var contador = 0;


    var contrasena1 = $("#txtPassword").val();
    var contrasena2 = $("#txtPasswordV").val();

    if(contrasena1 == null || contrasena1.length == 0 || contrasena1.length < 6 || /^\s+$/.test(contrasena1)) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('La contraseña deber ser mayor a 6 caracteres' ,10);
        contador++;
    }


    console.log(contador)
    if(contador == 0)
        return true;
    else
        return false;
}