function validarDatos(){
    var regexEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contrasena1 = $('#txtPassword').val();

    if(!regexEmail.test($.trim($('#txtCorreo').val()))){
        $("#alerta_editar_perfil").empty();
        $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Ingrese un correo válido' +
                '</div>');
        return false;
    //}else if(contrasena1.length < 6 || /^\s+$/.test(contrasena1)){
    //    $("#alerta_editar_perfil").empty();
    //    $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
    //            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
    //            '<strong>Error: </strong>La contraseña debe ser mayor a 6 caracteres' +
    //            '</div>');
    //    return false;
    }else if($('#txtPassword').val() != $('#txtPAswwordV').val()){
        $("#alerta_editar_perfil").empty();
        $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Las contraseñas no coinciden' +
                '</div>');
        return false;
    }else if($.trim($('#txtUsuario').val()) == "" || $.trim($('#txtCorreo').val()) == "" || $.trim($('#txtNombre').val()) == "" || $.trim($('#txtApellido').val()) == "" ||
        $.trim($('#txtDireccion').val()) == "" || $.trim($('#txtMovil').val()) == "" || $.trim($('#txtTlfono').val()) == "" || $('#txtPassword').val() == "" || $('#txtPAswwordV').val() == ""){
        $("#alerta_editar_perfil").empty();
        $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>LLene los campos obligatorios' +
                '</div>');
        return false;
    }else{
        return true;
    }
}

function validarCambiarPass(){
    var contrasena1 = $('#txtPasswordActual').val();
    var contrasenaA = $('#txtPasswordC').val();
    var contrasenaR = $('#txtPasswordN').val();
    //alert(contrasena1.length);
    //if(contrasena1.length < 6 || /^\s+$/.test(contrasena1)){
    //    $("#alerta_editar_perfil").empty();
    //    $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
    //            '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
    //            '<strong>Error: </strong>La contraseña debe ser mayor a 6 caracteres' +
    //            '</div>');
    //    return false;
    //}else
    if($('#txtPasswordN').val() != $('#txtPasswordC').val()){
        $("#alerta_editar_perfil").empty();
        $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>Las contraseñas no coinciden' +
                '</div>');
        return false;
    }else if(contrasenaA.length < 6 || /^\s+$/.test(contrasenaA) || contrasenaR.length < 6 || /^\s+$/.test(contrasenaR)){
        $("#alerta_editar_perfil").empty();
        $("#alerta_editar_perfil").append('<div class="alert alert-danger fade in">' +
                '<a href="#" class="close" data-dismiss="alert">&times;</a>' +
                '<strong>Error: </strong>La contraseña debe ser mayor a 6 caracteres' +
                '</div>');
        return false;
    }else{
        return true;
    }
}