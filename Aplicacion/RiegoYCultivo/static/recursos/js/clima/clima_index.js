function geolocalizacion_actual(){
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(objPosition)
		{
			var lon = objPosition.coords.longitude;
			var lat = objPosition.coords.latitude;
			var dir = "";
			var latlng = new google.maps.LatLng(lat, lon);
			geocoder = new google.maps.Geocoder();
			geocoder.geocode({"latLng": latlng}, function(results, status)
			{
				if (status == google.maps.GeocoderStatus.OK)
				{
					if (results[0])
					{
						dir = results[0].formatted_address;
                        weatherGeocode(dir);
					}
					else
					{
						$('#id_contenedor').empty();
                        $('#id_contenedor').html('Su ubicacion no es admitida')
					}
				}
				else
				{
					$('#id_contenedor').empty();
                    $('#id_contenedor').html('Error interno al obtener su ubicacion '+status)
				}
			});
		}, function(objPositionError)
		{
			switch (objPositionError.code)
			{
				case objPositionError.PERMISSION_DENIED:
                    $('#id_contenedor').empty();
                    $('#id_contenedor').html('Su configuracion deniega el acceso para obtener informaci sobre su ubicacion')
				break;
				case objPositionError.POSITION_UNAVAILABLE:
                    $('#id_contenedor').empty();
                    $('#id_contenedor').html('No se ha podido acceder a la información de su posición.')
				break;
				case objPositionError.TIMEOUT:
                    $('#id_contenedor').empty();
                    $('#id_contenedor').html('El servicio ha tardado demasiado tiempo en responder.')
				break;
				default:
                    $('#id_contenedor').empty();
                    $('#id_contenedor').html('Error desconocido.')
			}
		}, {
			maximumAge: 75000,
			timeout: 15000
		});
	}
	else
	{
        $('#id_contenedor').empty();
        $('#id_contenedor').html('Su navegador no soporta la API de geolocalización.')
	}
}


function weatherGeocode(search) {
		var status;
		var results;
		var html = '';
		var msg = '';

		var search = search;
		if (search) {
			now = new Date();
			var query = 'select * from geo.places where text="'+ search +'"';
			var api = 'http://query.yahooapis.com/v1/public/yql?q='+ encodeURIComponent(query) +'&rnd='+ now.getFullYear() + now.getMonth() + now.getDay() + now.getHours() +'&format=json&callback=?';
			$.ajax({
				type: 'GET',
				url: api,
				dataType: 'json',
				success: function(data) {
					if (data.query.count > 0 ) {
                        _getWeatherAddress(data.query.results.place);
                    }
				},
				error: function(data) {
                    $('#id_contenedor').empty();
                    $('#id_contenedor').html('Error interno.')
				}
			});

		} else {
                    $('#id_contenedor').empty();
                    $('#id_contenedor').html('Dyuyay no pudo encontrar su direccion')
		}
	}

	function _getWeatherAddress(data) {

		// Get address
		var address = data.name;
		if (data.admin2) address += ', ' + data.admin2.content;
		if (data.admin1) address += ', ' + data.admin1.content;
		address += ', ' + data.country.content;

		// Get WEOID
		var woeid = data.woeid;
        loadWeather(woeid);
	}





function loadWeather(woeid) {
    console.log('woeid '+woeid+ ' location '+location)
  $.simpleWeather({
    //location: location,
    woeid: woeid,
    unit: 'c',
    success: function(weather) {
      $('#id_ubicacion').html(''+weather.country+'-'+weather.city);
      $('#id_sol').html('<b>- Puesta de sol: </b>'+weather.sunset+'');
      $('#id_amanecer').html('<b>- Amanecer: </b> '+weather.sunrise+'');
      $('#id_contenedor_imagen > img').attr("src", ""+weather.image);
      $('#id_temperatura').val(parseInt(weather.temp)).trigger('change');
      $('#id_temperatura1').html('<b>- Actual: </b>'+weather.temp+' °'+weather.units.temp+'<b>- Min: </b>'+weather.low+' °'+weather.units.temp+'<b>- Max: </b>'+weather.high+' °'+weather.units.temp);
      $('#id_temperatura2').html('<b>- Actual: </b>'+weather.alt.temp+' °'+weather.alt.unit+'<b>- Min: </b>'+weather.alt.low+' °'+weather.alt.unit+'<b>- Max: </b>'+weather.alt.high+' °'+weather.alt.unit);
      $('#id_humedad').val(parseInt(weather.humidity)).trigger('change');
      $('#id_humedad1').html('<b>- Humedad: </b>'+weather.humidity+' %');
      $('#id_presion').html('<b>- Presión: </b>'+weather.pressure+' '+weather.units.pressure);
      $('#id_viento').val(parseInt(weather.wind.speed)).trigger('change');
      $('#id_velocidad').html('<b>- Velocidad: </b>'+weather.wind.speed+' '+weather.units.speed);
      $('#id_direccion').html('<b>- Dirección: </b>'+weather.wind.direction+' ');
    },
    error: function(error) {
      $('#id_contenedor').empty();
      $('#id_contenedor').html('Error al obtener los datos del clima.')
    }
  });
}