function geolocalizacion_actual(){
	navigator.geolocation.getCurrentPosition(function(position) {
		loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates
	});
}

function loadWeather(location, woeid) {
  $.simpleWeather({
    location: location,
    woeid: woeid,
    unit: 'c',
    success: function(weather) {
      $('#id_ubicacion').html(''+weather.country+'-'+weather.city);
      $('#id_sol').html('<b>- Puesta de sol: </b>'+weather.sunset+'');
      $('#id_amanecer').html('<b>- Amanecer: </b> '+weather.sunrise+'');
      $('#id_contenedor_imagen > img').attr("src", ""+weather.image);
      $('#id_temperatura').val(parseInt(weather.temp)).trigger('change');
      $('#id_temperatura1').html('<b>- Actual: </b>'+weather.temp+' °'+weather.units.temp+'<b>- Min: </b>'+weather.low+' °'+weather.units.temp+'<b>- Max: </b>'+weather.high+' °'+weather.units.temp);
      $('#id_temperatura2').html('<b>- Actual: </b>'+weather.alt.temp+' °'+weather.alt.unit+'<b>- Min: </b>'+weather.alt.low+' °'+weather.alt.unit+'<b>- Max: </b>'+weather.alt.high+' °'+weather.alt.unit);
      $('#id_humedad').val(parseInt(weather.humidity)).trigger('change');
      $('#id_humedad1').html('<b>- Humedad: </b>'+weather.humidity+' %');
      $('#id_presion').html('<b>- Presion: </b>'+weather.pressure+' '+weather.units.pressure);
      $('#id_viento').val(parseInt(weather.wind.speed)).trigger('change');
      $('#id_velocidad').html('<b>- Velocidad: </b>'+weather.wind.speed+' '+weather.units.speed);
      $('#id_direccion').html('<b>- Direccion: </b>'+weather.wind.direction+' ');
    },
    error: function(error) {
      $('#id_contenedor').empty();
      $('#id_contenedor').html('Error al obtener los datos del clima.')
    }
  });
}