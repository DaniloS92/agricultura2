function mostrarModalTipoSectores(){
    $("#alerta_tipo_sector_mod").empty();
    $("#alerta_sector_mod").empty();
    $('#btn_modificar_tipo_sector').attr("disabled", false);
    $("#modal_tipo_sector").modal('show');
    $("#id_nombre_tipo").val("");
    $("#id_nombre_tipo").focus();
}

function cargarDependencias(){
    $('#basics1').prop("disabled", false);  
}

function guardarSector(){
    if($("#id_nombre_ubicacion").val() == "" || document.getElementById("id_tipo_ubicacion").value == "0"){
        var men = "Complete todos los campos marcados como obligatorios.";
        toastr.options={"progressBar":true};
        toastr.error(men,'Sector');
    }else if($("#basics1").prop("disabled") == false){
        var men = "Escriba correctamente la dependencia del sector.";
        toastr.options={"progressBar":true};
        toastr.error(men,'Sector');
    }else{
        $('#btn_modificar_sector').attr("disabled", true);
        var agregado = $("#id_nombre_ubicacion").val();
        var form = document.forms.namedItem("formulario_sector_guardar");
        datos = new FormData(form);

        $.ajax({
            data: datos,
            url: '/empresa/sector/guardarSector/',
            processData: false,
            contentType: false,
            dataType: "json",
            async: true,
            type: 'post',            
            success: function (data) {
                if (data.result=="OK"){
                    // Mensaje
                    toastr.options={"progressBar":true};
                    toastr.success(data.mensaje,'Sector');
                    //LLamar al metodo para cargar datos al typeahead
                    cargarUbicaciones("/empresa/sector/ubicaciones/");
                    cargarDependencias_typeahead("/empresa/sector/cargarDependencias/");
                    $("#basics").load(location.href+" #basics>*","");
                    $("#basics").val(agregado);
                    cambiarUbicacionActual();

                    //Limpiar campos modal
                    $("#id_tipo_ubicacion").val("0");
                    $("#basics1").val("");
                    $("#basics1").prop('disabled', true);
                    $("#id_cambiar_ubicacion_typeahead").prop('disabled', true);
                    $("#id_nombre_ubicacion").val("");
                    $("#id_detalle_ubicacion").val("");
                    $('#btn_modificar_sector').attr("disabled", false);

                }else{
                    $('#btn_modificar_sector').attr("disabled", false);
                    toastr.options={"progressBar":true};
                    toastr.error(data.mensaje,'Sector');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btn_modificar_sector').attr("disabled", false);
                //alert(jqXHR.responseText );
                var men = "IOTMach sufrio un error inesperado, vuelva a intentar.";
                toastr.options={"progressBar":true};
                toastr.error(men,'Sector');
            }
        });
    }
}

//agregar tipo de sector
function guardarTipoSector(){
    var txt_nombre_tipo=$("#id_nombre_tipo").val();
    var txt_icono=$("#id_icono").val()+"";

     if($.trim(txt_nombre_tipo) == "" || $.trim(txt_icono) == ""){
        var men = "Complete todos los campos marcados como obligatorios.";
    }else{
        $('#btn_modificar_tipo_sector').attr("disabled", true);
        var form = document.forms.namedItem("formulario_tipo_sectorGuardar");
        datos = new FormData(form);
        $.ajax({
            data: datos,
            url: '/empresa/sector/registrarTipoSector/',
            processData: false,
            contentType: false,
            cache: false,
            type: 'post',
            success: function (data) {
                $("#id_tipo_ubicacion").load(location.href+" #id_tipo_ubicacion>*","");
                if (data.result=="OK"){
                    $('#modal_tipo_sector').modal('hide');
                    toastr.options={"progressBar":true};
                    toastr.success(data.mensaje,'Tipo Sector');
                    $("#id_nombre_tipo").val("");
                    $("#id_nombre_tipo").focus();
                    $('#basics1').prop("disabled", false);  
                }else{
                    $('#btn_modificar_tipo_sector').attr("disabled", false);
                    toastr.options={"progressBar":true};
                    toastr.error(data.mensaje,'Tipo Sector');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#btn_modificar_tipo_sector').attr("disabled", false);
                var men = "IOTMach sufrio un error inesperado, vuelva a intentar.";
                toastr.options={"progressBar":true};
                toastr.error(men,'Tipo Sector');
            }
        });
    }
}

/* Edición de Sectores */
function formar_arbol( ){
    $.ajax({
        data: {'identificador': 'ok'},
        url: "/empresa/sector/ubicacionVerTodos/",
        type: 'post',
        success: function (data) {
            if(data.length != 0){
                var vector_sectores = new Array();

                $("#id_arbol").empty();
                mensaje =  "<ul>";
                for(var i = 0; i<data.length; i++){
                    var id_sector=data[i].id_sector+"";
                    var nombre=data[i].nombre;
                    var padre_id=data[i].padre_id+"";
                    
                    if(id_sector == padre_id || padre_id == "0")
                    {
                        mensaje=mensaje+"<li><input type='checkbox' id='"+id_sector+"' />"+
                            "<label for='"+id_sector+"'>"+nombre+"</label> <a id='"+id_sector+"' href='#' onclick='cargar_sector_mod("+id_sector+");'>  Editar</a>";
                        agregar_items(id_sector, data);
                        mensaje =mensaje + "</li>";
                    }
                }
                mensaje = mensaje + "</ul>";
                $("#id_arbol").append(mensaje);
                $("#alerta_sector_mod").empty();
            }else{
                // error
                var men = "Sectores no encontrados.";
                toastr.options={"progressBar":true};
                toastr.error(men,'Tipo Sector');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //Error
            var men = "IOTMach sufrio un error inesperado, vuelva a intentar.";
            toastr.options={"progressBar":true};
            toastr.error(men,'Tipo Sector');
            alert(jqXHR.status+"::"+jqXHR.responseText);
        }
    });
}

/*
mod_sector.html
Busca y presenta los datos del sector seleccionado
*/
function cargar_sector_mod(id){
    $.ajax({
        data: {'identificador': id},
        url: '/empresa/sector/cargarSector/',
        type: 'post',
        success: function (data) {
            if(data.length != 0){
                console.log(data);
                $('#btn_modificar').attr("disabled", false);
                $("#id_nombre").val(data.nombre);
                $("#id_detalle").val(data.detalle);
                $("#id_tipo").val(data.tipo_sector);
                $("#id_sector").val(data.sector_padre);
                
                dato_id=data.id_sector;
                $("#id_nombre").focus();
                $("#modal_sector").modal('show');
            }else{
                // Error
                var men = "Sector no encontrado.";
                toastr.options={"progressBar":true};
                toastr.error(men,'Tipo Sector');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // Error 
            var men = "IOTMach sufrio un error inesperado, vuelva a intentar.";
            toastr.options={"progressBar":true};
            toastr.error(men,'Tipo Sector');
        }
    });
}

/*
mod_sector.html
Agrega items a los items principales
*/
function agregar_items(id_sector_aux, data)
{
    mensaje = mensaje + "<ul>";
    for(var i = 0; i<data.length; i++){
        var id_sector=data[i].id_sector+"";
        var nombre=data[i].nombre;
        var padre_id=data[i].padre_id+"";
        if(padre_id == id_sector_aux && id_sector != padre_id && id_sector != "0")
        {
            mensaje=mensaje+"<li><input type='checkbox' id='"+id_sector+"' />"+
            "<label for='"+id_sector+"'>"+nombre+" </label> <a id='"+id_sector+"' href='#' onclick='cargar_sector_mod("+id_sector+");'>  Editar</a>";
            agregar_items(id_sector, data);
            mensaje=mensaje+"</li>";
        }
    }
    mensaje = mensaje+ "</ul>";
}

/**
mod_sector.html
Modifica los Datos de Sector
**/
function modificar_sector_mod(){
    var txt_nombre=$("#id_nombre").val();
    var txt_detalle=$("#id_detalle").val();
    var sel_tipo=$("#id_tipo").val();
    var sel_sector=$("#id_sector").val();

    if(txt_nombre.trim() == "" || sel_tipo == null || sel_sector == null){
        var men = "Complete todos los campos marcados como obligatorios.";
        toastr.options={"progressBar":true};
        toastr.error(men,'Tipo Sector');
    }else{
        $('#btn_modificar').attr("disabled", true);
        var formData = new FormData();
        formData.append('id', dato_id);
        formData.append('txt_nombre', txt_nombre);
        formData.append('txt_detalle', txt_detalle);
        formData.append('sel_tipo', sel_tipo);
        formData.append('sel_sector', sel_sector);
        $.ajax({
            data: formData,
            url: '/empresa/sector/editarSector/',
            processData: false,
            contentType: false,
            cache: false,
            type: 'post',
            success: function (data) {
                formar_arbol();
                $('#modal_sector').modal('hide');
                if (data.result=="OK"){
                    toastr.options={"progressBar":true};
                    toastr.success(data.mensaje,'Tipo Sector');
                }else{
                    toastr.options={"progressBar":true};
                    toastr.error(data.mensaje,'Tipo Sector');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText )
                $('#modal_sector').modal('hide');
                var men = "IOTMach sufrio un error inesperado, vuelva a intentar.";
                toastr.options={"progressBar":true};
                toastr.error(men,'Tipo Sector');
            }
        });
    }
}