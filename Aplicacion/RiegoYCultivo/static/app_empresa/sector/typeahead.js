function cargarUbicaciones(url){
     $.ajax({
        url:url,
        dataType: "json",
        async: true,
        success: function(json){
            var options = {
                data: json,
                placeholder: "Elija una ubicación",
                getValue: "nombre",
                template: {
                    type: "description",
                    fields: {
                        description: "detalle"
                    }
                },
                list:{
                    maxNumberOfElements: 5,
                    onChooseEvent: function() {
                        var id = $("#basics").getSelectedItemData().id;
                        var value = $("#basics").getSelectedItemData().nombre;
                        var detalle = $("#basics").getSelectedItemData().detalle;
                        $("#basics").val(value+" "+detalle);
                        $("#id_ubicacion_typeahead").val(id);
                        $("#basics").prop('disabled', true);
                        $("#id_cambiar_ubicacion").prop('disabled', false);
                    },
                    match: {
                      enabled: true
                    },
                    showAnimation: {
                        type: "fade", //normal|slide|fade
                        time: 400,
                        callback: function() {}
                    },

                    hideAnimation: {
                        type: "slide", //normal|slide|fade
                        time: 400,
                        callback: function() {}
                    }
                }
            }
            $("#basics").easyAutocomplete(options);
            $(".eac-description").removeAttr('style');
        }
    });
}

function cambiarUbicacionActual(){
    $("#basics").prop('disabled', false);
    $("#basics").focus();
    $("#id_cambiar_ubicacion").prop('disabled', true);
}

function cargarDependencias_typeahead(url){
     $.ajax({
        url:url,
        dataType: "json",
        async: true,
        success: function(json){
            var options = {
                data: json,
                placeholder: "Escriba una ubicación",
                getValue: "nombre",
                template: {
                    type: "description",
                    fields: {
                        description: "detalle"
                    }
                },
                list:{
                    maxNumberOfElements: 5,
                    onChooseEvent: function() {
                        var id = $("#basics1").getSelectedItemData().id;
                        $("#id_dependencias_typeahead").val(id);
                        $("#basics1").prop('disabled', true);
                        $('#id_cambiar_ubicacion_typeahead').prop('disabled', false);
                    },
                    match: {
                      enabled: true
                    },
                    showAnimation: {
                        type: "fade", //normal|slide|fade
                        time: 400,
                        callback: function() {}
                    },

                    hideAnimation: {
                        type: "slide", //normal|slide|fade
                        time: 400,
                        callback: function() {}
                    }
                }
            }
            $("#basics1").easyAutocomplete(options);
            $(".eac-description").removeAttr('style');
        },
        error:function(xhr,data,err){
          alert(xhr.responseText);
        }
    });
}