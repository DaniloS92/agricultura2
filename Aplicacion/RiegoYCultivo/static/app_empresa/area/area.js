$(function(){
/////////////////////////////// GUARDAR /////////////////////
	
	$('#frmAre').on("submit",function(){
		event.preventDefault();
		var accion = $('#accion').val();
		var url;
		if(accion == "0") //guardar
		{
			url = "/empresa/areas/sav/";
		}
		else
		{
			url = "/empresa/areas/mod/";	
		}

		$('#accion').val("0");
		$('#are_est').is(":checked")? $('#are_est_g').val("true"):$('#are_est_g').val("false");
		$.ajax({
			type:"POST",
			url: url,
			dataType: 'json',
			data:$(this).serialize(),
			success: function(response){
				toastr.options={"progressBar":true};
				if(response.hasOwnProperty("mensaje")){
					$('#are_nom').val("");
					$('#are_des').val("");
					$('#are_etq').val("");
					$('#are_dir').val("");
					toastr.success(response.mensaje,'Área');
					$('#modalVer').modal('hide');
				}else{
					toastr.error(response.error,'Área');
				}
				$('#tblAre').DataTable().ajax.reload();				
			},

			error: function(jqxhr, data, err){
				alert(jqxhr.responseText);
				toastr.options={"progressBar":true};
				toastr.error("Error",'Área');
			}
		});

	});

	$('#modalVer').bind('shown.bs.modal', function(e){
		are_nom.focus();
	});

	$('#modalVer').bind('hidden.bs.modal', function(e){
		$('#title').html("Crear Área");
		$('#are_nom').val("");
		$('#are_des').val("");
		$('#are_etq').val("");
		$('#are_dir').val("");
		$('#id_dependencias_typeahead').val("");
		$('#basics1').val("");
		$("#basics1").prop('disabled', false);
    $("#id_cambiar_ubicacion_typeahead").prop('disabled', true);
		$('#are_est').prop("checked",false);
		$('#accion').val("0");
	});

//************************************************************
// 	Fill Combo Empresa
	$.getDataEmpresa = function(response)
	{
		var datos = "";
		$.each(response.data, function(i,value){
			datos+= "<option value="+value.emp_id+">"+value.emp_nombre_comercial+"</option>";
		});

		$('#emp').html(datos);		
	};
	$.get("/empresa/datos/get/",$.getDataEmpresa); // get without token_crsf

//************************************************************CARGAR TABLA*********************
	var btnsOpTblModels = "<button class='btn-white btn btn-xs' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"Editar"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		
		if (aData['estado'] === true)
		{
			$(nRow).append("<td>"+
								"<label class='switch'>"+
								"<input class='switch-input' type='checkbox' checked value="+aData['estado']+" id=check"+aData['id']+" name="+aData['id']+" data-url='/empresa/areas/est/'>"+
								"<span class='switch-label' data-on='Activo' data-off='Inactivo'></span>"+ 
								"<span class='switch-handle'></span>"+ 
								"</label>"+
							"</td>");
		}
		else
		{
			$(nRow).append("<td>"+
							"<label class='switch'>"+
								"<input class='switch-input' type='checkbox' value="+aData['estado']+" id=check"+aData['id']+" name="+aData['id']+" data-url='/empresa/areas/est/'>"+
								"<span class='switch-label' data-on='Activo' data-off='Inactivo'></span>"+ 
								"<span class='switch-handle'></span>"+ 
								"</label>"+
							"</td>");	
		}

		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']);
		$(nRow).attr('idEmpresa',aData['id_empresa']);
		$(nRow).attr('data-des',aData['descripcion']);
		$(nRow).attr('data-dir',aData['direccion']);
		$(nRow).attr('idSector',aData['sector']);
	};	

	$('#tblAre').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/areas/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){					
					datos.push({
						'id': item.ar_id,
						'nombre': item.ar_nombre,
						'descripcion': item.ar_descripcion,
						'etiqueta': item.ar_etiqueta,
						'empresa' : item.emp_id__emp_nombre_comercial,
						'id_empresa': item.emp_id_id,
						'direccion' : item.ar_direccion,
						'sector': item.ar_id_sector__sect_id,
						'nom_sector': item.ar_id_sector__sect_nombre,
						'estado' 	: item.ar_estado                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'},
			{'data': 'descripcion'},
			{'data': 'etiqueta'},
			{'data': 'empresa'},
			// {'data': 'sector'},
			{'data': 'nom_sector'},
			{'data': 'direccion'}
			//{'data': 'estado'}
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow,
        preDrawCallback: function () {
		            // Initialize the responsive datatables helper once.
	            if (!responsiveHelper) {
	                responsiveHelper = new ResponsiveDatatablesHelper($('#tblAre'), breakpointDefinition);
	            }
        },
        rowCallback    : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        drawCallback   : function (oSettings) {
            responsiveHelper.respond();
        }
	});
//******************************** EDITAR******************

$.editarModal = function(td)
	{
		$('#accion').val("1");
		$('#title').html("Modificar Área");
		var tr 		= $(td).parent().children();
		var cod 	= $(td).parent().attr('id'); //id
		var tipEmp 	= $(td).parent().attr('idEmpresa'); //idTipo
		var nom 	= tr[0].textContent; //area		
		var etq 	= tr[1].textContent; //etiqueta
		var sect  = $(td).parent().attr('idSector');; //sector
		var nom_sect = tr[3].textContent; //nombreSector

		$('#are_nom').val(nom);
		$('#are_des').val($(td).parent().attr('data-des'));
		$('#are_etq').val(etq);
		$('#emp').val(tipEmp);
		$('#are_id').val(cod);
		$('#id_dependencias_typeahead').val(sect);
		$('#basics1').val(nom_sect);
		$("#basics1").prop('disabled', true);
    $("#id_cambiar_ubicacion_typeahead").prop('disabled', false);
		$('#are_dir').val($(td).parent().attr('data-dir'));
		$('#are_est').prop("checked", (tr[3].textContent === "true")? true: false);
		cargarDependencias_typeahead("/empresa/sector/cargarDependencias/");
    cargarUbicaciones("/empresa/sector/ubicaciones/");
	};

	
	/*
	CONTROLA LOS EVENTOS DE LOS CHECKBOXES
	*/
	$(document).on('change', '[type=checkbox]', function(e){
		//alert($(this).attr('id'));
		var token_crsf 	= getCookie('csrftoken');
		var id 			= $(this).attr('name');	
		var estado 		= $(this).val();
		var url 		= $(this).attr('data-url');
		var name 		= $(this).attr('id');
		$.ajax({
				
				type:"POST",
				url: url,
				dataType: 'json',
				data:{
						id 					: id,
						estado 				: estado,
						csrfmiddlewaretoken : token_crsf
					},

				success: function(response){				
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Área');
					$('#tblAre').DataTable().ajax.reload();
					/*
					if (estado === "true") 
					{
						$(name).val('false');
					}
					else
					{
						$(name).val('true');
					}*/
					
				},

				error: function(){
					toastr.options={"progressBar":true};
					toastr.error("Error",'Área');
				}
		});
	});


});