$(function(){
	/////////////////////////////// GUARDAR /////////////////////
	$('#frmSub').on("submit",function(){
		event.preventDefault();
		$.ajax({
			type:"POST",
			url: "/empresa/subarea/sav/",
			dataType: 'json',
			data:$(this).serialize(),
			success: function(response){
				$('#sub_nom').val("");
				$('#sub_etq').val("");
				$('#sub_lat').val("");
				$('#sub_lng').val("");
				$('#sub_hla').val("");
				$('#sub_hlo').val("");
				
				$('#modalVer').modal('hide');
				toastr.options={"progressBar":true};
				toastr.success(response.mensaje,'Subarea');
				$('#tblSub').DataTable().ajax.reload();				
			},

			error: function(){
				toastr.options={"progressBar":true};
				toastr.error("Error",'Subarea');
			}
		});
	});
//******************************** CARGAR COMBO AREA **************************
var data="";
$.getDataArea = function(response)
	{
		var datos = "";
		data = response.data;
		$.each(response.data, function(i,value){
			datos+= "<option value="+value.ar_id+">"+value.ar_nombre+"</option>";
		});

		$('#are_id').html(datos);		
	};
	$.get("/empresa/areas/get/",$.getDataArea); // get without token_crsf

//***************************************** CARGAR TABLA *******************
var btnsOpTblModels = "<button class='btn-white btn btn-xs' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"Editar"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']);
		$(nRow).attr('idArea',aData['ar_id']);
		$(nRow).attr('data-anm',aData['alt_nvl_mar']);
		$(nRow).attr('data-reg',aData['reg']);
		$(nRow).attr('data-lat',aData['lat']);
		$(nRow).attr('data-lon',aData['lon']);
		$(nRow).attr('data-hlat',aData['hlat']);
		$(nRow).attr('data-hlon',aData['hlon']);
		$(nRow).attr('data-etq',aData['etiqueta']);
		$(nRow).attr('idRegion',aData['reg_id']);
	};	

	$('#tblSub').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/subarea/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){			
					datos.push({
						'id': item.sar_id,
						'nombre': item.sar_nombre,						
						'etiqueta': item.sar_etiqueta,
						'lat_lon': item.sar_latitud +" | "+item.sar_hem_lat,						
						'hem_lat_hem_lng': item.sar_longitud +" | "+item.sar_hem_lon,
						'ar_id': item.ar_id_id,
						'area': item.ar_id__ar_nombre,
						'lat': item.sar_latitud,
						'lon': item.sar_longitud,
						'hlat': item.sar_hem_lat,
						'hlon': item.sar_hem_lon,
						'alt_nvl_mar' : item.sar_asnm,
						'reg' : item.sar_region__region,
						'reg_id' : item.sar_region__id
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'},
			{'data': 'etiqueta'},
			{'data': 'alt_nvl_mar'},
			{'data': 'reg'},
			{'data': 'lat_lon'},			
			{'data': 'hem_lat_hem_lng'},
			{'data': 'area'},
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow,
        preDrawCallback: function () {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper) {
                responsiveHelper = new ResponsiveDatatablesHelper($('#tblSub'), breakpointDefinition);
            }
        },
        rowCallback    : function (nRow) {
            responsiveHelper.createExpandIcon(nRow);
        },
        drawCallback   : function (oSettings) {
            responsiveHelper.respond();
        }
	});
//******************************** EDITAR******************
$.editarModal = function(td)
	{
		var datos = "";
		$.each(data,function(i , value){						
			datos+= "<option value="+value.ar_id+">"+value.ar_nombre+"</option>";			
		});
		$('#are_id_m').html(datos);

		var tr 		= $(td).parent().children();
		var cod 	= $(td).parent().attr('id'); //id
		var ar_id 	= $(td).parent().attr('idArea'); //idTipo
		var nom 	= tr[0].textContent; //area
		var lat 	= $(td).parent().attr('data-lat'); //etiqueta
		var lng 	= $(td).parent().attr('data-lon'); //direccion
		var hla 	= $(td).parent().attr('data-hlat');
		var hlo 	= $(td).parent().attr('data-hlon');
		var idReg 	= $(td).parent().attr('idRegion');
		var anm 	= $(td).parent().attr('data-anm');

		$('#sub_nom_m').val(nom);
		$('#sub_etq_m').val($(td).parent().attr('data-etq'));
		$('#sub_lat_m').val(lat); //latitud
		$('#sub_lng_m').val(lng); //longitud
		$('#sub_hla_m').val(hla); //hml
		$('#sub_hlo_m').val(hlo);		
		$('#are_id_m').val(ar_id);
		$('#sub_id').val(cod);
		$('#regiones_m').val(idReg);
		$('#alt_nivel_m').val(anm);
	};

$('#modalVer').bind('shown.bs.modal' , function(){
		sub_nom_m.focus();
	});

$('#frmSubMod').on("submit",function(){
		event.preventDefault();
		$.ajax({
			type:"POST",
			url: "/empresa/subarea/mod/",
			dataType: 'json',
			data:$(this).serialize(),
			success: function(response){
				$('#sub_nom_m').val("");
				$('#sub_etq_m').val("");
				$('#sub_lat_m').val("");
				$('#sub_lng_m').val("");
				$('#sub_hla_m').val("");
				$('#sub_hlo_m').val("");
				
				$('#modalVer').modal('hide');
				toastr.options={"progressBar":true};
				toastr.success(response.mensaje,'Subarea');
				$('#tblSub').DataTable().ajax.reload();				
			},

			error: function(){
				toastr.options={"progressBar":true};
				toastr.error("Error",'Subarea');
			}
		});
	});
//***************************************************************
});