var hola= function (nombre) {
	 /* body... */ 
	//alert("hola "+nombre);
}

var rules_basic = {
	id: "1",
  condition: 'AND',
  rules: [{
    id: 'sen01',
    operator: 'less',
    value: 15
  }, {
  	id: "2",
    condition: 'OR',
    rules: [{
      id: 'id',
      operator: 'equal',
      value: 2
    }, {
      id: 'category',
      operator: 'equal',
      value: 1
    }]
  }]
};



$('[data-toggle="tooltip"]').tooltip();//muetsrar los errores

let options = {
	allow_empty: true,
	default_filter: "name",
	sort_filters: true,

	optgroups: {
		core: {
			en: "Core",
			fr: "Coeur"
		}
	},
	plugins: {
		"bt-tooltip-errors": {delay: 100},
		"sortable": null,
		"filter-description": {mode: "bootbox"},
		"bt-selectpicker": null,
		"unique-filter": null,
		"bt-checkbox": { color: "primary"},
		"invert": null
	},

	//standard operators in custom optgroups
	operators:[
		{type: "equal",			optgroup: "basica"},
		{type: "not_equal",		optgroup: "basic"},
		{type: "in",			optgroup: "basic"},
		{type:"not_in", 		optgroup: "basic"},
		{type: "less",			optgroup: "numbers"},
		{type: "less_or_equal", optgroup: "numbers"},
		{type: "greater", 		optgroup: "numbers"},
		{type: "greater_or_equal",optgroup: "numbers"},
		{type: "between", 		optgroup: "numbers"},
		{type: "not_between", 	optgroup: "numbers"},
		{type: "begins_with", 	optgroup: "strings"},
		{type: "not_begins_with",optgroup: "strings"},
		{type: "contains",		optgroup: "strings"},
		{type: "not_contains",	optgroup: "strings"},
		{type: "ends_with",		optgroup: "strings"},
		{type: "not_ends_with", optgroup: "strings"},
		{type: "is_empty"},
		{type: "is_not_empty"},
		{type: "is_null"},
		{type: "is_not_null"},
		{type: "micondicionante", optgroup: "custom", nb_inputs: 2, multiple: false, apply_to: ["number"]}
	],

	filters: [
	/**
	 * Basic
	 */
	{
		id: "name",
		label: {
			en: "Name",
			fr: "Nombre"
		},
		type: "string",
		optgroup: "corejk",
		default_value: "Misticossss",
		size: 30,
		unique: true
	},
	/**
	 * Textarea
	 */
	{
		id: "bson",
		label: "BSON",
		type: "string",
		input: "textarea",
		operators: ["equal"],
		size: 30,
		rows: 4
	},
	{
		id: "in_stock",
		label: "In stock",
		type: "integer",
		input: "radio",
		optgroup: "pluginjh",
		values: {
      1: 'Yes',
      0: 'No'
    },
		operators: ["equal"]
	},
	{
		id: "sen01",
		field: "sensor temp",
		label: "Sensor temperatura",
		type: "double",
		validation: {
			min: 0,
			step: 0.01
		}
	},
	{
		id: "id",
		label: "Identifier",
		type: "string",
		placeholder: "___.___",
		operators: ["equal", "not_equal"],
		validation: {
			format: /^.{4}-.{4}-.{4}$/
		}

	},
	{
		id: "state",
		label: "State",
		type: "string",
		input: "select",
		multiple: true,
		plugin: "selectize",
		plugin_config:{
			valueField: "id",
			labelField: "name",
			searchField: "name",
			sortField: "name",
			options: [
				{id: "AL", name: "Alabama"},
				{id: "AK", name: "Alaska"},
				{id: "AZ", name: "Arizona"},
				{id: "AR", name: "Arkanzas"},
				{id: "CA", name: "California"},
				{id: "CO", name: "Colorado"},
				{id: "CT", name: "Connecticut"},
				{id: "DE", name: "Delaware"},
				{id: "DC", name: "District of Columbia"},
				{id: "FL", name: "Forida"},
				{id: "GA", name: "Georgia"},
				{id: "HI", name: "Hawaii"},
				{id: "ID", name: "Idaho"},
			]
		},
		valueSetter: function(rule, value){
			rule.$el.find(".rule-value-container select")[0]
			.selectize.setValue(value);
		}
	},
	{
		id: "continent",
		label: "Continent",
		type: "string",
		input: "select",
		optgroup: "holi",
		placeholder: "Select something",
		values: {
			"eur": "Europe",
			"asia": "Asia",
			"oce": "Oceania",
			"afr": "Africa",
			"na": "North America",
			"sa": "South America"
		},
		operators: ["equal", "not_equal", "is_null", "is_not_null"]
	},
	{
		id: "category",
		label: "Category",
		type: "integer",
		input: "checkbox",
		optgroup: "core",
		values: {
			1: "Books",
			2: "Movies",
			3: "Music",
			4: "Tools",
			5: "Goodies",
			6: "Clothes",
		},
		colors: {
			1: "foo",
			1: "warning",
			1: "success",
		},
		operators: ["in", "not_in", "equal", "not_equal", "is_null", "is_not_null", "micondicionante"]
	},
	{
		id: "rate",
		label: "Rate",
		type: "integer",
		validation: {
			min: 0,
			max: 100
		},
		plugin: "slider",
		plugin_config: {
			min: 0,
			max: 100,
			value: 2
		},
		onAfterSetValue: function (rule, value) {
			var input = rule.$el.find(".rule-value-container input");
			input.slider("setValue", value);
			input.val(value); 
		}
	}
	],
	rules: rules_basic
};

//$("#superreglas").queryBuilder(options);
$('#builder').on('afterCreateRuleInput.queryBuilder', function(e, rule) {
    if (rule.filter.plugin == 'selectize') {
        rule.$el.find('.rule-value-container').css('min-width', '200px')
          .find('.selectize-control').removeClass('form-control');
    }
});



// set rules
$('.setds').on('click', function() {
  $('#superreglas').queryBuilder('setRules', {
    condition: 'AND',
    flags: {
      condition_readonly: true
    },
    rules: [{
      id: 'price',
      operator: 'between',
      value: [10.25, 15.52],
      flags: {
        no_delete: true,
        filter_readonly: true
      },
      data: {
        unit: '€'
      }
    }, {
      id: 'state',
      operator: 'equal',
      value: 'AK',
    }, {
      condition: 'OR',
      flags: {
        no_delete: true
      },
      rules: [{
        id: 'category',
        operator: 'equal',
        value: 2
      }, {
        id: 'coord',
        operator: 'equal',
        value: 'B.3'
      }]
    }]
  });
});


// get rules
$('.parse-json').on('click', function() {
  $('#result').removeClass('hide')
    .find('pre').html(JSON.stringify(
      $('#superreglas').queryBuilder('getRules', {get_flags: true}),
      undefined, 2
    ));
});








let options2 = {
	allow_empty: true,
	default_filter: "name",
	sort_filters: true,

	optgroups: {
		core: {
			en: "Core",
			fr: "Coeur"
		}
	},
	plugins: {
		"bt-tooltip-errors": {delay: 100},
		"sortable": null,
		"filter-description": {mode: "bootbox"},
		"bt-selectpicker": null,
		"unique-filter": null,
		"bt-checkbox": { color: "primary"},
		"invert": null
	},

	//standard operators in custom optgroups
	operators:[
		{type: "equal",			optgroup: "basica"},
		{type: "not_equal",		optgroup: "basic"},
		{type: "in",			optgroup: "basic"},
		{type:"not_in", 		optgroup: "basic"},
		{type: "less",			optgroup: "numbers"},
		{type: "less_or_equal", optgroup: "numbers"},
		{type: "greater", 		optgroup: "numbers"},
		{type: "greater_or_equal",optgroup: "numbers"},
		{type: "between", 		optgroup: "numbers"},
		{type: "not_between", 	optgroup: "numbers"},
		{type: "begins_with", 	optgroup: "strings"},
		{type: "not_begins_with",optgroup: "strings"},
		{type: "contains",		optgroup: "strings"},
		{type: "not_contains",	optgroup: "strings"},
		{type: "ends_with",		optgroup: "strings"},
		{type: "not_ends_with", optgroup: "strings"},
		{type: "is_empty"},
		{type: "is_not_empty"},
		{type: "is_null"},
		{type: "is_not_null"},
		{type: "micondicionante", optgroup: "custom", nb_inputs: 2, multiple: false, apply_to: ["number"]}
	],

	filters: [
	/**
	 * Basic
	 */
	{
		id: "name",
		label: {
			en: "Name",
			fr: "Nombre"
		},
		type: "string",
		optgroup: "coredel2",
		default_value: "Misticos",
		size: 30,
		unique: true
	},
	/**
	 * Textarea
	 */
	{
		id: "bson",
		label: "BSON",
		type: "string",
		input: "textarea",
		operators: ["equal"],
		size: 30,
		rows: 4
	},
	{
		id: "in_stock",
		label: "In stock",
		type: "integer",
		input: "radio",
		optgroup: "plugin",
		values: {
      1: 'Yes',
      0: 'No'
    },
		operators: ["equal"]
	},
	{
		id: "sen01",
		field: "sensor temp",
		label: "Sensor temperatura",
		type: "double",
		validation: {
			min: 0,
			step: 0.01
		}
	},
	{
		id: "id",
		label: "Identifier",
		type: "string",
		placeholder: "___.___",
		operators: ["equal", "not_equal"],
		validation: {
			format: /^.{4}-.{4}-.{4}$/
		}

	},
	{
		id: "state",
		label: "State",
		type: "string",
		input: "select",
		multiple: true,
		plugin: "selectize",
		plugin_config:{
			valueField: "id",
			labelField: "name",
			searchField: "name",
			sortField: "name",
			options: [
				{id: "AL", name: "Alabama"},
				{id: "AK", name: "Alaska"},
				{id: "AZ", name: "Arizona"},
				{id: "AR", name: "Arkanzas"},
				{id: "CA", name: "California"},
				{id: "CO", name: "Colorado"},
				{id: "CT", name: "Connecticut"},
				{id: "DE", name: "Delaware"},
				{id: "DC", name: "District of Columbia"},
				{id: "FL", name: "Forida"},
				{id: "GA", name: "Georgia"},
				{id: "HI", name: "Hawaii"},
				{id: "ID", name: "Idaho"},
			]
		},
		valueSetter: function(rule, value){
			rule.$el.find(".rule-value-container select")[0]
			.selectize.setValue(value);
		}
	},
	{
		id: "continent",
		label: "Continent",
		type: "string",
		input: "select",
		optgroup: "core",
		placeholder: "Select something",
		values: {
			"eur": "Europe",
			"asia": "Asia",
			"oce": "Oceania",
			"afr": "Africa",
			"na": "North America",
			"sa": "South America"
		},
		operators: ["equal", "not_equal", "is_null", "is_not_null"]
	},
	{
		id: "category",
		label: "Category",
		type: "integer",
		input: "checkbox",
		optgroup: "core",
		values: {
			1: "Books",
			2: "Movies",
			3: "Music",
			4: "Tools",
			5: "Goodies",
			6: "Clothes",
		},
		colors: {
			1: "foo",
			1: "warning",
			1: "success",
		},
		operators: ["in", "not_in", "equal", "not_equal", "is_null", "is_not_null", "micondicionante"]
	},
	{
		id: "rate",
		label: "Rate",
		type: "integer",
		validation: {
			min: 0,
			max: 100
		},
		plugin: "slider",
		plugin_config: {
			min: 0,
			max: 100,
			value: 2
		},
		onAfterSetValue: function (rule, value) {
			var input = rule.$el.find(".rule-value-container input");
			input.slider("setValue", value);
			input.val(value); 
		}
	}
	],
	rules: rules_basic
};




var setmodelo = function (options) {
	console.log('setruless',options);
	$("#superreglas").queryBuilder("destroy");
	$("#superreglas").queryBuilder(options);
}
/*
 *
 * 
 */
var getFilters= function (actuadores) {
	let filters  = []	
	actuadores.forEach(function (actuadoritem, index) {
		console.log('>>',actuadoritem)
		let filter = {};
		filter.id = actuadoritem.iot_id;
		filter.field = actuadoritem.sa_nombre;
		filter.type = "double";
		filter.optgroup =  actuadoritem.dis_nombre;
		filter.validation = {
			min: 0,
			step: 0.1
		};
		filters.push(filter)
    	
	});
	return filters;
	
}

var addcondition = function (nodopadre, nodohijo) {
	
	if (nodopadre.condition_id === nodohijo.parent && (nodopadre.condition_id !== nodohijo.condition_id)) {
		console.log('Insertadndo')
		nodopadre.rules.push(nodohijo);
	}else {
		nodopadre.rules.forEach(function (cond, index) {
			console.log('Insertadndo')
			addcondition(cond, nodohijo)
		})
	}
	
}
var addrule = function (nodopadre, rule) {
	console.log('RRRRRRRRRRRRRR')
	if (nodopadre.condition_id === rule.con_id) {
		console.log('el node padre tmp ',nodopadre);
		nodopadre.rules.unshift(rule);
	}else {
		if (nodopadre.rules) {
			nodopadre.rules.forEach(function (cond, index) {
			addrule(cond, rule);
			})
		}
		
	}
	
}

var getRules = function (rules) {
	let rulestmp = [];
	rules.forEach(function (ruleitem, index) {
		let rule = {};
		rule.id = ruleitem.iot_id;
		rule.operator = ruleitem.operador;		
		rule.value = ruleitem.factor1;
		rule.con_id = ruleitem.con_id;
		rulestmp.push(rule);
	})
	return rulestmp;
};

var getConditions = function (conditions) {
	let condiciones = []
	conditions.forEach(function (conditionitem, index) {
		let condition = {};
		condition.condition_id = conditionitem.con_id;
		condition.condition = conditionitem.m_condicionn;
		condition.parent = conditionitem.m_parent;
		condition.rules = [];
		//condition.flags =  {
         //  condition_readonly: true
       // };
        condiciones.push(condition)

	});
	return condiciones;
}
/*
 *
 * 
 */
var getmodelo = function (reglas, actuadores, condiciones ) {
	console.log('Se ha recibido actuadores ',actuadores);
	let filters = getFilters(actuadores);
	let conditions = getConditions(condiciones);
	let rules = getRules(reglas);
	
	var nodopadrejh= conditions[0];
	conditions.forEach(function (condicionitem, index,listacond) {	
		addcondition(nodopadrejh, condicionitem);
	});
	console.log('ESTAS SON LAS CONDICIONES');
	console.log(condiciones)
	console.log('ESTO ES EL NODO actuadores');
	console.log(nodopadrejh);
	rules.forEach(function (ruleitem, index,listacond) {	
		addrule(nodopadrejh, ruleitem);
	});
	console.log('ESTO ES EL NODO PADRE');
	console.log(nodopadrejh);
console.log('-----------------');

	let options = {
		allow_empty: true,
		//default_filter: "name",
		sort_filters: true,
		//optgroups: {
		//	core: {
		//		en: "Core",
		//		fr: "Coeur"
		//	}
		//},
		plugins: {
			"bt-tooltip-errors": {delay: 100},
			"sortable": null,
			"filter-description": {mode: "bootbox"},
			"bt-selectpicker": null,
			"unique-filter": null,
			"bt-checkbox": { color: "primary"},
			//"invert": false
		},

		//standard operators in custom optgroups
		operators:[
			{type: "equal",			optgroup: "basica"},
			{type: "not_equal",		optgroup: "basic"},
			{type: "in",			optgroup: "basic"},
			{type:"not_in", 		optgroup: "basic"},
			{type: "less",			optgroup: "numbers"},
			{type: "less_or_equal", optgroup: "numbers"},
			{type: "greater", 		optgroup: "numbers"},
			{type: "greater_or_equal",optgroup: "numbers"},
			{type: "between", 		optgroup: "numbers"},
			{type: "not_between", 	optgroup: "numbers"},
			{type: "begins_with", 	optgroup: "strings"},
			{type: "not_begins_with",optgroup: "strings"},
			{type: "contains",		optgroup: "strings"},
			{type: "not_contains",	optgroup: "strings"},
			{type: "ends_with",		optgroup: "strings"},
			{type: "not_ends_with", optgroup: "strings"},
			{type: "is_empty"},
			{type: "is_not_empty"},
			{type: "is_null"},
			{type: "is_not_null"},
			//{type: "micondicionante", optgroup: "custom", nb_inputs: 2, multiple: false, apply_to: ["number"]}
		],
		filters: filters,
		rules: nodopadrejh
		};
		return options;
	}


var getmodeloiot = function (actuadores) {
	let filters = getFilters(actuadores);
	return filters;
}

// get rules
$('.parse-json').on('click', function() {
	console.log('las reglas son ', $('#superreglas').queryBuilder('getRules', {
			get_flags: true
		}),
		undefined, 2
	);
	$('#result').removeClass('hide')
		.find('pre').html(JSON.stringify(
			$('#superreglas').queryBuilder('getRules', {
				get_flags: true
			}),
			undefined, 2
		));
});

//setmodelo(options);
