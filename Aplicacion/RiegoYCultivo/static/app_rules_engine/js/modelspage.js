$(document).ready(function() {

    /**
     * [Delete method for a model]
     * @return {[type]}            [description]
     */
    /**
     * [description]
     * @param  {[type]} )          {                   swal({                                                           title:      "Esta usted seguro?",                               text: "Una vez que se elimina este modelos pasara a un estado inactivo",            type: "warning",            showCancelButton: true,            confirmButtonColor: "#DD6B55",            confirmButtonText: "Si, Eliminar",            closeOnConfirm: false        } [description]
     * @param  {[type]} function() {                              swal("Elimnado!", "El modelo x Esta en modo inactivo", "success");                           });    } [description]
     * @return {[type]}            [description]
     * modal data-id = id del modelo
     * 
     */

        // Get CSRFTOKEn using JQUERY
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }




    var renderModelTable = function (parametrobusqueda) {
        var url = "/reglas/api/modelo/?search="+parametrobusqueda
        console.log('aquii')

        $.ajax({
            type:"GET",
            url: url,
            dataType: 'json',

            
            //data:{busqueda: parametrobusqueda},
            success: function(response){
                console.log('response es ', response)
            },

            error: function(){
                console.log('Erorr ')
            }
        });
    }

    renderModelTable("");
    
     $('#btnguardarmodelo').on('click', function () {       
        console.log('GUARDADDNO modelo')
        var nombremodelo = $('#txtnamemodeladd').val();
        var idempresa = parseInt(empresa);
        var mod_estado = true;
        console.log('l nomrbre de modelo es ', idempresa)
        var url = "/reglas/api/modelo/";
         $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            headers: {
                 'X-CSRFToken': getCookie('csrftoken'),
                // "Content-Type": "application/json"
            },
            data:{mod_nombre: nombremodelo, empresa: 1, mod_descripcion:"-", mod_estado: mod_estado},
            success: function(response) {
                console.log('gradado modelo  es ', response)
                $('#modal-model-add').modal('hide');  
                location.reload();
            },

            error: function(error) {
                console.log('Erorr ', error)
            }
        });
    })


    $('.delete-model').on('click',function() {        
        var model_id = $(this).data('id');
        console.log('El id es ', model_id)
        url = "/reglas/api/modelo/"+model_id;
        // funcion ajax()

        swal({
            title: "Esta usted seguro?",
            text: "Una vez que se elimina este modelos pasara a un estado inactivo",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar",
            closeOnConfirm: false
        }, function() {
            $.ajax({
                type: "DELETE",
                url: url,
                dataType: 'json',
                headers: {
                 'X-CSRFToken': getCookie('csrftoken'),
                // "Content-Type": "application/json"
                },
                //data:{busqueda: parametrobusqueda},
                success: function(response) {
                    console.log('response es ', response)
                    swal("Elimnado!", "El modelo x Esta en modo inactivo", "success");
                    location.reload();
                },

                error: function(error) {
                    console.log('Erorr ', error)
                }
            });

            
        });
    });

    $('#btneditarmodelo').on('click', function () {
        var mod_id = $('#modal-model-edit').data('id');
        var mod_nombre = $('#txtnamemodeledit').val();
        var mod_descripcion = "-";
        var mod_estado = $('#mod_estado_edit').is(':checked');
        mod_estado = mod_estado? 'a' : 'i';
        console.log('estado es ')
        var url = "/reglas/api/modelo/"+mod_id;
        $.ajax({
            type: "PUT",
            url: url,
            dataType: 'json',
            headers: {
                 'X-CSRFToken': getCookie('csrftoken'),
                // "Content-Type": "application/json"
                },
            data:{mod_nombre: mod_nombre, mod_descripcion: mod_descripcion, empresa: empresa, mod_estado : mod_estado},
            success: function(response) {
                console.log('editado modelo es ', response)
                $('#modal-model-edit').modal('show');  
                location.reload();
            },

            error: function(error) {
                console.log('Erorr ', error)
            }
        });
        console.log('editar modelo')
    })

    $('.btn-regla-delete').on('click',function() {  
    alert()      
        var regla_id = $(this).data('id');
        console.log('El id es ', regla_id)
        url = "/reglas/api/regla/"+regla_id;
        // funcion ajax()

        swal({
            title: "Esta usted seguro?",
            text: "Una vez que se elimina este modelos pasara a un estado inactivo",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar",
            closeOnConfirm: false
        }, function() {
            $.ajax({
                type: "DELETE",
                url: url,
                dataType: 'json',
                headers: {
                 'X-CSRFToken': getCookie('csrftoken'),
                // "Content-Type": "application/json"
                },
                //data:{busqueda: parametrobusqueda},
                success: function(response) {
                    console.log('response es ', response)
                    swal("Elimnado!", "La regla ha sido eliminada", "success");
                    location.reload();
                },

                error: function(error) {
                    console.log('Erorr ', error)
                }
            });

            
        });
    });
    /**
     * Habra un boton con clase .edit-model , que tendra data-id  y data-name de su modelo
     *
     * Observaciones : El input txtnamemodel tendra e attr with the same name(ttnamemodel)
     * El modalmodel tendra data-id equivalente al id del modelo que representa
     * @param  {[type]} ) {                        var model_id [description]
     * @return {[type]}   [description]
     */
    $('.show-model-add').on('click', function () {
         console.log('show-modal-add');
         $('#modal-model-add').modal('show');
    });
    $('.show-model-edit').on('click', function() {
        console.log('show-modal-edit');
        var mod_nombre = $(this).data('name');
        var mod_id = $(this).data('id');
        var mod_estado = $(this).data('estado') == "a";
        $('#modal-model-edit').attr('data-id' , mod_id);
        $('input[name="txtnamemodeledit"]').val(mod_nombre);

        $('#mod_estado_edit').prop('checked', mod_estado);
        $('#modal-model-edit').modal('show');
        
    });

    $('.show-modal-model').on('click',function() {     
        console.log('-----')
         $('#modal-model').attr('data-id' , model_id);     
        var model_name = $(this).data('name');    
        $('#modal-model').attr('data-id' , model_id);
        $('#modal-model').attr('data-action' , "edit");  
        $("#btn-action-model").html('Editar')
       
        $('#modal-model').modal('show');
        $('input[name="txtnamemodel"]').val(model_name);
        var tmp = {}
        tmp.modelid = model_id;
        tmp.txt = $("#txtnamemodel");

        console.log("tmp ", tmp);
        // funcion ajax()        
    });


    $('.show-add-model').on('click', function () { 
        console.log('show add model')
        $("#btn-action-model").html('Guardar');     
        $('#modal-model').attr('data-action' , "save"); 
        $('#modal-model').modal('show');               

    });
    /**
     * Boton que lanzar un modal de agregar modelo
     * @param  {[type]} ) {                                     var model_id [description]
     * @return {[type]}   [description]
     */
    
   

    

    $('#btnguardarregla').on('click', function () {
        
    })

    
    $('#btnmodelosearch').on('click', function () {
        var modelosearch = $('#txtsearchmodelo').val();
        window.location.replace("/reglas/modelos/?modelosearch="+modelosearch);
    })

    $('#btn-regla-delete').on('click',function() {        
        var model_id = $(this).data('id');
        console.log('El id es ', model_id)
        url = "/reglas/api/regla/"+model_id;
        // funcion ajax()

        swal({
            title: "Esta usted seguro?",
            text: "Una vez que se elimina este modelos pasara a un estado inactivo",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Eliminar",
            closeOnConfirm: false
        }, function() {
            $.ajax({
                type: "DELETE",
                url: url,
                dataType: 'json',
                //data:{busqueda: parametrobusqueda},
                success: function(response) {
                    console.log('response es ', response)
                    swal("Elimnado!", "La regla Ha sido eliminada", "success");
                    location.reload();
                },

                error: function(error) {
                    console.log('Erorr ', error)
                }
            });

            
        });
    });

 


    $('#frmModel').on("submit",function(){
        event.preventDefault();
        var action = $(this).data('action');
        var url;
        if (action === 'edit') {
            url = "/modelo/save";            
        } else {            
            url = "/modelo/edit/id";
        }
               
        $.ajax({
            type:"POST",
            url: url,
            dataType: 'json',
            data:$(this).serialize(),
            success: function(response){
                $('#are_nom').val("");
                $('#are_des').val("");
                $('#are_etq').val("");
                $('#are_dir').val("");
                $('#modalVer').modal('hide');
                toastr.options={"progressBar":true};
                toastr.success(response.mensaje,'Area');
                $('#tblAre').DataTable().ajax.reload();             
            },

            error: function(){
                toastr.options={"progressBar":true};
                toastr.error("Error",'Area');
            }
        });

    });

});