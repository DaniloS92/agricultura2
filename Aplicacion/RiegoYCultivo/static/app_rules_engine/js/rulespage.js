/* body... */



var setmodelo = function(options) {
		console.log('setruless', options);
		$("#superreglas").queryBuilder("destroy");
		$("#superreglas").queryBuilder(options);
	}
	/*
	 *
	 *
	 */
var getFilters = function(actuadores) {
	let filters = []
	actuadores.forEach(function(actuadoritem, index) {
		console.log('>>', actuadoritem)
		let filter = {};
		filter.id = actuadoritem.iot_id;
		filter.field = actuadoritem.sa_categoria + " | " + actuadoritem.sa_nombre;
		filter.type = "double";
		filter.description = 'This filter is "unique", it can be used only once'
		if (actuadoritem.sa_senial == 'd') {
			filter.input = 'radio';
			filter.values = {
				5: 'ON',
				0: 'OFF'
			};
			filter.operators = ['equal', 'not_equal'];
			filter.colors = {
				5: 'success',
				0: 'danger'
			};
			// statement
		} else {
			filter.operators = ['equal', 'not_equal', 'less', 'less_or_equal', 'greater', 'greater_or_equal', 'between', 'not_between'];
			// statement
		}

		filter.optgroup = actuadoritem.dis_nombre;
		filter.validation = {
			min: 0,
			step: 0.1
		};
		filters.push(filter)

	});
	return filters;

}

var addcondition = function(nodopadre, nodohijo) {

	if (nodopadre.condition_id === nodohijo.parent && (nodopadre.condition_id !== nodohijo.condition_id)) {
		console.log('Insertadndo')
		nodopadre.rules.push(nodohijo);
	} else {
		nodopadre.rules.forEach(function(cond, index) {
			console.log('Insertadndo')
			addcondition(cond, nodohijo)
		})
	}

}
var addrule = function(nodopadre, rule) {
	console.log('RRRRRRRRRRRRRR')
	if (nodopadre.condition_id === rule.con_id) {
		console.log('el node padre tmp ', nodopadre);
		nodopadre.rules.unshift(rule);
	} else {
		if (nodopadre.rules) {
			nodopadre.rules.forEach(function(cond, index) {
				addrule(cond, rule);
			})
		}

	}

}

var getRules = function(rules) {
	let rulestmp = [];
	rules.forEach(function(ruleitem, index) {
		let rule = {};
		rule.id = ruleitem.iot_id;
		rule.operator = ruleitem.operador;

		if (ruleitem.factor2 == null || ruleitem.factor2 == '') {
			rule.value = ruleitem.factor1;
		} else {
			rule.value = [ruleitem.factor1, ruleitem.factor2];
		}
		console.log('value es >>>>>>>> ', rule.value)
		rule.con_id = ruleitem.con_id;
		rulestmp.push(rule);
	})
	return rulestmp;
};

var getConditions = function(conditions) {
		let condiciones = []
		conditions.forEach(function(conditionitem, index) {
			let condition = {};
			condition.condition_id = conditionitem.con_id;
			condition.condition = conditionitem.m_condicion;
			condition.parent = conditionitem.m_parent;
			condition.rules = [];
			//condition.flags =  {
			//  condition_readonly: true
			// };
			condiciones.push(condition)

		});
		console.log("COONDICIONES RETURN ", condiciones)
		return condiciones;
	}
	/*
	 *
	 *
	 */
var getmodelo = function (reglas, actuadores, condiciones ) {
	console.log('Se ha recibido actuadores ',actuadores);
	let filters = getFilters(actuadores);
	let conditions = getConditions(condiciones);
	let rules = getRules(reglas);

	var nodopadrejh= conditions[0];
	conditions.forEach(function (condicionitem, index,listacond) {
		addcondition(nodopadrejh, condicionitem);
	});

	rules.forEach(function (ruleitem, index,listacond) {
		addrule(nodopadrejh, ruleitem);
	});

	let options = {
		allow_empty: true,
		//default_filter: "name",
		sort_filters: false,
		//optgroups: {
		//	core: {
		//		en: "Core",
		//		fr: "Coeur"
		//	}
		//},
		plugins: {
			"bt-tooltip-errors": {delay: 100},
			"sortable": null,
			//"filter-description": {mode: "bootbox"},
			"filter-description" : null,

			"bt-selectpicker": null,
			//"unique-filter": null,
			"bt-checkbox": { color: "primary"},
			//"invert": false
		},

		//standard operators in custom optgroups
		operators:[
			{type: "equal",			optgroup: "basica"},
			{type: "not_equal",		optgroup: "basica"},
			{type: "in",			optgroup: "basic"},
			{type:"not_in", 		optgroup: "basic"},
			{type: "less",			optgroup: "numerica"},
			{type: "less_or_equal", optgroup: "numerica"},
			{type: "greater", 		optgroup: "numerica"},
			{type: "greater_or_equal",optgroup: "numerica"},
			{type: "between", 		optgroup: "numerica"},
			{type: "not_between", 	optgroup: "numerica"},
			{type: "begins_with", 	optgroup: "strings"},
			{type: "not_begins_with",optgroup: "strings"},
			{type: "contains",		optgroup: "strings"},
			{type: "not_contains",	optgroup: "strings"},
			{type: "ends_with",		optgroup: "strings"},
			{type: "not_ends_with", optgroup: "strings"},
			{type: "is_empty"},
			{type: "is_not_empty"},
			{type: "is_null"},
			{type: "is_not_null"},
			//{type: "micondicionante", optgroup: "custom", nb_inputs: 2, multiple: false, apply_to: ["number"]}
		],
		filters: filters,
		rules: nodopadrejh
		};
		return options;
	}



// get rules

$('.parse-jsonx').on('click', function() {
	console.log('las reglas son ', $('#superreglas').queryBuilder('getRules', {
			get_flags: true
		}),
		undefined, 2
	);
	$('#result').removeClass('hide')
		.find('pre').html(JSON.stringify(
			$('#superreglas').queryBuilder('getRules', {
				get_flags: true
			}),
			undefined, 2
		));
});





$('#chk-evento-activo').on('ifChecked', function(event){

	$(".btns-regla-guardar").collapse('hide');
	$(".panel-evento").collapse('show');

    //console.log($(this).val()); // alert value
});

$('#chk-evento-activo').on('ifUnchecked', function(event){
	console.log('uncheck')
	$(".panel-evento").collapse('hide');
	$(".btns-regla-guardar").collapse('show');
});

$(document).ready(function() {

	// using jQuery
	function getCookie(name) {
	    var cookieValue = null;
	    if (document.cookie && document.cookie !== '') {
	        var cookies = document.cookie.split(';');
	        for (var i = 0; i < cookies.length; i++) {
	            var cookie = jQuery.trim(cookies[i]);
	            // Does this cookie string begin with the name we want?
	            if (cookie.substring(0, name.length + 1) === (name + '=')) {
	                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                break;
	            }
	        }
	    }
	    return cookieValue;
	}



//==================  VALIDACIONES  ===========================================
	/**
	 * Validar formulario evento incluido acciones y alertas
	 * @return {Boolean} [description]
	 */
	//Formato de jsonelementos
	/*var jsontotal  = {
			modelo_id: modelo,
			m_regla: {
				reg_nombre: reglanombre,
				reglas: reglas,
			},
			m_evento: {

				ev_nombre: eventonombre,
				ev_descripcion: eventodescripcion,
			},
			evento_activo: eventoactivo,
			acciones: acciones,
			m_mensaje_alerta:{
				men_asunto: asunto,
				men_texto: contenido,
				men_destinatarios: destinatarios,

			} ,
			only_regla: only_regla

		}
*/

	var is_evento_valid = function (elementos) {
		 var is_valid = true;
		 /**
		  * Hacer todas las validaciones respectivas
		  */
		 return is_valid; // si es valido
	}


	/**
	 * Validar formulario de regla
	 * @return {Boolean} [description]
	 */

	var is_regla_valid = function (elementos) {
		 var is_rules_valid = $('#superreglas').queryBuilder('validate' );
		 console.log('<<< ', elementos)
		 var nombre_is_valid = elementos.m_regla.reg_nombre != "" //Ejm de validacion
		 /**
		  * Hacer todas las validaciones respectivas
		  */
		 return is_rules_valid && nombre_is_valid; //si es valido
	}


	$('#btn-regla-add').on('click', function () {
		console.log('edit  tes>>>>>> ',is_edit )

		is_rules_valid = $('#superreglas').queryBuilder('validate' );

		var url, method_type;
		if (is_edit=='True') {
			url = "/reglas/api/reglageneral/"+regla_general_id;
			method_type = 'PUT'
		} else {

			url = "/reglas/api/reglageneral/";
			method_type = 'POST';
		}
		var modelo = modeloid;
		var reglanombre = $('#txt-regla-nombre').val();
		var reglas = $('#superreglas').queryBuilder('getRules', {get_flags: true});

		var jsontotal  = {
			modelo_id: modelo,
			m_regla: {
				reg_nombre: reglanombre,
				reglas: reglas,
			},
			only_regla: true

		}
		console.log('token es ',  getCookie('csrftoken'))

		if (is_regla_valid(jsontotal) && is_evento_valid(jsontotal) ) {//Control de envio de formulario antes de ser enviado

			$.ajax({
				type: method_type,
				url: url,
				dataType: 'json',
				data: JSON.stringify(jsontotal),
				headers: {
	                 'X-CSRFToken': getCookie('csrftoken'),
	                 "Content-Type": "application/json"
	            },
				success: function(response) {
					console.log('response es ', response)
					location.reload();
					alertify.success('Regla guardada satisfactoriamente');
				},
				error: function(error) {
					console.log('Erorr ', error)
					alertify.error('Ha ocurrido un error al guardar la regla');
				}
			});

		}


	});


	// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

	$('#btn-evento-add').on('click',function () {
		var url, method_type;
		if (is_edit=='True') {
			url = "/reglas/api/reglageneral/"+regla_general_id;
			method_type = 'PUT'
		} else {

			url = "/reglas/api/reglageneral/";
			 method_type = 'POST';
		}

		console.log('prueba')
		var modelo = modeloid;
		var only_regla= false;
		var reglanombre = $('#txt-regla-nombre').val();
		var eventoactivo = $('#chk-evento-activo').is(':checked');
		var eventonombre = $('#txt-evento-nombre').val();
		var eventodescripcion = $('#txt-evento-descripcion').val();

		var acciones = [] ;
		var accionescheck = $("input:checkbox[name=activar]:checked")
		accionescheck.each(function(){
			var elemento = {};
			elemento.iot_id = $(this).data('id');
			var sa_senial =$(this).data('sa-senial');
			if (sa_senial=='a') {
				console.log('es analogico');
				elemento.acc_valor = $('#accion'+elemento.iot_id).val();
			} else if (sa_senial == 'd') {
				elemento.acc_valor = $('#accion'+elemento.iot_id).is(':checked');
				console.log('es digital')
			}
			elemento.sa_senial = sa_senial;

			acciones.push(elemento)

		});

		var asunto = $('#txt-asunto').val();
		var contenido = $('#txt-contenido').val();

		var destseleccionados = $('#select-destinatarios').find(':selected');
		var destinatarios = [];
		destseleccionados.each( function() {
			//console.log('===========')
			//console.log($(this).val())
			destinatarios.push($(this).val()) // id persona
		});

		var reglas = $('#superreglas').queryBuilder('getRules', {get_flags: true});

		var jsontotal  = {
			modelo_id: modelo,
			m_regla: {
				reg_nombre: reglanombre,
				reglas: reglas,
			},
			m_evento: {

				ev_nombre: eventonombre,
				ev_descripcion: eventodescripcion,
			},
			evento_activo: eventoactivo,
			acciones: acciones,
			m_mensaje_alerta:{
				men_asunto: asunto,
				men_texto: contenido,
				men_destinatarios: destinatarios,

			} ,
			only_regla: only_regla

		}
		console.log('JSON TOTAL ES  ', jsontotal)

		/**
		 * POST api/regla/general
		 * @type {String}
		 */
		console.log('token es ',  getCookie('csrftoken'))

		if (is_regla_valid(jsontotal) && is_evento_valid(jsontotal)) {

			$.ajax({
				type: method_type,
				url: url,
				dataType: 'json',
				data: JSON.stringify(jsontotal),
				headers: {
	                 'X-CSRFToken': getCookie('csrftoken'),
	                 "Content-Type": "application/json"
	            },
				success: function(response) {
					console.log('response es ', response)
					location.reload();

				},
				error: function(error) {
					console.log('Erorr ', error)
				}
			});
		}



	} );



	//$(this).is(':checked')) {


});

//btns-regla-guardar
//btns-evento-guardar