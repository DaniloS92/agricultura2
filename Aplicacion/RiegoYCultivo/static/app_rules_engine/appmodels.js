class Excel {
  constructor() {    
  }
  derecha(replace, cadena, lenString) {
    cadena = cadena + "";
    if (cadena.length === lenString) {
      return cadena;
    } else if (cadena.length <= lenString){
      cadena = replace.substr(0, (lenString - cadena.length))+ cadena;
      return cadena;
    } else {
      return parseInt(cadena);
    } 
  }
  izquierda(replace, cadena, lenString) {
    if (cadena.length === lenString) {
      return cadena;
    } else if (cadena.length <= lenString){
      cadena = cadena+ replace.substr(0, (lenString - cadena.length));
      return cadena;
    } else {
      return parseInt(cadena);
    } 
  }
  extraer(textoornumber, posicion_inicial, num_caracteres) {
    textoornumber = textoornumber + "";
    let subtexto = textoornumber.substr((posicion_inicial-1), num_caracteres);
    return subtexto;
  }
  
  redondear(amount, decimals) {

      amount += ''; // por si pasan un numero en vez de un string
      amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

      decimals = decimals || 0; // por si la variable no fue fue pasada

      // si no es un numero o es igual a cero retorno el mismo cero
      if (isNaN(amount) || amount === 0) 
          return parseFloat(0).toFixed(decimals);

      // si es mayor o menor que cero retorno el valor formateado como numero
      amount = '' + amount.toFixed(decimals);

      var amount_parts = amount.split('.'),
          regexp = /(\d+)(\d{3})/;

      while (regexp.test(amount_parts[0]))
          amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

      return amount_parts.join('.');
  }

  // methods
}

class HalfProductsMethod {
  constructor(nIterations, seed1, seed2) {
    this.nIterations = nIterations;
    this.seed1 = seed1;
    this.seed2 = seed2;
    this.totalRandomNumbers;
    this.rows = [];
    this.nTotal;
    this.error = "";
    
  }

  isValid() {
   
    if (!((this.seed1+"").length > 3) ) {
      console.log((this.seed+"").length)
      this.error = "La cantidad de digitos de la semilla 1 debe ser mayor a 3" 
      return false;
    }
    if (!((this.seed2+"").length > 3) ) {
      console.log((this.seed+"").length)
      this.error = "La cantidad de digitos de la semilla 2 debe ser mayor a 3" 
      return false;
    }

    if ((this.seed1+"").length % 2 !== 0) {
      this.error = "La cantidad de digitos de la semilla 1 debe ser par" 
      console.log((this.seed+"").length)
      return false;
    }
    if ((this.seed2+"").length % 2 !== 0) {
      this.error = "La cantidad de digitos de la semilla 2 debe ser par" 
      console.log((this.seed+"").length)
      return false;
    }
    return true;
    
  }
  work() {
    let excel = new Excel();
    let tseed1 = this.seed1;
    let tseed2 = this.seed2;

    for (let i = 0; i < this.nIterations; i++) {
      let row = {};
      row.serie = i+1;
      row.xcero = tseed1;
      row.xone = tseed2;
      row.producto = excel.derecha("0000000000", (row.xcero *  row.xone), 8);
      row.ycero = excel.extraer(row.producto, 3, 4);
      row.yone = "0."+ row.ycero;
      this.rows[i] = row;
     
      tseed1 = row.xone;
      tseed2 = row.ycero;
    }
  }

  getntotal() {
    let excel = new Excel();
    let tseed1 = this.seed1;
    let tseed2 = this.seed2;
    let tmp = [];

    for (let i = 0; true; i++) {
      let row = {};
      row.serie = i+1;
      row.xcero = tseed1;
      row.xone = tseed2;
      row.producto = excel.derecha("0000000000", (row.xcero *  row.xone), 8);
      row.ycero = excel.extraer(row.producto, 3, 4);
      row.yone = "0."+ row.ycero;
      this.rows[i] = row;
     
      tseed1 = row.xone;
      tseed2 = row.ycero;
      
      if (tmp.indexOf(row.ycero) !== -1) {
        console.log("el resultado total es ");
        console.log((i+1))
        this.nTotal = (i);
        break;
      }
      tmp[i] = row.ycero;
      if (i===10000) {
        break;
      } 
    }
  }


  // methods
}

class LinearCongruentialAlgorithm {
  constructor(x0, k, g) {
    this.x0 = x0;
    this.k = k;
    this.g = g;
    this.m = this.getM()
    this.c = this.getC()
    this.a = this.getA()
    this.rows = [];
    this.nTotal = 0;
    this.error = 0;
  }

  isPrimeNumber(number){
    for(let i=1;i<number;i++)
    {
      // If factor
      if(number/i == Math.round(number/i) && i!=1 && i!=number)
      {
        return false;
        break;
      };
    };
    return true;
  } 

  getM() {
    return Math.pow(2, this.g)
  }
  
  getC() {
    for(let i = this.m; i>0; i--) {
      if (this.isPrimeNumber(i)) {
        return i
      }
    }
    return 0;
  }

  getA() {
    return 1 + (4*(this.k))
  }

  work() {
    var excel = new Excel();
    let row = {};
    let tmp = [];
    let xi = this.x0;
    for (let i = 0; true ; i++) {
      let row = {};
      row.serie = i+1;
      row.xone = ((this.a*xi)+this.c) % this.m;

      row.yone = excel.redondear((row.xone / (this.m-1)), 4);   
      console.log("->>",xi);   
      console.log("->>>",this.m);       
     console.log("->>>>",row.yone);
      xi = row.xone;
      
      if (tmp.indexOf(row.xone) !== -1) {
          this.nTotal = (i);
        break;
      }
      this.rows[i] = row;
      tmp[i] = row.xone;
      if (i===1000) {
        console.log("vamo a descontrolarno");
        break;
      } 
    }
  }

  isValid() {
    if (true) {}
    if (isNaN(parseInt(this.x0)) || parseInt(this.x0)===0) {
      this.error = "Ingrese x0";
      return false;
    }
    if (isNaN(parseInt(this.k)) || parseInt(this.k)===0) {
      this.error = "Ingrese k";
      return false;
    }
    if (isNaN(parseInt(this.g))|| parseInt(this.g)===0) {
      this.error = "Ingrese g";
      return false;
    }
    return true;
  }
  
}

class AditiveCongruentialAlgorithm  {
  constructor(m, xns) {    
    console.log("los datos que vienron son");
    console.log(m);
    console.log(xns);
    this.m = m; 
    this.xns = xns;
    this.xn = xns.length;
    this.rows = [];
    this.nTotal = 0;
    for (let i = 0 ; i < xns.length; i++) {
      let row = {};
      row.xone = xns[i] ;
      this.rows[i] = row;
    }
    console.log("por ultimo");
    console.log(this.rows);
    console.log("======== JH ===");
  }

  work() {
    let row = {};
    let tmp = [];
    let excel = new Excel();
    console.log("total de work 1 es ");
    console.log( this.rows);
    for (let actual = this.xn, antes = 0; actual < 30 ; actual++, antes++) {
      let row = {};
      row.serie = antes+1;
      row.xone = (this.rows[actual-1].xone + this.rows[antes].xone) % this.m ;
      row.yone = excel.redondear((row.xone / (this.m - 1)), 4);          
           
      if (tmp.indexOf(row.xone) !== -1) {
          this.nTotal = (antes);
        break;
      }
            this.rows[actual] = row;
      tmp[actual] = row.xone;
      if (actual===1000) {
        console.log("vamo a decontrolarno");
        break;
      } 
    }
    for (let i = 0; i < this.xn; i++)  {
      this.rows.shift();
    }
    
  }

  isValid() {
    if (isNaN(parseInt(this.m))) {
      return false;
    }
    if (isNaN(this.m)) {
      console.log("arelyssss");
      return false;
    }
    for (var i = 0; i < this.xns.length; i++) {
      
      if (isNaN(this.xns[i])) {
        
        return false;
      }
    }
    return true;
  }
  // methods
}


//var jose = new AditiveCongruentialAlgorithm(100, [{value:65},{value:89},{value:98},{value:3},{value:69},])
//jose.work();
//console.log(jose.rows)
class MulConAlgorithm {
  constructor(xcero, k, g, nIterations) {
    this.xcero = xcero;
    this.k = k;
    this.g = g;
    this.nIterations = nIterations;
    this.nTotal = 0;
    this.a = 3 + (8 * this.k);
    this.m = Math.pow(2, this.g);
    this.rows = [];
    this.error = "jokoseej";
    
  }
  isvalid() {
    if (!(parseInt(this.xcero) > 0) ) {
      this.error = "El numero de xcero  debe ser mayor a cero";
      return false
    }
    if (!(parseInt(this.k)>0)) {
      this.error = "El numero de k  debe ser mayor a cero";
      return false
    }
    if (!(parseInt(this.g)>0)) {
      this.error = "El numero de g  debe ser mayor a cero";
      return false
    }
    if (parseInt(this.g) === 0) {
      console.log("erroror xcero")
      this.error = "El numero de xcero  no debe ser cero";
      return false
    }
    if (parseInt(this.xcero) % 2 === 0) {
      
      this.error = "El numero de xcero debe ser impar";
      return false
    }
    
    if (parseInt(this.k) === 0) {
      console.log("erroror xcero")
      this.error = "El numero de k no debe ser cero";
      return false
    }
    if (parseInt(this.g) === 0) {
      console.log("erroror xcero")
      this.error = "El numero de g no debe ser cero";
      return false
    }
    return true ;
  }

  work() {

    let excel = new Excel();
    let xi = this.xcero;

    for (let i = 0; i < this.nIterations; i++) {
      let row = {};
      row.serie = i+1;
      row.xone = (this.a * xi) % (this.m);
      row.yone = excel.redondear((row.xone / (this.m - 1)),4);      
      this.rows[i] = row;
     
      xi = row.xone;
    }
  }

  getNTotal() {
    this.rows=[];
    let excel = new Excel();
    let xi = this.xcero;
    let tmp = [];
    for (let i = 0; true ; i++) {
      let row = {};
      row.serie = i+1;
      row.xone = (this.a * xi) % (this.m);
      row.yone = excel.redondear((row.xone / (this.m - 1)),4);      
      
     
      xi = row.xone;
      
      if (tmp.indexOf(row.xone) !== -1) {
        
        this.nTotal = (i);
        break;
      }
      this.rows[i] = row;
      tmp[i] = row.xone;
      if (i===1000) {
        break;
      } 
    }
  }

  // methods
}
//let mivar = new MulConAlgorithm(19, 3, 4, 20);
//mivar.work();
//console.log(mivar.rows);


class randomNumber {
  constructor(nNumbers, seed, interval) {
    this.nNumbers = nNumbers;
    this.seed = seed; 
    this.rows = []; 
    this.d = (this.seed+"").length;
    this.rows = [];
    this.interval = interval;
    //for Graphic
    this.labels = [];
    this.data = [];
  }
  isValid(){
    console.log((this.seed+"").length)
    console.log(! ((this.seed+"").length>3))
    if (!((this.seed+"").length > 3) ) {
      console.log((this.seed+"").length)
      this.error = "La cantidad de digitos de la semilla debe ser mayor a 3" 
      return false;
    }

    if ((this.seed+"").length % 2 !== 0) {
      this.error = "La cantidad de digitos de la semilla debe ser par" 
      console.log((this.seed+"").length)
      return false;
    }
    return true;
  }
  work(){
    
    let gmedia = [];
    let gyone = [];
    for (let i = 0; i < this.nNumbers; i++) {

        let row = {};
        row.serie = (i + 1);
       // console.log("row ",row);
        row.xcero = i === 0 ? this.seed :  this.rows[i-1].xone;
        row.xcerosquare = this.derecha("00000000000",(Math.pow(row.xcero, 2))+"", (this.d*2)); // Math.pow(row.xcero, 2);

        let sizesquare = (row.xcerosquare+"").length;
        let tmp =((row.xcerosquare+"").length - this.d )/2 ;/// 2;
        row.xone = this.derecha("00000000000", parseInt((row.xcerosquare + "").substr(tmp, this.d))+"", 4 ) ;
        row.yone = this.redondear((row.xone / (Math.pow(10, this.d))), 4);
        
        if ((i+1) < this.interval) {
          
          row.media = 0;
        } else {
          let suma = 0;
          
          for (let j = 0, minidex = i; j < this.interval; j++,minidex--) {
            if (minidex === i) {
              suma += parseFloat(row.yone)  ;
            } else {
              suma += parseFloat(this.rows[minidex].yone)  ;
            }
              
          }

          row.media = this.redondear((suma / this.interval), 4);  //this.rows[i-1].xone + this.rows[i-1].xone ;
        }
        
        console.log(row);
        this.rows[i] = row;
        //for Graphics
        this.labels[i]= (i+1);
        gmedia[i] = row.media;
        gyone[i] = row.yone;

    }
    console.log("rows es ");
    console.log(this.rows);
    //for Graphics 
    this.data[0] = gmedia;
    this.data[1] = gyone;
  }

  mostrar(){
    console.log(`n es ${this.nNumbers} y la semilla es ${this.seed}`);
    console.log("bien");
  }

  derecha(replace, cadena, lenString) {
    if (cadena.length === lenString) {
      return cadena;
    } else if (cadena.length <= lenString){
      cadena = replace.substr(0, (lenString - cadena.length))+ cadena;
      return cadena;
    } else {
      return parseInt(cadena);
    } 
  }
  izquierda(replace, cadena, lenString) {
    if (cadena.length === lenString) {
      return cadena;
    } else if (cadena.length <= lenString){
      cadena = cadena+ replace.substr(0, (lenString - cadena.length));
      return cadena;
    } else {
      return parseInt(cadena);
    } 
  }
  redondear(amount, decimals) {

      amount += ''; // por si pasan un numero en vez de un string
      amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

      decimals = decimals || 0; // por si la variable no fue fue pasada

      // si no es un numero o es igual a cero retorno el mismo cero
      if (isNaN(amount) || amount === 0) 
          return parseFloat(0).toFixed(decimals);

      // si es mayor o menor que cero retorno el valor formateado como numero
      amount = '' + amount.toFixed(decimals);

      var amount_parts = amount.split('.'),
          regexp = /(\d+)(\d{3})/;

      while (regexp.test(amount_parts[0]))
          amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

      return amount_parts.join('.');
  }

}


(function() {
    'use strict';

    var app = angular.module('modelos', ['ui.bootstrap']);



    app.controller('MenuCtrl', function($scope) {
      $scope.modelsearch = "assss";
      $scope.charts = ['Line', 'Bar', 'Doughnut', 'Pie', 'Polar Area', 'Radar', 'Base'];
    });

    app.controller('', ['$scope',' $uibModalInstance', function ($scope, $uibModalInstance, items) {
       scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
    }]);
    app.controller('ModelsCtrl', ['$scope', '$timeout','$http', '$uibModal', '$log', function($scope, $timeout, $http,  $uibModal, $log) {
      
      
        $scope.items = ['item1', 'item2', 'item3'];

        $scope.animationsEnabled = true;

        $scope.open = function (size) {

          var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
              items: function () {
                return $scope.items;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

        $scope.toggleAnimation = function () {
          $scope.animationsEnabled = !$scope.animationsEnabled;
        };






      var scope = $scope;


      var setmodelos = function(parametrobusqueda) {
        // Cuando se cargue la página, pide del API todos los TODOs
        var url = '/rules_engine/api/modelo/?search=' + parametrobusqueda
        $http.get(url)
          .success(function(data) {
            scope.modelos = data;
            console.log(data)
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });

      }
      var setrulesbymodeloid = function (modeloid) {
        var url = '/rules_engine/api/regla/?modelo='+modeloid;
          $http.get(url)
          .success(function(data) {
            scope.reglas = data;
            console.log(data)
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });

      }

      var setmodelo = function (id) {
          var url = '/rules_engine/api/modelo/'+id;

          $http.get(url)
          .success(function(data) {console.log('data es ', data)
            scope.modelogeneral.modelo = data;

            console.log(data)
          })
          .error(function(data) {
            console.log('Error: ' + data);
          });
      }
      setmodelos("");

      scope.modelsearch = "jejjej"; //Busqueda de modelos
      scope.rulesearch = "hola"; //Busqueda de reglas por nombre
      scope.modelos = []; //mostrar modelos 
      scope.modelogeneral = {modelo: "as", reglas: []};//Lo que se presenta en el sengundo panel
      

      scope.filtrarmodelos = function(argument) {
        /* body... */
      };
      
      /**
       * Funciones de los botones de la pagina
       */
       scope.showeditarmodelo = function (modelo_id) {
          setmodelo(modelo_id);     

         // angular.element('#btn-show-edit-model').trigger('click');
       }
       scope.verreglas = function (modeloid) {
          scope.modelo.reglas = getrulesbymodeloid(modeloid);
       }
       scope.editarmodelo = function (idmodelo) {
          console.log('he llegado ', idmodelo)
       }
       scope.eliminarmodelo = function (modeloid) {
          
       }

       scope.guardarmodelo = function () {
          /* body... */ 
       }

        $scope.loadEditForm = function () {
        $scope.checkItem = "yes";
        //$("#modal-form-edit").modal();
        console.log('llll', $("#modal-form-edit"))
    };




  }]);
//=======================================================================================
  app.controller('HalfSquare', ['$scope', '$timeout', function ($scope, $timeout) {
   
    $scope.withmedia = false;//4457;
    $scope.change = function () {
      if ($scope.withmedia) {
          $scope.labels = random.labels;
          $scope.data = random.data;
      }else {
         $scope.labels = [1,2];
          $scope.data = [  [0,0],   [0,0]  ];
      }
      
       /* body... */ 
    };//4457;
    $scope.seed = "";//4457;
    $scope.nNumber = "";//10;
    $scope.interval = "";
    $scope.rows = []
    $scope.calcData = {};
    let random;
    $scope.uno= function () {
       console.log(`La entidad es ${$scope.interval}`)
       if ($scope.interval == "") {
          $scope.interval = 3;
        }
       random = new randomNumber($scope.nNumber,$scope.seed, $scope.interval);

       // random.mostrar();
        random.work();        
        if (random.isValid()) {
          $scope.rows = random.rows;
          $scope.labels = random.labels;
          $scope.data = random.data;
          $scope.error = "";
        } else {
          console.log('EROROOROROORO');
          console.log(">",random.error);
          $scope.error = random.error;
        }
        
    };
  }
  ]
  );

  app.controller('HalfProduct', ['$scope', '$timeout', function ($scope, $timeout) {
    $scope.message =  "este es mi msj";
         /* body... */ 
    //4457;
    $scope.seed1 = "";//1547;
    $scope.seed1 = "";//1015;
    $scope.nNumber = "";//10;
   
    $scope.rows = []
    $scope.calcData = {};
    let random;
    $scope.work= function () {
      random = new HalfProductsMethod($scope.nNumber, $scope.seed1, $scope.seed2);
      if (random.isValid()) {
        $scope.error = "";
        //random.work();
        random.getntotal();
        $scope.message = random.nTotal;        
        $scope.rows = random.rows;
      } else {
        $scope.error = random.error;
        $scope.rows = [];
        $scope.message = ""; 
      }
       // random.mostrar();
        

        //$scope.labels = random.labels;
       //$scope.data = random.data;
    };
   

   // $timeout(function () {
    //  $scope.labels = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
     // $scope.data = [
     //   [28, 48, 40, 19, 86, 27, 90],
     //   [65, 59, 80, 81, 56, 55, 40]
     // ];
     // $scope.series = ['Series C', 'Series D'];
  //  }, 3000);
  }
  ]
  );
  app.controller('MulConAlgorithmCtrl', ['$scope', function ($scope) {
    $scope.nTotal =  "";
         /* body... */ 
    //4457;constructor(xcero, k, g, nIterations) {
    $scope.xcero = "";//19
    $scope.k = "";//3
    $scope.g = "";//4
    $scope.nNumber = "";//25;
    $scope.error = "";
    $scope.rows = []
   
    let random;
    $scope.work= function () {

      
       random = new MulConAlgorithm($scope.xcero, $scope.k, $scope.g, 3);

       // random.mostrar();
        random.work();
        if (!random.isvalid()) {
          $scope.error = random.error;
          $scope.rows = "" ;
        $scope.nTotal = "" ;
        } else {
          // statement
        $scope.error = "";
        
        random.getNTotal();

        console.log("ok=============");
        console.log(random.rows);
        $scope.rows = random.rows;
        $scope.nTotal = random.nTotal;
        }
    };
   
  }
  ]
  );


  app.controller('LinearCongruentialAlgorithm', ['$scope', function ($scope) {
    $scope.nTotal =  "";
         /* body... */ 
    //4457;constructor(xcero, k, g, nIterations) {
    $scope.xcero = "";//19
    $scope.k = "";//3
    $scope.g = "";//4
    $scope.nNumber = "";//25;
    $scope.error = "";

    $scope.rows = []
    $scope.m = "";
    $scope.c = "";
    $scope.a = "";
    let random;
    $scope.work= function () {      
       random = new LinearCongruentialAlgorithm($scope.xcero, $scope.k, $scope.g);

       // random.mostrar();
        random.work();
        if (!random.isValid()) {
          $scope.error = random.error;
        } else {
          // statement
        $scope.error = "";
        
        $scope.rows = random.rows;
        $scope.nTotal = random.nTotal;
        $scope.m = random.m;
        $scope.c = random.c;
        $scope.a = random.a;
        }
    };
   
  }
  ]
  );


    app.controller('AditiveCongruentialAlg', ['$scope', function ($scope) {
    $scope.m =  "";
         /* body... */ 
    //4457;constructor(xcero, k, g, nIterations) {
    $scope.xns = [];//19
    $scope.k = "";//3
    $scope.error = "";
    $scope.rows = []
    $scope.xn = "";
    let mirandom;
    $scope.generatesFields = function (keyEvent) {
      if (keyEvent.which === 13) {
        for (let i=0 ; i < $scope.xn ; i++) {
          $scope.xns[i] = {value: ""};
       }

      }
    
       
    };
    $scope.working= function () {    
      let tmp = [];
      for (let i = 0; i< $scope.xn; i++) {
        tmp[i] =parseInt($scope.xns[i].value);
      }

      mirandom = new AditiveCongruentialAlgorithm($scope.m,tmp);
       // random.mostrar();
        mirandom.work();
        if (!mirandom.isValid()) {
          $scope.error = mirandom.error;
          $scope.rows = [];
        } else {
          // statement
        $scope.error = "";
        $scope.rows = mirandom.rows;

        $scope.nTotal = mirandom.nTotal;
        }
    };
   
  }
  ]);


 
  function getRandomValue (data) {
    var l = data.length, previous = l ? data[l - 1] : 50;
    var y = previous + Math.random() * 10 - 5;
    return y < 0 ? 0 : y > 100 ? 100 : y;
  }
})();
