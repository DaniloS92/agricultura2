/**
 * Created by Danilo on 13/05/2016.
 */
(function()
{
    var contador = 0;
    freeboard.addStyle('.boton-widget-wrapper', "margin-bottom:10px !important;");

    var botonWidget = function (settings) {
        var self = this;
        var currentSettings = settings;

        contador++;
        var botonId = 'switch-input-'+contador;
        var ledId = 'led-'+contador;
        var titleElement = $('<h2 class="section-title"></h2>');
        var gaugeElement = $('<label class="switch switch-green"><input id="'+botonId+'" type="checkbox" class="switch-input"><span class="switch-label" data-on="'+currentSettings.labelEncendido.substr(0, 3).toUpperCase()+'" data-off="'+currentSettings.labelApagado.substr(0, 3).toUpperCase()+'"></span><span class="switch-handle"></span></label>');
        var rendered = false;
        var tiempoConf, tiempoDespliegueConf;

        function gestionarCambio() {
            if (confirm("¿Enviar comando?")) {
                $('#'+ledId).html('');
                clearInterval(tiempoConf);
                var cmd;
                var interr = $(this);
                if ($(this).prop("checked")) {
                    cmd = 1;
                } else {
                    cmd = 0;
                }
                ++contadorMensajesPub;
                var mensaje = new Paho.MQTT.Message('{"id":'+contadorMensajesPub+',"cmd": '+cmd+', "var": "'+currentSettings.variable+'"}');
                mensaje.destinationName = currentSettings.topicoEscritura;

                mensaje.qos = 1;
                client.send(mensaje);
                mensajesPub[contadorMensajesPub.toString()] = $(this).parent().parent().find('#'+ledId);

                tiempoConf = setInterval(function(){
                    var contConfir = interr.parent().parent();
                    if (!contConfir.find('#'+ledId).find('.double-check').length) {
                        $('#'+ledId).html('<i class="icon-check" style="margin-left:-1px;margin-top:-2px"><i class="icon-cancel" style="margin-left:-13px;margin-top:3px"></i>');
                            tiempoDespliegueConf = setInterval(function(){
                            clearInterval(tiempoDespliegueConf);
                            $('#'+botonId).off('change');
                            $('#'+ledId).html('');
                            $('#'+botonId).prop('checked', !$('#'+botonId).prop('checked'));
                            $('#'+botonId).on('change', gestionarCambio);
                        },2000);
                    }
                    clearInterval(tiempoConf);

                }, currentSettings.tiempo*60000);
            }
            else{
                $('#'+botonId).off('change');
                            $('#'+ledId).html('');
                            $('#'+botonId).prop('checked', !$('#'+botonId).prop('checked'));
                            $('#'+botonId).on('change', gestionarCambio);
            }
        }

        this.render = function (element) {
            rendered = true;

            titleElement.html(currentSettings.titulo);
            $(element).append(titleElement).append($('<div class="boton-widget-wrapper"></div>').append($('<div class="cont" style="width:100px;margin:0 auto"></div>').append(gaugeElement).append($('<div style="float:right;margin-top:-21px;font-size:1.1em;width:20px" id="'+ledId+'"></div>'))));


            $('#'+botonId).change(gestionarCambio);
        }

        this.onSettingsChanged = function (newSettings) {
            currentSettings = newSettings;
            titleElement.html(newSettings.titulo);
        }

        this.onCalculatedValueChanged = function (settingName, newValue) {

            for (var id in mensajesPub) {
                if (newValue.id == id){
                    if (newValue.cmd){
                        mensajesPub[id].html('<i class="icon-check" style="margin-left:5px;color:#4C9ED9"></i><i style="margin-left:-27px;color:#4C9ED9" class="icon-check double-check"></i>');

                    }
                    else {
                        mensajesPub[id].html('<i class="icon-check" style="margin-left:5px"></i><i style="margin-left:-27px" class="icon-check double-check"></i>');

                    }
                    delete mensajesPub[id];
                    break;
                }
            }
        }

        this.onDispose = function () {
        }

        this.getHeight = function () {
            return 1.1;
        }

        this.onSettingsChanged(settings);
    };

    freeboard.loadWidgetPlugin({
        type_name: "boton",
        display_name: "Interruptor",
        settings: [
            {
                name: "titulo",
                display_name: "Titulo",
                type: "text"
            },
            {
                name: "labelEncendido",
                display_name: "Texto en encendido",
                type: "text",
                description: "Máximo tres caracteres, si se ingresan más se mostrarán sólo los tres primeros."
            },
            {
                name: "labelApagado",
                display_name: "Texto en apagado",
                type: "text",
                description: "Máximo tres caracteres, si se ingresan más se mostrarán sólo los tres primeros."
            },
            {
                name: "variable",
                display_name: "Variable",
                type: "text"
            },
            {
                name: "topicoEscritura",
                display_name: "Tópico de envío",
                type: "option",
                options: gtopicos
            },
            {
                name: "topicoLectura",
                display_name: "Tópico de recepción",
                type: "calculated",
                description: "El tópico por el cual se reciben los comandos desde algún dispositivo para mostrar su estado actual."
            },
            {
                name: "tiempo",
                display_name: "Tiempo máximo de confirmación (minutos)",
                type: "number",
                description: "El tiempo inicia al momento de enviar el comando, y al caducar, se verificará si ya ha llegado el mensaje de confirmación, de no ser así; se supondrá un error."
            }
        ],
        newInstance: function (settings, newInstanceCallback) {
            newInstanceCallback(new botonWidget(settings));
        }
    });

}());