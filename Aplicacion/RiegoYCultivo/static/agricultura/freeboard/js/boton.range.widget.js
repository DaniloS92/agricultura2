/**
 * Created by Danilo on 13/05/2016.
 */
(function() {
    var contador = 0;
    var botonWidget = function(settings) {
        var self = this,
        socket;
        var currentSettings = settings;
        
        contador++;
        var botonId = 'btn_analogico_'+contador;
        var estadoID = 'btn_estado-a_' + contador;
        var estadoEspera = 'btn_estado_espera_a_' + contador;
        var titleElement = $('<h2 class="section-title"></h2>');
        var gaugeElement = $('<input id="'+botonId+'" title="0" type="text" value="" />');
        
        var tempElement = $('<div style="float:right;margin-top:-60px;font-size:1.1em"><label id="'+estadoID+'" style="margin-right: 70px;"></label></div>');
        var statElement = $('<span style="margin-left: 150px;" id="' + estadoEspera + '"></span>');
        var statElementWait = $('<i class="fa fa-spinner fa-pulse fa-2x fa-fw" style=" color: white;position:absolute;margin-top: -50px;margin-left: 80px;"></i>');
        var statElementOK = $('<i class="fa fa-check fa-2x" aria-hidden="true" style=" color: green;position:absolute;margin-top: -50px;margin-left: 80px;"><spam style="font-size: 14px;"><spam></i>');
        var statElementError = $('<i class="fa fa-times fa-3x hvr-pulse" aria-hidden="true" style=" color: #F45B5B;position:absolute;margin-top: -50px;margin-left: 80px;cursor: pointer;" onclick="consulta(0);"><spam style="font-size: 14px;"><spam></i>');
        var rendered = false;
        var tiempoConf, tiempoDespliegueConf;
        var estado_acciones = true;
        var valueChange;
        var segundos = parseInt(currentSettings.tiempo * 60) - 2;

        function gestionarCambio() {

            console.log('Ingreso:'+botonId);

            var slider = $('#'+botonId).data("ionRangeSlider");
            
            if (confirm("¿Enviar comando?")) {

                self.socket = io.connect(currentSettings.url);
                clearInterval(tiempoConf);
                clearInterval(tiempoDespliegueConf);
                var cmd = slider.result.from;

                var datos_envio = {
                    estado : cmd,
                    idActuador : currentSettings.actuador
                };

                self.socket.emit('evento', datos_envio);
                estado_acciones = false;
                // document.getElementById(estadoID).innerHtml(statElementWait);
                $('#' + estadoEspera).empty();
                $('#' + estadoEspera).append(statElementWait);

                tiempoConf = setTimeout(function(){

                    $('#' + estadoEspera).empty();
                    $('#' + estadoEspera).append(statElementError);
                    estado_acciones = true;
                    clearInterval(tiempoDespliegueConf);
                    segundos = parseInt(currentSettings.tiempo * 60) - 2;
                    document.getElementById(estadoID).innerHTML = '';

                },currentSettings.tiempo*60000);

                // Pregunta cada 1 seg si llego la señal esperada
                // Validacion de si llega la accions solicitada
                tiempoDespliegueConf = setInterval(function(){
                    if(valueChange == cmd){
                        clearInterval(tiempoConf);
                        clearInterval(tiempoDespliegueConf);
                        $('#' + estadoEspera).empty();
                        $('#' + estadoEspera).append(statElementOK);
                        estado_acciones = true;
                        valueChange=null;
                        segundos = parseInt(currentSettings.tiempo * 60) - 2;
                        document.getElementById(estadoID).innerHTML = '';
                    }else{
                        document.getElementById(estadoID).innerHTML = segundos+' seg';
                    }
                    segundos = segundos - 1;
                },1000);

            } else {
                slider.update({
                    from: valueChange
                });
            }
        }

        this.render = function(element) {
            rendered = true;

            titleElement.html(currentSettings.titulo);
            $(element)
            .append(titleElement)
            .append($('<div></div>')
                .append($('<div class="cont" style="width:150px";></div>')//margin:0 auto
                    .append(gaugeElement)
                    .append(statElement)).append(tempElement));

            $('#'+botonId).ionRangeSlider({
                min: 0,
                max: 100,
                onFinish : function (data){
                    gestionarCambio();
                }
            });
        };

        this.onSettingsChanged = function(newSettings) {
            currentSettings = newSettings;
            titleElement.html(newSettings.titulo);
        };

        this.onCalculatedValueChanged = function(settingName, newValue) {
            valueChange = newValue;
            if(estado_acciones){
                var slider = $('#'+botonId).data("ionRangeSlider");
                slider.update({
                    from: valueChange
                });
            }
        };

        this.onDispose = function() {};

        this.getHeight = function() {
            return 1;
        };

        this.onSettingsChanged(settings);
    };

    freeboard.loadWidgetPlugin({
        type_name: "boton_analogico",
        display_name: "Interruptor Analogico",
        settings: [{
            name: "titulo",
            display_name: "Titulo",
            type: "text"
        }, {
            name: "url",
            display_name: "Url Servidor",
            type: "text",
            description: "Url del servidor nodejs donde se desea enviar la accion a gestionar. <br> ej: http://localhost:8686/iotmach_actuador"
        }, {
            name: "actuador",
            display_name: "ID Actuador",
            type: "text"
        }, {
            name: "estado_actuador",
            display_name: "senial estado actuador",
            type: "calculated"
        }, {
            name: "tiempo", display_name: "Tiempo máximo de confirmación (minutos)",
            type: "number",
            description: "El tiempo inicia al momento de enviar el comando, y al caducar, se verificará si ya ha llegado el mensaje de confirmación, de no ser así; se supondrá un error."
        }],
        newInstance: function(settings, newInstanceCallback) {
            newInstanceCallback(new botonWidget(settings));
        }
    });

}());
