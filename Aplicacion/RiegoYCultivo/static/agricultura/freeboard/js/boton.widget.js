/**
 * Created by Danilo on 13/05/2016.
 */
(function() {
    var contador = 0;
    freeboard.addStyle('.boton-widget-wrapper', "margin-bottom:5px !important;");

    var botonWidget = function(settings) {
        var self = this,
        socket;
        var currentSettings = settings;
        
        contador++;
        var botonId = 'switch-input-' + contador;
        var estadoID = 'led-' + contador;
        var titleElement = $('<h2 class="section-title"></h2>');
        var gaugeElement = $('<label class="switch switch-green"><input id="' + botonId + '" type="checkbox" class="switch-input botonGestion"><span class="switch-label" data-on="' + currentSettings.labelEncendido.substr(0, 3).toUpperCase() + '" data-off="' + currentSettings.labelApagado.substr(0, 3).toUpperCase() + '"></span><span class="switch-handle"></span></label>');
        
        var tempElement = $('<div style="float:right;margin-top:-25px;font-size:1.1em"><label id="temp_'+estadoID+'" style="margin-right: 120px;"></label></div>');
        var statElement = $('<spam style="margin-left: 150px;" id="' + estadoID + '"></spam>');
        var statElementWait = $('<i class="fa fa-spinner fa-pulse fa-2x fa-fw" style=" color: white;margin-left: 5px;"></i>');
        var statElementOK = $('<i class="fa fa-check fa-2x" aria-hidden="true" style=" color: green;margin-left: 5px;"><spam style="font-size: 14px;"><spam></i>');
        var statElementError = $('<i class="fa fa-times fa-3x hvr-pulse" aria-hidden="true" style=" color: #F45B5B;margin-left: 5px;cursor: pointer;" onclick="consulta('+currentSettings.actuador+');"><spam style="font-size: 14px;"><spam></i>');
        var rendered = false;
        var tiempoConf, tiempoDespliegueConf;
        var estado_acciones = true;
        var valueChange;
        var segundos = parseInt(currentSettings.tiempo * 60) - 2;

        function gestionarCambio() {
            
            if (confirm("¿Enviar comando? - "+currentSettings.titulo)) {

                self.socket = io.connect(currentSettings.url);
                clearInterval(tiempoConf);
                clearInterval(tiempoDespliegueConf);
                var cmd;
                var interr = $(this);

                if ($(this).prop("checked")) {
                    cmd = 1;
                } else {
                    cmd = 0;
                }

                var datos_envio = {
                    estado : cmd,
                    idActuador : currentSettings.actuador
                };

                self.socket.emit('evento', datos_envio);
                estado_acciones = false;
                // document.getElementById(estadoID).innerHtml(statElementWait);
                $('#' + estadoID).empty();
                $('#' + estadoID).append(statElementWait);

                tiempoConf = setTimeout(function(){

                    $('#' + estadoID).empty();
                    $('#' + estadoID).append(statElementError);
                    $('#' + botonId).off('change');
                    $('#' + botonId).prop('checked', !$('#' + botonId).prop('checked'));
                    $('#' + botonId).on('change', gestionarCambio);
                    estado_acciones = true;
                    clearInterval(tiempoDespliegueConf);
                    valueChange=null;
                    segundos = parseInt(currentSettings.tiempo * 60) - 2;
                    document.getElementById("temp_"+estadoID).innerHTML = '';

                },currentSettings.tiempo*60000);

                // Pregunta cada 1 seg si llego la señal esperada
                // Validacion de si llega la accions solicitada
                tiempoDespliegueConf = setInterval(function(){
                    if(valueChange == cmd){
                        clearInterval(tiempoConf);
                        clearInterval(tiempoDespliegueConf);
                        $('#' + estadoID).empty();
                        $('#' + estadoID).append(statElementOK);
                        estado_acciones = true;
                        valueChange=null;
                        segundos = parseInt(currentSettings.tiempo * 60) - 2;
                        document.getElementById("temp_"+estadoID).innerHTML = '';
                    }else{
                        document.getElementById("temp_"+estadoID).innerHTML = segundos+' seg';
                    }
                    segundos = segundos - 1;
                },1000);

            } else {
                $('#' + botonId).off('change');
                $('#' + botonId).prop('checked', !$('#' + botonId).prop('checked'));
                $('#' + botonId).on('change', gestionarCambio);
            }
        }

        this.render = function(element) {
            rendered = true;

            titleElement.html(currentSettings.titulo);
            $(element)
            .append(titleElement)
            .append($('<div class="boton-widget-wrapper"></div>')
                .append($('<div class="cont" style="width:100px";></div>')//margin:0 auto
                    .append(gaugeElement)
                    .append(statElement)).append(tempElement));

            $('#' + botonId).change(gestionarCambio);
        };

        this.onSettingsChanged = function(newSettings) {
            currentSettings = newSettings;
            titleElement.html(newSettings.titulo);
        };

        this.onCalculatedValueChanged = function(settingName, newValue) {
            valueChange = newValue;
            if(estado_acciones){

                if(newValue == '1'){
                    $('#' + botonId).prop('checked',true);                
                }else{
                    $('#' + botonId).prop('checked', false);
                }
            }
        };

        this.onDispose = function() {};

        this.getHeight = function() {
            return 1;
        };

        this.onSettingsChanged(settings);
    };

    freeboard.loadWidgetPlugin({
        type_name: "boton",
        display_name: "Interruptor",
        settings: [{
            name: "titulo",
            display_name: "Titulo",
            type: "text"
        }, {
            name: "labelEncendido",
            display_name: "Texto en encendido",
            type: "text",
            description: "Máximo tres caracteres, si se ingresan más se mostrarán sólo los tres primeros."
        }, {
            name: "labelApagado",
            display_name: "Texto en apagado",
            type: "text",
            description: "Máximo tres caracteres, si se ingresan más se mostrarán sólo los tres primeros."
        }, {
            name: "url",
            display_name: "Url Servidor",
            type: "text",
            description: "Url del servidor nodejs donde se desea enviar la accion a gestionar. <br> ej: http://localhost:8686/iotmach_actuador"
        }, {
            name: "actuador",
            display_name: "ID Actuador",
            type: "text"
        }, {
            name: "estado_actuador",
            display_name: "senial estado actuador",
            type: "calculated"
        }, {
            name: "tiempo", display_name: "Tiempo máximo de confirmación (minutos)",
            type: "number",
            description: "El tiempo inicia al momento de enviar el comando, y al caducar, se verificará si ya ha llegado el mensaje de confirmación, de no ser así; se supondrá un error."
        }],
        newInstance: function(settings, newInstanceCallback) {
            newInstanceCallback(new botonWidget(settings));
        }
    });

}());
