/**
 * Created by Danilo on 13/05/2016.
 */
(function()
{
    var contador = 0;
    freeboard.addStyle('.alarma-widget-wrapper', "margin-bottom:10px !important;");

    var alarmaWidget = function (settings) {
        var self = this;
        var currentSettings = settings;

        contador++;
        var alarmaId = 'alarma-widget-'+contador;
        var titleElement = $('<h2 class="section-title"></h2>');
        var alarmaElement = $('<div id="'+alarmaId+'" class="alarma" style="margin-top:8px"></div>');
        var rendered = false;
        var tiempoConf, tiempoDespliegueConf;

        this.render = function (element) {
            rendered = true;

            titleElement.html(currentSettings.titulo);
            $(element).append(titleElement).append($('<div class="alarma-widget-wrapper"></div>').append($('<div class="cont" style="width:100px;margin:0 auto"></div>').append(alarmaElement)));

        }

        this.onSettingsChanged = function (newSettings) {
            currentSettings = newSettings;
            titleElement.html(newSettings.titulo);
        }

        this.onCalculatedValueChanged = function (settingName, newValue) {

            $("#"+alarmaId).addClass("alarma-anim");
        }

        this.onDispose = function () {
        }

        this.getHeight = function () {
            return 2;
        }

        this.onSettingsChanged(settings);
    };

    freeboard.loadWidgetPlugin({
        type_name: "alarma",
        display_name: "Alarma",
        settings: [
            {
                name: "titulo",
                display_name: "Titulo",
                type: "text"
            },
            {
                name: "topico",
                display_name: "Tópico de recepción",
                type: "calculated",
                description: "El tópico por el cual se reciben los mensajes de alarma."
            }
        ],
        newInstance: function (settings, newInstanceCallback) {
            newInstanceCallback(new alarmaWidget(settings));
        }
    });

}());