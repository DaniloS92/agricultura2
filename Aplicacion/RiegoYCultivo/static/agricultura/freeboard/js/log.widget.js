(function()
{
    var contador = 0;
    freeboard.addStyle('.log-widget-wrapper', "margin-top:15px !important;");
    freeboard.addStyle('.ul-log *', "display:inline;padding-bottom: 5px !important;");
    freeboard.addStyle('.ul-log', "padding-left: 0 !important;margin-left: 0 !important;margin-right: 3px !important");

this.render = function (element) {
    rendered = true;


}

this.onSettingsChanged = function (newSettings) {
    currentSettings = newSettings;
    titleElement.html(newSettings.titulo);
}

this.onCalculatedValueChanged = function (settingName, newValue) {
    console.log(newValue);
    var a = $('<div style="display:none" class="log-msj rojo-log"><div class="mensaje-log">'+newValue.DATA+'</div><time class="timeago" id="'+newValue.ID+'" title="'+new Date(newValue.DATE).toUTCString()+'" datetime="'+ new Date(newValue.DATE).toUTCString() +'"></time><input class="check-log" type="checkbox"></div>');
    a.slideDown();
}

this.onDispose = function () {
}

this.getHeight = function () {
    return 5;
}

this.onSettingsChanged(settings);
};

freeboard.loadWidgetPlugin({
    type_name: "log",
    display_name: "Notificación de alarmas",
    settings: [
    {
        name: "topico",
        display_name: "Tópico",
        type: "calculated",
        description: "El tópico por el cual se reciben los mensajes de alarma."
    }
    ],
    newInstance: function (settings, newInstanceCallback) {
        newInstanceCallback(new logWidget(settings));
    }
});

}());   