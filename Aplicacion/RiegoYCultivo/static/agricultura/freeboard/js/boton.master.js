/**
 * Created by Danilo on 26/08/2016.
 */
(function() {
    var contador = 0;
    freeboard.addStyle('.boton-widget-wrapper', "margin-bottom:5px !important;");

    var botonWidget = function(settings) {
        var self = this,
        socket;
        var currentSettings = settings;
        
        contador++;
        var botonId = 'switch-input-' + contador +'-master';
        var titleElement = $('<h2 class="section-title"></h2>');
        // var gaugeElement = $('<label class="switch switch-green"><input id="' + botonId + '" type="checkbox" class="switch-input"><span class="switch-label" data-on="' + currentSettings.labelEncendido.substr(0, 3).toUpperCase() + '" data-off="' + currentSettings.labelApagado.substr(0, 3).toUpperCase() + '"></span><span class="switch-handle"></span></label>');
        var gaugeElement = $('<a id="'+botonId+'" href="#" class="action-button shadow animate green" style="margin-left: 35px;">ON</a><a id="'+botonId+'-2" href="#" class="action-button shadow animate red">OFF</a>');
        
        var rendered = false;
        var tiempoConf, tiempoDespliegueConf;
        var estado_acciones = true;
        var valueChange;
        var segundos = parseInt(currentSettings.tiempo * 60);

        function gestionMaster(num) {

            var vecSwitchs = $('.botonGestion');

            for (var i = vecSwitchs.length - 1; i >= 0; i--) {

                if (num==='0' && $(vecSwitchs[i]).prop("checked") === true) {//si se esta enviando a que se apague y esta encendido el actuador
                    $(vecSwitchs[i]).prop('checked', false);
                    $(vecSwitchs[i]).trigger("change");
                }
                if (num==='1' && $(vecSwitchs[i]).prop("checked") === false) {//si se esta enviando a que se encienda y esta apagado el actuador
                    $(vecSwitchs[i]).prop('checked', true);    
                    $(vecSwitchs[i]).trigger("change");            
                }
            }
        }

        this.render = function(element) {
            rendered = true;

            titleElement.html(currentSettings.titulo);
            $(element)
            .append(titleElement)
            .append($('<div></div>').append(gaugeElement));

            $("body").on("click","a#"+botonId+"",function(){
                event.preventDefault();
                gestionMaster('1');
            });

            $("body").on("click","a#"+botonId+"-2",function(){
                event.preventDefault();
                gestionMaster('0');
            });
        };

        this.onSettingsChanged = function(newSettings) {
            currentSettings = newSettings;
            titleElement.html(newSettings.titulo);
        };

        this.onCalculatedValueChanged = function(settingName, newValue) {
            valueChange = newValue;
            if(estado_acciones){

                if(newValue == '1'){
                    $('#' + botonId).prop('checked',true);                
                }else{
                    $('#' + botonId).prop('checked', false);
                }
            }
        };

        this.onDispose = function() {};

        this.getHeight = function() {
            return 1;
        };

        this.onSettingsChanged(settings);
    };

    freeboard.loadWidgetPlugin({
        type_name: "boton_master",
        display_name: "Interruptor Global",
        settings: [{
            name: "titulo",
            display_name: "Titulo",
            type: "text"
        }, {
            name: "labelEncendido",
            display_name: "Texto en encendido",
            type: "text",
            description: "Máximo tres caracteres, si se ingresan más se mostrarán sólo los tres primeros."
        }, {
            name: "labelApagado",
            display_name: "Texto en apagado",
            type: "text",
            description: "Máximo tres caracteres, si se ingresan más se mostrarán sólo los tres primeros."
        }],
        newInstance: function(settings, newInstanceCallback) {
            newInstanceCallback(new botonWidget(settings));
        }
    });

}());
