(function()
{
	var fuentes = {};
	function onConnect() {
		console.log("Conectado");
	};

	function onConnectionLost(responseObject) {
		if (responseObject.errorCode !== 0)
			console.log("onConnectionLost:"+responseObject.errorMessage);
	};

	function onMessageArrived(message) {//se tiene q enviar con doble comilla
		console.log(message.payloadString);
		console.log(message.destinationName);
		for (var topico in fuentes) {
			if (message.destinationName == topico){
				fuentes[topico](JSON.parse(message.payloadString));
				break;
			}
		}
		//if (message.destinationName == topicoAlarmas){
		//	var a = setInterval(function(){
		//		if ($(".rojo-log").length > 0 && $(".amarillo-log").length > 0 && $(".verde-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-rojo-amarillo-verde");
		//        else if ($(".rojo-log").length > 0 && $(".amarillo-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-rojo-amarillo");
		//        else if ($(".rojo-log").length > 0 && $(".verde-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-rojo-verde");
		//        else if ($(".amarillo-log").length > 0 && $(".verde-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-amarillo-verde");
		//        else if ($(".rojo-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-rojo");
		//        else if ($(".amarillo-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-amarillo");
		//        else if ($(".verde-log").length > 0) $(".alarma").removeClass().addClass("alarma alarma-anim-verde");
		//        clearInterval(a);
		//	},100);
		//
		//}
	};

	function onMessageDelivered(message) {
		//console.log(message);
		mensajesPub[JSON.parse(message.payloadString).id].html('<i class="icon-check" style="margin-left:-1px"></i>');
	};

	client = new Paho.MQTT.Client('192.168.1.100',9001,"web_" + parseInt(Math.random() * 100));
	client.onConnectionLost = onConnectionLost;
	client.onMessageArrived = onMessageArrived;
	client.onMessageDelivered = onMessageDelivered;
	client.connect({onSuccess:onConnect, userName: 'admin', password: '123456',
		useSSL: false});


	var mqttDatasource = function (settings, updateCallback) {
		var self = this;
		var currentSettings = settings;


		this.updateNow = function () {
			
		}

		this.onDispose = function () {
			
		}

		this.onSettingsChanged = function (newSettings) {
			client.unsubscribe(currentSettings.topic);
			currentSettings = newSettings;
			client.subscribe(currentSettings.topic);
		}

		fuentes[currentSettings.topic] = updateCallback;
		client.subscribe(currentSettings.topic);
		console.log(currentSettings.topic);
	};
/*
	// TO-DO - lograr quitar external_scripts, ahora funciona de forma dudosa y no se lo puede quitar
	freeboard.loadDatasourcePlugin({
		"type_name"   : "paho_mqtt",
		"display_name": "MQTT",
		"description" : "Recibe datos desde un broker MQTT.",
		"external_scripts":["/static/agricultura/freeboard/js/mqttws31.js"],
		"settings"    : [
			{
				"name"        : "topic",
				"display_name": "Tópico",
				"type": "option",
                "options": gtopicos
            }
        ],
		newInstance   : function(settings, newInstanceCallback, updateCallback){
			newInstanceCallback(new mqttDatasource(settings, updateCallback));
		}
	});
*/
	freeboard.loadDatasourcePlugin({
		"type_name"   : "paho_mqtt",
		"display_name": "MQTT",
		"description" : "Recibe datos desde un broker MQTT.",
		"external_scripts":["/static/agricultura/freeboard/js/mqttws31.js"],
		"settings"    : [
			{
				"name"        : "topic",
				"display_name": "Tópico",
				"type": "option",
                "options": gtopicos,
            }
        ],
		newInstance   : function(settings, newInstanceCallback, updateCallback){
			newInstanceCallback(new mqttDatasource(settings, updateCallback));
		}
	});

}());
