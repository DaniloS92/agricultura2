/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO
        document.onload = inicio();
        function inicio(){
            select();

        }
        $('#modalVer').on('show.bs.modal', function(e) {
            var id = $(e.relatedTarget).data('par-id');
            var nombre = $(e.relatedTarget).data('par-nombre');
            var descripcion = $(e.relatedTarget).data('par-descripcion');
            var abreviatura = $(e.relatedTarget).data('par-abreviatura');
            var valorMinimo = $(e.relatedTarget).data('par-minimo');
            var valorMaximo = $(e.relatedTarget).data('par-maximo');
            var valor = $(e.relatedTarget).data('par-valor');
            $.ajax({
                data: {'id': id},
                url: '/agricultura/verTipos/',
                type: 'get',
                success: function (data) {
                    if (data.result=="OK"){
                        $("#contenedorLista").empty();
                        htmlLista='<ol>';
                        var c=1;
                        for(var i=0; i<data.resultado[0].conta;i++){
                            htmlLista+='<li>'+data.resultado[c].nombre+': </li>';
                            c++;
                        }
                        htmlLista+='</ol>';
                        $("#contenedorLista").append(htmlLista);
                    }else{
                    }
                }
            });
            $(e.currentTarget).find('small[id="nombre"]').html(nombre);
            $(e.currentTarget).find('small[id="descripcion"]').html(descripcion);
            $(e.currentTarget).find('small[id="abreviatura"]').html(abreviatura);
            $(e.currentTarget).find('small[id="minimo_maximo"]').html(valorMinimo+"/"+valorMaximo);
            $(e.currentTarget).find('small[id="valor"]').html(valor);

   });


//OPERACIONES
        function baja_parametro(elemento, id){
            var estado=true;
            if(elemento.checked == false){
                estado = false;
            }
            $.ajax({
                data: {'id': id, 'estado': estado},
                url: '/agricultura/bajaParametros/',
                type: 'get',
                success: function (data) {
                    if (data.result=="OK"){
                    }else{
                    }
                }
            });
        }
        function editar_parametro(id){
        $.ajax({
            data: {'id': id},
            url: '/agricultura/ver_datos_parametros/',
            type: 'get',
            success: function (data) {
                if(data.length != 0){
                    $("input[type=text]").focusin();
                    $("#contenedor_btn").empty();
                    $("#contenedor_btn").append('<button onclick="enviar_datos('+data[0].id+')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button>')
                    $("#txt_nombre").val(data[0].nombre)
                    $("#id").val(data[0].id)
                    $("#txt_descripcion").val(data[0].descripcion)
                    $("#txt_abreviatura").val(data[0].abreviatura)
                    $("#txt_minimo").val(data[0].minimo)
                    $("#txt_maximo").val(data[0].maximo)
                    $("#txt_valor").val(data[0].valor)
                    $('#select_categoria_especie').innerHTML=""
                    $("#contenedorSelect").empty();
                    htmlSelect='<select data-placeholder="Seleccione el tipo..." multiple class="chosen-select" style="width:250px;" tabindex="4" id="select_parametro" name="select_parametro">';
                    var c=3;
                    for(var i=0; i<data[1].contEscogidos;i++){
                        htmlSelect+='<option selected value="'+data[c].tipo_id+'">'+data[c].tipo_nombre+'</option>';
                        c++;
                    }
                    for(var i=0; i<data[2].contTodos;i++){
                        htmlSelect+='<option value="'+data[c].tipo_id+'">'+data[c].tipo_nombre+'</option>';
                        c++;
                    }
                    htmlSelect+='</select>';
                    $("#contenedorSelect").append(htmlSelect)
                     select();
                    $("#div_estado").empty();
                        if(data[0].estado==true){
                    }else{
                     }
                }else{
                    alert('Parametro no encontrado')
                }
            }
            });
        }
        function enviar_datos(id){
        //var formulario=new FormData($('#form_especie')[0]);
        var datos = new FormData($('#form_parametros')[0]);
        $.ajax({
            data: datos,
            url: '/agricultura/modificar_datos_parametros/',
            type: 'POST',
            contentType:false,
            'dataType': 'json',
            processData:false,
            success: function (data) {
                if(data.result=='OK'){
                    location.href="/agricultura/parametros_cultivo"
                }else{
                    alert('Error al modificar')
                }
            }
            });
    }

function select(){
               var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
}
