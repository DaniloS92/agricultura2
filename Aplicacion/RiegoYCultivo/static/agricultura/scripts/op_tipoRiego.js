/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO
        document.onload = inicio()

        function inicio(){
            select();
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            //configuracion de la tabla
            $('.footable').footable();
        }


        $('#modalVer').on('show.bs.modal', function(e) {
            var id = $(e.relatedTarget).data('rie-id');
            var nombre = $(e.relatedTarget).data('rie-nombre');
            var descripcion = $(e.relatedTarget).data('rie-descripcion');
            var formulas = $(e.relatedTarget).data('rie-formulas');
            if (formulas==""){
                formulas='No se encuentra cargada'
            }

            $(e.currentTarget).find('small[id="nombre"]').html(nombre);
            $(e.currentTarget).find('small[id="descripcion"]').html(descripcion);
            $(e.currentTarget).find('small[id="formulas"]').html(formulas);
   });


        function editar_riego(id){
        $.ajax({
            data: {'id': id},
            url: '/agricultura/ver_datos_riego/',
            type: 'get',
            success: function (data) {
                if(data.length != 0){
                    $("input[type=text]").focusin();
                    $("#contenedor_btn").empty();
                    $("#contenedor_btn").append('<button onclick="enviar_datos('+data[0].id+')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button>')
                    $("#contenedor_btn").append('<div class="col-md-4"><button onclick="regresar()" class="btn btn-danger" id="btn_cancelar" type="button" >Cancelar</button></div>');
                    $("#tipoRiego").val(data[0].tipoRiego)
                    $("#descripcionRiego").val(data[0].descripcion)
                    $("#id").val(data[0].id)
                }else{
                    alert('Parametro no encontrado')
                }
            }
            });
        }

        function enviar_datos(id){
        //var formulario=new FormData($('#form_especie')[0]);
        var nombre = $("#tipoRiego").val();
        if (nombre != "") {
            var datos = new FormData($('#form_parametros')[0]);
            $.ajax({
                data: datos,
                url: '/agricultura/modificar_datos_riego/',
                type: 'POST',
                contentType:false,
                'dataType': 'json',
                processData:false,
                success: function (data) {
                    if(data.result=='OK'){
                        location.href="/agricultura/tipo_riego"
                    }else{
                        alert('Error al modificar')
                    }
                }
            });        
        }else{
            swal("Error en los datos", "El campo Tipo de Riego no puede estar vacio", "error");
        }

        }

        function select(){
            var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        }

function regresar(){
   location.href="/agricultura/tipo_riego/";
}