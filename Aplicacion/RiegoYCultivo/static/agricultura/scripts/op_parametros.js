/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO
        document.onload = inicio();
        function inicio(){

         var elem = document.querySelector('.js-switch');
         var switchery = new Switchery(elem, { color: '#1AB394' });
            //configuracion de la tabla
            $('.footable').footable();
            select();
        }
        $('#modalVer').on('show.bs.modal', function(e) {
            var id = $(e.relatedTarget).data('par-id');
            var nombre = $(e.relatedTarget).data('par-nombre');
            var descripcion = $(e.relatedTarget).data('par-descripcion');
            var abreviatura = $(e.relatedTarget).data('par-abreviatura');
            var valorMinimo = $(e.relatedTarget).data('par-minimo');
            var valorMaximo = $(e.relatedTarget).data('par-maximo');
            var valor = $(e.relatedTarget).data('par-valor');
            $.ajax({
                data: {'id': id},
                url: '/agricultura/verTipos/',
                type: 'get',
                success: function (data) {
                    if (data.result=="OK"){
                        $("#contenedorLista").empty();
                        htmlLista='<ol>';
                        var c=1;
                        for(var i=0; i<data.resultado[0].conta;i++){
                            htmlLista+='<li>'+data.resultado[c].nombre+'</li>';
                            c++;
                        }
                        htmlLista+='</ol>';
                        $("#contenedorLista").append(htmlLista);
                    }else{
                    }
                }
            });
            $(e.currentTarget).find('small[id="nombre"]').html(nombre);
            $(e.currentTarget).find('small[id="descripcion"]').html(descripcion);
            $(e.currentTarget).find('small[id="abreviatura"]').html(abreviatura);
            $(e.currentTarget).find('small[id="minimo_maximo"]').html(valorMinimo+"/"+valorMaximo);
            $(e.currentTarget).find('small[id="valor"]').html(valor);
   });


//OPERACIONES
        function baja_parametro(elemento, id){
            var estado=true;
            if(elemento.checked == false){
                estado = false;
            }
            $.ajax({
                data: {'id': id, 'estado': estado},
                url: '/agricultura/bajaParametros/',
                type: 'get',
                success: function (data) {
                    if (data.result=="OK"){
                                                toastr.options={"progressBar": true}
                        toastr.success('Se cambio de estado','Estado')
                    }else{
                    }
                }
            });
        }
        function editar_parametro(id){
        $.ajax({
            data: {'id': id},
            url: '/agricultura/ver_datos_parametros/',
            type: 'get',
            success: function (data) {
                if(data.length != 0){
                    $("input[type=text]").focusin();
                    $("#contenedor_btn").empty();
                    $("#contenedor_btn").append('<button onclick="enviar_datos('+data[0].id+')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button>')
                    $("#contenedor_btn").append('<div class="col-md-4"><button onclick="regresar()" class="btn btn-danger" id="btn_cancelar" type="button" >Cancelar</button></div>');
                    $("#txt_nombre").val(data[0].nombre);
                    $("#id").val(data[0].id);
                    $("#txt_descripcion").val(data[0].descripcion);
                    $("#txt_abreviatura").val(data[0].abreviatura);
                    $("#txt_minimo").val(data[0].minimo);
                    $("#txt_maximo").val(data[0].maximo);
                    $("#txt_valor").val(data[0].valor);
                    $("#select_medida").val(data[0].medida);
                    $("#select_medida").trigger("chosen:updated");
                    $("#div_estado").empty();
                        if(data[0].estado==true){
                    }else{
                     }
                }else{
                    alert('Parametro no encontrado');
                }
            }
            });
        }

        function enviar_datos(id){
        
            var nombre = $("#txt_nombre").val();
            var desc = $("#txt_descripcion").val();
            var abre = $("#txt_abreviatura").val();
            var valMin = $("#txt_minimo").val();
            var valMax = $("#txt_maximo").val();
            var valor = $("#txt_valor").val();

            if(nombre != "" && desc != "" && abre != "" && valMin != "" && valMax != "" && valor != ""){
                var datos = new FormData($('#form_parametros')[0]);
                $.ajax({
                data: datos,
                url: '/agricultura/modificar_datos_parametros/',
                type: 'POST',
                contentType:false,
                'dataType': 'json',
                processData:false,
                success: function (data) {
                    if(data.result=='OK'){
                        location.href="/agricultura/parametros_cultivo";
                    }else{
                        alert('Error al modificar');
                    }
                }
                });
            }else{
                swal("Error en los datos", "No pueden existir campos vacios en el formulario", "error");
            }
    }
// function agregar(){
//     var datos = new FormData($('#form_tipo_parametro')[0]);
//
//     var nombre = $("#txtNombre").val();
//     var des = $("#txtDescripcion").val();
//     var abre = $("#txtAbreviatura").val();
//
//     if(nombre != "" && des != "" && abre != ""){
//
//         $.ajax({
//             data: datos,
//             url: '/agricultura/ingresarNuevoTipoParametro/',
//             type: 'POST',
//             contentType:false,
//             'dataType': 'json',
//             processData:false,
//             success: function (data) {
//                 if(data.result=='OK'){
//                     var valores = [];
//                     $("#txtNombre").val('');
//                     $("#txtDescripcion").val('');
//                     $("#txtAbreviatura").val('');
//                     valores = $("#select_parametros").val();
//                     $("#select_parametros").append('<option value="'+data.valor+'">'+data.nombre+'</option>');
//                     if (valores != null){
//                         valores.push(data.valor+"");
//                         $("#select_parametros").val(valores);
//                     }else{
//                         $("#select_parametros").val(data.valor+"");
//                     }
//
//                     $("#select_parametros").trigger("chosen:updated");
//                     $('#modalTipo').modal('hide');
//                 }else{
//                     alert('Error al modificar');
//                 }
//             }
//         });
//
//     }else {
//         swal("Error en los datos", "No pueden existir campos vacios en el formulario", "error");
//     }
// }
function select(){
           var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            };
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
}

function regresar(){
   location.href="/agricultura/parametros_cultivo/";
}