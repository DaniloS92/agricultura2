//Validar contraseñas
function validarContrasena(c1, c2){
    if(c1 == c2)
        return true;
    else
        return false;
}

//Funcion que permite solo Numeros
function validaSoloNumeros() {
    var resultado = true;
 if ((event.keyCode < 44) || (event.keyCode > 57 )){
    resultado = false;
 }else if ((event.keyCode == 46) || (event.keyCode == 44)) {
    resultado = true;
 }
 event.returnValue = resultado;
}

//Valida solo texto o letras
function validaSoloTexto() {
 if ((event.keyCode != 32) && (event.keyCode < 65) || (event.keyCode > 90) && (event.keyCode < 97) || (event.keyCode > 122))
    event.returnValue = false;
}

//validacion tamanio de pass
function comprobarTamanio(txt){
    console.log(txt.value.length)
    if(txt.value.length<6){
        txt.focus();
         var notification = alertify.notify('El tamaño de la contraseña debe ser mayor a 6 caracteres','error',
                        3, function(){  console.log('dismissed'); });
    }
}

function comprovarMinMax(min,max){
    respuesta = true;
    if(min > max){
        respuesta = false;
    }
    return respuesta;
}

function validarEntre (min,max,valor) {
    var esValido = true;
    if (valor < min || valor > max) {
        esValido = false;
    };
    return esValido;
}


function validarFormParametros(form){
    var respuesta = true;
    if(comprovarMinMax(form.txt_minimo.value,form.txt_maximo.value)==false){
        swal("Error en los datos", "El valor min no puede ser mayor al valor max", "error");
        respuesta = false;
    }else if (validarEntre(form.txt_minimo.value,form.txt_maximo.value,form.txt_valor.value)==false) {
        swal("Error en los datos", "El valor tiene que estar en el rango del min y max", "error");
        respuesta = false;
    };
    return respuesta;
}

//Validaciones varias
function informacionCorrecta(){
    var contador = 0;


    var contrasena1 = $("#txtPassword").val();
    var contrasena2 = $("#txtPasswordV").val();

    if(contrasena1 == null || contrasena1.length == 0 || contrasena1.length < 6 || /^\s+$/.test(contrasena1)) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('La contraseña deber ser mayor a 6 caracteres' ,10);
        contador++;
    }


    console.log(contador)
    if(contador == 0)
        return true;
    else
        return false;
}