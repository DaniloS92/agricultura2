document.onload = inicio()
    function inicio(){
    	    $('#frmAre').on("submit",function(){
        event.preventDefault();
        url = "/empresa/areas/sav/";
        $.ajax({
            type:"POST",
            url: url,
            dataType: 'json',
            data:$(this).serialize(),
            success: function(response){
                $('#are_des').val("");
                $('#are_etq').val("");
                $('#are_dir').val("");
                $('#modalArea').modal('hide');
                toastr.options={"progressBar":true};
                toastr.success(response.mensaje,'Area');
                // $("#are_id").load(location.href+" #are_id>*","");
                // $('#are_id').ajax.reload();    
                $.get("/empresa/areas/get/",$.getDataArea);
                $('#are_nom').val("");
                $('#modalSubArea').modal('show');

            },

            error: function(){
                toastr.options={"progressBar":true};
                toastr.error("Error",'Area');
            }
        });
    });

    $('#modalArea').bind('shown.bs.modal', function(e){
        are_nom.focus();
    });

    $('#modalArea').bind('hidden.bs.modal', function(e){
        $('#title').html("Crear Area");
        $('#are_nom').val("");
        $('#are_des').val("");
        $('#are_etq').val("");
        $('#are_dir').val("");
        $('#are_est').prop("checked",false);
        $('#accion').val("0");
    });
    // SUBAREAS

    var data="";

      $.getDataArea = function(response)
    {
        var datos = "";
        data = response.data;
        $.each(response.data, function(i,value){
            datos+= "<option value="+value.ar_id+">"+value.ar_nombre+"</option>";
        });

        $('#are_id').html(datos);       
    };
    $.get("/empresa/areas/get/",$.getDataArea); // get without token_crsf
    }


   
    $.getDataSuelo = function(response)
    {
        var datos = "";
        data = response.data;
        $.each(response.data, function(i,value){
            datos+= "<option value="+value.tsue_id+">"+value.tsue_nombre+"</option>";
        });
        $('#select_suelo').html(datos);       
        $("#select_suelo").trigger("chosen:updated");
    };    

    $('#modalSuelo').bind('shown.bs.modal', function(e){
    $.get("/agricultura/tiposSuelo/",$.getDataSuelo); // get without token_crsf
        txt_nombre.focus();
    });