/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO

document.onload = inicio()

function inicio() {
    var $image = $(".image-crop > img")
    $($image).cropper({
        aspectRatio: 1.618,
        preview: ".img-preview",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });

    var $inputImage = $("#inputImage");
    if (window.FileReader) {
        $inputImage.change(function() {
            var fileReader = new FileReader(),
                files = this.files,
                file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function() {
                    $inputImage.val("");
                    $image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                alert('Deber ser imagen')
            }
        });
    } else {
        $inputImage.addClass("hide");
    }

    $("#setDrag").click(function() {
        $image.cropper("setDragMode", "crop");
    });

    select();

    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, { color: '#1AB394' });
    //configuracion de la tabla
    $('.footable').footable();

}

$('#modalVer').on('show.bs.modal', function(e) {
    var imagen = $(e.relatedTarget).data('esp-imagen');
    document.getElementById('imagen').src = imagen;
});
$('#modalParametros').on('show.bs.modal', function(e) {
    $('#form-Modal').empty()
    var banderaU = 0
    var banderaD = 0
    var c = 1;
    var id = $(e.relatedTarget).data('esp-id');
    $.ajax({
        data: { 'id': id },
        url: '/agricultura/tipos_parametros_especie/',
        type: 'get',
        success: function(data) {
            htmlSelect = ''
            for (var j = 0; j < data[0].contParametrosEspecie; j++) {
                htmlSelect += '<div class="col-lg-6 form-group" > <label control-label">' + data[c].nombre + '</label><br/>'
                banderaD = c + 1
                c = c + 2
                for (var k = 0; k < data[banderaD].contTipos; k++) {
                    htmlSelect += '<div class="col-lg-9">' + data[c].nombreTipo + '<input type="text" ' +
                        'id="tipo-' + data[c].idTipo + '" name="tipo-' + data[c].idTipo + '" class="form-control" value="' + data[c].valor + '"  onkeypress="validaSoloNumeros()"></div>'; //
                    //htmlSelect += 'nombreTipo : ' + data[c].nombreTipo + ' ';
                    c = c + 1;
                }
                htmlSelect += '</div>';
            }
            var nombre = $(e.relatedTarget).data('esp-nombre');
            var id = $(e.relatedTarget).data('esp-id');
            $(e.currentTarget).find('label[id="nombre"]').html(nombre);
            $(e.currentTarget).find('input[id="id"]').val(id);
            // htmlSelect+=('<br/><button class="btn btn-primary" type="submit">Guardar</button>')
            $('#form-Modal').append(htmlSelect)

        }
    });
});

//OPERACIONES
function baja_especie(elemento, id) {
    var estado = true;
    if (elemento.checked == false) {
        estado = false;
    }
    $.ajax({
        data: { 'id': id, 'estado': estado },
        url: '/agricultura/bajaEspecie/',
        type: 'get',
        success: function(data) {
            if (data.result == "OK") {
                toastr.options = { "progressBar": true }
                toastr.success('Se cambio de estado', 'Estado')
            } else {}
        }
    });
}

function editar_especie(id) {
    $.ajax({
        data: { 'id': id },
        url: '/agricultura/ver_datos_especie/',
        type: 'get',
        success: function(data) {
            if (data.length != 0) {
                $("input[type=text]").focusin();
                $("#contenedor_btn").empty();
                $("#contenedor_btn").append('<div class="col-md-4"><button onclick="enviar_datos(' + data[0].id + ')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button></div>')
                $("#contenedor_btn").append('<div class="col-md-4"><button onclick="regresar()" class="btn btn-danger" id="btn_cancelar" type="button" >Cancelar</button></div>');
                $("#txt_especie").val(data[0].nombre)
                $("#id").val(data[0].id)
                $("#txt_altura").val(data[0].altura)
                $("#contenedor_img").empty();
                $("#contenedor_img").append('<img src=' + data[0].imagen + ' width="100" height="100">');
                $('#select_categoria_especie').innerHTML = ""
                $("#contenedorSelect").empty();
                htmlSelect = '<select data-placeholder="Escoga Categoria..." class="chosen-select" style="width:250px;" tabindex="2" id="select_categoria_especie" name="select_categoria_especie">';
                $("#contenedorSelect").append('');
                htmlSelect += '<option value="' + data[0].categoria_id + '">' + data[0].categoria_nombre + '</option>';
                var c = 2;
                for (var i = 0; i < data[1].contCategorias; i++) {
                    htmlSelect += '<option value="' + data[c].categorias_id + '">' + data[c].categorias_nombre + '</option>';
                    c++;
                }
                htmlSelect += '</select>';
                $("#contenedorSelect").append(htmlSelect)
                select();
                $("#div_estado").empty();
                if (data[0].estado == true) {} else {}
            } else {
                alert('Categoria no encontrado')
            }
        }
    });
}

function enviar_datos(id) {
    //var formulario=new FormData($('#form_especie')[0]);
    var datos = new FormData($('#form_especie')[0]);
    var especie = $("#txt_especie").val();
    var altura = $("#txt_altura").val();
    if (especie != "" && altura != "") {
        $.ajax({
            data: datos,
            url: '/agricultura/modificar_datos_especie/',
            type: 'POST',
            contentType: false,
            'dataType': 'json',
            processData: false,
            success: function(data) {
                if (data.result == 'OK') {
                    toastr.options = { "progressBar": true, "showDuration": "400" }
                    toastr.success('Se modifico', 'Estado')
                    location.href = "/agricultura/especie"
                } else {
                    swal("Error en los datos", "Verifique los datos ingesados e intente nuevamente", "error");
                }
            }
        });
    } else {
        swal("Error en los datos", "Verifique los datos ingesados e intente nuevamente", "error");
    }
}

function select() {
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "95%" }
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
}
    