/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO
        document.onload = inicio()
        function inicio(){

            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            //configuracion de la tabla
            $('.footable').footable();
            select();
        }



//OPERACIONES
        function editar_parametro_especie(id){
        $.ajax({
            data: {'id': id},
            url: '/agricultura/ver_datos_parametros_especie/',
            type: 'get',
            success: function (data) {
                if(data.length != 0){
                    $("input[type=text]").focusin();
                    $("#contenedor_btn").empty();
                    $("#contenedor_btn").append('<button onclick="enviar_datos('+data[0].id+')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button>')
                    $("#txt_nombre").val(data[0].nombre)
                    $("#id").val(data[0].id)
                    $("#txt_minimo").val(data[0].minimo)
                    $("#txt_maximo").val(data[0].maximo)
                    $("#txt_valor").val(data[0].valor)
                    $('#select_categoria_especie').innerHTML=""
                    $("#contenedorSelect").empty();
                    htmlSelect='<select data-placeholder="Seleccione el tipo..." class="chosen-select" style="width:250px;" tabindex="4" id="select_parametros" name="select_parametros">';
                    var c=3;
                    for(var i=0; i<data[1].contEscogidos;i++){
                        htmlSelect+='<option selected value="'+data[c].tipo_id+'">'+data[c].tipo_nombre+'</option>';
                        c++;
                    }
                    for(var i=0; i<data[2].contTodos;i++){
                        htmlSelect+='<option value="'+data[c].tipo_id+'">'+data[c].tipo_nombre+'</option>';
                        c++;
                    }
                    htmlSelect+='</select>';
                    $("#contenedorSelect").append(htmlSelect)
                    select();
                }else{
                    alert('Parametro no encontrado')
                }
            }
            });
        }
        function enviar_datos(id){
        //var formulario=new FormData($('#form_especie')[0]);
        var datos = new FormData($('#form_parametros')[0]);
        $.ajax({
            data: datos,
            url: '/agricultura/modificar_datos_parametros_especie/',
            type: 'POST',
            contentType:false,
            'dataType': 'json',
            processData:false,
            success: function (data) {
                if(data.result=='OK'){
                    location.href="/agricultura/parametros_especie"
                }else{
                    alert('Error al modificar')
                }
            }
            });
    }
function select(){
               var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
}