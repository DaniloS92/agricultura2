/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO

document.onload = inicio()

function inicio() {
    select();

    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, { color: '#1AB394' });
    //configuracion de la tabla
    $('.footable').footable();

}

//OPERACIONES
function baja_zona(elemento, id) {
    var estado = true;
    if (elemento.checked == false) {
        estado = false;
    }
    document.getElementById('edit-'+id).disabled=!estado;
    document.getElementById('config-'+id).disabled=!estado;
    document.getElementById('dash-'+id).disabled=!estado;
    $.ajax({
        data: { 'id': id, 'estado': estado },
        url: '/agricultura/bajaZona/',
        type: 'get',
        success: function(data) {
            var data = JSON.parse(data);
            if (data.result == "OK") {
                toastr.options = { "progressBar": true, "showDuration": "200" };
                toastr.success('Se cambio de estado', 'Estado');
            } else {
                console.error('Hubo un problema al cambiar el estado');
            }
        }
    });
}

function editar_zona(id) {
    $.ajax({
        data: { 'id': id },
        url: '/agricultura/ver_datos_zona/',
        type: 'get',
        success: function(data) {
            if (data.length != 0) {
                var data = JSON.parse(data);
                $("input[type=text]").focusin();
                $("#contenedor_btn").empty();
                $("#contenedor_btn").append('<div class="col-md-4"><button onclick="enviar_datos(' + data.id + ')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button></div>')
                $("#contenedor_btn").append('<div class="col-md-4"><button onclick="regresar()" class="btn btn-danger" id="btn_cancelar" type="button" >Cancelar</button></div>');
                $("#txt_zona").val(data.zsa_nombre);
                $("#id").val(data.id);
                $("#txt_descripcion").val(data.zsa_descripcion);
                $("#txt_localizacion").val(data.zsa_localizacion);
                $("#contenedor_img").empty();
                $("#contenedor_img").append('<img src=' + data.zsa_imagen + ' width="100" height="100">');
                $('#select_sub_area').innerHTML = ""
                $("#contenedorSelect").empty();
                htmlSelect = '<select data-placeholder="Escoga Categoria..." class="chosen-select" style="width:250px;" tabindex="2" id="select_sub_area" name="select_sub_area">';
                $("#contenedorSelect").append('');
                htmlSelect += '<option value="' + data.sar_id + '">' + data.sar_nombre + '</option>';
                for (var i = 0; i < data.sub_areas.length; i++) {
                    console.log(data.sub_areas[i].sar_id);
                    htmlSelect += '<option value="' + data.sub_areas[i].sar_id + '">' + data.sub_areas[i].sar_nombre + '</option>';
                }
                htmlSelect += '</select>';
                $("#contenedorSelect").append(htmlSelect);
                select();
                $("#div_estado").empty();
                if (data.zas_estado == true) {} else {}
            } else {
                alert('Categoria no encontrado');
            }
        }
    });
}

function enviar_datos(id) {
    var hhost = document.location.pathname;
    //var formulario=new FormData($('#form_especie')[0]);
    var datos = new FormData($('#form_zonas')[0]);
    var zona = $("#txt_zona").val();
    var localizacion = $("#txt_localizacion").val();
    if (zona != "" && localizacion != "") {
        $.ajax({
            data: datos,
            url: '/agricultura/modificar_datos_zonas/',
            type: 'POST',
            contentType: false,
            'dataType': 'json',
            processData: false,
            success: function(data) {
                if (data.result == 'OK') {
                    toastr.options = { "progressBar": true, "showDuration": "400" }
                    toastr.success('Se modifico', 'Estado')
                    location.href = hhost
                } else {
                    swal("Error en los datos", "Verifique los datos ingesados e intente nuevamente", "error");
                }
            }
        });
    } else {
        swal("Error en los datos", "Verifique los datos ingesados e intente nuevamente", "error");
    }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$('#modalConfiguracion').on('show.bs.modal', function(e) {

    $('#selectSensores').empty();
    $("#selectActuadores").empty();

    var id = $(e.relatedTarget).data('id');
    var nombre = $(e.relatedTarget).data('nombre-zona');
    var vecSen = [];
    var vecAct = [];

    $.ajax({
        data: { 'id': id },
        url: '/agricultura/consulta_sen_act/',
        type: 'get',
        success: function(data) {

            data = JSON.parse(data);

            for (var i = 0; i < data.sensoresActuadores.length; i++) {
                if (data.sensoresActuadores[i].categoria == 's') {
                    if (data.sensoresActuadores[i].estado == true){
                        $("#selectSensores").append('<option value = "' + data.sensoresActuadores[i].id + '">' + data.sensoresActuadores[i].dispositivo + ' - ' +data.sensoresActuadores[i].nombre + ' - ' + data.sensoresActuadores[i].interfaz + '<option>');
                    }else{
                        $("#selectSensores").append('<option disabled value = "' + data.sensoresActuadores[i].id + '">' + data.sensoresActuadores[i].dispositivo + ' - ' +data.sensoresActuadores[i].nombre + ' - ' + data.sensoresActuadores[i].interfaz +' - (Ocupado)-'+data.sensoresActuadores[i].zona +'<option>');
                    }
                } else {
                    if (data.sensoresActuadores[i].estado == true){
                        $("#selectActuadores").append('<option value = "' + data.sensoresActuadores[i].id + '">' + data.sensoresActuadores[i].dispositivo + ' - ' +data.sensoresActuadores[i].nombre + ' - ' + data.sensoresActuadores[i].interfaz + '<option>');
                    }else{
                        $("#selectActuadores").append('<option disabled value = "' + data.sensoresActuadores[i].id + '">' + data.sensoresActuadores[i].dispositivo + ' - ' +data.sensoresActuadores[i].nombre + ' - ' + data.sensoresActuadores[i].interfaz +' - (Ocupado)-'+data.sensoresActuadores[i].zona + '<option>');
                    }
                }
            }

            for (var i = 0; i < data.sensoresActuadoresZona.length; i++) {
                if (data.sensoresActuadoresZona[i].categoria == 's') {
                    vecSen.push(data.sensoresActuadoresZona[i].id);
                } else {
                    vecAct.push(data.sensoresActuadoresZona[i].id);
                }
            }

            $(e.currentTarget).find('label[id="nombre"]').html(nombre);

            $("#selectSensores").val(vecSen);
            $("#selectActuadores").val(vecAct);

            $("#selectSensores").trigger("chosen:updated");
            $("#selectActuadores").trigger("chosen:updated");

            if($("#selectSensores").val().length > 0 || $("#selectActuadores").val().length > 0 ){
                $("#mensaje_alerta").addClass('active');
            }
        }
    });

    $("#contenedorBoton").empty();
    $("#contenedorBoton").append('<button onclick="guardarConfiguracion(' + id + ')" class="btn btn-primary" type="button" data-dismiss="modal">Guardar Configuración</button>');
});

$('#modalConfiguracion').on('hidden.bs.modal', function(e) {
    $("#mensaje_alerta").removeClass('active');
    $('#selectSensores').empty();
    $("#selectActuadores").empty();
});

function guardarConfiguracion(id) {

    var csrftoken = getCookie('csrftoken');
    var arraySensores = [];
    var arrayActuadores = [];

    if ($("#selectSensores").val() != null) {
        arraySensores = $("#selectSensores").val();
    } 
    if ($("#selectActuadores").val() != null) {
        arrayActuadores = $("#selectActuadores").val();
    }

    $.ajax({
        type: 'POST',
        url: '/agricultura/actualizar_sen_act_zona/',
        data: {
            'id': id,
            'sen[]': arraySensores,
            'act[]': arrayActuadores,
            csrfmiddlewaretoken: csrftoken
        },
        success: function(data) {
            data = JSON.parse(data);
            toastr.options={  "closeButton": true,
                                  "debug": false,
                                  "progressBar": true,
                                  "preventDuplicates": false,
                                  "positionClass": "toast-top-right",
                                  "onclick": null,
                                  "showDuration": "400",
                                  "hideDuration": "1000",
                                  "timeOut": "7000",
                                  "extendedTimeOut": "1000",
                                  "showEasing": "swing",
                                  "hideEasing": "linear",
                                  "showMethod": "fadeIn",
                                  "hideMethod": "fadeOut"};
            if(data.result == 'OK'){
                toastr.success('Configuración guardada con éxito.');
                document.getElementById("lbl-"+id).style.display = 'none';
                document.getElementById(id).disabled = false;
                var check = $("#"+id);
                check.prop('checked', true);
                baja_zona(check, id);  
                $("#config-"+id).removeClass('btn-danger');
                $("#config-"+id).addClass('btn-white');
            }else{
                toastr.error('Lo sentimos hay problemas al guardar la configuración.');
            }
        }
    });
}



function ver_zona(id) {
    location.href = '/agricultura/monitoreo/' + id;
}

function select() {
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "95%" }
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
}

function regresar() {
    var hhost = document.location.pathname;
    location.href = hhost;
}

function buscar(valor) {
    $("#filter").val($(valor).val());
    var e = $.Event('keyup');
    e.keyCode = 13; // enter
    $('#filter').trigger(e);
}
