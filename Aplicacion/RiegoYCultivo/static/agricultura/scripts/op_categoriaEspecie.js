/**
 * Created by Dyuyay on 20/12/2015.
 */

//DISEÑO
document.onload = inicio()
    function inicio(){
        var $image = $(".image-crop > img")
        $($image).cropper({
            aspectRatio: 1.618,
            preview: ".img-preview",
            done: function(data) {
                  // Output the result data for cropping image.
                }
            });

            var $inputImage = $("#inputImage");
            if (window.FileReader) {
                $inputImage.change(function() {
                    var fileReader = new FileReader(),
                            files = this.files,
                            file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        showMessage("Please choose an image file.");
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }

            $("#setDrag").click(function() {
                $image.cropper("setDragMode", "crop");
            });

            var config = {
                '.chosen-select'           : {},
                '.chosen-select-deselect'  : {allow_single_deselect:true},
                '.chosen-select-no-single' : {disable_search_threshold:10},
                '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                '.chosen-select-width'     : {width:"95%"}
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }

            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            //configuracion de la tabla
            var oTable = $('#editable').DataTable();
        }
        $('#modalVer').on('show.bs.modal', function(e) {
        var nombre = $(e.relatedTarget).data('cat-nombre');
        var observacion = $(e.relatedTarget).data('cat-observacion');
        $(e.currentTarget).find('small[id="nombre"]').html(nombre);
        $(e.currentTarget).find('small[id="observacion"]').html(observacion);
   });


//OPERACIONES
        function baja_categoria(elemento, id){
            var estado=true;
            if(elemento.checked == false){
                estado = false;
            }
            $.ajax({
                data: {'id': id, 'estado': estado},
                url: '/agricultura/bajaCategoria/',
                type: 'get',
                success: function (data) {
                    if (data.result=="OK"){
                        toastr.options={"progressBar": true}
                        toastr.success('Se cambio de estado','Estado')
                    }else{
                    }
                }
            });
        }
        function editar_categoria(id){
        $.ajax({
            data: {'id': id},
            url: '/agricultura/ver_datos_categoria/',
            type: 'get',
            success: function (data) {
                if(data.length != 0){
                    $("input[type=text]").focusin();
                    $("#contenedor_btn").empty();
                    $("#contenedor_btn").append('<button onclick="enviar_datos('+data[0].id+')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button>')
                    $("#contenedor_btn").append('<div class="col-md-4"><button onclick="regresar()" class="btn btn-danger" id="btn_cancelar" type="button" >Cancelar</button></div>');
                    $("#txt_cat_nombre").val(data[0].nombre)
                    $("#txt_cat_observacion").val(data[0].observacion)
                    $("#div_estado").empty();
                    if(data[0].estado==true){
                    }else{
                     }
                }else{
                    alert('Categoria no encontrado')
                }
            }
            });
        }
        function enviar_datos(id){
            var nombre = $("#txt_cat_nombre").val();
            if (nombre != "") {
                $.ajax({
                data: {'id': id, 'txt_cat_nombre':$("#txt_cat_nombre").val(),
                    'txt_cat_observacion':$("#txt_cat_observacion").val(), 'estado':$("#check_cat_estado").val()},
                url: '/agricultura/modificar_datos_categoria/',
                type: 'get',
                success: function (data) {
                    if(data.result=='OK'){
                        location.href="/agricultura/categoria_especie"
                        //$("#tabla_categoria").load(location.href+" #tabla_categoria>*","");
                        //$("#form_categoria").load(location.href+" #form_categoria>*","");
                    }else{
                        alert('Error al modificar')
                    }
                }
            });
            } else{
               swal("Error en los datos", "Por favor ingrese un nombre en la categoria", "error"); 
            };
            
    }

function regresar(){
   location.href="/agricultura/categoria_especie/";
}