function validar_form_plaga(){
    var cul_id = $("#cul_id").val();
    var tipo = $("#tipo_plaga").val();
    var fecha = $("#fecha").val();
    var cant_plantas = $("#pa_cant_plantas").val();
    var cant_hectareas = $("#pa_cant_hectareas").val();
    var observaciones = $("#pa_observaciones").val();
    if (tipo == "" || fecha == "" || cant_plantas == "" || cant_hectareas == "" || observaciones == ""){
        return false;
    }else{
        return true;
    }
}

function guardar_plaga(){
    // Llegan
    if(validar_form_plaga()){
        var datos = new FormData($('#frmPlaga')[0]);
        return $.ajax({
            data: datos,
            url: '/agricultura/guardar_plaga/',
            type: 'post',
            contentType:false,
            'dataType': 'json',
            processData:false,
            success: function (data) {
                if (data.result=="OK"){
                    toastr.options={"progressBar": true}
                    toastr.success('Plaga creada satisfactoriamente','Estado');
                }else{
                    toastr.options={"progressBar": true}
                    toastr.error('Ha habido un error interno por favor intente más tarde','Estado');
                }
                $("#tblPlagas").load(location.href+" #tblPlagas>*","");
                $(".modal").modal("hide");
                $(".modal-backdrop").remove();
            }
        });
    }else{
        toastr.options={"progressBar": true}
        toastr.warning('Ingrese todos los datos','Error');
        return false;
    }
}

$.editarModal = function(td)
{
    $('#accion').val("1");
    $('#title').html("Modificar Plaga");
    var tr      = $(td).parent().children();
    var tipo_plaga     = tr[0].textContent; //tipo_plaga
    var fecha     = tr[1].textContent; //fecha
    var plantas_afectadas = tr[2].textContent;
    var hectareas_afectadas = tr[3].textContent;
    var observaciones  = tr[4].textContent;
    var plaga_id = $(td).parent().attr("id").split("-")[1];

    $("tipo_plaga option").filter(function() {
    //may want to use $.trim in here
        return $(this).text() == tipo_plaga; 
    }).prop('selected', true);
    
    $('#plaga_id').val(plaga_id);
    $('#fecha').val(fecha);
    $('#pa_cant_plantas').val(plantas_afectadas);
    $('#pa_cant_hectareas').val(hectareas_afectadas);
    $('#pa_observaciones').val(observaciones);
};

function baja_plaga(elemento,id){
    var estado= elemento.checked == false ? false : true;
    
    $.ajax({
        data: {'id': id, 'estado': estado},
        url: '/agricultura/bajaPlaga/',
        type: 'get',
        success: function (data) {
            if (data.result=="OK"){
                toastr.options={"progressBar": true}
                toastr.success('Se cambio de estado','Estado')
            }else{
            }
        },
        error:function(jqXHR,data,lol){
            alert(data.responseText);
        }

    });
}