/**
 * Created by DYUYAY on 20/12/2015.
 */

//DISEÑO

document.onload = inicio()

function inicio() {

    select();

    var elem = document.querySelector('.js-switch');
    var switchery = new Switchery(elem, { color: '#1AB394' });
    //configuracion de la tabla
    $('.footable').footable();

}

//OPERACIONES
function baja_disp(elemento, id) {
    var estado = true;
    if (elemento.checked == false) {
        estado = false;
    }
    $.ajax({
        data: { 'id': id, 'estado': estado },
        url: '/iot/bajaDisp/',
        type: 'get',
        success: function(data) {
            if (data.result == "OK") {
                toastr.options = { "progressBar": true }
                toastr.success('Se cambio de estado', 'Estado')
            } else {}
        }
    });
}

function editar_dispositivo(id) {
    $.ajax({
        data: { 'id': id },
        url: '/iot/ver_datos_dispositivo/',
        type: 'get',
        success: function(data) {
            if (data.length != 0) {
                                var data = JSON.parse(data);
                $("input[type=text]").focusin();
                $("#contenedor_btn").empty();
                $("#contenedor_btn").append('<div class="col-md-4"><button onclick="enviar_datos(' + data.id + ')" class="btn btn-primary" id="btn_modificar" type="button" >Modificar</button></div>')
                $("#contenedor_btn").append('<div class="col-md-4"><button onclick="regresar()" class="btn btn-danger" id="btn_cancelar" type="button" >Cancelar</button></div>');
                $("#dis_nom").val(data.dis_nombre);
                $("#id").val(data.id);
                $("#dis_des").val(data.dis_descripcion);
                $("#dis_mac").val(data.dis_mac);
                $("#dis_reg").val(data.dis_fecha_registro);
                $("#dis_localizacion").val(data.dis_localizacion);
                $("#contenedor_img").empty();
                $("#contenedor_img").append('<img src=' + data.imagen + ' width="100" height="100">');
                $('#select_sub_area').innerHTML = "";
                $("#contenedorSelect").empty();
                htmlSelect = '<select data-placeholder="Escoga Categoria..." class="chosen-select" style="width:250px;" tabindex="2" id="select_sub_area" name="select_sub_area">';
                $("#contenedorSelect").append('');
                htmlSelect += '<option value="' + data.sar_id + '">' + data.sar_nombre + '</option>';
                for (var i = 0; i < data.sub_areas.length; i++) {
                    console.log(data.sub_areas[i].sar_id);
                    htmlSelect += '<option value="' + data.sub_areas[i].sar_id + '">' + data.sub_areas[i].sar_nombre + '</option>';
                }
                htmlSelect += '</select>';
                $("#contenedorSelect").append(htmlSelect)
                select();
                $("#div_estado").empty();
            } else {
                alert('Categoria no encontrado')
            }
        }
    });
}

function enviar_datos(id) {
    //var formulario=new FormData($('#form_especie')[0]);
    var datos = new FormData($('#frmDis')[0]);
        $.ajax({
            data: datos,
            url: '/iot/modificar_datos_dispositivos/',
            type: 'POST',
            contentType: false,
            'dataType': 'json',
            processData: false,
            success: function(data) {
                if (data.result == 'OK') {
                    toastr.options = { "progressBar": true, "showDuration": "400" };
                    toastr.success('Se modifico', 'Estado');
                    location.href = "/iot/dispositivo";
                } else {
                    swal("Error en los datos", "Verifique los datos ingesados e intente nuevamente", "error");
                }
            }
        });
}

function select() {
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': { allow_single_deselect: true },
        '.chosen-select-no-single': { disable_search_threshold: 10 },
        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
        '.chosen-select-width': { width: "95%" }
    };
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
}

function regresar() {
    location.href = "/iot/dispositivo";
}
