$(function(){
	//****************************************** GUARDAR ****************************
	$("#btnSave").on("click", function(e){
		e.preventDefault(); 
		var csrftoken = getCookie('csrftoken'); //the token 
		$.ajax({
				type: "POST",
				url: "/empresa/actuador/tipo/sav/", 
				dataType: 'json', 
				data:{
						csrfmiddlewaretoken : csrftoken,
						act_nom 			: $('#tip_act_nom').val()						
					},
				success : function(response)
				{					
					$('#tip_act_nom').val("");
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Tipo de Actuador');
					$('#tblTipAct').DataTable().ajax.reload();
					tip_act_nom.focus();
				},

				error : function(response)
				{
					$('#tip_act_nom').val("");
					toastr.options={"progressBar":true};
					toastr.error("Error al guardar",'Tipo de Actuador');
				}
			});//
	});
	////*****************************************************************************

	/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var tr = $(td).parent().children();
		var cod = $(td).parent().attr('id'); //codigo
		var des = tr[0].textContent; //descripcion
		$('#txtNom').val(des);
		$('#txtId').val(cod);
	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		txtNom.focus();
	});

	$("#btnModalGuardar").on("click", function(e){
		e.preventDefault(); 
		var csrftoken = getCookie('csrftoken'); //the token 
		$.ajax({
				type: "POST",
				url: "/empresa/actuador/tipo/mod/", 
				dataType: 'json', 
				data:{
						csrfmiddlewaretoken : csrftoken,
						act_id 				: $('#txtId').val(),
						act_nom 			: $('#txtNom').val()
					},
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Tipo de Actuador');
					$('#tblTipAct').DataTable().ajax.reload();
				},

				error : function(response)
				{
					console.log('actuador NO guardado');
				}
			});//
	});

	////////////////////////////////

	var btnsOpTblModels = "<button style='border: 0; background: transparent' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"<img src='' title='Editar'>"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']); //codig
	};
	
	var lngEsp = {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		}
	};
	

	$('#tblTipAct').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/actuador/tipo/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){					
					datos.push({
						'id': item.id,
						'nombre': item.tact_nombre
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'}			
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow
	});
});