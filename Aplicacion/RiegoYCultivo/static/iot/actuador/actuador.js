$(function(){
/////////////////////////////// GUARDAR /////////////////////
	$('#frmAct').on("submit",function(){
		event.preventDefault();
		console.log($(this).serialize());
		$.ajax({
			type:"POST",
			url: "/empresa/actuador/sav/",
			dataType: 'json',
			data:$(this).serialize(),
			success: function(response){
				$('#act_nom').val("");
				$('#act_des').val("");
				$('#act_ser').val("");
				$('#act_est').val("");
				$('#act_int').val("0");
				toastr.options={"progressBar":true};
				toastr.success(response.mensaje,'Actuador');
				$('#tblAct').DataTable().ajax.reload();
				act_nom.focus();
			},

			error: function(){
				toastr.options={"progressBar":true};
				toastr.error("Error al guardar",'Actuador');
			}
		});

	});
////////////////////////////CARGAR //////////////////////////

	var btnsOpTblModels = "<button style='border: 0; background: transparent' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"<img src='' title='Editar'>"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']);
		$(nRow).attr('idTipo',aData['id_tipo']);
	};	

	$('#tblAct').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/actuador/get/",
			"dataSrc": function(response){
				var datos = [];								
				$.each(response.data, function(i, item){					
					datos.push({
						'id': item.id,
						'nombre': item.act_nombre,
						'descripcion': item.act_descripcion,
						'serie': item.act_serie,
						'tipo' : item.tact_id__tact_nombre,
						'estado': item.act_estado,
						'id_tipo': item.tact_id_id,
						'interfaz' : item.act_interfaz
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'},
			{'data': 'descripcion'},
			{'data': 'interfaz'},
			{'data': 'serie'},
			{'data': 'tipo'},
			{'data':'estado'}
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow
	});
////////////////////////////////////////////////////////////////// CARGAR TIPO
	// Llena el comboBox tipo
	var data = "";
	$.getDataTipo = function(response)
	{
		data = response.data;
		var datos = "";
		$.each(response.data, function(i,value){
			datos+= "<option value="+value.id+">"+value.tact_nombre+"</option>";
		});

		$('#tip_act').html(datos);
	};

	//llamada ComboBox
	$.get("/empresa/actuador/tipo/get/",$.getDataTipo);
/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var datos = "";
		$.each(data, function(i,value){
			datos+= "<option value="+value.id+">"+value.tact_nombre+"</option>";
		});		
		$('#tip_act_m').html(datos);

		var tr 		= $(td).parent().children();
		var cod 	= $(td).parent().attr('id'); //id
		var tipId 	= $(td).parent().attr('idTipo'); //idTipo
		var nom 	= tr[0].textContent; //nombre
		var des 	= tr[1].textContent; //des
		var inter 	= tr[2].textContent;
		var ser 	= tr[3].textContent; //serie
		var est 	= tr[5].textContent; //estado
		$('#act_nom_m').val(nom);
		$('#act_des_m').val(des);
		$('#act_int_m').val(inter);
		$('#act_ser_m').val(ser);
		$('#act_est_m').val(est);
		$('#act_id').val(cod);
		$('#tip_act_m').val(tipId);
	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		act_nom_m.focus();
	});

	$("#frmActMod").on("submit", function(e){
		e.preventDefault();
		console.log($(this).serialize());
		$.ajax({
				type: "POST",
				url: "/empresa/actuador/mod/", 
				dataType: 'json', 
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Actuador');
					$('#tblAct').DataTable().ajax.reload();
				},

				error : function(response)
				{
					$('#modalVer').modal('hide');
					toastr.options={"progressBar":true};
					toastr.error("Error al Modificar",'Actuador');
				}
			});//
	});

	////////////////////////////////
});