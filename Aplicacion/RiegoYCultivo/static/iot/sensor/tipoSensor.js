$(function(){
	
	//****************************************** GUARDAR ****************************
	$("#btnSave").on("click", function(e){
		e.preventDefault(); 
		var csrftoken = getCookie('csrftoken'); //the token 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/tipo/sav/", 
				dataType: 'json', 
				data:{
						csrfmiddlewaretoken : csrftoken,
						sen_nom 			: $('#txtTipSen').val()						
					},
				success : function(response)
				{					
					$('#txtTipSen').val("");
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Tipo de Sensor');
					$('#tblTipSen').DataTable().ajax.reload();
				},

				error : function(response)
				{
					console.log('sensor NO guardado');
				}
			});//
	});
	////*****************************************************************************

	/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var tr = $(td).parent().children();
		var cod = $(td).parent().attr('id'); //codigo
		var des = tr[0].textContent; //descripcion
		$('#txtNom').val(des);
		$('#txtId').val(cod);
	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		txtNom.focus();
	});

	$("#btnModalGuardar").on("click", function(e){
		e.preventDefault(); 
		var csrftoken = getCookie('csrftoken'); //the token 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/tipo/mod/", 
				dataType: 'json', 
				data:{
						csrfmiddlewaretoken : csrftoken,
						sen_id 				: $('#txtId').val(),
						sen_nom 			: $('#txtNom').val()
					},
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Tipo de Sensor');
					$('#tblTipSen').DataTable().ajax.reload();
				},

				error : function(response)
				{
					console.log('sensor NO guardado');
				}
			});//
	});

//***********************************
	var btnsOpTblModels = "<button style='border: 0; background: transparent' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"<img src='' title='Editar'>"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']); //codig
	};
	
	$('#tblTipSen').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/sensor/tipo/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){					
					datos.push({
						'id': item.id,
						'nombre': item.ts_nombre
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'}			
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow
	});
//************************************** CARGA LA TABLA **********************
});