$(function(){

/** GUARDAR **/
$("#frmTed").on("submit", function(e){
		e.preventDefault(); 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/ted/sav/", 
				dataType: 'json', 
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#ted_for').val("");
					$('#ted_max').val("");
					$('#ted_min').val("");
					$('#ted_ser').val("");
					$('#ted_des').val("");
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Ted');
					$('#tblTed').DataTable().ajax.reload();
					ted_for.focus();
				},

				error : function(response)
				{
					toastr.options={"progressBar":true};
					toastr.error("Error al Guardar",'Ted');
				}
			});//
	});
////////////////// CARGAR TABLA **************
var btnsOpTblModels = "<button style='border: 0; background: transparent' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"<img src='' title='Editar'>"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']); //codigo
		$(nRow).attr('med_id',aData['idMed']);
		$(nRow).attr('fab_id',aData['idFab']);
		$(nRow).attr('tse_id',aData['tId']);
	};
	
	$('#tblTed').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/sensor/ted/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){					
					datos.push({
						'id'		: item.id,
						'formula'	: item.ted_formula,
						'serie'		: item.ted_serie,
						'min'		: item.ted_min,
						'max'		: item.ted_max,
						'med'		: item.umed_id__umed_nombre,
						'idMed'		: item.umed_id_id,
						'fab'		: item.fac_id__fac_nombre,
						'idFab'		: item.fac_id_id,
						'tse'		: item.ts_id__ts_nombre,
						'tId'		: item.ts_id_id,
						'descrip'	: item.ted_descripcion
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'formula'},
			{'data': 'max'},
			{'data': 'min'},
			{'data': 'serie'},
			{'data': 'descrip'},
			{'data': 'tse'},
			{'data': 'med'},
			{'data': 'fab'}			
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow
	});
//*********************************************************************
	var dataTipSen="" , dataUmed = "" , dataFab = "";
	$.getDataTipSen = function(response)
		{
			var datos = "";
			dataTipSen = response.data;
			$.each(dataTipSen, function(i,value){
				datos+= "<option value="+value.id+">"+value.ts_nombre+"</option>";
			});

			$('#tip').html(datos);		
		};

	$.getDataUmed = function(response)
		{
			var datos = "";
			dataUmed = response.data;
			$.each(dataUmed, function(i,value){
				datos+= "<option value="+value.id+">"+value.umed_nombre+"</option>";
			});

			$('#med').html(datos);		
		};

	$.getDataFab = function(response)
		{
			var datos = "";
			dataFab = response.data;
			$.each(dataFab, function(i,value){
				datos+= "<option value="+value.id+">"+value.fac_nombre+"</option>";
			});

			$('#fab').html(datos);		
		};
	
	$.get("/empresa/sensor/tipo/get/",$.getDataTipSen); // get without token_crsf
	$.get("/empresa/sensor/unimed/get/",$.getDataUmed);
	$.get("/empresa/sensor/fabricante/get/",$.getDataFab);

/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var datosTip = "", datosUmed="", datosFab = "";
		$.each(dataTipSen, function(i,value){
			datosTip+= "<option value="+value.id+">"+value.ts_nombre+"</option>";
		});		
		$('#tip_m').html(datosTip);

		
		$.each(dataUmed, function(i,value){
			datosUmed+= "<option value="+value.id+">"+value.umed_nombre+"</option>";
		});		
		$('#med_m').html(datosUmed);

		
		$.each(dataFab, function(i,value){
			datosFab+= "<option value="+value.id+">"+value.fac_nombre+"</option>";
		});		
		$('#fab_m').html(datosFab);

		var tr 		= $(td).parent().children();
		var cod 	= $(td).parent().attr('id'); //id
		var tipo 	= $(td).parent().attr('tse_id');
		var umed 	= $(td).parent().attr('med_id');
		var fab 	= $(td).parent().attr('fab_id');
		var formula = tr[0].textContent;
		var max 	= tr[1].textContent;
		var min 	= tr[2].textContent;
		var serie 	= tr[3].textContent;
		var des 	= tr[4].textContent;
		$('#ted_id').val(cod);
		$('#ted_for_m').val(formula);
		$('#ted_max_m').val(max);
		$('#ted_min_m').val(min);
		$('#ted_ser_m').val(serie);		
		$('#ted_des_m').val(des);
		$('#tip_m').val(tipo);
		$('#med_m').val(umed);
		$('#fab_m').val(fab);
	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		ted_for_m.focus();
	});

	$("#frmTedMod").on("submit", function(e){
		e.preventDefault(); 		
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/ted/mod/", 
				dataType: 'json', 
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Ted');
					$('#tblTed').DataTable().ajax.reload();
				},

				error : function(response)
				{
					$('#modalVer').modal('hide');
					toastr.options={"progressBar":true};
					toastr.error("Error al Modificar",'Ted');
				}
			});//
	});
	
});