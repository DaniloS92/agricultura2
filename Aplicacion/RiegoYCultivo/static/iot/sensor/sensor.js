$(function(){

/** GUARDAR **/
$("#frmSen").on("submit", function(e){
		e.preventDefault();
		$('#sen_est').is(":checked")?$('#sen_est_g').val("true"):$('#sen_est_g').val("false");
		$.ajax({
				type: "POST",
				url: "/iot/sensor/sav/",
				dataType: 'json',
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#sen_nom').val("");
					$('#sen_des').val("");
					$('#sen_sen').val("");
					$('#sen_cat').val("");
					$('#sen_int').val("");
					$('#sen_url').val("");
					$('#sen_for').val("");
					$('#sen_est').prop("checked",false);
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Sensor');
					$('#tblSen').DataTable().ajax.reload();
					sen_nom.focus();
				},

				error : function(response)
				{
					toastr.options={"progressBar":true};
					toastr.error("Error al Guardar",'Sensor');
				}
			});//
	});
////////////////// CARGAR TABLA **************
var btnsOpTblModels = "<button class='btn-white btn btn-xs' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"Editar"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td>"+btnsOpTblModels+"</td>");		
		$(nRow).attr('id',aData['id']); //codigo
		$(nRow).attr('dis_id',aData['dis_id']);
		$(nRow).attr('data-cat',aData['cat']);
		$(nRow).attr('data-sen',aData['sen']);
		$(nRow).attr('data-des',aData['descripcion']);
		$(nRow).attr('data-url',aData['url']);
		$(nRow).attr('data-for',aData['formula']);
	};
	
	$('#tblSen').DataTable({
				autoWidth : false,
				"ajax":{
					"type": "GET",
					"url": "/iot/sensor/get/",
					"dataSrc": function(response){
						var datos = [];			
						$.each(response.data, function(i, item){
							var cat="", sen="";
							(item.sa_categoria === "a") ? cat = "Actuador"	: cat = "Sensor";
							(item.sa_senial === "d") 	? sen = "Digital"	: sen = "Analogica";
							datos.push({
								'id'			: item.iot_id,
								'nombre'		: item.sa_nombre,						
								'interfaz'		: item.sa_interfaz,
								'descripcion'	: item.sa_descripcion,
								'categoria'		: cat,
								'senial'		: sen,
								'estado'		: item.sa_estado,
								'url'			: item.sa_url_ted,
								'formula'		: item.sa_formula,
								'dis_id'		: item.dis_id,
								'dis'			: item.dis_id__dis_nombre,
								'cat'			: item.sa_categoria,
								'sen'			: item.sa_senial
							});				
						});
						return datos;
					},
				},
				"columns": 
				[
					{'data': 'nombre'},
					{'data': 'descripcion'},
					{'data': 'interfaz'},
					{'data': 'categoria'},
					{'data': 'senial'},
					{'data': 'estado'},
					{'data': 'url'},
					{'data': 'formula'},
					{'data': 'dis'}			
				],
		        "language": lngEsp,		        
		        "fnCreatedRow": $.renderizeRow,
		        /*
		        "fnInitComplete": function(oSettings, json)
			        {
					    $('#tblSen').footable();
					}*/
				preDrawCallback: function () {
		            // Initialize the responsive datatables helper once.
		            if (!responsiveHelper) {
		                responsiveHelper = new ResponsiveDatatablesHelper($('#tblSen'), breakpointDefinition);
		            }
		        },
		        rowCallback    : function (nRow) {
		            responsiveHelper.createExpandIcon(nRow);
		        },
		        drawCallback   : function (oSettings) {
		            responsiveHelper.respond();
		        }
			});
	
//*********************************************************************
	var dataDis="";
	
	$.getDataDis = function(response)
		{
			var datos = "";
			dataDis = response.data;
			$.each(dataDis, function(i,value){
				datos+= "<option value="+value.dis_id+">"+value.dis_nombre+"</option>";
			});

			$('#dis').html(datos);		
		};
	
	$.get("/iot/dispositivo/get/",$.getDataDis); // get without token_crsf
	

/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var datosDis = "", datosTed="";
		$.each(dataDis, function(i,value){
			datosDis+= "<option value="+value.dis_id+">"+value.dis_nombre+"</option>";
		});		
		$('#dis_m').html(datosDis);

		var tr 		= $(td).parent().children();
		var cod 	= $(td).parent().attr('id'); //id
		var dis 	= $(td).parent().attr('dis_id');
		$('#sen_id').val(cod);
		$('#sen_nom_m').val(tr[0].textContent);
		$('#sen_des_m').val($(td).parent().attr('data-des'));		
		$('#sen_int_m').val(tr[2].textContent);
		$('#sen_cat_m').val($(td).parent().attr('data-cat'));
		$('#sen_sen_m').val($(td).parent().attr('data-sen'));
		$('#sen_url_m').val($(td).parent().attr('data-url'));
		$('#sen_for_m').val($(td).parent().attr('data-for'));
		$('#dis_m').val(dis);
		$('#sen_est_m').prop("checked", (tr[5].textContent === "true") ? true : false);

	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		sen_nom_m.focus();
	});

	$("#frmSenMod").on("submit", function(e){
		e.preventDefault();
		//$('#sen_est_m').is(":checked")? $('#sen_est_ma').val("true"):$('#sen_est_ma').val("false");
		$.ajax({
				type: "POST",
				url: "/iot/sensor/mod/", 
				dataType: 'json', 
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Sensor');
					$('#tblSen').DataTable().ajax.reload();
				},

				error : function(response)
				{
					$('#modalVer').modal('hide');
					toastr.options={"progressBar":true};
					toastr.error("Error al Modificar",'Sensor');
				}
			});//
	});	
	
});