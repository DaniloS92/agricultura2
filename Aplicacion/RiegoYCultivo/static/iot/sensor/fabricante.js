$(function(){

	//****************************************** GUARDAR ****************************
	$("#frmFab").on("submit", function(e){
		e.preventDefault(); 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/fabricante/sav/", 
				dataType: 'json', 
				data: $(this).serialize(),
				success : function(response)
				{					
					$('#fab_nom').val("");					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Fabricante');
					$('#tblFab').DataTable().ajax.reload();
					fab_nom.focus();
				},

				error : function(response)
				{
					toastr.options={"progressBar":true};
					toastr.error("Error al Guardar",'Fabricante');
				}
			});//
	});
//************************************************* CARGAR TABLA ********
var btnsOpTblModels = "<button style='border: 0; background: transparent' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"<img src='' title='Editar'>"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']); //codig
	};
	
	$('#tblFab').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/sensor/fabricante/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){					
					datos.push({
						'id': item.id,
						'nombre': item.fac_nombre						
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'}
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow
	});

/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var tr 	= $(td).parent().children();
		var cod = $(td).parent().attr('id'); //codigo
		var des = tr[0].textContent; //nombre		
		$('#fab_nom_m').val(des);		
		$('#fab_id').val(cod);
	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		fab_nom_m.focus();
	});

	$("#frmFabMod").on("submit", function(e){
		e.preventDefault(); 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/fabricante/mod/", 
				dataType: 'json', 
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Fabricante');
					$('#tblFab').DataTable().ajax.reload();
				},

				error : function(response)
				{
					toastr.options={"progressBar":true};
					toastr.error("Error al Modificar",'Fabricante');
				}
			});//
	});

});