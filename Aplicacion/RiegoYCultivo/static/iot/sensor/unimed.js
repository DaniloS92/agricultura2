$(function(){

	//****************************************** GUARDAR ****************************
	$("#frmUniMed").on("submit", function(e){
		e.preventDefault(); 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/unimed/sav/", 
				dataType: 'json', 
				data: $(this).serialize(),
				success : function(response)
				{					
					$('#uni_nom').val("");
					$('#uni_abr').val("");
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Unidad de Medida');
					$('#tblUniMed').DataTable().ajax.reload();
				},

				error : function(response)
				{
					toastr.options={"progressBar":true};
					toastr.error("Error al Guardar",'Unidad de Medida');
				}
			});//
	});
//************************************************* CARGAR TABLA ********
var btnsOpTblModels = "<button style='border: 0; background: transparent' data-target='#modalVer' data-toggle='modal' onclick='$.editarModal($(this).parent())'>"+
							"<img src='' title='Editar'>"+
						  "</button>";

	$.renderizeRow = function( nRow, aData, iDataIndex )
	{
		$(nRow).append("<td class='text-center'>"+btnsOpTblModels+"</td>");
		$(nRow).attr('id',aData['id']); //codig
	};
	
	$('#tblUniMed').DataTable({
		"ajax":{
			"type": "GET",
			"url": "/empresa/sensor/unimed/get/",
			"dataSrc": function(response){
				var datos = [];				
				$.each(response.data, function(i, item){					
					datos.push({
						'id': item.id,
						'nombre': item.umed_nombre,
						'abreviatura': item.umed_abreviatura
					});				
				});
				return datos;
			},
		},
		"columns": 
		[
			{'data': 'nombre'},
			{'data': 'abreviatura'}
		],
        "language": lngEsp,
        "fnCreatedRow": $.renderizeRow
	});

/////////////////////////////////////////////////////////////// EDITAR //////////////////////////////
	$.editarModal = function(td)
	{
		var tr 	= $(td).parent().children();
		var cod = $(td).parent().attr('id'); //codigo
		var des = tr[0].textContent; //nombre
		var abr = tr[1].textContent;
		$('#uni_nom_m').val(des);
		$('#uni_abr_m').val(abr);
		$('#uni_id').val(cod);
	};

	$('#modalVer').bind('shown.bs.modal' , function(){
		uni_nom_m.focus();
	});

	$("#frmUniMedMod").on("submit", function(e){
		e.preventDefault(); 
		$.ajax({
				type: "POST",
				url: "/empresa/sensor/unimed/mod/", 
				dataType: 'json', 
				data:$(this).serialize(),
				success : function(response)
				{					
					$('#modalVer').modal('hide');					
					toastr.options={"progressBar":true};
					toastr.success(response.mensaje,'Unidad de Medida');
					$('#tblUniMed').DataTable().ajax.reload();
				},

				error : function(response)
				{
					toastr.options={"progressBar":true};
					toastr.error("Error al Modificar",'Unidad de Medida');
				}
			});//
	});

});