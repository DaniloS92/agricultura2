$(function(){

	// Llena el comboBox tipo
	var data = "";
	var jsonObj=[];
	$.getDataTipo = function(response)
	{
		data = response.data;
		var datos = "<option value=-1>---</option>"+
					"<option value=0>Todos</option>";		
		$.each(response.data, function(i,value){
			datos+= "<option value="+value.dis_id+">"+value.dis_nombre+"</option>";
		});

		$('#dis_nom').html(datos);
	};

	//call ComboBox
	$.get("/iot/getDispositivo/",$.getDataTipo);

	
	function getDateFormatted (f)
	{
		var dia = f.substring(8,10);
		var mth = f.substring(5,7);
		var nio = f.substring(0,4);		
		return dia+"/"+mth+"/"+nio;
	}

	/**
	EVENTO QUE CONTROLA EL CHANGE DEL COMBO BOX DISPOSITIVO --> LLENA DATATABLE CON JSON OBJECT
	**/
	$('#dis_nom').on("change",function(){
		
		if ($('#dis_nom').val() !== "-1")
		{
			var token_crsf 	= getCookie('csrftoken');
			
			var fec_1 		= getDateFormatted($('#fec_ini').val());
			var fec_2 		= getDateFormatted($('#fec_fin').val());

			$('#dispositivo').val($('#dis_nom option:selected').text());
			$('#interfaz').val($('#int_nom').val());
			if ($('#dis_nom').val() === "0"){
				$('#interfaz').val("0");
			}

			$.ajax({
				type: "POST",
				url: "/reportes/interfaz/", 
				dataType: 'json',
				data:{
						dis_id  			:$('#dis_nom').val(),
						numero 				:$('#int_num').val(),
						cmb_order 			:$('#cmb_ord').val(),
						fec_1				:fec_1,
						fec_2 				:fec_2,
						csrfmiddlewaretoken : token_crsf
					},
				success : function(response)
				{
					if (response.data.length > 0)
					{			
						var datos= "";
						jsonObj=[];
						var contador = 0;
						datos = "<option value=0>---</option>";			

						$.each(response.data,function(i,item)
						{			
							if (typeof item.datos !== "undefined" ) // si existe el objeto
							{
								var datosLength = item.datos.length;
								
								$.each(item.datos,function(y,v)
								{	
									if (contador < datosLength && $('#dis_nom').val() > 0)
									{
										datos+= "<option value="+v.datos_item.interfaz+">"+item.sa_nombre+" - "+v.datos_item.interfaz+"</option>";
										contador++;
									}

									jsonObj.push({
										'dsp' : item.dis_nombre,
										'int' : item.sa_nombre+" - "+v.datos_item.interfaz,
										'val' : v.datos_item.valor+" "+item.umed_abreviatura,
										'fec' : item.fecha1,
										'into': v.datos_item.interfaz
									});
								});
								
							}
							
						});				
						//console.log(jsonObj);
						$('#form_data').val(JSON.stringify(jsonObj));
						$('#int_nom').html(datos);
						
						if($('#dis_nom').val() != "0") //diferente de todos
						{
							cargarDataTable(jsonObj,false,true);
						}
						else
						{
							cargarDataTable(jsonObj,true,true);	
						}
					}
					else
					{
						toastr.error("Lo sentimos, no existen lecturas con los parámetros definidos",'Reporte');
					}
					
				},

				error : function(response)
				{
					$('#modalVer').modal('hide');
					toastr.options={"progressBar":true};
					toastr.error("No hay conexión con el servidor",'Reporte');
				}			
			});
		}
		else
		{
			jsonObj = [];
			$('#form_data').val("");
			$('#int_nom').html("<option value='0'>---</option>");
			cargarDataTable(jsonObj,true,true);

		}
		
	});

	
	/*
	* CARGA LOS DATOS POR PARAMETROS
		recibe un objeto JSON
	*/
	function cargarDataTable (jsonObj, vd, vi){

		$('#tblRep').DataTable({
			destroy:true,
			searching:false,										
			data: jsonObj,
	        columns: 
	        [
	            { 'data'	: 'dsp' },
	            { 'data'	: 'int' },
	            { 'data' 	: 'val' },
	            { 'data' 	: 'fec' }
	        ],
	        columnDefs:
	        [
	        	{"searchable":false, "targets" : 0 , "visible":vd},
	        	{"searchable":false, "targets" : 1 , "visible":vi},
	        ],
	        dom: 'Bfrtip',
	        buttons: ['excel',]
		}); // close DataTable

	} // close cargarDataTable

	
	/*
	*
	*EVENTO DEL COMBO BOX INTERFAZ
	*/
	$('#int_nom').on("change",function(){
		
		if ($('#int_nom').val() !== "0") // solo 1 interfaz
		{
			$('#interfaz').val($('#int_nom option:selected').text());
			var dataInt =[]; // json object
			$.each(jsonObj,function(i,item){

				if (item.into === $('#int_nom').val())
				{
					dataInt.push({
						'dsp' : item.dsp,
						'int' : item.int,
						'val' : item.val,
						'fec' : item.fec
					});
				}

			});
			$('#form_data').val(JSON.stringify(dataInt));
			cargarDataTable(dataInt,false,false);
		}
		else // todas las interfaces
		{
			$('#interfaz').val("0");
			var dataInt =[]; // json object
			$.each(jsonObj,function(i,item){
				
				dataInt.push({
					'dsp' : item.dsp,
					'int' : item.int,
					'val' : item.val,
					'fec' : item.fec
				});			

			});
			$('#form_data').val(JSON.stringify(dataInt));
			cargarDataTable(dataInt,false,true);
		}
	}); // FINAL EVENTO COMBO BOX INTERFAZ

	/*
	* REPORTE EN PDF CON EL CONDENADO REPORT LAB :(
	
	$('#btnReporte').on("click",function(){
		
		var token_crsf 	= getCookie('csrftoken');
		$.ajax({
			type: "POST",
			url: "/reportes/lectura/", 
			dataType: 'json', 
			data:{
					jsonObj  			: JSON.stringify(jsonObj),					
					csrfmiddlewaretoken : token_crsf
				},
			beforeSend: function(x){
				if(x && x.overrideMimeType){
					x.overrideMimeType("application/j-son;charset=UTF-8");
				}
			},

			success : function(response)
			{
				console.log(response);
				toastr.options={"progressBar":true};
				toastr.success("Se enviaron los datos correctamente",'Reporte');				
			},

			error : function(response)
			{
				//console.log(response);
				//window.open(response);
				toastr.options={"progressBar":true};
				toastr.error("Error al cargar los datos",'Reporte');
			}			

		});
	});*/

	$('#frmReporte').on("submit", function(e){
		
		if( $('#form_data').val() !== "")
		{
			$('#frmReporte').submit();
		}
		else
		{
			e.preventDefault();
			toastr.error("Lo sentimos, no existen lecturas. Los filtros fecha y dispositivo son obligatorios",'Reporte');
		}
	});

	$(document).ajaxStart(function(){
        $("#wait").css("display", "block");
    });

    $(document).ajaxComplete(function(){
        $("#wait").css("display", "none");
    });
});