"""
Django settings for RiegoYCultivo project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from unipath import Path
import os
BASE_DIR = Path(__file__).ancestor(2)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%&r=6&!1j+3*)cu@t839qv*kah$oj3w#(5_*$yc^^s5i$l6aeb'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app_empresa',
    'iot',
    'agricultura',
    'app_seguridad',
    # 'app_modular',
    'app_eventos',
    'app_reportes',
)

DAB_FIELD_RENDERER = 'django_admin_bootstrapped.renderers.BootstrapFieldRenderer'

from django.contrib import messages

# MESSAGE_TAGS = {
#             messages.SUCCESS: 'alert-success success',
#             messages.WARNING: 'alert-warning warning',
#             messages.ERROR: 'alert-danger error'
# }



MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'RiegoYCultivo.urls'

# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': ['templates'],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]

WSGI_APPLICATION = 'RiegoYCultivo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'db_Iotmach',
         # 'HOST':'192.168.0.43',
         'HOST':'127.0.0.1',
         'PORT':'5432',
         'PASSWORD':'iotmach_2016',
         'USER':'iotmachuser',
         # 'PASSWORD': '12345678',
         # 'USER': 'postgres'
     }
 }



# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': 'eventosDB.sqlite3',
#     }
# }


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-es'

TIME_ZONE = 'America/Los_Angeles'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


MEDIA_ROOT=os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'media')).replace('\\','/')
MEDIA_URL = '/media/'

STATIC_URL = '/static/'

#STATIC_ROOT = os.path.join(BASE_DIR, STATIC_URL.strip("/"))
##STATIC_ROOT = "/usr/share/nginx/agricultura/Aplicacion/RiegoYCultivo/static/"

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

STATICFILES_DIRS=(
    os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'static')).replace('\\','/'),
)

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'iotmach@utmachala.edu.ec'
EMAIL_HOST_PASSWORD = 'Iotm@ch2016'
EMAIL_PORT = 587
EMAIL_USE_TLS = True