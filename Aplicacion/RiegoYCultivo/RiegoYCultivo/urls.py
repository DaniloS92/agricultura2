from django.conf.urls import patterns, include, url, handler404, handler500
from agricultura.views import *

from django.contrib import admin
admin.autodiscover()
# from django.views.generic import RedirectView
from django.conf import settings
from agricultura.views import error404
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'RiegoYCultivo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', base, name="url_inicio"),
    url(r'^agricultura/', include('agricultura.urls', namespace="agricultura")),
    #url(r'^skin-config$', conf_tema , name="url_conf-tema"),
    url(r'^iniciosesion/$', inicioSesion),
    url(r'^cerrarsesion/$', cierre_node),
    url(r'^recuperar_cuenta/(?P<username_user>\w+)/(?P<email_user>.*)/$', verifica_recuperacion_cuenta, name="URLverifica_recuperacion_cuenta"),
    url(r'^recuperar_password/$', cambiar_password, name="URLcambiar_password"),
    url(r'^modular/', include('app_modular.urls')),
    url(r'^empresa/', include('app_empresa.urls', namespace='URLempresa')),
    url(r'^seguridad/', include('app_seguridad.urls')),
    url(r'^iot/', include('iot.urls', namespace='iot')),
    url(r'^reglas/', include('app_eventos.urls', namespace="reglas")),
    url(r'^reportes/', include('app_reportes.urls', namespace='reportes')),
)
if settings.DEBUG:
    urlpatterns += patterns("",
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT,}
        ),
    )

handler404=error404
handler500=error500