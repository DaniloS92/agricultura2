from __future__ import absolute_import

from celery import Celery

app = Celery('app_celery',broker='redis://localhost',
                        backend='redis://localhost',
                        include=['app_celery.tasks'])

app.config_from_object('app_celery.celeryconfig')

if __name__ == '__main__':
    app.start()