from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from app_reportes.views import *
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns('',    
    # Sensores
    #url(r'^inicio/$', base_iot, name="url_sensor"),
    url(r'^inicio/$', openReportes, name="url_reporte"),
    url(r'^interfaz/$', getInterfaz, name="url_interfaz"),
    url(r'^lectura/$', reporteLectura, name="url_reporte_lectura"),
)