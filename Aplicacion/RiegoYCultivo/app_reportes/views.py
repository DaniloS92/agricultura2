#-*- coding: utf-8 -*-
import json
import os
import requests
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import *
from django.contrib.auth.hashers import make_password
from django.shortcuts import render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect, render,Http404
from django.db import transaction
from django.contrib import messages
from unipath import Path
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER, TA_LEFT
from reportlab.lib.pagesizes import A4, landscape
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table, TableStyle, Flowable
from reportlab.lib.utils import ImageReader
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm, inch
from reportlab.lib import colors
from reportlab.graphics import renderPDF
from  datetime import datetime
from .models import *
from agricultura.metodos import *

estilos = getSampleStyleSheet()

estilos.add(ParagraphStyle(name="centrado",
                           alignment=TA_CENTER
))
estilos.add(ParagraphStyle(name="izquierda",
                           alignment=TA_LEFT
))

styles = getSampleStyleSheet()

styleN = styles["BodyText"]
styleN.alignment = TA_LEFT
styleN.fontSize=6.2

styleBH = styles["Normal"]
styleBH.alignment = TA_CENTER
styleBH.fontSize=8

PAGE_WIDTH = A4[0]
PAGE_HEIGHT = A4[1]
logoEspecie = ''
vCultivo = ''
vEmpresa = ''

# Create your views here.
def openReportes(request):
    if request.user.is_authenticated():
        dict=armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['empresa']=getEmpresa(request)
        dict['persona'] = obtenerPersona(request)
        return render_to_response("app_reportes/base_reportes.html", dict, context_instance = RequestContext(request))

    else:
        return render_to_response("index.html", context_instance = RequestContext(request))

def getInterfaz(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            print request.POST
            print ("-------------------------------")
            dis_id 	= request.POST['dis_id']
            numero 	= request.POST['numero']
            order   = request.POST['cmb_order']
            fec_1 	= request.POST['fec_1']
            fec_2 	= request.POST['fec_2']
            
            params = {}
            if dis_id == '0':#TODOS
            	params = {'$count':numero , '$format':'json'}
            else:#POR DISPOSITIVO
            	params = {'dis_id':dis_id,'$count':numero ,'$format':'json'}
            #pasar los parametros with params = works fine  
            ws      = requests.get('http://localhost:9090/server/iotmach/ws_disp_lectura/views/disp_lectura?$orderby=fecha1+'+order+'&$filter=fecha1+>%3D+%27'+fec_1+'%27+AND+fecha2+<%3D+%27'+fec_2+'%27',params=params)
            print (ws.url)            
            datos   = ws.json()
            return HttpResponse(
                json.dumps({"data":list(datos["elements"])},cls=DjangoJSONEncoder),
                content_type = "application/json; charset=utf8"
            )
        else:
            return redirect('/')
    else:
        return redirect('/')

def reporteLectura(request):    
    global vEmpresa
    #print request.POST
    print (" ----- ENTRANDO A REPORTE ------------------")
    empresa=request.user.id_persona.emp_id.emp_id         
    persona = proy_persona.objects.get(emp_id=empresa)
    vEmpresa= proy_empresa.objects.get(emp_id=empresa)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename=reporteLecturas-'+datetime.now().strftime("%Y-%m-%d")+'.pdf'
    doc = SimpleDocTemplate(response, pagesize=A4,
                            rightMargin=30, leftMargin=30,
                            topMargin=55, bottomMargin=55 )
    documento = []

    #----------- IMAGEN --------------------------------
    f1=Image('media/iotmach.png',width=70, height=70)
    f1.drawWidth = 2*cm
    documento.append(f1)
    documento.append(Spacer(1, 5))

    if request.POST['interfaz'] == "0":        
        texto = """<font size=12>Reporte De Lecturas: <b>"""+request.POST['dispositivo']+"""</b> Desde """+request.POST['fec_ini']+""" Hasta """+request.POST['fec_fin']+"""</font>"""
    else:
        texto = """<font size=12>Reporte De Lecturas: <b>"""+request.POST['dispositivo']+"""</b> Interfaz: <b>"""+request.POST['interfaz']+"""</b> Desde <b>"""+request.POST['fec_ini']+"""</b> Hasta <b>"""+request.POST['fec_fin']+"""</b> </font>"""
    documento.append(Paragraph(texto, estilos['izquierda']))
    documento.append(Spacer(1, 20))
    
    print("---- ------DATOS LECTURAS -----------")    
    lecturas = json.loads(request.POST["form_data"]) # convierte a json
    #print lecturas

    contador = 0
    for i in lecturas:
        if contador == 0:
            if request.POST['dispositivo'] == "Todos":
                dispositivo     = Paragraph("""<font size=11>DISPOSITIVO</font>""", estilos['centrado'])            
            if request.POST['interfaz'] == "0":
                interfaz        = Paragraph("""<font size=11>INTERFAZ</font>""", estilos['centrado'])
            valor           = Paragraph("""<font size=11>TEMPERATURA</font>""", estilos['centrado'])
            fecha           = Paragraph("""<font size=11>FECHA</font>""", estilos['centrado'])
            
            if request.POST['dispositivo'] == "Todos":
                data= [[dispositivo,interfaz,valor,fecha]]
            else:
                if request.POST['interfaz'] == "0":
                    data = [[interfaz,valor,fecha]]
                else:
                    data= [[valor,fecha]]

            t=Table(data,style=[
                ('GRID',(0,0),(-1,-1),0.0,colors.gray)
                ],colWidths=[100,100])
            documento.append(t)
            documento.append(Spacer(1, 0))

            if request.POST['dispositivo'] == "Todos":
                dispositivo     = Paragraph("""<font size=10>"""+i['dsp']+"""</font>""", estilos['centrado'])
            if request.POST['interfaz'] == "0":
                interfaz        = Paragraph("""<font size=10>"""+i['int']+"""</font>""", estilos['centrado'])
            valor           = Paragraph("""<font size=10>"""+i['val']+"""</font>""", estilos['centrado'])
            fecha           = Paragraph("""<font size=10>"""+i['fec']+"""</font>""", estilos['centrado'])
            
            if request.POST['dispositivo'] == "Todos":
                data= [[dispositivo,interfaz,valor,fecha]]
            else:
                if request.POST['interfaz'] == "0":
                    data = [[interfaz,valor,fecha]]
                else:
                    data= [[valor,fecha]]

            t=Table(data,style=[
                ('GRID',(0,0),(-1,-1),0.0,colors.gray)
                ],colWidths=[100,100])
            
            documento.append(t)
            documento.append(Spacer(1, 0))

        else:
            if request.POST['dispositivo'] == "Todos":
                dispositivo     = Paragraph("""<font size=10>"""+i['dsp']+"""</font>""", estilos['centrado'])
            if request.POST['interfaz'] == "0":
                interfaz        = Paragraph("""<font size=10>"""+i['int']+"""</font>""", estilos['centrado'])
            valor           = Paragraph("""<font size=10>"""+i['val']+"""</font>""", estilos['centrado'])
            fecha           = Paragraph("""<font size=10>"""+i['fec']+"""</font>""", estilos['centrado'])
            
            if request.POST['dispositivo'] == "Todos":
                data= [[dispositivo,interfaz,valor,fecha]]
            else:
                if request.POST['interfaz'] == "0":
                    data = [[interfaz,valor,fecha]]
                else:
                    data= [[valor,fecha]]

            t=Table(data,style=[
                ('GRID',(0,0),(-1,-1),0.0,colors.gray)
                ],colWidths=[100,100])
            
            documento.append(t)
            documento.append(Spacer(1, 0))
        contador= contador +1
    
    print ("ESTOY SALIENDO Y CREO QUE TODO ESTA IMPRESISIMO----")
    
    doc.build(documento, onFirstPage = myFirstPage, onLaterPages = myLaterPages)    
    print ("---- FUERA DEL DIC----")
    return response

def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.line(30, A4[1] - 50, 560, A4[1] - 50)
    canvas.line(30, 0.80*inch, 560, 0.80*inch)
    canvas.drawString(55, A4[1] - 40, vEmpresa.emp_nombre_juridico)
    canvas.drawString(450, A4[1] - 40, vEmpresa.emp_ruc)
    canvas.drawString(450, 0.55 * inch, datetime.now().strftime("%Y-%m-%d"))
    canvas.drawString(535, 0.65 * inch, "| %d" %(doc.page))
    nuevoLogo = ImageReader('media/iotmach.png')
    canvas.drawImage(nuevoLogo, 30,0.55 * inch, width=15, height=15)

    canvas.restoreState()

def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.line(30, 0.80*inch, 560, 0.80*inch)
    canvas.line(30, A4[1] - 50, 560, A4[1] - 50)
    canvas.drawString(55, A4[1] - 40, vEmpresa.emp_nombre_juridico)
    canvas.drawString(450, A4[1] - 40, vEmpresa.emp_ruc)
    canvas.drawString(450, 0.55 * inch, datetime.now().strftime("%Y-%m-%d"))
    canvas.drawString(535, 0.65 * inch, "| %d" % (doc.page))
    

    nuevoLogo = ImageReader('media/iotmach.png')
    canvas.drawImage(nuevoLogo, 30,0.55 * inch, width=15, height=15)

    
