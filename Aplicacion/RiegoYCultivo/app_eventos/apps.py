from __future__ import unicode_literals

from django.apps import AppConfig


class EventosConfig(AppConfig):
    name = 'app_eventos'
    verbose_name = 'Eventos'
