from rest_framework import serializers
from .models import *


class m_modeloSerializer(serializers.ModelSerializer):

	class Meta:
		model = MModeloReglas
		fields = ('mod_id', 'mod_nombre', 'mod_descripcion', 'mod_estado', 'empresa')


class m_reglaSerializer(serializers.ModelSerializer):

	class Meta:
		model = MReglas
		fields = ('reg_id', 'reg_nombre', 'reg_estado', 'modelo')



			
		
class m_condicionSerializer(serializers.ModelSerializer):

	class Meta:
		model = MCondiciones
		fields = ('con_id', 'con_operador_logico',  'reg_id', 'condicion_parent')

class m_subruleSerializer(serializers.ModelSerializer):

	class Meta:
		model = MSubregla
		fields = ('sub_id', 'iot_id',  'sub_operador', 'sub_valor1', 'sub_valor2', "condicion")

