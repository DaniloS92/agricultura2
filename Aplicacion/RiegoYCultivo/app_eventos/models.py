from django.db import models
from iot.models import iot_sensor_actuador
from app_empresa.models import proy_persona, proy_empresa

class MModeloReglas(models.Model):
    mod_id = models.AutoField(primary_key = True)
    empresa = models.ForeignKey(proy_empresa, blank = False, null = False)
    mod_nombre = models.CharField(max_length=200, blank = False)
    mod_descripcion = models.TextField()
    mod_estado =  models.CharField(max_length=5)
    class Meta:
        managed = True
        db_table = 'm_modelo_reglas'
        verbose_name = 'Modelo Regla'
        verbose_name_plural = 'Modelos Reglas'


class MModeloAccion(models.Model):
    mod_id = models.AutoField(primary_key = True)
    empresa = models.ForeignKey(proy_empresa, blank = False, null = False)
    mod_nombre = models.CharField(max_length=200, blank = False)
    mod_descripcion = models.TextField()
    mod_estado =  models.CharField(max_length=5)
    class Meta:
        managed = True
        db_table = 'm_modelo_accion'


class MReglas(models.Model):
    reg_id = models.AutoField(primary_key=True)
    reg_nombre = models.TextField(blank=True)
    reg_estado = models.NullBooleanField()
    reg_estructura = models.TextField(blank=True)
    reg_datos_acc = models.TextField(blank=True)
    reg_datos_not = models.TextField(blank=True)
    reg_tipo=models.TextField(blank=True)
    mod = models.ForeignKey(MModeloReglas, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'm_reglas'
        verbose_name = 'Regla'
        verbose_name_plural = 'Reglas'


class MEvento(models.Model):
    ev_id = models.AutoField(primary_key=True)
    ev_fecha_hora = models.DateTimeField(blank=True, null=True)
    ev_nombre = models.CharField(max_length=50,blank=True)
    ev_descripcion = models.CharField(max_length=50,blank=True)
    reg = models.ForeignKey(MReglas, blank=True, null=True)
    class Meta:
        default_permissions = ()
        managed = True
        db_table = 'm_evento'
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'

class MAccion(models.Model):
    acc_id = models.AutoField("ID",primary_key=True)
    acc_nombre = models.CharField("Nombre",max_length=100)
    acc_fecha_inicio = models.DateField("Fecha inicio",blank=True, null=True)
    acc_fecha_fin = models.DateField("Fecha fin",blank=True, null=True)
    acc_descripcion = models.CharField("Descripcion",max_length=200,blank=True)
    acc_estado = models.CharField("Estado",max_length=50,blank=True)
    acc_interval_ejec = models.IntegerField("Intervalo",blank=True,null=True)
    acc_iejt = models.CharField(max_length=100,blank=True, null=True)
    acc_interval_espera = models.IntegerField(blank=True, null=True)
    acc_iest= models.CharField(max_length=100,blank=True, null=True)
    acc_num_intervalo = models.IntegerField(blank=True, null=True)
    acc_nint= models.CharField(max_length=100,blank=True, null=True)
    acc_hora_inicio = models.TimeField("Hora inicio",blank=True, null=True)
    acc_hora_fin = models.TimeField("Hora fin",blank=True, null=True)
    acc_tipo = models.CharField(max_length=50,blank=True, null=True)
    ev = models.ForeignKey(MEvento, blank=True, null=True)
    mod = models.ForeignKey(MModeloAccion, blank=True, null=True)
    class Meta:
        default_permissions = ()
        managed = True
        db_table = 'm_accion'
        verbose_name = 'Accion'
        verbose_name_plural = 'Acciones'

class MAccionActuador(models.Model):
    acc_act_id = models.AutoField("Id",primary_key=True)
    acc = models.ForeignKey(MAccion, blank=True, null=True)
    iot = models.ForeignKey(iot_sensor_actuador, blank=True, null=True)
    acc_valor = models.IntegerField(blank=True, null=True)
    class Meta:
        default_permissions = ()
        managed = True
        db_table = 'm_accion_actuador'
        verbose_name = 'Accion Actuador'
        verbose_name_plural = 'Acciones Actuadores'


class MMensajeAlerta(models.Model):
    men_id = models.AutoField(primary_key=True)
    men_asunto = models.TextField(blank=True)
    men_texto = models.TextField(blank=True)
    men_estado = models.TextField(blank=True)
    ev = models.ForeignKey(MEvento, blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'm_mensaje_alerta'
        verbose_name = 'Mensaje de Alerta'
        verbose_name_plural = 'Mensajes de Alertas'


class MHistorialMensajes(models.Model):
    hm_id = models.AutoField(primary_key=True)
    hm_fecha = models.DateTimeField(blank=True, null=True)
    hm_asunto = models.TextField(blank=True)
    hm_mensaje = models.TextField(blank=True)
    hm_destinatario = models.TextField(blank=True)
    hm_tipo_envio = models.TextField(blank=True)
    men = models.ForeignKey(MMensajeAlerta, blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'm_historial_mensajes'
        verbose_name = 'Historial de Mensaje'
        verbose_name_plural = 'Historial de Mensajes'


class MPaqueteControl(models.Model):
    pc_id = models.AutoField(primary_key=True)
    pc_paquete = models.TextField(blank=True, null=True)
    pc_fecha = models.DateField(blank=True, null=True)
    pc_hora = models.TimeField(blank=True, null=True)
    pc_estado = models.TextField(blank=True, null=True)
    pc_mac = models.TextField(blank=True)
    pc_datos = models.TextField(blank=True)
    pc_datos_not = models.TextField(blank=True)
    pc_time_out = models.IntegerField(blank=True, null=True)
    acc_est_act = models.CharField(max_length=100)
    acc = models.ForeignKey(MAccion, blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'm_paquete_control'
        verbose_name = 'Paquete de Control'
        verbose_name_plural = 'Paquetes de controles'



class MCondiciones(models.Model):
    con_id = models.AutoField(primary_key=True)
    con_orden = models.BigIntegerField(blank=True, null=True)
    con_operador = models.TextField(blank=True, null=True)
    con_valor1 = models.FloatField(blank=True, null=True)
    con_valor2 = models.FloatField(blank=True,null=True)
    con_operador_logico = models.TextField(blank=True, null=True)
    con_valor_actuador = models.TextField(blank=True, null=True)
    con_datos = models.TextField(blank=True, null=True)
    con_datos_not = models.TextField(blank=True, null=True)
    iot = models.ForeignKey(iot_sensor_actuador, blank=False, null=False)
    reg = models.ForeignKey(MReglas, blank=True, null=True)
    condicion_parent = models.ForeignKey('self', blank = True, null = True)
    class Meta:
        managed = True
        db_table = 'm_condiciones'
        verbose_name = 'Condicion'
        verbose_name_plural = 'Condiciones'



class MSubregla(models.Model):

    sub_id = models.AutoField(primary_key = True)
    iot_id = models.ForeignKey(iot_sensor_actuador, blank = False, null = False)
    sub_operador = models.CharField(max_length=20, blank = False, null = False)
    sub_valor1 = models.CharField(max_length=50, blank = True, null = True)
    sub_valor2 = models.CharField(max_length=50, blank = True, null = True)
    condicion = models.ForeignKey(MCondiciones, blank = False, null = False)
    class Meta:
        """docstring for Meta"""
        db_table = 'm_subrule'
        verbose_name = 'Subregla'
        verbose_name_plural = 'Subreglas'
        default_permissions = ()
        permissions = ()


class MensajeDestinatario(models.Model):
    per = models.ForeignKey(proy_persona, blank=True, null=True)
    men = models.ForeignKey(MMensajeAlerta, blank=True, null=True)
    md_estado = models.TextField(blank=True)
    md_destino = models.CharField( max_length=50)
    md_tipo = models.CharField( max_length=5, blank=True, null=True)
    class Meta:
        managed = True
        db_table = 'm_mensaje_destinatario'
        verbose_name = 'Mensaje Detinatario'
        verbose_name_plural = 'Mensajes Destinatarios'



    def __unicode__(self):
        return str(self.iot_id.sa_interfaz)+'-'+str(self.acc_valor)

