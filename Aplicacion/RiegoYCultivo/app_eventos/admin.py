from django.contrib import admin
from app_eventos.models import *




class MEventoAdmin(admin.ModelAdmin):
    list_display = ('ev_id', 'ev_fecha_hora', 'ev_nombre','ev_descripcion','reg')
    list_filter= ['ev_fecha_hora']
    search_fields = ['ev_nombre']
    list_per_page = 10


class MAccionActuadorAdmin(admin.ModelAdmin):
    list_display = ('acc_act_id', 'acc', 'iot','acc_valor')
    list_filter= ['acc']
    search_fields = ['acc']
    list_per_page = 10

class MAccionAdmin(admin.ModelAdmin):
    list_display = ('acc_id', 'acc_nombre', 'acc_fecha_inicio','acc_fecha_fin','acc_descripcion','acc_estado','acc_interval_ejec','acc_hora_inicio','acc_hora_fin')
    list_filter= ['acc_nombre']
    search_fields = ['acc_nombre']
    list_per_page = 10




admin.site.register(MEvento, MEventoAdmin)
admin.site.register(MAccion, MAccionAdmin)
admin.site.register(MAccionActuador, MAccionActuadorAdmin)