from app_eventos.models import *
from iot.models import iot_dispositivo, iot_sensor_actuador
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json
from django.contrib import messages
from django.shortcuts import render_to_response, RequestContext, HttpResponse, redirect
from app_empresa.models import proy_persona
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from .models import *
from django.http import JsonResponse
from rest_framework import status
from django.shortcuts import render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect, render,Http404
from iot.models import  *
from app_empresa.models import *
import json


import random
from datetime import datetime, time,date,timedelta
import calendar

def acciones(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id
	acciones=MAccion.objects.filter(mod=modelo_id)
	eventos = MEvento.objects.all()
	dic={
		'reglas': MReglas.objects.all(),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'dispositivo':iot_dispositivo.objects.filter(emp_id=empresa_id),
		'sensor_actuador':iot_sensor_actuador.objects.all(),
		'personas':proy_persona.objects.filter(emp_id=empresa_id),
		'accion_actuador':MAccionActuador.objects.all(),
		'modelo': MModeloReglas.objects.all(),
		'paquetes':MPaqueteControl.objects.all()
	}
	return render_to_response('app_eventos/templateAccionesPlanificadas.html', dic,context_instance=RequestContext(request))

def accionesDiarias(request):
	acciones=MAccion.objects.all()
	eventos = MEvento.objects.all()
	dic={
		'reglas': MReglas.objects.all(),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'dispositivo':iot_dispositivo.objects.all(),
		'sensor_actuador':iot_sensor_actuador.objects.all(),
		'personas':proy_persona.objects.all(),
		'accion_actuador':MAccionActuador.objects.all(),
		'paquetes':MPaqueteControl.objects.all()
	}
	return render_to_response('app_eventos/templateAccionesDiarias.html', dic,context_instance=RequestContext(request))


def acciones_repetitivas(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id
	acciones=MAccion.objects.filter(mod=modelo_id)
	eventos = MEvento.objects.all()

	dic={
		'reglas': MReglas.objects.all(),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'dispositivo':iot_dispositivo.objects.filter(emp_id=empresa_id),
		'sensor_actuador':iot_sensor_actuador.objects.all(),
		'personas':proy_persona.objects.filter(emp_id=empresa_id),
		'accion_actuador':MAccionActuador.objects.all(),
		'paquetes':MPaqueteControl.objects.all()
	}
	return render_to_response('app_eventos/template_repetitiva.html', dic,context_instance=RequestContext(request))

#MUESTRA LA PAGINA DE REGLAS Y ENVIA LOS MODELS AL TEMPLATE
def acciones_reglas(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	acciones=MAccion.objects.all()
	eventos = MEvento.objects.all()
	dispositivo = iot_dispositivo.objects.filter(emp_id=empresa_id)
	sensor_actuador = iot_sensor_actuador.objects.all()
	modelo_id = MModeloReglas.objects.get(empresa=empresa_id).mod_id
	dic={
		'reglas': MReglas.objects.filter(mod=modelo_id),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'modelo_id':modelo_id,
		'dispositivo':dispositivo,
		'sensor_actuador':sensor_actuador,
		'personas':proy_persona.objects.filter(emp_id=empresa_id),
		'accion_actuador':MAccionActuador.objects.all()
	}
	return render_to_response('app_eventos/templateReglas.html', dic,context_instance=RequestContext(request))


def eliminar_regla(request, id_regla):
	regla = MReglas.objects.get(reg_id=id_regla)
	regla.delete()
	messages.add_message(request, messages.SUCCESS, 'Regla Eliminada Satisfactoriamente')

	return redirect('/reglas/acciones_reglas')

def eliminar_historial(request, id_historial):
	paquete = MPaqueteControl.objects.get(pc_id=id_historial)
	paquete.delete()
	messages.add_message(request, messages.SUCCESS, 'Paquete Eliminado Satisfactoriamente')

	return redirect('/reglas/monitoreo_paquetes/')


def reenviar(request, id_paquete):
	obj = MPaqueteControl.objects.get(pc_id=id_paquete)
	today = datetime.now()+timedelta(minutes=1)
	obj.pc_fecha = today.strftime("%Y-%m-%d")
	#obj.pc_hora =  date.today("%H:%M:%S")+timedelta(minutes=1
	obj.pc_hora =  today.strftime("%H:%M:%S")
	obj.pc_time_out=0
	obj.pc_estado = "Pendiente"
	obj.save()
	messages.add_message(request, messages.SUCCESS, 'Paquetes Reenviado')
	return redirect('/reglas/monitoreo_paquetes/')


def eliminar_accionPlanificada(request, id_accion):
	accion = MAccion.objects.get(acc_id=id_accion)
	evento = MEvento.objects.get(ev_id=accion.ev_id)
	mensajealerta = MMensajeAlerta.objects.get(ev=evento.ev_id)
	accion.delete()
	evento.delete()
	mensajealerta.delete()


	messages.add_message(request, messages.SUCCESS, 'Accion Eliminada Satisfactoriamente')
	return redirect('/reglas/acciones/')

def eliminar_accionRepetitiva(request, id_accion):
	accion = MAccion.objects.get(acc_id=id_accion)
	evento = MEvento.objects.get(ev_id=accion.ev_id)
	mensajealerta = MMensajeAlerta.objects.get(ev=evento.ev_id)
	accion.delete()
	evento.delete()
	mensajealerta.delete()
	messages.add_message(request, messages.SUCCESS, 'Accion Eliminada Satisfactoriamente')
	return redirect('/reglas/acciones_repetitivas/')


#MUESTRA LA PAGINA DE MONITOREO Y CONTROL DE PAQUETES
def monitoreo_paquetes(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id
	acciones=MAccion.objects.filter(mod=modelo_id)
	eventos = MEvento.objects.all()
	dic={
		'reglas': MReglas.objects.all(),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'dispositivo':iot_dispositivo.objects.all(),
		'sensor_actuador':iot_sensor_actuador.objects.all(),
		'personas':proy_persona.objects.all(),
		'accion_actuador':MAccionActuador.objects.all(),
		'paquetes':MPaqueteControl.objects.all().order_by('pc_id')
	}
	return render_to_response('app_eventos/template_monitoreo_paquetes.html', dic,context_instance=RequestContext(request))


def historial_paquetes(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id
	print modelo_id
	acciones=MAccion.objects.filter(mod=modelo_id)
	print acciones
	eventos = MEvento.objects.all()
	dic={
		'reglas': MReglas.objects.all(),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'dispositivo':iot_dispositivo.objects.all(),
		'sensor_actuador':iot_sensor_actuador.objects.all(),
		'personas':proy_persona.objects.all(),
		'accion_actuador':MAccionActuador.objects.all(),
		'paquetes':MPaqueteControl.objects.all().order_by('pc_id')
	}
	return render_to_response('app_eventos/template_historial_paquetes.html', dic,context_instance=RequestContext(request))



def cargar(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id
	acciones=MAccion.objects.filter(mod=modelo_id)
	eventos = MEvento.objects.all()
	dic={
		'reglas': MReglas.objects.all(),
		'condiciones': MCondiciones.objects.all(),
		'mensajeAlerta': MMensajeAlerta.objects.all(),
		'mendajeDestinatario': MensajeDestinatario.objects.all(),
		'acciones':acciones,
		'eventos':eventos,
		'dispositivo':iot_dispositivo.objects.all(),
		'sensor_actuador':iot_sensor_actuador.objects.all(),
		'personas':proy_persona.objects.all(),
		'accion_actuador':MAccionActuador.objects.all(),
		'paquetes':MPaqueteControl.objects.all()
	}
	return render_to_response('app_eventos/cargar.html', dic,context_instance=RequestContext(request))


def cargar_lista_sensores(request):
	datos = {}
	nombre_dispositivo = int(request.GET.get("id"))
	lista_sensores = iot_sensor_actuador.objects.filter(dis_id = nombre_dispositivo, sa_categoria = "s")
	datos["tam"] = len(lista_sensores)
	contador = 0
	for i in lista_sensores:
		datos["id"+str(contador)]=i.iot_id
		datos["nombre"+str(contador)]=i.ted_id.ted_name
		contador+=1
	return HttpResponse (json.dumps(datos),content_type="application/json")



def cargar_lista_actuadores(request):
	datos = {}
	nombre_dispositivo = int(request.GET.get("id"))

	lista_actuadores = iot_sensor_actuador.objects.filter(dis_id = nombre_dispositivo, sa_categoria = "a")
	datos["tam"] = len(lista_actuadores)
	contador = 0
	for i in lista_actuadores:
		datos["id"+str(contador)]=i.iot_id
		datos["nombre"+str(contador)]=i.ted_id.ted_name
		contador+=1

	return HttpResponse (json.dumps(datos),content_type="application/json")


#GUARDA LOS DATOS DEL FORMULARIO DE EL TEMPLATE REGLAS
@login_required(login_url='/')
def agregar_acciones(request):
	'''Agregar parametros a la Base de datos
	Redirecciona a la pagina de parametros de cultivo
	:param request: Informacion de la pagina de donde viene la peticion
	:return Redicciona a la pagina de parametros de cultivo
	:exception Si existe un error al momento de guardar
'''
	if request.user.is_authenticated():
		if request.method == "POST":
			try:
				id = request.POST["id"]
				reg_nombre = request.POST["txt_nombre"]
				reg_descrip = request.POST["txt_descripcion"]
				id_dispositivo = request.POST["select_dispositivo"]
				mac=iot_dispositivo.objects.get(dis_id=id_dispositivo).dis_mac
				id_sensores = request.POST.getlist('select_sensores')
				reg_condicion = request.POST["select_condicion"]
				reg_valor1 = request.POST["txt_valor1"]
				reg_valor2= request.POST["txt_valor2"]
				id_actuadores = request.POST.getlist('select_actuadores')
				reg_act_estado = request.POST.getlist('check_estado')
				estado_num = 1
				estado_txt = ""
				if(reg_act_estado):
					estado_txt="On"
					estado_num=1
				else:
					estado_txt="Off"
					estado_num=0

				reg_notificacion = request.POST['select_notificacion']
				txt_asunto =request.POST["txt_asunto"]
				lista_usuarios = request.POST.getlist('select_usuarios')
				txt_mensaje = request.POST["txt_mensaje"]
				if request.user.is_authenticated():
					per_id = request.user.id_persona_id
				empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
				modelo_id = MModeloReglas.objects.get(empresa=empresa_id).mod_id
				hola=2
				if (hola == 2):
					if(id != ""):
						reglass = MReglas.objects.get(reg_id=id)
						print ("Veracidad "+str(reglass.reg_id))
						reglass.delete()

					random_numero = random.randint(1,65534) # returns a random integer

					datos_actuadores = "{"
					for i in id_actuadores:
						consulta_interfaces = iot_sensor_actuador.objects.get(iot_id=i).sa_interfaz
						datos_actuadores = datos_actuadores + '"interfaz": "'+consulta_interfaces+'", "valor": '+str(estado_num)+'},{'

					json_regla = '{"paquete_id":"'+str(random_numero)+'", "mac":"'+mac+'", "datos": ['+datos_actuadores+'],"fecha": '

					correos_elec = ""

					for i in lista_usuarios:
						correos_elec = correos_elec + i + ","

					json_mensaje = 'k8s_e_212343s = { "usuarios": "'+ correos_elec + '", "asunto": "'+txt_asunto+'", "mensaje": "'+txt_mensaje+'" }'

					if(reg_notificacion == "email"):
						guardarRegla = MReglas(mod_id=modelo_id, reg_nombre=reg_nombre, reg_tipo="Simple", reg_estado=True, reg_estructura="Simple", reg_datos_acc=json_regla, reg_datos_not=json_mensaje)
						guardarRegla.save()
					else:
						guardarRegla = MReglas(mod_id=modelo_id, reg_nombre=reg_nombre, reg_tipo="Simple", reg_estado=True, reg_estructura="Simple", reg_datos_acc=json_regla)
						guardarRegla.save()


					for i in id_sensores:
						if(reg_valor2==""):
							guardarCondicion = MCondiciones(con_orden=1, con_operador=reg_condicion, con_valor1=float(reg_valor1), con_operador_logico="1", con_datos="1", con_datos_not="1", con_valor_actuador=estado_txt, iot_id=i, reg_id=(MReglas.objects.last().reg_id))
							guardarCondicion.save()
						else:
							guardarCondicion = MCondiciones(con_orden=1, con_operador=reg_condicion, con_valor1=float(reg_valor1), con_valor2=float(reg_valor2), con_valor_actuador=estado_txt, iot_id=i, reg_id=(MReglas.objects.last().reg_id))
							guardarCondicion.save()
					guardarEvento = MEvento(ev_fecha_hora=datetime.now(), ev_nombre="Evento de Regla Simple ", ev_descripcion = reg_descrip, reg_id=(MReglas.objects.last()).reg_id)
					guardarEvento.save()

					guardarAccion = MAccion(acc_nombre=reg_nombre, acc_descripcion=reg_descrip, acc_estado="Activado", acc_tipo="Regla_Simple", ev_id=(MEvento.objects.last()).ev_id)
					guardarAccion.save()

					for i in id_actuadores:
						guardarAccionActuador = MAccionActuador(acc_id=(MAccion.objects.last()).acc_id, iot_id=i, acc_valor=estado_num)
						guardarAccionActuador.save()
					reg_notificacion = "email"
					if(reg_notificacion == "email"):
						if(txt_asunto==""):
							txt_asunto="sin asunto"
							txt_mensaje="sin mensaje"

						guardarMensajeAlerta = MMensajeAlerta(men_asunto=txt_asunto, men_texto=txt_mensaje, men_estado="Activos", ev_id=(MEvento.objects.last()).ev_id)
						guardarMensajeAlerta.save()
						for i in lista_usuarios:
								id_persona = proy_persona.objects.get(per_email=i).per_id
								guardarMensajeDestinatario = MensajeDestinatario(per_id=id_persona, men_id= (MMensajeAlerta.objects.last().men_id), md_estado="Activado")
								guardarMensajeDestinatario.save()

					if(id != ""):
					   messages.add_message(request, messages.SUCCESS, 'Regla Actualizada Satisfactoriamente')
					else:
					   messages.add_message(request, messages.SUCCESS, 'Regla Guardada Satisfactoriamente')



			except Exception as e:
				messages.add_message(request, messages.ERROR, e.message)
			return redirect('/reglas/acciones_reglas')
	else:
		return redirect('/')





def agregar_acciones_repetitivas(request):
	if request.user.is_authenticated():
		if request.method == "POST":
			try:
				id = request.POST["id"]

				plan_nombre = request.POST["txt_nombre"]
				plan_descrip = request.POST["txt_descripcion"]
				id_dispositivo = request.POST["select_dispositivo"]
				mac=iot_dispositivo.objects.get(dis_id=id_dispositivo).dis_mac
				id_actuadores = request.POST.getlist('select_actuadores')

				numero_repeticiones = request.POST["txt_interval_rep"]

				#RECOGIDA DE FECHA FORMATO UNICODE
				hora_inicio_recogida = request.POST["txt_hora_inicio"]

				##SEPARANDO FECCHA Y HORA DE INICIO STR
				hora_inicio = datetime.strptime(hora_inicio_recogida, "%Y-%m-%dT%H:%M").strftime("%H:%M:%S")
				fecha_inicio = datetime.strptime(hora_inicio_recogida, "%Y-%m-%dT%H:%M").strftime("%Y-%m-%d")

				#FECHAS FORMATO DATETIME
				hora_inicio_recogidaDT=datetime.strptime(hora_inicio_recogida,'%Y-%m-%dT%H:%M')
				hora_inicioDT=datetime.strptime(hora_inicio,"%H:%M:%S")
				fecha_inicioDT=datetime.strptime(fecha_inicio,"%Y-%m-%d")

				inter_ejec = request.POST["txt_interval_ejec"]
				tiempo_inter_ejec = request.POST["select_time_iej"]
				inter_esper = request.POST["txt_interval_espera"]
				tiempo_inter_esper = request.POST["select_time_ies"]
				reg_notificacion = request.POST['select_notificacion']
				txt_asunto =request.POST["txt_asunto"]
				lista_usuarios = request.POST.getlist('select_usuarios')
				txt_mensaje = request.POST["txt_mensaje"]
				if request.user.is_authenticated():
					per_id = request.user.id_persona_id
				empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
				modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id

				hola=2
				if (hola == 2):
						if(id != ""):
							print("hola")
							accion = MAccion.objects.get(acc_id=id)
							evento = MEvento.objects.get(ev_id=accion.ev_id)
							mensajealerta = MMensajeAlerta.objects.get(ev=evento.ev_id)
							accion.delete()
							evento.delete()
							mensajealerta.delete()


						guardarEvento = MEvento(ev_fecha_hora=datetime.now(), ev_nombre=plan_nombre, ev_descripcion = plan_descrip)
						guardarEvento.save()

						guardarAccion = MAccion(mod_id=modelo_id, acc_nombre=plan_nombre,acc_fecha_inicio=datetime.now(), acc_descripcion = plan_descrip, acc_estado="Activado", acc_interval_ejec= inter_ejec, acc_iejt=tiempo_inter_ejec, acc_interval_espera=inter_esper, acc_num_intervalo=int(numero_repeticiones), acc_iest=tiempo_inter_esper, acc_hora_inicio=hora_inicioDT, acc_tipo = "Repetitiva", ev_id=(MEvento.objects.last()).ev_id)
						guardarAccion.save()

						for i in id_actuadores:
							guardarAccionActuador = MAccionActuador(acc_id=(MAccion.objects.last()).acc_id, iot_id=i, acc_valor=1)
							guardarAccionActuador.save()
						reg_notificacions = "email"
						if(reg_notificacions == "email"):
							if(txt_asunto==""):
								txt_asunto="sin asunto"
								txt_mensaje="sin mensaje"

							guardarMensajeAlerta = MMensajeAlerta(men_asunto=txt_asunto, men_texto=txt_mensaje, men_estado="Activos", ev_id=(MEvento.objects.last()).ev_id)
							guardarMensajeAlerta.save()
							for i in lista_usuarios:
								id_persona = proy_persona.objects.get(per_email=i).per_id
								guardarMensajeDestinatario = MensajeDestinatario(per_id=id_persona, men_id= (MMensajeAlerta.objects.last().men_id), md_estado="Activado")
								guardarMensajeDestinatario.save()

						#REGAS CADA #DIAS DURANTE #HORAS
						contador = 0
						repeticion = int(numero_repeticiones)
						x = 0
						while contador < repeticion:
								random_numero=random.randint(1,65534)

								if(tiempo_inter_ejec == "Minutos"):
									hora_inicio_recogidaDT += timedelta(minutes=x)
								if(tiempo_inter_ejec == "Horas"):
									hora_inicio_recogidaDT += timedelta(hours=x)
								if(tiempo_inter_ejec == "Dias"):
									hora_inicio_recogidaDT += timedelta(days=x)
								txt_mensajes=""
								txt_mensajes2=""

								if(contador < repeticion):
									print("encender: "+str(hora_inicio_recogidaDT))
									datos_actuadores = "{"
									for i in id_actuadores:
										consulta_interfaces = iot_sensor_actuador.objects.get(iot_id=i).sa_interfaz
										datos_actuadores = datos_actuadores + '"interfaz": "'+consulta_interfaces+'", "valor": 1},{'
									json_regla = '{"paquete_id": "'+str(random_numero)+'", "mac":"'+mac+'", "datos": ['+datos_actuadores+'],"fecha": '
									correos_elec = ""
									for i in lista_usuarios:
										correos_elec = correos_elec + i + ","
									if(reg_notificacion == "email"):
										txt_mensajes = "ESTADO DEL ACTUADOR: ENCENDIDO   ;  " + txt_mensaje
										json_mensaje = 'k8s_e_212343s = { "usuarios": "'+ correos_elec + '", "asunto": "'+txt_asunto+'", "mensaje": "'+txt_mensajes+'" }'
									else:
										json_mensaje=""
									gurdar_paquete=MPaqueteControl(pc_paquete=random_numero, pc_fecha=datetime.strftime(hora_inicio_recogidaDT,"%Y-%m-%d"), pc_hora=datetime.strftime(hora_inicio_recogidaDT,"%H:%M:%S"), pc_estado="Pendiente", pc_mac=mac, pc_datos=json_regla, pc_datos_not=json_mensaje, acc_id=(MAccion.objects.last()).acc_id, acc_est_act="ON", pc_time_out=0)
									gurdar_paquete.save()
									if(tiempo_inter_esper == "Minutos"):
										hora_inicio_recogidaDT += timedelta(minutes=int(inter_esper))
									if(tiempo_inter_esper == "Horas"):
										hora_inicio_recogidaDT += timedelta(hours=int(inter_esper))
									if(tiempo_inter_esper == "Dias"):
										hora_inicio_recogidaDT += timedelta(days=int(inter_esper))

									print("apagar  : "+str(hora_inicio_recogidaDT))
									random_numero2=random.randint(1,65534)
									datos_actuadores = "{"
									for i in id_actuadores:
										consulta_interfaces = iot_sensor_actuador.objects.get(iot_id=i).sa_interfaz
										datos_actuadores = datos_actuadores + '"interfaz": "'+consulta_interfaces+'", "valor": 0},{'
									json_regla = '{"paquete_id": "'+str(random_numero2)+'", "mac":"'+mac+'", "datos": ['+datos_actuadores+'],"fecha": '
									if(reg_notificacion == "email"):
										txt_mensajes2 = "ESTADO DEL ACTUADOR: APAGADO ;   " + txt_mensaje
										json_mensaje = 'k8s_e_212343s = { "usuarios": "'+ correos_elec + '", "asunto": "'+txt_asunto+'", "mensaje": "'+txt_mensajes2+'" }'
									else:
										json_mensaje=""
									gurdar_paquete=MPaqueteControl(pc_paquete=random_numero2, pc_fecha=datetime.strftime(hora_inicio_recogidaDT,"%Y-%m-%d"), pc_hora=datetime.strftime(hora_inicio_recogidaDT,"%H:%M:%S"), pc_estado="Pendiente", pc_mac=mac, pc_datos=json_regla, pc_datos_not=json_mensaje, acc_id=(MAccion.objects.last()).acc_id, acc_est_act="OFF", pc_time_out=0)
									gurdar_paquete.save()
									x = int(inter_ejec)
									contador += 1
						if(id != ""):
							messages.add_message(request, messages.SUCCESS, 'Accion Actualizada Satisfactoriamente')
						else:
							messages.add_message(request, messages.SUCCESS, 'Accion Guardada Satisfactoriamente')



			except Exception as e:
				messages.add_message(request, messages.ERROR, e.message)
			return redirect('/reglas/acciones_repetitivas')
	else:
		return redirect('/')
















#GUARDA EL FORMULARIO DE ACCIONES PLANIFICADAS RUTA /REGLAS/ACCIONES
@login_required(login_url='/')
def agregar_acciones_planificadas(request):

	if request.user.is_authenticated():
		if request.method == "POST":
			try:
				id = request.POST["id"]
				plan_nombre = request.POST["txt_nombre"]
				plan_descrip = request.POST["txt_descripcion"]
				id_dispositivo = request.POST["select_dispositivo"]
				mac=iot_dispositivo.objects.get(dis_id=id_dispositivo).dis_mac
				id_actuadores = request.POST.getlist('select_actuadores')

				#RECOGIDA DE FECHA FORMATO UNICODE
				hora_inicio_recogida = request.POST["txt_hora_inicio"]
				hora_fin_recogida = request.POST["txt_hora_fin"]

				##SEPARANDO FECCHA Y HORA DE INICIO STR
				hora_inicio = datetime.strptime(hora_inicio_recogida, "%Y-%m-%dT%H:%M").strftime("%H:%M:%S")
				fecha_inicio = datetime.strptime(hora_inicio_recogida, "%Y-%m-%dT%H:%M").strftime("%Y-%m-%d")

				##SEPARANDO FECHA Y HORA FIN STR
				hora_fin = datetime.strptime(hora_fin_recogida, "%Y-%m-%dT%H:%M").strftime("%H:%M:%S")
				fecha_fin = datetime.strptime(hora_fin_recogida, "%Y-%m-%dT%H:%M").strftime("%Y-%m-%d")

				#FECHAS FORMATO DATETIME
				hora_inicio_recogidaDT=datetime.strptime(hora_inicio_recogida,'%Y-%m-%dT%H:%M')
				hora_fin_recogidaDT=datetime.strptime(hora_fin_recogida,'%Y-%m-%dT%H:%M')
				hora_inicioDT=datetime.strptime(hora_inicio,"%H:%M:%S")
				fecha_inicioDT=datetime.strptime(fecha_inicio,"%Y-%m-%d")
				hora_finDT=datetime.strptime(hora_fin,"%H:%M:%S")
				fecha_finDT=datetime.strptime(fecha_fin,"%Y-%m-%d")




				inter_ejec = request.POST["txt_interval_ejec"]
				tiempo_inter_ejec = request.POST["select_time_iej"]
				inter_esper = request.POST["txt_interval_espera"]
				tiempo_inter_esper = request.POST["select_time_ies"]
				reg_notificacion = request.POST['select_notificacion']
				txt_asunto =request.POST["txt_asunto"]
				lista_usuarios = request.POST.getlist('select_usuarios')
				txt_mensaje = request.POST["txt_mensaje"]
				if request.user.is_authenticated():
					per_id = request.user.id_persona_id
				empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
				modelo_id = MModeloAccion.objects.get(empresa=empresa_id).mod_id





				hola=2
				if (hola == 2):
						if(id != ""):
							accion = MAccion.objects.get(acc_id=id)
							evento = MEvento.objects.get(ev_id=accion.ev_id)
							mensajealerta = MMensajeAlerta.objects.get(ev=evento.ev_id)
							accion.delete()
							evento.delete()
							mensajealerta.delete()

						guardarEvento = MEvento(ev_fecha_hora=datetime.now(), ev_nombre=plan_nombre, ev_descripcion = plan_descrip)
						guardarEvento.save()

						guardarAccion = MAccion(mod_id=modelo_id, acc_nombre=plan_nombre,acc_fecha_inicio=fecha_inicioDT, acc_fecha_fin=fecha_finDT, acc_descripcion = plan_descrip, acc_estado="Activado", acc_interval_ejec= inter_ejec, acc_iejt=tiempo_inter_ejec, acc_interval_espera=inter_esper, acc_iest=tiempo_inter_esper, acc_hora_inicio=hora_inicioDT, acc_hora_fin=hora_finDT, acc_tipo = "Planificada", ev_id=(MEvento.objects.last()).ev_id)
						guardarAccion.save()

						for i in id_actuadores:
							guardarAccionActuador = MAccionActuador(acc_id=(MAccion.objects.last()).acc_id, iot_id=i, acc_valor=1)
							guardarAccionActuador.save()
						reg_notificacions = "email"
						if(reg_notificacions == "email"):
							if(txt_asunto==""):
								txt_asunto="sin asunto"
								txt_mensaje="sin mensaje"
							guardarMensajeAlerta = MMensajeAlerta(men_asunto=txt_asunto, men_texto=txt_mensaje, men_estado="Activos", ev_id=(MEvento.objects.last()).ev_id)
							guardarMensajeAlerta.save()
							for i in lista_usuarios:
									id_persona = proy_persona.objects.get(per_email=i).per_id
									guardarMensajeDestinatario = MensajeDestinatario(per_id=id_persona, men_id= (MMensajeAlerta.objects.last().men_id), md_estado="Activado")
									guardarMensajeDestinatario.save()


						days_btw_dats=(hora_fin_recogidaDT-hora_inicio_recogidaDT).days
						print("diferencia"+str(days_btw_dats))

						#REGAS CADA #DIAS DURANTE #HORAS

						x = 0
						while hora_inicio_recogidaDT <= hora_fin_recogidaDT:
								random_numero=random.randint(1,65534)

								if(tiempo_inter_ejec == "Minutos"):
									hora_inicio_recogidaDT += timedelta(minutes=x)
								if(tiempo_inter_ejec == "Horas"):
									hora_inicio_recogidaDT += timedelta(hours=x)
								if(tiempo_inter_ejec == "Dias"):
									hora_inicio_recogidaDT += timedelta(days=x)
								txt_mensajes = ""
								txt_mensajes2 = ""

								if(hora_inicio_recogidaDT <= hora_fin_recogidaDT):
									print("encender: "+str(hora_inicio_recogidaDT))
									datos_actuadores = "{"
									for i in id_actuadores:
										consulta_interfaces = iot_sensor_actuador.objects.get(iot_id=i).sa_interfaz
										datos_actuadores = datos_actuadores + '"interfaz": "'+consulta_interfaces+'", "valor": 1},{'
									json_regla = '{ "paquete_id": "'+str(random_numero)+'", "mac":"'+mac+'", "datos": ['+datos_actuadores+'],"fecha": '
									correos_elec = ""
									for i in lista_usuarios:
										correos_elec = correos_elec + i + ","
									if(reg_notificacion == "email"):
										txt_mensajes = "ESTADO DEL ACTUADOR: ENCENDIDO   ;   " + txt_mensaje
										json_mensaje = 'k8s_e_212343s = { "usuarios": "'+ correos_elec + '", "asunto": "'+txt_asunto+'", "mensaje": "'+txt_mensajes+'" }'
									else:
										json_mensaje=""
									gurdar_paquete=MPaqueteControl(pc_paquete=random_numero, pc_fecha=datetime.strftime(hora_inicio_recogidaDT,"%Y-%m-%d"), pc_hora=datetime.strftime(hora_inicio_recogidaDT,"%H:%M:%S"), pc_estado="Pendiente", pc_mac=mac, pc_datos=json_regla, pc_datos_not=json_mensaje, acc_id=(MAccion.objects.last()).acc_id, acc_est_act="ON", pc_time_out=0)
									gurdar_paquete.save()
									if(tiempo_inter_esper == "Minutos"):
										hora_inicio_recogidaDT += timedelta(minutes=int(inter_esper))
									if(tiempo_inter_esper == "Horas"):
										hora_inicio_recogidaDT += timedelta(hours=int(inter_esper))
									if(tiempo_inter_esper == "Dias"):
										hora_inicio_recogidaDT += timedelta(days=int(inter_esper))

									print("apagar  : "+str(hora_inicio_recogidaDT))
									random_numero2=random.randint(1,65534)
									datos_actuadores = "{"
									for i in id_actuadores:
										consulta_interfaces = iot_sensor_actuador.objects.get(iot_id=i).sa_interfaz
										datos_actuadores = datos_actuadores + '"interfaz": "'+consulta_interfaces+'", "valor": 0},{'
									json_regla = '{ "paquete_id": "'+str(random_numero2)+'", "mac":"'+mac+'", "datos": ['+datos_actuadores+'],"fecha": '
									if(reg_notificacion == "email"):
										txt_mensajes2 = "ESTADO DEL ACTUADOR: APAGADO  ;   " + txt_mensaje
										json_mensaje = 'k8s_e_212343s = { "usuarios": "'+ correos_elec + '", "asunto": "'+txt_asunto+'", "mensaje": "'+txt_mensajes2+'" }'
									else:
										json_mensaje=""
									gurdar_paquete=MPaqueteControl(pc_paquete=random_numero2, pc_fecha=datetime.strftime(hora_inicio_recogidaDT,"%Y-%m-%d"), pc_hora=datetime.strftime(hora_inicio_recogidaDT,"%H:%M:%S"), pc_estado="Pendiente", pc_mac=mac, pc_datos=json_regla, pc_datos_not=json_mensaje, acc_id=(MAccion.objects.last()).acc_id, acc_est_act="OFF", pc_time_out=0)
									gurdar_paquete.save()
									x = int(inter_ejec)

						if(id != ""):
							messages.add_message(request, messages.SUCCESS, 'Accion Actualizada Satisfactoriamente')
						else:
							messages.add_message(request, messages.SUCCESS, 'Accion Guardada Satisfactoriamente')



			except Exception as e:
				messages.add_message(request, messages.ERROR, e.message)
			return redirect('/reglas/acciones')
	else:
		return redirect('/')



def ver_datos_reglas(request, id):
	print("hi")
	data = []
	try:
		regla_model = MReglas.objects.get(reg_id=id)
		evento_model = MEvento.objects.get(reg=regla_model.reg_id)
		condicion_model = MCondiciones.objects.get(reg=regla_model.reg_id)
		print("hola 1")
		mensaje = MMensajeAlerta.objects.get(ev=evento_model.ev_id)
		print("hola")

		data.append({'id': regla_model.reg_id, 'nombre': regla_model.reg_nombre,
					 'descripcion': evento_model.ev_descripcion,
					 'valor1': condicion_model.con_valor1,
					 'valor2': condicion_model.con_valor2,
					 'asunto': mensaje.men_asunto,
					 'mensaje': mensaje.men_texto
					})

		return JsonResponse(data, safe=False)
	except Exception as error:
		print(error)
		data = []
	return JsonResponse(data, safe=False)



def ver_datos_planificadas(request, id):
	data = []
	try:
		print("holaaaaaaaaa1")

		accion_model = MAccion.objects.get(acc_id=id)
		evento_model = MEvento.objects.get(ev_id=accion_model.ev_id)
		mensaje = MMensajeAlerta.objects.get(ev=evento_model.ev_id)
		finicio=accion_model.acc_fecha_inicio
		hinicio=accion_model.acc_hora_inicio
		ffin=accion_model.acc_fecha_fin
		hfin=accion_model.acc_hora_fin
		hora_inicio=datetime(finicio.year, finicio.month, finicio.day, hinicio.hour, hinicio.minute, hinicio.second)
		hora_fin=datetime(ffin.year, ffin.month, ffin.day, hfin.hour, hfin.minute, hfin.second)

		data.append({'id': accion_model.acc_id,
					 'nombre': accion_model.acc_nombre,
					 'descripcion': accion_model.acc_descripcion,
					 'hora_inicio': hora_inicio,
					 'hora_fin': hora_fin,
					 'intervalo_ejec': accion_model.acc_interval_ejec,
					 'intervalo_espera': accion_model.acc_interval_espera,
					 'asunto': mensaje.men_asunto,
					 'mensaje': mensaje.men_texto
					})

		return JsonResponse(data, safe=False)
	except Exception as error:
		print(error)
		data = []
	return JsonResponse(data, safe=False)

status_api = {}
status_api[200] = status.HTTP_200_OK
status_api[201] = status.HTTP_201_CREATED
status_api[204] = status.HTTP_204_NO_CONTENT
status_api[400] = status.HTTP_400_BAD_REQUEST
status_api[401] = status.HTTP_401_UNAUTHORIZED
status_api[403] = status.HTTP_403_FORBIDDEN
status_api[404] = status.HTTP_404_NOT_FOUND
status_api[405] = status.HTTP_405_METHOD_NOT_ALLOWED
status_api[409] = status.HTTP_409_CONFLICT
status_api[500] = status.HTTP_500_INTERNAL_SERVER_ERROR



def ver_datos_repetitivas(request, id):
	data = []
	try:
		print("hola1")

		accion_model = MAccion.objects.get(acc_id=id)
		evento_model = MEvento.objects.get(ev_id=accion_model.ev_id)
		mensaje = MMensajeAlerta.objects.get(ev=evento_model.ev_id)
		finicio=accion_model.acc_fecha_inicio
		hinicio=accion_model.acc_hora_inicio
		hora_inicio=datetime(finicio.year, finicio.month, finicio.day, hinicio.hour, hinicio.minute, hinicio.second)

		data.append({'id': accion_model.acc_id,
					 'nombre': accion_model.acc_nombre,
					 'descripcion': accion_model.acc_descripcion,
					 'hora_inicio': hora_inicio,
					 'repeticion': accion_model.acc_num_intervalo,
					 'intervalo_ejec': accion_model.acc_interval_ejec,
					 'intervalo_espera': accion_model.acc_interval_espera,
					 'asunto': mensaje.men_asunto,
					 'mensaje': mensaje.men_texto
					})

		return JsonResponse(data, safe=False)
	except Exception as error:
		print(error)
		data = []
	return JsonResponse(data, safe=False)



def base_eventos(request):
	if request.user.is_authenticated():
		dict=armaMenu(request.user.get_all_permissions())
		dict['usuario'] = request.user
		dict['empresa']=getEmpresa(request)
		dict['persona'] = obtenerPersona(request)
		# dict['roles'] = obtenerRoles(request)
		return render_to_response("app_eventos/base_eventos.html", dict, context_instance = RequestContext(request))

	else:
		return render_to_response("index.html", context_instance = RequestContext(request))




def modelos_page(request):

	dic = {}
	dic['usuario']=request.user
	if request.method == 'GET':
		searchmodelo = request.GET.get('modelosearch', '')
		searchregla = request.GET.get('reglasearh', '')
		modeloid = request.GET.get('modelo', '')
		modeloid = int(modeloid) if modeloid.isdigit() else -1
		modelos = MModeloReglas.objects.filter(mod_nombre__icontains=searchmodelo)
		reglas = MReglas.objects.filter(reg_nombre__icontains=searchregla, mod = modeloid )
		modelo = MModeloReglas.objects.filter(mod_id = modeloid)
		modelo = modelo[0] if len(modelo) > 0 else ""

		dic['modelos'] = modelos
		dic['reglas'] = reglas
		dic['modelo'] = modelo
		dic['empresa'] = 1
		return render_to_response('app_eventos/modelos_page.html',dic, context_instance=RequestContext(request))




@login_required(login_url='/')
@api_view(['GET', 'POST'])
def modelo(request):
	user = request.user
	if request.method == 'GET':
		search = request.GET.get('modelosearch', '')
		modelos = MModeloReglas.objects.filter(mod_nombre__icontains=search)
		serializer = m_modeloSerializer(modelos, many=True)
		return Response(serializer.data)
	if request.method == 'POST':
		serializer = m_modeloSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@login_required(login_url='/')
@api_view(['GET', 'PUT', 'DELETE'])
def modelo_id(request, pk):

	"""
	Retrieve, update or delete a snippet instance.
	"""
	try:
		modelo = MModeloReglas.objects.get(pk=pk)
	except MModeloReglas.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = m_modeloSerializer(modelo)
		return Response(serializer.data)

	elif request.method == 'PUT':
		serializer = m_modeloSerializer(modelo, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		modelo.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

@login_required(login_url='/')
@api_view(['GET', 'POST'])
def regla(request):
	user = request.user
	if request.method == 'GET':
		modelo = request.GET.get('modelo')
		if modelo == None or modelo == "":
			modelo = -1
		reglas = MReglas.objects.filter(modelo = modelo)
		serializer = m_reglaSerializer(reglas, many=True)
		return Response(serializer.data)
	if request.method == 'POST':
		serializer = m_reglaSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@login_required(login_url='/')
@api_view(['GET', 'PUT', 'DELETE'])
def regla_id(request, pk):
	"""
	Retrieve, update or delete a snippet instance.
	"""

	try:
		regla = MReglas.objects.get(pk=pk)
	except MReglas.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = m_reglaSerializer(regla)
		return Response(serializer.data)

	elif request.method == 'PUT':
		serializer = m_reglaSerializer(regla, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		evento = MEvento.objects.filter(reg_id = regla)
		if len(evento) > 0 :
			delete_actions_alertas_by_ev_id(evento[0].ev_id)
		delete_reglas_by_reg_id(pk)
		regla.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)

@login_required(login_url='/')
@api_view(['GET', 'POST'])
def condicion(request):
	user = request.user
	if request.method == 'GET':
		search = request.GET.get('regla_id', 'all')
		condicion = MCondiciones.objects.filter(reg_id = regla)
		serializer = m_condicionSerializer(condicion, many=True)
		return Response(serializer.data)
	if request.method == 'POST':
		serializer = m_condicionSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@login_required(login_url='/')
@api_view(['GET', 'PUT', 'DELETE'])
def condicion_id(request, pk):

	try:
		condicion = MCondiciones.objects.get(pk=pk)
	except MReglas.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = m_condicionSerializer(condicion)
		return Response(serializer.data)

	elif request.method == 'PUT':
		serializer = m_condicionSerializer(condicion, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		condicion.remove()#POSIBLE ERROR
		return Response(status=status.HTTP_204_NO_CONTENT)


@login_required(login_url='/')
@api_view(['GET', 'POST'])
def m_subregla(request):
	user = request.user
	if request.method == 'GET':
		condicion_id = request.GET.get('condicion_id', 'all')
		subrules = MSubregla.objects.filter(condicion = condicion_id)
		serializer = m_subruleSerializer(subrules, many=True)
		return Response(serializer.data)
	if request.method == 'POST':
		serializer = m_subruleSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@login_required(login_url='/')
@api_view(['GET', 'PUT', 'DELETE'])
def m_subrule_id(request, pk):
	"""
	Retrieve, update or delete a snippet instance.
	"""
	try:
		subrules = MSubregla.objects.get(pk=pk)
	except MReglas.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = m_subruleSerializer(subrules)
		return Response(serializer.data)

	elif request.method == 'PUT':
		serializer = m_subruleSerializer(condicion, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		subrules.remove()#POSIBLE ERROR
		return Response(status=status.HTTP_204_NO_CONTENT)

@login_required(login_url='/')
@api_view(['GET'])
def subreglasbyrule(request, pk):
	"""
	Retrieve, update or delete a snippet instance.
	"""
	try:
		subrules = MSubregla.objects.get(pk = pk)
	except MReglas.DoesNotExist:
		return Response(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = m_subruleSerializer(subrules)
		return Response(serializer.data)



def sens_act_fromempresax(empresaid):
	dispositivos = iot_dispositivo.objects.filter(emp_id= empresaid)
	consultatotal=[]
	for dispositivo in dispositivos:
		sas= iot_sensor_actuador.objects.filter(dis_id= dispositivo.dis_id).order_by('-sa_categoria')
		for sa in sas:
			consultatotal.append(sa)
	return consultatotal


def get_sub_reglas_from_reg_id(reg_id):

	regla = MReglas.objects.get(reg_id = reg_id)
	condiciones = MCondiciones.objects.filter(reg_id = regla)
	subreglaslist = []
	for condicion in condiciones:
		subreglas  = MSubregla.objects.filter(condicion = condicion)
		for subregla in subreglas:
			subreglaslist.append(subregla)
	return regla, condiciones, subreglaslist

@login_required(login_url='/')
def reglas_page_agregar(request):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa1 = proy_persona.objects.get(per_id=per_id).emp_id
	empresa = empresa1
	sensores_actuadores=sens_act_fromempresax(empresa)
	actuadores = getonlyactuadoresfromempresax(empresa)
	usuarios = getpersonsfromempresax(empresa)
	modeloid = request.GET.get('modelo_id', '')
	reg_id = request.GET.get('reg_id', '')

	dic = {}
	dic['sactuadores']= sensores_actuadores
	dic['actuadores']= actuadores
	dic['usuarios']= usuarios
	dic['empresa1']=empresa


	dic['editar'] = False
	if request.method == 'GET':
		if reg_id != '':
			dic['regla'], dic['condiciones'], dic['subreglas'] = get_sub_reglas_from_reg_id(reg_id)
			dic['editar'] = True

			dic['mensaje_alerta'], dic['destinatarios']= get_mensaje_alert_by_reg_id(reg_id)
			dic['evento'], dic['accion'] , dic['acciones_actuadores'] = get_evento_by_reg_id(reg_id)
			modelo = dic['regla'].mod

		else:
			modelo = MModeloReglas.objects.get(mod_id = modeloid)

		#dic['modelo']= modelo
		modelo_id = MModeloReglas.objects.get(empresa=empresa).mod_id
		dic['modelo']=modelo_id

		return render_to_response('app_eventos/reglas_page_agregar.html',dic, context_instance=RequestContext(request))

	if request.method == 'POST':


		return render_to_response('app_eventos/reglas_page_agregar.html',dic, context_instance=RequestContext(request))

def get_mensaje_alert_by_reg_id(reg_id):
	evento = MEvento.objects.filter(reg_id = reg_id)
	if len(evento) == 0:
		mensaje = []
	else:
		mensaje = MMensajeAlerta.objects.filter(ev_id = evento)# solo debe devolver 1
	if len(mensaje)==0:
		destinatarios = []
	else:
		destinatarios = MensajeDestinatario.objects.filter(men_id = mensaje) #
	mensaje = [] if len(mensaje) == 0 else mensaje[0]
	return mensaje, destinatarios

def get_evento_by_reg_id(reg_id):

	evento = MEvento.objects.filter(reg_id = reg_id)
	accion = [] if len(evento) == 0 else MAccion.objects.filter(ev_id = evento[0])
	if len(accion) == 0:
		acciones_act= []
	else:
		acciones_act = MAccionActuador.objects.filter(acc_id = accion)
	evento = [] if len(evento) == 0 else evento[0]
	return evento, accion, acciones_act

def getpersonsfromempresax(empresaid):
	personas = proy_persona.objects.filter(emp_id = empresaid)
	return personas
#
def getonlyactuadoresfromempresax(empresaid):
	dispositivos = iot_dispositivo.objects.filter(emp_id= empresaid)
	consultatotal=[]
	for dispositivo in dispositivos:
		sas= iot_sensor_actuador.objects.filter(dis_id= dispositivo.dis_id, sa_categoria = 'a')
		for sa in sas:
			consultatotal.append(sa)
	return consultatotal

def guardarreglas(subreglas, condicionpadre, regla, id_empresa):

	a = id_empresa
	i = 5
	haverules = subreglas.get('rules')!=None
	iscondition = subreglas.get('condition')!=None
	issubrule = subreglas.get('operator')!=None

	if iscondition:
		print subreglas['condition']
		condicion = MCondiciones()
		try:
			condicion.reg_id = regla.reg_id
			condicion.con_operador_logico = subreglas['condition']
			condicion.con_orden = 1
			condicion.con_operador = "andd"
			condicion.con_valor1 = 1
			condicion.con_valor2 = 1
			condicion.con_datos = "json"
			condicion.con_datos_not = "json"
			condicion.con_valor_actuador = "Condicion Logica"
			condicion.iot_id = 1
			condicion.save()
			condicion.condicion_parent = condicion if condicionpadre ==None else condicionpadre
			condicion.save()

		except Exception, e:
			print e
		else:
			pass
		finally:
			pass
		reglas = subreglas['rules']
		for miregla in reglas:
			guardarreglas(miregla, condicion, regla, id_empresa)
	if issubrule:
		subregla = MSubregla()

		sa = iot_sensor_actuador.objects.get(pk= int(subreglas['id']))
		subregla.iot_id = sa
		subregla.sub_operador = subreglas['operator']
		if type(subreglas['value']) == list:
			subregla.sub_valor1 = subreglas['value'][0]
			subregla.sub_valor2 = subreglas['value'][1]
		else:
			subregla.sub_valor1 = subreglas['value']

		subregla.condicion = condicionpadre
		subregla.save()
		print subreglas['operator']
#Elimina acciones, accino?actuador alertas y destinatarios
#Recibe el id del evento
#Nota: El evento no se elmina
def delete_actions_alertas_by_ev_id(ev_id):

	evento = MEvento.objects.get(ev_id = ev_id)
	acciones = MAccion.objects.filter(ev_id = evento)
	for accion in acciones:
		aa = MAccionActuador.objects.filter(acc_id = accion)
		aa.delete()
	acciones.delete()
	alertas = MMensajeAlerta.objects.filter(ev_id = evento)
	for alerta in alertas:
		destinatarios = MensajeDestinatario.objects.filter(men_id = alerta)
		destinatarios.delete()
	alertas.delete()
#Elimina condiciones y subreglas de una regla
#Nota :  la regla no se elimina
def delete_reglas_by_reg_id(reg_id):

	regla = MReglas.objects.get(reg_id = reg_id)
	condiciones = MCondiciones.objects.filter(reg_id = regla)
	for condicion in condiciones:
		subreglas = MSubregla.objects.filter(condicion = condicion)
		subreglas.delete()
	condiciones.delete()
#Elimina accion_actuadors, accion, evento
#Recibe el id de la regla
def delete_evento_actions_msj_by_reg_id(reg_id):
	regla = MReglas.objects.get(reg_id = reg_id)
	evento = MEvento.objects.get(reg_id = regla)
	acciones = MAccion.objects.filter(ev_id = evento)
	for accion in acciones:
		accact = MAccionActuador.objects.filter(acc_id = accion)
		accact.delete()
	acciones.delete()
	evento.delete()
@login_required(login_url='/')
@api_view(['POST'])
def reglageneral(request):
	datos = request.data
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa_id = proy_persona.objects.get(per_id=per_id).emp_id
	
	empresa=empresa_id
	
	modelo = MModeloReglas.objects.get(mod_id = datos['modelo_id'])
	if request.method == 'POST':
		regla = MReglas()
		regla.reg_nombre = datos['m_regla']['reg_nombre']
		regla.mod = modelo
		regla.reg_estado = 'a'
		regla.save()

		if datos['only_regla']:
			guardarreglas(datos['m_regla']['reglas'], None, regla, empresa )
			print ("HOLA2")

		else:
			print("entra")
			evento = MEvento()
			evento.ev_nombre = datos['m_evento']['ev_nombre']
			print ("HOLA")
			evento.ev_descripcion = datos['m_evento']['ev_descripcion']
			print ("HOLA3")
			evento.reg_id = regla.reg_id
			evento.ev_fecha_hora = datetime.now()
			evento.reg_tipo = 'Logica'
			print ("HOLA4"+str(regla.reg_id))
			evento.save()
			print ("HOLA5")


			maccion = MAccion()
			maccion.acc_nombre = evento.ev_nombre + "-accion"
			maccion.ev_id = evento.ev_id
			maccion.save()
			acciones = datos['acciones']
			for accion in acciones:
				valor = accion['acc_valor']
				iot = iot_sensor_actuador.objects.get(iot_id= accion['iot_id'])
				aa = MAccionActuador()
				aa.iot_id = iot.iot_id
				aa.acc_id = maccion.acc_id

				if (type(valor) == bool):
					valor = 5 if valor else 0
				aa.acc_valor = valor
				aa.save()
			ma = MMensajeAlerta()
			ma.men_asunto = datos['m_mensaje_alerta']['men_asunto']
			ma.men_texto = datos['m_mensaje_alerta']['men_texto']
			ma.ev_id = evento.ev_id
			mensaje_json1 = '{"usuarios":"'

			mensaje_json2 = '","asunto":"'+ma.men_asunto+'","mensaje":"'+ma.men_texto+'"}'
			usuariosjson = ""
			ma.save()
			
			destinatarios = datos['m_mensaje_alerta']['men_destinatarios']
			for destinatario in destinatarios:
				persona  = proy_persona.objects.get(per_id = destinatario)
				usuariosjson = usuariosjson + persona.per_email+","
				md = MensajeDestinatario()
				md.men_id = ma.men_id
				md.per_id = persona.per_id
				md.md_tipo = 'E'
				md.save()
			usuariosjson = usuariosjson+","
			usuariosjson = usuariosjson.replace(",,","")
			jsontotal = mensaje_json1+usuariosjson+mensaje_json2
			regla.reg_datos_not = jsontotal
			regla.save()

			guardarreglas(datos['m_regla']['reglas'], None, regla, empresa)
	return Response({}, status=status.HTTP_201_CREATED)



@login_required(login_url='/')
@api_view(['PUT'])
def reglageneral_id(request, reg_id):
	if request.user.is_authenticated():
		per_id = request.user.id_persona_id
	empresa1 = proy_persona.objects.get(per_id=per_id).emp_id
	
	datos = request.data
	#modelo = MModeloReglas.objects.get(mod_id = datos['modelo_id'])
	if request.method == 'PUT':
		regla = MReglas.objects.get(reg_id = reg_id)
		regla.reg_nombre = datos['m_regla']['reg_nombre']

		#regla.modelo = modelo
		regla.reg_estado = 'a'
		regla.save()
		if datos['only_regla']:

			#guardarreglas(datos['m_regla']['reglas'], None, regla)
			evento = MEvento.objects.filter(reg_id = regla)
			if len(evento) != 0:
				delete_evento_actions_msj_by_reg_id(regla.reg_id)


		else:
			evento = MEvento.objects.filter(reg_id = regla)
			if len(evento) == 0:
				evento = MEvento()
			else:
				evento = evento[0]
				delete_actions_alertas_by_ev_id(evento.ev_id)
			evento.ev_nombre = datos['m_evento']['ev_nombre']
			evento.ev_descripcion = datos['m_evento']['ev_descripcion']
			evento.reg_id = regla.reg_id
			evento.save()

			maccion = MAccion()
			maccion.acc_nombre = evento.ev_nombre + "-accion"
			maccion.ev_id = evento.ev_id
			maccion.save()
			acciones = datos['acciones']
			for accion in acciones:
				valor = accion['acc_valor']
				iot = iot_sensor_actuador.objects.get(iot_id= accion['iot_id'])
				aa = MAccionActuador()
				aa.iot_id = iot.iot_id
				aa.acc_id = maccion.acc_id

				if (type(valor) == bool):
					valor = 5 if valor else 0
				aa.acc_valor = valor
				aa.save()
			ma = MMensajeAlerta()
			ma.men_asunto = datos['m_mensaje_alerta']['men_asunto']
			ma.men_texto = datos['m_mensaje_alerta']['men_texto']
			ma.ev_id = evento.ev_id
			ma.save()
			destinatarios = datos['m_mensaje_alerta']['men_destinatarios']
			for destinatario in destinatarios:
				persona  = proy_persona.objects.get(per_id = destinatario)
				md = MensajeDestinatario()
				md.men_id = ma.men_id
				md.per_id = persona.per_id
				md.md_tipo = 'E'
				md.save()
		delete_reglas_by_reg_id(regla.reg_id)
		#import pdb;pdb.set_trace()
		
		guardarreglas(datos['m_regla']['reglas'], None, regla, empresa1.emp_id)
		#guardarreglas(datos['m_regla']['reglas'], None, regla, datos['m_regla']['empresa'])
		return Response({}, status=status_api[200])
