from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from app_eventos.views import *
from django.contrib import admin
from django.conf import settings
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse, redirect, HttpResponseRedirect

urlpatterns = patterns('',
    # Sensores
    # url(r'^sensor/$', openSensor, name="url_sensor"),
    url(r'^inicio/$', base_eventos, name="url_inicio_eventos"),
    url(r'^acciones/$', acciones, name="url_acciones"),





    url(r'^acciones_repetitivas/$', acciones_repetitivas, name="url_acciones_repetitivas"),
    url(r'^acciones_diarias/$', accionesDiarias, name="url_acciones_diarias"),

    url(r'^acciones_reglas/$', acciones_reglas, name="url_acciones_reglas"),
    url(r'^acciones_agregar/$', agregar_acciones, name="url_acciones_agregar"),
    url(r'^acciones_agregar_plan/$', agregar_acciones_planificadas, name="url_acciones_agregar_planificadas"),
    url(r'^acciones_agregar_rep/$', agregar_acciones_repetitivas, name="url_acciones_agregar_repetitivas"),

    url(r'^monitoreo_paquetes/$', monitoreo_paquetes, name="url_monitoreo_paquetes"),
    url(r'^historial_paquetes/$', historial_paquetes, name="url_historial_paquetes"),
    url(r'^cargar/$', cargar, name="url_cargar"),

	url(r'^reglas_agregar/$', reglas_page_agregar, name='url_reglas_page_agregar'),

    url(r'^api/reglageneral/$', reglageneral, name="url_reglageneral"),
	url(r'^api/reglageneral/(.+)$', reglageneral_id),
    url(r'^modelos/$', modelos_page, name='url_modelos_page'),
    url(r'^api/modelo/$', modelo, name='url_modelo'),
    #url(r'^api/modelo/search/(?P<page_search>\w+)/$', modelosearch),
    url(r'^api/modelo/(.+)/$', modelo_id, name = 'url_modeloid'),

    url(r'^verRegla/(\d+)/$', ver_datos_reglas),
    url(r'^verPlanificada/(\d+)/$', ver_datos_planificadas),
    url(r'^verRepetitivas/(\d+)/$', ver_datos_repetitivas),

    url(r'^verAccionReglas/(\d+)/$', ver_datos_reglas),
    url(r'^listar_sensores_disp/$', cargar_lista_sensores, name="url_cargar_lista_sensores"),

    url(r'^listar_actuadores_disp/$', cargar_lista_actuadores, name="url_cargar_lista_actuadores"),
    url(r'eliminar/(?P<id_regla>\d+)$', eliminar_regla , name="url_eliminar_regla"),
    url(r'eliminarAccp/(?P<id_accion>\d+)$', eliminar_accionPlanificada , name="url_eliminar_accionPlanificada"),
    url(r'eliminarAccr/(?P<id_accion>\d+)$', eliminar_accionRepetitiva , name="url_eliminar_accionRepetitiva"),
    url(r'eliminarHistorial/(?P<id_historial>\d+)$', eliminar_historial , name="url_eliminar_historial"),
    url(r'reenviar/(?P<id_paquete>\d+)$', reenviar , name="url_reenviar"),

)