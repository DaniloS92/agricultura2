from django.shortcuts import render

from app_empresa.models import proy_empresa
from iot.models import iot_dispositivo, iot_sensor_actuador


def informacion(request, dispositivo_mac):
    if iot_dispositivo.objects.filter(dis_mac=dispositivo_mac).exists():
        dispositivo = iot_dispositivo.objects.get(dis_mac=dispositivo_mac)
        empresa = proy_empresa.objects.get(emp_id=dispositivo.emp_id.emp_id)

        # Sensores asociados
        lista_sensores = iot_sensor_actuador.objects.filter(dis_id=dispositivo.dis_id, sa_categoria="s")
        lista_actuadores = iot_sensor_actuador.objects.filter(dis_id=dispositivo.dis_id, sa_categoria="a")
    else:
        dispositivo = None
        empresa = None
        lista_sensores = None
        lista_actuadores = None

    context = {
        'dispositivo': dispositivo,
        'empresa': empresa,
        'lista_sensores': lista_sensores,
        'lista_actuadores': lista_actuadores,
    }
    return render(request, 'iot/informacion/TEDS.html', context=context)
