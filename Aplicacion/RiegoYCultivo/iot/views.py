#-*- coding: utf-8 -*-
import json
import os
import requests
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import *
from django.contrib.auth.hashers import make_password
from django.shortcuts import render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect, render,Http404
from django.db import transaction
from django.contrib import messages
from agricultura.metodos import *

from iot.models import *
from agricultura.metodos import *
from app_eventos.models import *
from iot.metodos import *

def base_iot(request):
    if request.user.is_authenticated():
        dict=armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['empresa']=getEmpresa(request)
        dict['persona'] = obtenerPersona(request)
        # dict['roles'] = obtenerRoles(request)
        return render_to_response("iot/base_iot.html", dict, context_instance = RequestContext(request))

    else:
        return render_to_response("index.html", context_instance = RequestContext(request))
# Create your views here.
@login_required(login_url='/')
def cargarDispositivo(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)

        if request.user.has_perm('iot.change_iot_dispositivo') or request.user.has_perm('iot.list_iot_dispositivo'):
            empresa=request.user.id_persona.emp_id.emp_id
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas) 
            dic['dispositivos'] = iot_dispositivo.objects.filter(emp_id = empresa)
            dic['subareas'] = subareas
            print(dic)
            return render_to_response('iot/dispositivo/templateDispositivo.html',dic, context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", context_instance = RequestContext(request))
    else:
        return redirect('/')

def baja_dispositivo(request):
    id = request.GET['id']
    estado = request.GET['estado']
    #print(estado)
    try:
        eliminar=iot_dispositivo.objects.get(dis_id=id)
        if(estado=='true'):
            eliminar.dis_activo=True
        else:
            eliminar.dis_activo=False
        eliminar.save()
        print('est',eliminar.dis_activo)
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def agregar_dispositivo(request):
    if request.method == "POST":
        msj={'codigo':0,'msj':'Se guardo'}
        empresa=request.user.id_persona.emp_id.emp_id
        emp1 = proy_empresa.objects.get(emp_id = empresa)
        try:
            nombre= request.POST["dis_nom"]
            description= request.POST["dis_des"]
            mac = request.POST["dis_mac"]
            fecha_registro = request.POST["dis_reg"]
            activo = True
            dis_localizacion = request.POST["dis_localizacion"]
            emp = emp1
            bsubarea = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
            try:
                guardarDisp=iot_dispositivo(dis_nombre=nombre, dis_descripcion=description, 
                    dis_mac=mac,dis_fecha_registro = fecha_registro,dis_activo = activo,
                    dis_localizacion = dis_localizacion,emp_id = emp,sar_id = bsubarea, dis_imagen=request.FILES['file1'])
            except Exception as error:
                print 'Imagen por defecto'
                guardarDisp=iot_dispositivo(dis_nombre=nombre, dis_descripcion=description, dis_mac=mac,dis_fecha_registro = fecha_registro,dis_activo = activo,dis_localizacion = dis_localizacion,emp_id = emp,sar_id = bsubarea)
            guardarDisp.save()
            messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
        except Exception as e:
            print e
            msj['codigo']=1
            msj['msj']="Error inesperado"
            messages.add_message(request, messages.ERROR, e.message)
        return redirect('/iot/dispositivo')

def ver_datos_dispositivo(request):
    id = request.GET['id']
    data = {}
    vector = []
    try:
        disp = iot_dispositivo.objects.get(dis_id = id)
        empresa=request.user.id_persona.emp_id.emp_id
        areas = proy_area.objects.filter(emp_id_id=empresa)
        subareas = proy_subarea.objects.filter(ar_id=areas) 

        if subareas.exists():
            print 'Hola'
            for sub_ar in subareas:
                vector.append({'sar_id': sub_ar.sar_id,'sar_nombre' : sub_ar.sar_nombre})
            
            if(disp.sar_id != None):
                data = {
                    'id' : disp.dis_id,
                    'dis_nombre' : disp.dis_nombre,
                    'dis_descripcion' : disp.dis_descripcion,
                    'dis_mac' : disp.dis_mac,
                    'dis_localizacion' : disp.dis_localizacion,
                    'dis_fecha_registro' : disp.dis_fecha_registro,
                    'sar_id'    : disp.sar_id.sar_id,
                    'imagen':disp.dis_imagen.url,
                    'sar_nombre' : disp.sar_id.sar_nombre,
                    'sub_areas' : vector,
                    }
            else:
                data = {
                    'id' : disp.dis_id,
                    'dis_nombre' : disp.dis_nombre,
                    'dis_descripcion' : disp.dis_descripcion,
                    'dis_mac' : disp.dis_mac,
                    'dis_localizacion' : disp.dis_localizacion,
                    'dis_fecha_registro' : disp.dis_fecha_registro,
                    'imagen':disp.dis_imagen.url,
                    'sar_id'    : '',
                    'sar_nombre' : '',
                    'sub_areas' : vector,
                    }

        response = JsonResponse(data)
        return HttpResponse(response.content)
    except Exception as error:
        print(error)
        data = {}
        response = JsonResponse(data)
    return HttpResponse(response.content)

def modificar_datos_dispositivos(request):
    try:
        id = request.POST["id"]
        nombre= request.POST["dis_nom"]
        descripcion = request.POST['dis_des']
        mac = request.POST["dis_mac"]
        fecha_registro = request.POST["dis_reg"]
        dis_localizacion = request.POST["dis_localizacion"]
        sub_area_id = request.POST["select_sub_area"]
        
        if sub_area_id == '':
            sub_area = None
        else:
            sub_area = proy_subarea.objects.get(sar_id = sub_area_id)

        saveDisp = iot_dispositivo.objects.get(dis_id = id)

        saveDisp.dis_nombre = nombre
        saveDisp.dis_descripcion = descripcion
        saveDisp.dis_mac = mac
        saveDisp.sar_id = sub_area
        saveDisp.dis_fecha_registro = fecha_registro
        saveDisp.dis_localizacion = dis_localizacion
        saveDisp.sar_id = sub_area
        try:
            saveDisp.dis_imagen=request.FILES['file1']
        except:
            print('No se cambia imagen')
        saveDisp.save()

        dic = {
            'result': 'OK'
        }
    except Exception as error:
        print(error)
        dic = {
            'result': 'Error al modificar'
        }
    response = JsonResponse(dic)
    return HttpResponse(response.content)


@login_required(login_url='/')
def openSensor(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('iot.list_iot_sensor_actuador') or request.user.has_perm('iot.change_iot_sensor_actuador'):
            empresa=request.user.id_persona.emp_id.emp_id            
            dispositivo = iot_dispositivo.objects.filter(emp_id_id=empresa)        
            dic['dispositivo']=dispositivo
            dic['datos_sensores']=iot_sensor_actuador.objects.filter(dis_id_id=dispositivo)
            dic['ted']=Ted.objects.all()
            return render_to_response('iot/sensor/templateSensor.html',dic,context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", context_instance = RequestContext(request))
    else:
        return redirect('/')

@login_required(login_url='/')
def baja_sensor(request):
    '''Cambiar de estado a el sensor
    Devuelve datos serializados
    :param request: Informacion de la pagina de donde viene la peticion
    :return dic: Tipo JSON
    :exception Si existe un error cuando se realiza la busquedad y se da de baja
    '''
    try:
        id = request.GET['id']
        estado = request.GET['estado']
        darBaja=iot_sensor_actuador.objects.get(iot_id=id)
        if(estado=='true'):
            darBaja.sa_estado=True
        else:
            darBaja.sa_estado=False
        darBaja.save()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print e
        dic = {
            'result': 'Error al dar de baja'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

def guardarSensor(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            try:
                id = request.POST["id"]
                local_interfaz      = request.POST['interfaz'],
                local_nombre        = request.POST['nombre'],
                local_ult_medida    = float(request.POST['ult_medida']),
                local_categoria     = request.POST['categoria'],
                local_senial        = request.POST['senial'],            
                local_dipositivo    = int(request.POST['dispositivos'])
                local_ted           = int(request.POST['ted'])
                dis=iot_dispositivo.objects.get(dis_id=local_dipositivo)
                ted=Ted.objects.get(ted_id=local_ted)
                if (id == ''):
                    sensor = iot_sensor_actuador(
                    sa_interfaz     = local_interfaz[0],
                    sa_nombre       = local_nombre[0],
                    sa_ult_medida   =  float(local_ult_medida[0]),
                    sa_categoria    = local_categoria[0],
                    sa_senial       = local_senial[0],
                    sa_estado       = True,             
                    dis_id          = dis,
                    ted_id          =  ted
                    )
                    sensor.save()
                    messages.add_message(request, messages.SUCCESS, 'Registro Guardado')
                else:
                    guardarSensor = iot_sensor_actuador.objects.get(iot_id=id)
                    guardarSensor.sa_interfaz = local_interfaz[0]
                    guardarSensor.sa_nombre = local_nombre[0]
                    guardarSensor.sa_categoria = local_categoria[0]
                    guardarSensor.sa_ult_medida = local_ult_medida[0]
                    guardarSensor.sa_senial = local_senial[0]
                    guardarSensor.dis_id= dis
                    guardarSensor.ted_id=ted
                    guardarSensor.save()
                    messages.add_message(request, messages.SUCCESS, 'Registro Actualizado')
            except Exception as e:
                messages.add_message(request, messages.ERROR, e.message)
            return redirect('/iot/sensor')
    else:
        return redirect('/') 

def modificarSensor(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            try:
                s = iot_sensor_actuador.objects.get( iot_id = request.POST['sen_id'])
                s.sa_nombre         = request.POST['sen_nom_m']
                s.sa_descripcion    = request.POST['sen_des_m']
                s.sa_interfaz       = request.POST['sen_int_m']
                s.sa_categoria      = request.POST['sen_cat_m']
                s.sa_senial         = request.POST['sen_sen_m']
                s.sa_estado         = True
                s.sa_url_ted        = request.POST['sen_url_m']
                s.sa_formula        = request.POST['sen_for_m']
                s.dis_id_id         = int(request.POST['dis_m'])
                s.save()
                return HttpResponse(
                            json.dumps({"mensaje":"Modificado Correctamente"}),content_type = "application/json; charset=utf8")
            except Exception as e:               
                print(e)
    else:
        raise Http404

def getSensor(request, id):
    data = []
    try:
        sensor =iot_sensor_actuador.objects.get(iot_id = id)
        data.append({
            'id' : sensor.iot_id,
            'nombre' : sensor.sa_nombre,
            'interfaz' : sensor.sa_interfaz,
            'categoria' : sensor.sa_categoria,
            'senial' : sensor.sa_senial,
            'ult_medida' : sensor.sa_ult_medida,
            'dispositivo' : sensor.dis_id.dis_id,
            'ted' : sensor.ted_id.ted_id,
            })
        return JsonResponse(data, safe=False)
    except Exception as error:
        print(error)
        data = []
    return JsonResponse(data, safe=False)

@login_required
def ver_zonas(request):
    if request.user.is_authenticated():
        verificar_update(request)
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=getEmpresa(request)
        if request.user.has_perm('agricultura.list_agr_zona_sensor_actuador'):
            empresa=request.user.id_persona.emp_id.emp_id
            areas = proy_area.objects.filter(emp_id_id=empresa)
            subareas = proy_subarea.objects.filter(ar_id=areas)
            dic['zonas'] = agr_zona_sensor_actuador.objects.filter(sar_id = subareas).order_by('-sar_id')
            dic['sub_areas'] = subareas
            dic['regiones']=reg_coef_expe.objects.all()
            return render_to_response('iot/zona/listarZonas.html',dic,context_instance = RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html", context_instance = RequestContext(request))
    else:
        return redirect('/')

def mostrar_update(request):
    try:
        if verificacion_zonas(request):
            data = {'result' : 'OK'}
        else:
            data = {'result' : 'NO'}
    except Exception as error:
        print error
        data = {'result' : 'ERROR'}

    response = JsonResponse(data)
    print response
    return HttpResponse(response.content)

def buscar_zonas(request):
    data = {}
    vector = []
    empresa=request.user.id_persona.emp_id.emp_id
    areas = proy_area.objects.filter(emp_id_id=empresa)
    subareas = proy_subarea.objects.filter(ar_id=areas)
    try:
        if request.POST['select_sub_area'] == 'todos' and request.POST['select_zonas'] == 'todos' and request.POST['select_estado'] == 'true':
            print 'Nadie se volteo'
            find_zona = agr_zona_sensor_actuador.objects.filter(sar_id = subareas, zsa_estado=True).order_by('-sar_id')
        elif request.POST['select_sub_area'] != 'todos' and request.POST['select_zonas'] == 'todos' and request.POST['select_estado'] == 'true':
            print 'sub area se volteo'
            find_sub_area = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
            find_zona = agr_zona_sensor_actuador.objects.filter(sar_id = find_sub_area, zsa_estado=True).order_by('-sar_id')
        elif request.POST['select_sub_area'] == 'todos' and request.POST['select_zonas'] != 'todos' and request.POST['select_estado'] == 'true':
            print 'zonas se volteo'
            find_zona = agr_zona_sensor_actuador.objects.filter(zsa_id = request.POST['select_zonas'], zsa_estado=True)
        elif request.POST['select_sub_area'] == 'todos' and request.POST['select_zonas'] == 'todos' and request.POST['select_estado'] != 'true':
            print 'estado se volteo'
            find_zona = agr_zona_sensor_actuador.objects.filter(sar_id = subareas, zsa_estado = False)
        elif request.POST['select_sub_area'] != 'todos' and request.POST['select_zonas'] == 'todos' and request.POST['select_estado'] != 'true':
            print 'se volteo sub area y estado'
            find_sub_area = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
            find_zona = agr_zona_sensor_actuador.objects.filter(sar_id = find_sub_area, zsa_estado = False)
        elif request.POST['select_sub_area'] == 'todos' and request.POST['select_zonas'] != 'todos' and request.POST['select_estado'] != 'true':
            print 'se volteo zona y estado'
            find_zona = agr_zona_sensor_actuador.objects.get(zsa_id = request.POST['select_zonas'], zsa_estado = False)
        elif request.POST['select_sub_area'] != 'todos' and request.POST['select_zonas'] != 'todos' and request.POST['select_estado'] == 'true':
            print 'se volteo zona y sub area'
            find_sub_area = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
            find_zona = agr_zona_sensor_actuador.objects.filter(sar_id = find_sub_area, zsa_id = request.POST['select_zonas'], zsa_estado=True)
        else:
            print 'todos se voltearon'
            find_sub_area = proy_subarea.objects.get(sar_id = request.POST['select_sub_area'])
            find_zona = agr_zona_sensor_actuador.objects.filter(sar_id = find_sub_area, zsa_id = request.POST['select_zonas'], zsa_estado=False)

        for zonas in find_zona:
            vector.append({
                'id'            : zonas.zsa_id,
                'subareas'      : zonas.sar_id.sar_nombre,
                'nombre'        : zonas.zsa_nombre,
                'descripcion'   : zonas.zsa_descripcion,
                'estado'        : zonas.zsa_estado,
                'imagen'        : zonas.zsa_imagen.url,
                'act'           : zonas.zsa_nec_act
                })
        data = {
            'zonas' : vector,
            'result' : 'OK',
        }

    except Exception as error:
        print error
        data = {'result' : 'ERROR'}

    response = JsonResponse(data)
    print response
    return HttpResponse(response.content)

def monitoreo_acciones(request, id):

    try:
        
        find_actuador = iot_sensor_actuador.objects.get(iot_id = id)
        find_accion_actuador = MAccionActuador.objects.filter(iot_id = find_actuador).order_by('-acc_act_id')[0]
        find_accion = MAccion.objects.get(acc_id = find_accion_actuador.acc_id.acc_id)
        print find_accion
    except Exception, e:
        print e

    return render_to_response('iot/monitoreo/monitoreo_acciones.html',context_instance = RequestContext(request))

def template_details(request, template_id):
    """
    Representa en un formato json las propiedades o comandos asociados con un id de un template.

    :param request: Representa la peticion de un usuario
    :param template_id: ID de un template comprendido entre los valores de  25 y 42,  segun el standard de National Instruments https://standards.ieee.org/develop/regauth/tut/teds.pdf#page=3
    :return: Template Informativo extraido de los parametros especificados previuamente por un administrador del sistema.
    """
    dic = {}
    dic["status"] = False

    if Ted.objects.filter(ted_template=template_id).exists():
        dic['status'] = True

        try:
            template = Ted.objects.get(ted_template=template_id)
        except:
            template = None
            dic["message"] = 'Existe mas de un ted en la db'

        if template:

            # Almaceno los grupos que pertenecena al template, obteniendo solo los ids de los grupos.
            ids_grupos = Ted_param.objects.values_list('par_id__pt_id', flat=True).filter(ted_id=template).distinct()

            dic_grupos = []
            # Recorremos los grupos
            for id_grupo in ids_grupos:
                grupo = ParamTipo.objects.get(pt_id=id_grupo)
                dic_grupo_detalle = {'grupo': grupo.pt_nombre}

                # Agrego a cada grupo los parametros que se encuentran vinculados.
                dic_parametros = []
                parametros_grupos = Ted_param.objects.filter(ted_id=template, par_id__pt_id=grupo)
                for parametro in parametros_grupos:
                    dic_parametros.append({
                        'command': parametro.par_id.par_property,
                        'description': parametro.par_id.par_description,
                        'access': parametro.tp_access,
                        'bits': '%s' % (parametro.tp_bits),
                        'data_type': parametro.par_id.dt_id.dt_nombre,
                        'units': '%s' % (parametro.par_id.unid_id),
                    })
                # Cargamos la lista de parametros al diccionario con un nombre clave: "parametros"
                dic_grupo_detalle['parametros'] = dic_parametros

                dic_grupos.append(dic_grupo_detalle)

            dic_template = {
                'id': template.ted_template,
                'nombre': template.ted_name,
                'descripcion': template.ted_description,
                'formula': template.ted_formula,
                'grupos': dic_grupos,
                'unidad_nombre': template.um_id.umed_nombre,
                'unidad_abreviatura': template.um_id.umed_abreviatura,
            }

            dic['ted'] = dic_template

    return JsonResponse(dic)

def getDispositivo(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            datos = iot_dispositivo.objects.filter(emp_id = request.user.id_persona.emp_id.emp_id).order_by('dis_id').values('dis_id','dis_nombre')
            return HttpResponse(
                json.dumps({"data":list(datos)},cls=DjangoJSONEncoder),
                content_type = "application/json; charset=utf8" 
            )
        else:
            return redirect('/')
            
    else:
        return redirect('/')

@login_required(login_url='/')
def eliminar_dispositivo(request):
    '''Eliminar Dispositivo
    Devuelve datos serializados
    :param request: Informacion de la pagina de donde viene la peticion
    :return dic: Tipo JSON
    :exception Si existe un error cuando se realiza la busquedad y se da de baja
    '''
    try:
        id = request.GET['id']
        formula=iot_dispositivo.objects.get(dis_id=id)
        formula.delete()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al eliminar'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

@login_required(login_url='/')
def eliminar_sen_act(request):
    try:
        id = request.GET['id']
        sen_act=iot_sensor_actuador.objects.get(iot_id=id)
        sen_act.delete()
        dic = {
            'result': 'OK'
        }
    except Exception as e:
        print(e)
        dic = {
            'result': 'Error al eliminar'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")