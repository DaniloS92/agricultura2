from __future__ import unicode_literals

from django.apps import AppConfig


class IotConfig(AppConfig):
    name = 'iot'
    verbose_name = 'Iot'
