from django.db import models
from app_empresa.models import *
from agricultura.models import agr_zona_sensor_actuador
from agricultura.models import r_unidad_medida


# Create your models here.

class DataType(models.Model):
    """
    Los tipos de datos dentro del Modulo IOT se encuentran representado en este modelo.

    No existen campos opcionales para el modelo actual.
    """

    class Meta:
        db_table = 'iot_data_type'
        verbose_name = 'Tipo Dato'
        verbose_name_plural = 'Tipos de Datos'

    dt_id = models.AutoField(primary_key=True)
    dt_nombre = models.CharField('Nombre', max_length=100)

    def __unicode__(self):
        return '%s' % (self.dt_nombre)


class ParamTipo(models.Model):
    """
    Los tipos de Parametros dentro del Modulo IOT se encuentran representado en este modelo.

    El identificador y el nombre del parametro son campos obligatorios.
    """

    class Meta:
        db_table = 'iot_param_type'
        verbose_name = 'Tipo Parametro'
        verbose_name_plural = 'Tipos de Parametros'

    pt_id = models.AutoField(primary_key=True)
    pt_nombre = models.CharField('Nombre', max_length=50)
    pt_descripcion = models.TextField('Descripcion', blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.pt_nombre)


class Parametro(models.Model):
    """
    Los Parametros de configuracion que integran a la plantilla de un TED a utilizarse en el modulo IOT se encuentran representado en este modelo.

    Los campos: dt_id, pt_id y unid_id Aceptan valores nulos debido a que no siempre se especificaran valores en algunos comandos para el TED.
    """

    class Meta:
        db_table = 'iot_parametro'
        verbose_name = 'Parametro'
        verbose_name_plural = 'Parametros'

    par_id = models.AutoField(primary_key=True)
    par_property = models.CharField('Property/Command', max_length=25)
    par_description = models.CharField('Description', max_length=50)

    dt_id = models.ForeignKey(DataType, verbose_name='Tipo Dato', blank=True, null=True)
    pt_id = models.ForeignKey(ParamTipo, verbose_name='Tipo Parametro', blank=True, null=True)
    unid_id = models.ForeignKey(r_unidad_medida, verbose_name='Unidad', blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.par_property)


class Ted(models.Model):
    """
    Los Templates que seran objeto de asignacion de datos a utilizarse en el modulo IOT se encuentran representado en este modelo.

    Los campos: ted_description, ted_formula pueden aceptar valores nulos.
    Los campos: ted_id, ted_template, deberan ser especificados por cada TED, ademas el codigo 'ID' del templare debera ser unico.
    """

    class Meta:
        db_table = 'iot_ted'
        verbose_name = 'Ted'
        verbose_name_plural = 'Teds'

    ted_id = models.AutoField(primary_key=True)
    ted_name = models.CharField('Nombre Sensor', max_length=100)
    ted_template = models.PositiveIntegerField('TEMPLATE ID', unique=True)
    ted_description = models.TextField('Descripcion', blank=True, null=True)
    ted_formula = models.TextField('Formula', blank=True, null=True)
    um_id = models.ForeignKey(r_unidad_medida, verbose_name='Unidad', blank=True, null=True)

    def __unicode__(self):
        return '%s  -  %s' % (self.ted_template, self.ted_name)


class Ted_param(models.Model):
    """
    Los parametros que integran un Template y sus respectivos valores para uso principal del modulo IOT se encuentran representado en este modelo.

    Los campos: tp_access, tp_bits, tp_valor_fin, tp_interval_tolerance, tp_case, tp_case_value. Pueden aceptar valores nulos.
    """

    class Meta:
        db_table = 'iot_ted_param'
        verbose_name = 'Parametro'
        verbose_name_plural = 'Parametros del Ted'

    ted_id = models.ForeignKey(Ted, verbose_name='Template')
    par_id = models.ForeignKey(Parametro, verbose_name='Parametro')

    ID = 'ID'
    CAL = 'CAL'
    USR = 'USR'

    CHOICES_ACCESS = ((ID, ID), (CAL, CAL), (USR, USR))

    tp_access = models.CharField("Access", max_length=3, choices=CHOICES_ACCESS, blank=True, null=True)
    tp_bits = models.PositiveIntegerField('Bits', blank=True, null=True)

    tp_valor = models.CharField('Valor', max_length=15)
    tp_valor_fin = models.CharField('Valor Final', max_length=15, blank=True, null=True)
    tp_interval_tolerance = models.CharField('Tolerancia', max_length=15, blank=True, null=True)
    tp_case = models.CharField('Case', max_length=10, blank=True, null=True)
    tp_case_value = models.CharField('Value', max_length=10, blank=True, null=True)

    def column_select(self):
        return '%s %s' % (self.tp_case, self.tp_case_value)

    column_select.short_description = "Select"
    full_values = property(column_select)

    def rango_command(self):
        return '%s' % (self.par_id.par_property)

    rango_command.short_description = "Property/Command"
    full_values = property(rango_command)

    def column_description(self):
        return '%s' % (self.par_id.par_description)

    column_description.short_description = "Description"
    full_values = property(column_description)

    def column_access(self):
        return '%s' % (self.tp_access)

    column_access.short_description = "Access"
    full_values = property(column_access)

    def column_data_type(self):
        if self.tp_interval_tolerance:
            result = '%s (%s to %s, %s)' % (
                self.par_id.dt_id.dt_nombre, self.tp_valor, self.tp_valor_fin, self.tp_interval_tolerance)
        else:
            result = '%s = %s' % (self.par_id.dt_id.dt_nombre, self.tp_valor)
        return result

    column_data_type.short_description = "Data Type (and Range)"
    full_values = property(column_data_type)

    def column_units(self):
        return '%s' % (self.tp_bits)

    column_units.short_description = "Units"
    full_values = property(column_units)

    def column_template_id(self):
        return '%s' % (self.ted_id.ted_name)

    column_template_id.short_description = "Template ID"
    full_values = property(column_template_id)


class iot_dispositivo(models.Model):
    dis_id                      = models.AutoField("Id",primary_key = True, null = False)
    dis_nombre                  = models.CharField("Nombre",max_length=100)
    dis_descripcion             = models.CharField("Descripcion",max_length=100)
    dis_mac                     = models.CharField("Mac",max_length=50)
    dis_fecha_registro          = models.DateField("Fecha Registro")
    dis_activo                  = models.BooleanField("Estado",default=True)
    dis_fecha_ultima_lectura    = models.DateTimeField("Fecha Ultima Lectura",null = True, blank = True)
    dis_localizacion            = models.CharField("Localizacion",max_length=50)
    dis_imagen                  = models.ImageField("Imagen",upload_to='img_dispositivo', blank=True, null=True, default='img_dispositivo/sin_imagen.jpg')
    dis_update                  = models.BooleanField(default=False)
    emp_id                      = models.ForeignKey(proy_empresa, blank=True, null=True)
    sar_id                      = models.ForeignKey(proy_subarea, blank=True, null=True)

    class Meta:
        db_table = 'iot_dispositivo'
        verbose_name = 'Dispositivo'
        verbose_name_plural = 'Dispositivos'
        default_permissions = (['change','list'])

    def __unicode__(self):
        return self.dis_nombre

class iot_sensor_actuador(models.Model):
    iot_id              = models.AutoField("Id",primary_key = True, null = False)
    sa_nombre           = models.CharField("Nombre",max_length=200,blank=True,null=True)
    sa_interfaz         = models.CharField("Interfaz",max_length=10)
    sa_categoria        = models.CharField("Categoria",max_length=50)
    sa_senial           = models.CharField("Senial",max_length=50)
    sa_estado           = models.BooleanField("Estado")
    sa_ult_medida       = models.FloatField("Ultima Medida")
    dis_id              = models.ForeignKey(iot_dispositivo)
    zsa_id              = models.ForeignKey(agr_zona_sensor_actuador, blank=True, null=True)
    ted_id              = models.ForeignKey(Ted, blank=True, null=True)

    class Meta:
        db_table = 'iot_sensor_actuador'
        verbose_name = 'Sensor Actuador'
        verbose_name_plural = 'Sensor Actuador'
        default_permissions = (['change','list','print'])

    def __unicode__(self):
        return self.sa_nombre