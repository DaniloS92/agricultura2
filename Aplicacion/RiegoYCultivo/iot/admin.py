from django.contrib import admin
from iot.models import *
# from agricultura.models import r_unidad_medida


class Ted_paramAdmin(admin.ModelAdmin):
    list_display = ('column_template_id', 'column_select', 'rango_command', 'column_description', 'column_access',
                    'column_data_type', 'column_units')

    search_fields = ['column_description']
    list_per_page = 10

class TedAdmin(admin.ModelAdmin):
    list_display = ('ted_id', 'ted_name', 'ted_template', 'ted_description', 'ted_formula')
    list_filter= ['ted_name']
    search_fields = ['ted_name']
    list_per_page = 10

class ParametroAdmin(admin.ModelAdmin):
    list_display = ('par_id', 'par_property', 'par_description')
    list_filter= ['par_property']
    search_fields = ['par_property']
    list_per_page = 10

class ParamTipoAdmin(admin.ModelAdmin):
    list_display = ('pt_id', 'pt_nombre', 'pt_descripcion')
    list_filter= ['pt_nombre']
    search_fields = ['pt_nombre']
    list_per_page = 10


class DataTypeAdmin(admin.ModelAdmin):
    list_display = ('dt_id', 'dt_nombre')
    list_filter= ['dt_nombre']
    search_fields = ['dt_nombre']
    list_per_page = 10

class iot_sensor_actuadorAdmin(admin.ModelAdmin):
    list_display = ('iot_id', 'sa_nombre','sa_interfaz','sa_categoria','sa_senial','sa_estado','sa_ult_medida')
    list_filter= ['sa_nombre']
    search_fields = ['sa_nombre']
    list_per_page = 10

class iot_dispositivoAdmin(admin.ModelAdmin):
    list_display = ('dis_id', 'dis_nombre','dis_descripcion','dis_mac','dis_fecha_registro','dis_activo','dis_update','dis_fecha_ultima_lectura')
    list_filter= ['dis_nombre']
    search_fields = ['dis_nombre']
    list_per_page = 10

admin.site.register(iot_dispositivo, iot_dispositivoAdmin)
admin.site.register(iot_sensor_actuador, iot_sensor_actuadorAdmin)


# admin.site.register(r_unidad_medida)
admin.site.register(DataType, DataTypeAdmin)
admin.site.register(ParamTipo, ParamTipoAdmin)
admin.site.register(Parametro, ParametroAdmin)
admin.site.register(Ted, TedAdmin)
admin.site.register(Ted_param, Ted_paramAdmin)
