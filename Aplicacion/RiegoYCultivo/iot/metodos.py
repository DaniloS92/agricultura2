from iot.models import *
from agricultura.models import *

def verificar_update(request):
    # verifica si se necesita actualizar la zona si ese es el caso la desactiva para su reconfiguracion
    empresa = request.user.id_persona.emp_id
    areas = proy_area.objects.filter(emp_id_id=empresa)
    subareas = proy_subarea.objects.filter(ar_id=areas)
    try:
        find_zonas = agr_zona_sensor_actuador.objects.filter(zsa_nec_act=True,sar_id = subareas)
        if find_zonas.exists():
            find_zonas.update(zsa_estado=False)
        else:
            print('No hay actualizaciones en zonas')
    except Exception as e:
        print(e)

def act_estado_disp(id_zona):
    # actualiza el estado del dispositivo donde uno o varios de sus sensores pertenescan a una zona de riego
    try:
        find_zona = agr_zona_sensor_actuador.objects.get(zsa_id = id_zona)
        find_sensor = iot_sensor_actuador.objects.filter(zsa_id = find_zona)
        for sensor_atuador in find_sensor:
            sensor_atuador.dis_id.dis_update=False
            sensor_atuador.dis_id.save()
        find_zona.zsa_nec_act = False
        find_zona.save()
        return True
    except Exception as e:
        print e
        return False

def verificacion_zonas(request):
    # verifica si aun hay zonas sin actualizar en la empresa obviamente :v
    empresa = request.user.id_persona.emp_id
    areas = proy_area.objects.filter(emp_id_id=empresa)
    subareas = proy_subarea.objects.filter(ar_id=areas)
    try:
        find_sensor_actuador = iot_sensor_actuador.objects.filter(dis_id__dis_update=True,dis_id__emp_id_id=empresa).exclude(zsa_id=None)
        find_zonas = agr_zona_sensor_actuador.objects.filter(zsa_nec_act=True,sar_id = subareas)
        if find_sensor_actuador.exists() or find_zonas.exists():
            return True
        else:
            return False
    except Exception as e:
        print(e)