from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from iot.views import *
from django.contrib import admin
from django.conf import settings
from agricultura.parcelas.views import zona_riego
from iot import views_public
urlpatterns = patterns('',
    # Sensores
    url(r'^inicio/$', base_iot, name="url_sensor"),
    url(r'^sensor/$', openSensor, name="url_sensor"),
    url(r'^sensor/sav/$', guardarSensor,  name="url_sensor_agregar"),
    url(r'^obtenerSensor/(\d+)/$', getSensor),
    url(r'^sensor/mod/$', modificarSensor),
    url(r'^bajaSensor/$', baja_sensor),
    url(r'^eliminar_sen_act/$', eliminar_sen_act),

    url(r'^dispositivo/$', cargarDispositivo, name="url_dispositivo"),
    url(r'^bajaDisp/$', baja_dispositivo),
    url(r'^disp_agregar/$', agregar_dispositivo, name="url_disp_agregar"),
    url(r'^ver_datos_dispositivo/$', ver_datos_dispositivo),
    url(r'^modificar_datos_dispositivos/$', modificar_datos_dispositivos),
    url(r'^getDispositivo/$', getDispositivo),

    url(r'^zona_riego/(?P<id>.*)/$', zona_riego),
    url(r'^visualizar_zonas/$', ver_zonas),
    url(r'^buscar_zonas/$', buscar_zonas),
    url(r'^eliminar_dispositivo/$', eliminar_dispositivo),
    url(r'^verificar_update/$', mostrar_update),

    url(r'^monitoreo_acciones/(\d+)/$', monitoreo_acciones),

    # Permite vicualizar las configuraciones asociadas a un TED
    url(r'^teds/(\d{1,2})$', template_details, name='index'),

    url(r'^dispositivo_info/(?P<dispositivo_mac>.+)/$', views_public.informacion, name='dispositivo_info'),
)
