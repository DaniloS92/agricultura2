from django.contrib import admin
from app_empresa.models import *



class emp_tipoSectorAdmin(admin.ModelAdmin):
    list_display = ('tsec_id', 'tsec_nombre', 'tsec_icono')
    list_filter= ['tsec_nombre']
    search_fields = ['tsec_nombre']
    list_per_page = 10

class emp_sectorAdmin(admin.ModelAdmin):
    list_display = ('sect_id', 'sect_nombre', 'sect_detalle','sect_id_tsector','sect_id_padre')
    list_filter= ['sect_nombre']
    search_fields = ['sect_nombre']
    list_per_page = 10

admin.site.register(emp_tipoSector, emp_tipoSectorAdmin)
admin.site.register(emp_sector, emp_sectorAdmin)
admin.site.register(proy_empresa)
admin.site.register(proy_persona)
admin.site.register(proy_representante)
admin.site.register(proy_area)
admin.site.register(proy_subarea)
admin.site.register(reg_coef_expe)