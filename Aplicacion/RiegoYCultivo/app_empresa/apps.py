from __future__ import unicode_literals

from django.apps import AppConfig


class EmpresaConfig(AppConfig):
    name = 'app_empresa'
    verbose_name = 'Empresa'
