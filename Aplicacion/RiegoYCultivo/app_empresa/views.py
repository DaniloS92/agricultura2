#-*- coding: utf-8 -*-
import json
import os
import requests
import unicodedata
from django.core.serializers.json import DjangoJSONEncoder
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import *
from django.contrib.auth.hashers import make_password
from django.shortcuts import render_to_response, RequestContext, redirect, HttpResponse, HttpResponseRedirect, render,Http404
from django.db import transaction
from django.contrib import messages

from .models import *
from agricultura.metodos import *


def frmRegistroEmpresa(request):
    if not request.user.is_authenticated():
        return render_to_response("app_empresa/registro_empresa.html", context_instance = RequestContext(request))
    else:
        return redirect("/")

def frmEmpresa(request):
    if request.user.is_authenticated():
        dict=armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['empresa']=request.user.id_persona.emp_id
        dict['persona'] = obtenerPersona(request)
        dict['listUsuarios'] = User.objects.filter(id_persona__emp_id=request.user.id_persona.emp_id).exclude(is_superuser=True,
                                                                                                   is_staff=True)
        return render_to_response("app_empresa/base.html", dict, context_instance = RequestContext(request))
        # return render_to_response("pruebas.html", dict, context_instance = RequestContext(request))
    else:
        return redirect("/")

@login_required(login_url='/')
def frmModificarEmpresa(request):
    if request.user.is_authenticated():
        dict = armaMenu(request.user.get_all_permissions())
        dict['usuario'] = request.user
        dict['persona'] = obtenerPersona(request)
        dict['roles'] = obtenerRoles(request)
        dict['empresa']=request.user.id_persona.emp_id
        if request.user.has_perm('app_empresa.change_proy_empresa'):
            dict['empresa_editar']=request.user.id_persona.emp_id
            return render(request, "app_empresa/modificar_empresa.html", dict)
        else:
            return render_to_response("seguridad/errores/error_403.html", dict,context_instance = RequestContext(request))
    else:
        return redirect('/')

@csrf_exempt
@transaction.atomic
def guardar_datos_empresa(request):
    #Datos de la empresa
    datos = {}
    ruc = request.POST.get('txt_ruc')
    comercial = request.POST.get('txt_nombre_c')
    juridico = request.POST.get('txt_nombre_j')
    grupo = request.POST.get('txt_grupo')
    correo = request.POST.get('txt_correo_e')
    web = request.POST.get('txt_web')
    direccion = request.POST.get('txt_direccion_e')
    logo = request.FILES.get('txt_logo')
    dominio = request.POST.get('txt_dominio')
    descripcion = request.POST.get('txt_descripcion_e')
    if not descripcion:
        descripcion = "Sin detalles"

    #Datos de la persona
    per_nombre = request.POST.get('txt_nombre_p')
    per_apellido = request.POST.get('txt_apellido_p')
    per_correo = request.POST.get('txt_correo_p')
    per_direccion = request.POST.get('txt_direccion_p')
    per_fijo = request.POST.get('txt_telefono_f')
    per_movil = request.POST.get('txt_telefono_m')

    #Datos del representante
    rep_fechaI = request.POST.get('txt_fecha_i')
    rep_fechaF = request.POST.get('txt_fecha_f')
    rep_descripcion = request.POST.get('txt_descripcion_p')

    #Datos del usuario (admin)
    usu_nombre = request.POST.get('txt_usuario_p')
    usu_clave = request.POST.get('txt_password')

    if not grupo:
        grupo = "Sin detalles"


    listaRegistrados = proy_empresa.objects.filter(emp_ruc = ruc)
    if listaRegistrados.exists():
        datos['message'] = "¡No se ha podido ingresar los datos de la empresa. El RUC ya se encuentra registrado!"
        datos['result'] = "XRUC"
        return JsonResponse(datos)

    #Usar transacciones
    empresa = proy_empresa(emp_ruc = ruc,
                           emp_nombre_comercial = comercial,
                           emp_nombre_juridico = juridico,
                           emp_grupo_empresa = grupo,
                           emp_direccion = direccion,
                           emp_email = correo,
                           emp_web = web,
                           emp_logo = logo,
                           emp_dominio_correo = dominio,
                           emp_descripcion = descripcion
                        )

    usuariosRegistrados = User.objects.filter(username = usu_nombre)
    if usuariosRegistrados.exists():
        datos['message'] = "¡No se ha podido ingresar el usuario. El nombre de usuario ya existe!"
        datos['result'] = "XREP"
        return JsonResponse(datos)


    _transaccion = transaction.savepoint() #crear un punto de referencia para iniciar la transaccion

    try:
        empresa.save()
        persona = proy_persona(per_nombres = per_nombre,
                               per_apellidos = per_apellido,
                               per_direccion = per_direccion,
                               per_telef_movil = per_movil,
                               per_email = per_correo,
                               # per_foto= "logo",
                               per_telef_fijo = per_fijo,
                               emp_id = empresa,
                               )
        persona.save()

        usuario = User(password = make_password(usu_clave),
                       username = usu_nombre,
                       first_name = per_nombre,
                       last_name = per_apellido,
                       email = per_correo,
                       is_active = True,
                       id_persona= persona)
        usuario.save()


        representante = proy_representante(per_id = persona,
                                           rep_fecha_inicio = rep_fechaI,
                                           rep_fecha_fin = rep_fechaF,
                                           rep_descripcion = rep_descripcion)
        representante.save()

        vRol=Group.objects.create(name = "Administrador " + empresa.emp_nombre_comercial,
                     id_empresa = empresa)

        darPermisos(vRol)
        usuario.groups.add(vRol)

        transaction.savepoint_commit(_transaccion) #ejecuta el commit

        datos['message'] = "¡Registro de empresa guardado correctamente!"
        datos['result'] = "OK"
        messages.add_message(request, messages.SUCCESS, datos['message'])
        return JsonResponse(datos)

    except Exception as error:
        print("Error al guardar-->transaccion" + str(error))
        transaction.savepoint_rollback(_transaccion)
        datos['message'] = "¡Ha ocurrido un error al tratar de ingresar los datos de la empresa!"
        datos['result'] = "X"
        return JsonResponse(datos)

@csrf_exempt
@login_required(login_url='/')
def modRegistroEmpresa(request):
    if request.user.is_authenticated():
        if request.user.has_perm('app_empresa.change_proy_empresa'):
            id_empresa = request.user.id_persona.emp_id.emp_id
            comercial = request.POST.get('txt_nombre_c')
            juridico = request.POST.get('txt_nombre_j')
            grupo = request.POST.get('txt_grupo')
            correo = request.POST.get('txt_correo_e')
            web = request.POST.get('txt_web')
            direccion = request.POST.get('txt_direccion_e')
            dominio = request.POST.get('txt_dominio')
            descripcion = request.POST.get('txt_descripcion_e')
            if not descripcion:
                descripcion = "Sin detalles"

            if not grupo:
                grupo = "Sin detalles"

            empresa = proy_empresa.objects.get(pk = id_empresa)
            empresa.emp_nombre_comercial = comercial
            empresa.emp_nombre_juridico = juridico
            empresa.emp_grupo_empresa = grupo
            empresa.emp_direccion = direccion
            empresa. emp_email = correo
            empresa.emp_web = web
            empresa.emp_dominio_correo = dominio
            empresa.emp_descripcion = descripcion
            try:
                empresa.save()
                messages.add_message(request, messages.SUCCESS, '¡Registro de empresa actualizado correctamente!')
                return redirect('/empresa/datos/modificar/')
            except Exception as error:
                messages.add_message(request, messages.ERROR, '¡Ha ocurrido un error al tratar de actualizar los datos de la empresa!')
                return redirect('/empresa/datos/modificar/')
        else:
            messages.add_message(request, messages.ERROR,"¡NO TIENES PERMISOS!")
            return redirect('/empresa/datos/modificar/')
    else:
        return redirect('/')

def redireccionarUsuarioEmpresa(request):
    usu = request.GET.get("usuario")
    clave = request.GET.get("clave")
    try:
        usuario = User.objects.get(username=usu)
        user=authenticate(username=usuario.username, password=clave)
        login(request, user)
        return redirect('/')
    except Exception as e:
        print(e)
        return HttpResponseRedirect('/')

def getEmpresa(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            datos = proy_empresa.objects.filter(emp_estado = True).values('emp_id', 'emp_nombre_comercial')              
            return HttpResponse(
                json.dumps({"data":list(datos)},cls=DjangoJSONEncoder),
                content_type = "application/json; charset=utf8"
            )
    else:
        return redirect('/')

@csrf_exempt
def obtenerEmpresa(request, ruc):
    vEmpresa=proy_empresa.objects.filter(emp_ruc=ruc)
    if vEmpresa.count() == 0:
        dic={'emp_nombre':'',
            'emp_ruc': ''}
    else:
        dic={
            'emp_nombre':vEmpresa[0].emp_nombre_juridico,
            'emp_ruc': vEmpresa[0].emp_ruc
            }
    return JsonResponse(dic)

@login_required(login_url='/')
def editarPersonas(request):
    dict = armaMenu(request.user.get_all_permissions())
    dict['usuario'] = request.user
    dict['empresa']= getEmpresa(request)
    dict['persona'] = obtenerPersona(request)

    lstLogs=[]
    for j in seg_log_acciones.objects.filter(usu_id=request.user.id):
        lstLogs.append(j)
    dict['lstLogs']=lstLogs
    dict['roles'] = obtenerRoles(request)
    return render_to_response("app_empresa/editPerfil.html", dict,context_instance = RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def actualizarPersonas(request):
    vUser=User.objects.get(username=request.user.username)
    if vUser.check_password(request.POST['txtPassword']):
        vUser.first_name=request.POST['txtNombre']
        vUser.last_name=request.POST['txtApellido']
        vUser.username=request.POST['txtUsuario']
        vUser.email=request.POST['txtCorreo']

        if proy_persona.objects.filter(user_id=request.user.id).exists():
            vPersona=proy_persona.objects.get(user_id=request.user.id)
            vPersona.per_nombres=request.POST['txtNombre']
            vPersona.per_apellidos=request.POST['txtApellido']
            vPersona.per_direccion=request.POST['txtDireccion']
            vPersona.per_email=request.POST['txtCorreo']
            #vPersona.per_foto=request.FILES['txtImagen']
            vPersona.per_telef_fijo=request.POST['txtTlfono']
            vPersona.per_telef_movil=request.POST['txtMovil']
            vPersona.save()
        else:
            vPersona=proy_persona(per_nombres = request.POST['txtNombre'],
                                  per_apellidos = request.POST['txtApellido'],
                                  per_direccion = request.POST['txtDireccion'],
                                  per_email = request.POST['txtCorreo'],
                                  #per_foto = request.FILES['txtImagen'],
                                  per_telef_fijo = request.POST['txtTlfono'],
                                  per_telef_movil = request.POST['txtMovil'],
                                  emp_id=getEmpresa(request),
                                  user_id=request.user)
            vPersona.save()
            acciones("Genero persona "+vUser.get_full_name(), request.user)
        vUser.save()
        acciones("Actualiza Usuario "+vUser.get_full_name(), request.user)
        messages.add_message(request, messages.SUCCESS, 'Perfil de usuario actualizado con éxito')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        messages.add_message(request, messages.ERROR, 'La contraseña ingresada es incorrecta')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required(login_url='/')
@csrf_exempt
def cambiarFoto(request):
    if request.user.is_authenticated():
        try:
            fotoActual = request.FILES['nuevaFoto']
            vPersona=proy_persona.objects.get(user_id=request.user.id)
            fotoVieja=vPersona.per_foto
            # print(fotoVieja)
            if not fotoActual:
                messages.add_message(request, messages.SUCCESS, 'Su imágen de perfil sigue siendo la misma')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            else:
                vPersona.per_foto=fotoActual
                vPersona.save()
                # if fotoVieja != None:
                #     os.remove('media/'+str(fotoVieja))
                acciones("Cambio Foto de Perfil", request.user)
                messages.add_message(request, messages.SUCCESS, 'Se actualizó su foto de perfil con éxito')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        except Exception as e:
            print(e)
            messages.add_message(request, messages.ERROR, 'Ha ocurrido un problema al actualizar su foto. Inténtelo nuevamente ')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return redirect('/')

@login_required(login_url='/')
def areasListar(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']=request.user
        dic['persona']=obtenerPersona(request)
        dic['roles']=obtenerRoles(request)
        dic['empresa']=request.user.id_persona.emp_id
        if request.user.has_perm('app_empresa.list_proy_area') :
            empresa=request.user.id_persona.emp_id.emp_id            
            areas = proy_area.objects.filter(emp_id_id=empresa)
            dic['datos_area']=areas
            return render_to_response('app_empresa/proy_area/templateAreas.html',dic, context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html",context_instance = RequestContext(request))
    else:
        return redirect('/')


def verSubAreas(request):
    if request.user.is_authenticated():
        dic=armaMenu(request.user.get_all_permissions())
        dic['usuario']  = request.user
        dic['persona']  = obtenerPersona(request)
        dic['roles']    = obtenerRoles(request)
        dic['empresa']  = request.user.id_persona.emp_id
        dic['regiones'] = reg_coef_expe.objects.all()        
        if request.user.has_perm('app_empresa.list_proy_subarea'):                       
            return render_to_response('app_empresa/proy_area/templateSubareas.html',dic, context_instance=RequestContext(request))
        else:
            return render_to_response("seguridad/errores/error_403.html",context_instance = RequestContext(request))
    else:
        return redirect('/')

#********************* GESTIONES DE AREAS Y SUBAREAS **************************
def guardarArea(request):
    if request.user.is_authenticated():
        if request.is_ajax():        
            sector_id = request.POST.get("txt_dependencias_typeahead","")
            if(sector_id != ""):

                # Verificar dependencia de "Ninguno", es decir id=0
                sector = None
                try:
                    sector = emp_sector.objects.get(sect_id=sector_id)
                except emp_sector.DoesNotExist:
                    pass

                if(sector != None):
                    empresa=proy_empresa.objects.get(emp_id=request.user.id_persona.emp_id.emp_id)         
                    area = proy_area(
                        ar_nombre       = request.POST['are_nom'],
                        ar_descripcion  = request.POST['are_des'],
                        ar_etiqueta     = request.POST['are_etq'],
                        ar_estado       = True,
                        ar_direccion    = request.POST['are_dir'],
                        ar_id_sector    = sector,
                        emp_id          = empresa 
                        )
                    area.save()

                    print "2do"
                    return HttpResponse(json.dumps({"mensaje":"Guardado Correctamente", "id":area.ar_id}),content_type = "application/json; charset=utf8")
                else:
                    return HttpResponse(json.dumps({"error":"No se aceptan áreas sin una ubicación", "id":"0"}),content_type = "application/json; charset=utf8")           
            else:
                return HttpResponse(json.dumps({"error":"Ingrese la Ubicación correctamente", "id":"0"}),content_type = "application/json; charset=utf8")   
        else:
            raise Http404
    else:
        return redirect('/')

def getArea(request):
    if request.user.is_authenticated():
        if request.is_ajax():

            datos = proy_area.objects.all().order_by("-ar_id").values('ar_id','ar_nombre','ar_descripcion','ar_etiqueta','emp_id_id','emp_id__emp_nombre_comercial','ar_id_sector__sect_id','ar_id_sector__sect_nombre','ar_direccion','ar_estado')
            return HttpResponse(
                json.dumps({"data":list(datos)},cls=DjangoJSONEncoder),
                content_type = "application/json; charset=utf8"
            )
        else:
            return redirect('/')
            
    else:
        return redirect('/')

def modificarArea(request):
    if request.user.is_authenticated():        
        if request.is_ajax():            
            area = proy_area.objects.get(ar_id = request.POST['are_id'])
            area.ar_nombre      = request.POST['are_nom']
            area.ar_descripcion = request.POST['are_des']
            area.ar_etiqueta    = request.POST['are_etq']
            #area.ar_estado      = True
            area.ar_direccion   = request.POST['are_dir']
            id_sector = request.POST['txt_dependencias_typeahead']
            area.ar_id_sector   = emp_sector.objects.get(pk=id_sector)
            #area.emp_id__emp_id = int(request.POST['emp'])    
            area.save()
            return HttpResponse(json.dumps({"mensaje":"Modificado correctamente"}),content_type = "application/json; charset=utf8")
    else:
        return redirect('/')

def estadoArea(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            print (request.POST['estado'])
            estado = request.POST['estado']
            if (estado  != "false"):
                estado = False
            else:
                estado = True
            area = proy_area.objects.get(ar_id = request.POST['id'])
            area.ar_estado = estado
            area.save()
            return HttpResponse(json.dumps({"mensaje":"Estado cambiado con éxito"}),content_type = "application/json; charset=utf8")
        else:

            return redirect('/')
    else:
        raise Http404


def guardarSubarea(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            try:
                region = reg_coef_expe.objects.get(pk = request.POST['regiones'])
                subarea = proy_subarea(
                    sar_nombre      = request.POST['sub_nom'],
                    sar_etiqueta    = request.POST['sub_etq'],                
                    sar_latitud     = request.POST['sub_lat'],
                    sar_longitud    = request.POST['sub_lng'],
                    sar_hem_lat     = request.POST['sub_hla'],
                    sar_hem_lon     = request.POST['sub_hlo'],
                    sar_asnm        = request.POST['alt_nivel'],
                    sar_region      = region,
                    ar_id_id        = int(request.POST['are_id'])
                    )
                subarea.save()
                return HttpResponse(json.dumps({"mensaje":"Guardado Correctamente"}),content_type = "application/json; charset=utf8")
            except Exception, e:
                print e
        else:
            raise Http404
    else:
        return redirect('/')

def modificarSubarea(request):
    if request.user.is_authenticated():        
        if request.is_ajax():            
            subarea = proy_subarea.objects.get(sar_id = request.POST['sub_id'])
            subarea.sar_nombre      = request.POST['sub_nom_m']
            subarea.sar_etiqueta    = request.POST['sub_etq_m']
            subarea.sar_latitud     = request.POST['sub_lat_m']
            subarea.sar_longitud    = request.POST['sub_lng_m']
            subarea.sar_hem_lat     = request.POST['sub_hla_m']
            subarea.sar_hem_lon     = request.POST['sub_hlo_m']
            subarea.sar_region_id      = int(request.POST['regiones_m'])
            subarea.sar_asnm        = request.POST['alt_nivel_m']
            subarea.ar_id_id        = int(request.POST['are_id_m'])
            subarea.save()
            return HttpResponse(
                        json.dumps({"mensaje":"Modificado Correctamente"}),content_type = "application/json; charset=utf8")
    else:
        return redirect('/')

def getSubarea(request):
    if request.user.is_authenticated():
        if request.is_ajax():
            datos = proy_subarea.objects.all().order_by("-sar_id").values('sar_id','sar_nombre','sar_asnm','sar_region__region','sar_region__id','sar_latitud','sar_etiqueta','sar_longitud','sar_hem_lat','sar_hem_lon','ar_id_id','ar_id__ar_nombre')
            #ws      = requests.get('http://localhost:9090/server/iot/view_area/views/view_area?$format=json')
            #datos   = ws.json()
            return HttpResponse(
                json.dumps({"data":list(datos)},cls=DjangoJSONEncoder),
                content_type = "application/json; charset=utf8"
            )
        else:
            return redirect('/')            
    else:
        return redirect('/')

@login_required(login_url="/")
def verSectores(request):
    dict=armaMenu(request.user.get_all_permissions())
    dict['usuario'] = request.user
    dict['empresa']=request.user.id_persona.emp_id
    dict['persona'] = obtenerPersona(request)
    dict['sectores'] = emp_sector.objects.all().order_by('sect_id_padre')
    if request.method == "POST":
        pass
    return render(request,"app_empresa/proy_sector/listar_sector.html",dict)

@login_required(login_url="/")
def modificarSector(request):
    dict=armaMenu(request.user.get_all_permissions())
    dict['usuario'] = request.user
    dict['empresa']=request.user.id_persona.emp_id
    dict['persona'] = obtenerPersona(request)
    dict['tipo_sector'] = emp_tipoSector.objects.all()
    dict['sectores'] = emp_sector.objects.all()
    if request.method == "POST":
        pass
    return render(request,"app_empresa/proy_sector/modificar_sector.html",dict)  

@login_required(login_url='/')
@csrf_exempt
def cargar_sectores(request):
    data = []
    try:
        sectores = emp_sector.objects.all().order_by('sect_nombre')
        for p in sectores:
            _sector_padre="0"
            try:
                _sector_padre = p.sect_id_padre.pk
            except Exception as error:
                _sector_padre="0"
            data.append({'id_sector': p.pk, 'nombre': p.sect_nombre, 'padre_id': _sector_padre})
    except Exception as error:
        print(error.message)
        data = ''
    return HttpResponse(json.dumps(data), content_type='application/json')

@login_required(login_url="/")
def guardarSector(request):
    dict=armaMenu(request.user.get_all_permissions())
    dict['usuario'] = request.user
    dict['empresa']=request.user.id_persona.emp_id
    dict['persona'] = obtenerPersona(request)
    dict['tipo_sector'] = emp_tipoSector.objects.all()
    if request.method == "POST":
        pass
    return render(request,"app_empresa/proy_sector/registro_sector.html",dict)        

def obtenerPadre(a):
    detalle = ""
    if a.sect_id_padre == 0:
        detalle = ""
    else:
        bandera = False
        while(bandera == False):
            if (a.sect_id_padre > 0):
                sector = emp_sector.objects.get(pk=a.sect_id_padre.pk)
                detalle = (detalle)+", "+sector.sect_nombre
                a = sector
            else:
                detalle = (detalle)+""
                bandera = True

    return detalle

@csrf_exempt
@login_required(login_url='/')
def add_sector_JSON(request):
    datos = {}
    if request.method == "POST":
        _nombre = request.POST.get("txt_nombre_ubicacion","")
        _tipo = request.POST.get("cmb_tipo_ubicacion","")
        _detalle = request.POST.get("txt_detalle_ubicacion","")
        _padre = request.POST.get("txt_dependencias_typeahead","")
        print("padre {0}".format(_padre))
        if (valida_cantidad_caracteres(_nombre,200)):
            try:
                tipo_sector = emp_tipoSector.objects.get(pk=_tipo)
                sector_padre=emp_sector()
                try:
                   sector_padre = emp_sector.objects.get(pk=_padre)
                except emp_sector.DoesNotExist:
                   sector_padre = None

                repetido = emp_sector.objects.all()
                for i in repetido:
                    if eliminar_tildes(_nombre).upper() == eliminar_tildes(i.sect_nombre).upper():
                        datos['mensaje'] = "Ya existe un sector con el mismo nombre."
                        datos['result'] = "X"
                        return HttpResponse(json.dumps(datos), content_type="application/json")

                sector_guardar = emp_sector()
                sector_guardar.sect_nombre=_nombre
                sector_guardar.sect_detalle=_detalle
                sector_guardar.sect_id_tsector=tipo_sector
                sector_guardar.sect_id_padre=sector_padre
                sector_guardar.save()
                datos['mensaje'] = "Los datos de la ubicacion se han guardado correctamente."
                datos['result'] = "OK"

                return HttpResponse(json.dumps(datos), content_type="application/json")
            except Exception as e:
                print e
                datos['mensaje'] = "IOTMach sufrio un error inesperado, vuelva a intentar."
                datos['result'] = "X"

                return HttpResponse(json.dumps(datos), content_type="application/json")
        else:
            datos['mensaje'] = "Algunos de los campos han superado el limite de caracteres permitido."
            datos['result'] = "X"
            return HttpResponse(json.dumps(datos), content_type="application/json")
    return redirect('/empresa/sector/registrar/')

#Agregar tipo de sector...................................................................................
@login_required(login_url='/')
@csrf_exempt
def add_tipo_sector(request):
    dic = {}
    if request.method == "POST":
        _nombre = request.POST.get("txt_nombre_tipo")
        _icono = request.POST.get("txt_icono")

        if _icono == None:
            _icono="no"

        if (valida_cantidad_caracteres(_nombre,200) and valida_cantidad_caracteres(_icono,  200)):
            try:
                repetido = emp_tipoSector.objects.all()
                for i in repetido:
                    if eliminar_tildes(_nombre).upper() == eliminar_tildes(i.tsec_nombre).upper():
                        dic = {
                            'mensaje': "Ya existe un tipo de sector con el mismo nombre.",
                            'result': 'X'
                        }
                        return HttpResponse(json.dumps(dic), content_type="application/json")

                tsector_agregar = emp_tipoSector()
                tsector_agregar.tsec_nombre = _nombre
                tsector_agregar.tsec_icono = _icono
                tsector_agregar.save()
                dic = {
                    'mensaje': "Los datos del tipo de ubicacion se han guardado correctamente.",
                    'result': 'OK'
                }
            except Exception as e:
                dic = {
                    'mensaje': "Dyuyay sufrio un error inesperado, vuelva a intentar.",
                    'result': 'X'
                }
        else:
            dic = {
                'mensaje': "Algunos de los campos han superado el limite de caracteres permitido.",
                'result': 'X'
            }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="aplication/json")

### Cargar dependencias ###
def cargar_dependencias(request):
    #tipo_sector = request.GET.get("tipo_sector")
    datos = []
    ciudad = emp_sector.objects.all().order_by('sect_id_tsector__tsec_id')
    datos.append({'id': 0, 'nombre': "Ninguno", 'detalle': "-No depende de ningún sector"})
    for a in ciudad:
        detalle = obtenerPadre(a)
        datos.append({'id': a.sect_id, 'nombre': a.sect_nombre, 'detalle': detalle})

    print (json.dumps(datos))
    return HttpResponse(json.dumps(datos), content_type="application/json")

def get_ubicaciones(request):
    datos = []
    ciudad = emp_sector.objects.all()
    for a in ciudad:
        detalle = obtenerPadre(a)
        datos.append({'id': a.sect_id, 'nombre': a.sect_nombre, 'detalle': detalle})

    return HttpResponse(json.dumps(datos), content_type="application/json")

def valida_cantidad_caracteres(campo_validar, maximo_caracteres):
    if len(campo_validar)<=maximo_caracteres and len(campo_validar)>0:
        return True
    else:
        return False

def valida_contiene(campo_validar, caracteres):
    if caracteres in campo_validar:
        return True
    else:
        return False

def eliminar_tildes(s):
   return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

## Cargar sector modal
@login_required(login_url='/')
@csrf_exempt
def cargar_sector(request):
    _id = request.POST['identificador']
    dic = {}
    try:
        _sector = emp_sector.objects.get(pk=_id)
        _id_sector=str(_sector.pk)
        _nombre=_sector.sect_nombre
        _detalle=str(_sector.sect_detalle)
        _tipo_sector="0"
        if(_sector.sect_id_tsector):
            _tipo_sector=str(_sector.sect_id_tsector.pk)
        _sector_padre="0"
        try:
            _sector_padre = _sector.sect_id_padre.pk
        except Exception as error:
            _sector_padre="0"
        dic ={
            'id_sector': _id_sector,
            'nombre': _nombre, 
            'tipo_sector': _tipo_sector, 
            'detalle': _detalle, 
            'sector_padre': _sector_padre,
        }
    except Exception as error:
        print error
        dic = {}
    data=json.dumps(dic)
    return HttpResponse(data, content_type="application/json")

@login_required(login_url='/')
@csrf_exempt
def editar_sector(request):
    _id= request.POST["id"]
    _nombre= request.POST["txt_nombre"]
    _detalle= request.POST["txt_detalle"]
    _tipo= request.POST["sel_tipo"]
    _sect_padre_id= request.POST['sel_sector']

    if valida_cantidad_caracteres(_nombre, 200):
        try:
            #Tipo de Sector
            tipo_sector = emp_tipoSector.objects.get(pk=_tipo)
            #Sector Padre
            try:
                sector_padre = emp_sector.objects.get(pk=_sect_padre_id)
            except emp_sector.DoesNotExist:
                sector_padre = None
            sector_modificar=emp_sector.objects.get(pk=_id)
            sector_modificar.sect_nombre=_nombre
            sector_modificar.sect_detalle=_detalle
            sector_modificar.sect_id_tsector=tipo_sector
            sector_modificar.sect_id_padre=sector_padre
            sector_modificar.estado_sincronizacion=False
            sector_modificar.save()
            dic = {
                'mensaje': "Los datos de la ubicación se han guardado correctamente.",
                'result': 'OK'
            }
        except Exception as e:
            print e
            dic = {
                'mensaje': "IOTMach sufrio un error inesperado, vuelva a intentar.",
                'result': 'X'
            }
    else:
        dic = {
            'mensaje': "Algunos de los campos han superado el límite de carácteres permitido.",
            'result': 'X'
        }
    data=json.dumps(dic)
    return HttpResponse(data, content_type="application/json")    