from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from app_empresa.views import *
from django.conf import settings

urlpatterns = patterns('',
     #Datos de la empresa
     url(r'^inicio/$', frmEmpresa),
     url(r'^datos/registrar/$', frmRegistroEmpresa),
     url(r'^datos/editar_datos/$', modRegistroEmpresa),
     url(r'^datos/modificar/$', frmModificarEmpresa),
     url(r'^datos/completo/$', redireccionarUsuarioEmpresa),
     url(r'^datos/guardar/$', guardar_datos_empresa, name='empresa_guardar'),
     url(r'^datos/get/$', getEmpresa),

     #webservices
     url(r'^obtener/datos/(\w+)$', obtenerEmpresa),

     #datos de la persona/usuario
     url(r'^personas/editar/$', editarPersonas, name='editar_personas'),
     url(r'^personas/actualizar/$', actualizarPersonas, name='editar_perfil'),
     url(r'^personas/cambiar/foto/$', cambiarFoto),

     #datos de las Areas
     url(r'^areas/listar/$', areasListar, name='listar_area'),
     url(r'^areas/ver/(\d+)/$', verSubAreas),
     url(r'^areas/sav/$', guardarArea),
     url(r'^areas/get/$', getArea),
     url(r'^areas/mod/$', modificarArea),
     url(r'^areas/est/$', estadoArea),

    # SubAreas
    url(r'^subarea/$', verSubAreas, name = "url_subarea"),
    url(r'^subarea/sav/$', guardarSubarea),
    url(r'^subarea/get/$', getSubarea),
    url(r'^subarea/mod/$', modificarSubarea),    

    # Sectores
    url(r'^sector/$', verSectores, name = "url_sector"),
    url(r'^sector/editar/$', modificarSector),
    url(r'^sector/registrar/$', guardarSector),
    url(r'^sector/cargarDependencias/$', cargar_dependencias),
    url(r'^sector/guardarSector/$', add_sector_JSON),
    url(r'^sector/ubicaciones/$', get_ubicaciones, name="url_ubicaciones"),
    url(r'^sector/registrarTipoSector/$', add_tipo_sector),
    url(r'^sector/ubicacionVerTodos/$', cargar_sectores),
    url(r'^sector/cargarSector/$', cargar_sector),
    url(r'^sector/editarSector/$', editar_sector),
)
