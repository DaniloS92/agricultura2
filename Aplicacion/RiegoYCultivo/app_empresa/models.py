#-*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User, Group
from django.contrib import messages
from app_seguridad.models import seg_apps

class proy_empresa(models.Model):
    emp_id = models.AutoField(primary_key=True)
    emp_ruc = models.CharField(max_length=50, unique=True)
    emp_nombre_comercial = models.CharField(max_length=200)
    emp_nombre_juridico = models.CharField(max_length=200)
    emp_grupo_empresa = models.CharField(max_length=200)
    emp_direccion = models.CharField(max_length=200)
    emp_email = models.CharField(max_length=500, unique=True)
    emp_web = models.CharField(max_length=500)
    emp_logo = models.ImageField(upload_to='empresa_logos', blank=True, null=True)
    emp_estado = models.BooleanField(default=True)
    emp_dominio_correo = models.CharField(max_length=500)
    emp_descripcion = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        db_table = 'e_empresa'
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'
        default_permissions = ()
        permissions = (
            ('change_proy_empresa', "Puede editar Empresa"),
        )


    def __unicode__(self):
        return self.emp_nombre_comercial

class proy_emp_telefono(models.Model):
    telf_id = models.AutoField(primary_key=True)
    telf_numero = models.CharField(max_length=10)
    telf_proveedor = models.CharField(max_length=10)
    emp_id = models.ForeignKey(proy_empresa)

    class Meta:
        db_table="e_emp_telefono"
        verbose_name = 'Empresa Telefono'
        verbose_name_plural = 'Empresas Telefonos'
        default_permissions = ()

class proy_persona(models.Model):
    per_id = models.AutoField(primary_key=True)
    per_nombres = models.CharField(max_length=200)
    per_apellidos = models.CharField(max_length=200)
    per_direccion = models.CharField(max_length=500)
    per_telef_movil = models.CharField(max_length=10)
    per_email = models.CharField(max_length=200)
    per_foto = models.ImageField(upload_to="fotos_personas", blank=True, null=True)
    per_telef_fijo = models.CharField(max_length=10)
    per_aplicaciones=models.CharField(max_length=100, default='server')
    emp_id =models.ForeignKey(proy_empresa)
    apps = models.ManyToManyField(seg_apps)

    class Meta:
        db_table = 'e_persona'
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        # permissions = (("list_persona", "Can list persona"),)
        default_permissions = ()

    def __unicode__(self):
        return self.per_nombres+" "+self.per_apellidos

class proy_representante(models.Model):
    rep_id = models.AutoField(primary_key=True)
    # emp_id = models.ForeignKey(proy_empresa)
    per_id = models.ForeignKey(proy_persona)
    rep_fecha_inicio = models.DateField()
    rep_fecha_fin = models.DateField()
    rep_descripcion = models.CharField(max_length=800)
    rep_estado = models.BooleanField(default=True)

    class Meta:
        db_table = 'e_representante'
        verbose_name = 'Representate'
        verbose_name_plural = 'Representantes'
        # permissions = (("list_representante", "Can list representante"),)
        default_permissions = ()
        permissions = (
            ('change_proy_representante', "Puede editar Representante"),
            ('list_proy_representante', "Puede listar Representante"),
        )


    def __unicode__(self):
        return self.per_id

class reg_coef_expe(models.Model): 
    region=models.CharField(max_length=50)
    valor_a1 = models.FloatField(default=0)
    valor_b1 = models.FloatField(default=0)
    class Meta:
        default_permissions=(['add','change','delete','list'])
        verbose_name = 'Region de coeficiente experimental'
        verbose_name_plural = 'Region de coeficientes experimentales'
    def __unicode__(self):
        return self.region

class emp_tipoSector(models.Model):
    tsec_id = models.AutoField("Id",primary_key=True)
    tsec_nombre=models.CharField("Nombre",max_length=200)
    tsec_icono=models.CharField("Icono",max_length=200)

    class Meta:
        db_table = 'e_tipo_sector'
        verbose_name = 'Tipo Sector'
        verbose_name_plural = 'Tipos Sectores'
        default_permissions=(['add','change','delete','list'])
        
    def __unicode__(self):
        return self.tsec_nombre

class emp_sector(models.Model):
    sect_id = models.AutoField("Id",primary_key=True, db_column='sec_id')
    sect_nombre=models.CharField("Nombre",max_length=200, db_column='sec_nombre')
    sect_detalle=models.CharField("Detalle",max_length=200, blank=True, db_column='sec_detalle')
    sect_id_tsector=models.ForeignKey(emp_tipoSector, db_column='tsec_id',verbose_name="Id sector")
    sect_id_padre=models.ForeignKey('self', null=True, db_column='sec_id_padre', verbose_name="Id Padre")

    class Meta:
        db_table = 'e_sector'
        verbose_name = 'Sector'
        verbose_name_plural = 'Sectores'
        default_permissions=(['add','change','delete','list'])

    def __unicode__(self):
        return self.sect_nombre


class proy_area(models.Model):
    ar_id           = models.AutoField(primary_key = True , null = False)
    ar_nombre       = models.CharField('Nombre', max_length=100)
    ar_descripcion  = models.CharField('Descripcion', max_length=500)
    ar_etiqueta     = models.CharField('Etiqueta', max_length=100)
    ar_estado       = models.BooleanField('Estado', default=True)
    ar_direccion    = models.CharField('Direccion', max_length=100)
    ar_logo         = models.ImageField('Logo', upload_to='areas_logos', blank=True, null=True, default='img_parcelas/sin_imagen.gif')
    ar_id_sector    = models.ForeignKey(emp_sector, verbose_name='Sector')
    emp_id          = models.ForeignKey(proy_empresa, verbose_name='Empresa')
    

    def save(self, *args, **kwargs):
        super(proy_area, self).save(*args, **kwargs)    
    class Meta:
        verbose_name = 'Áreas'
        db_table = 'e_area'
        verbose_name = 'Area'
        verbose_name_plural = 'Areas'
        default_permissions=(['add','change','delete','list'])

class proy_subarea(models.Model):
    sar_id = models.AutoField(primary_key = True, null = False)
    sar_nombre = models.CharField(max_length=100)
    sar_etiqueta=models.CharField(max_length=100)
    sar_imagen = models.ImageField(upload_to='img_parcelas', blank=True, null=True, default='img_parcelas/sin_imagen.gif')
    sar_latitud = models.FloatField(max_length=100)
    sar_longitud = models.FloatField(max_length=50)
    sar_hem_lat = models.CharField(max_length=50)
    sar_hem_lon = models.CharField(max_length=50)
    sar_asnm= models.BigIntegerField(default=0)
    sar_region=models.ForeignKey(reg_coef_expe, null=True)
    ar_id= models.ForeignKey(proy_area)
    class Meta:
        verbose_name = 'Sub Áreas'
        db_table = 'e_subarea'
        verbose_name = 'Subarea'
        verbose_name_plural = 'Subareas'
        default_permissions=(['add','change','delete','list'])

    def __unicode__(self):
        return self.sar_nombre

User.add_to_class('id_persona', models.OneToOneField(proy_persona, blank=True, null=True))
# Group.add_to_class('id_empresa', models.ForeignKey(proy_empresa, blank=True, null=True))
