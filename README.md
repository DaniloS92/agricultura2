# IOTMACH SERVER

### Requisitos

Se requiere tener instalado python 2.7, django 1.8, node 4.4.4, virtualenv

### Instalacion requisitos DJANGO   
### Crear entorno virtual

```sh
$ virtualenv agricultura
```
### Accediendo al entorno virtual
### Windows
```sh
$ agricultura\scripts\activate
```
### Linux
```sh
$ source agricultura\bin\activate
```
Una vez activado el entorno virtual:

Instalar los requisitos que se encuentran en la direccion agricultura\:
```sh
$ pip install -r requisitos.txt
```

### Instalacion requisitos NODEJS
Instalar los modulos de nodejs que se encuentran en la direccion:

\agricultura\Aplicacion\RiegoYCultivo\node_project:
```sh
$ npm install
```
### Borrar migraciones

Se deben borrar el contenido de las carpetas migrations a excepción de archivo __ init__.py

* app_agricultura
* app_iot
* app_empresa
* app_modular
* app_seguridad

### Ejecución de la aplicación
Una vez iniciado el entorno virtual:
```sh
$ cd agricultura\Aplicacion\RiegoYCultivo
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```
